/**
* VASS
* @author 			Juan Cardona juan.cardona@vass.es
* Project:			Gas Natural Fenosa
* Description:		Desencadenador sobre el objeto Concepto
*					
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-07-12		Juan Cardona (JSC)		Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_ConceptoTriggerHandler_cls 
{
	
	
	/*
	*/
    public static void on_beforeInsert( list<Concepto__c> list_inConceptos )
    {
    	list<Concepto__c> list_conversionValoresConcepto;


     	system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.on_beforeInsert>> list_inConceptos: ' + list_inConceptos );
     	
     	if( list_inConceptos==null || list_inConceptos.isEmpty() )
    		return;     		
     	
     	list_conversionValoresConcepto = new list<Concepto__c>();
     	   	
    	for( Concepto__c obj_c : list_inConceptos)
    	{
    		if( /*obj_c.Pais_Cliente__c!=null &&*/ ( obj_c.Valor_de_F__c!=null || obj_c.GNF_ValorK__c!=null || obj_c.Precio__c!=null )  )
    		{
    			list_conversionValoresConcepto.add( obj_c );
    		}
    	}
    
    	if( !list_conversionValoresConcepto.isEmpty() ) 
    		GNF_integracionData_cls.conversionValoresConceptos( null, list_conversionValoresConcepto );
    }//ends on_beforeInsert	
 
 
 	/*
	*/
    public static void on_beforeUpdate( map<Id, Concepto__c> map_inOldMap, map<Id, Concepto__c> map_inNewMap )
    {
    	list<Concepto__c> 				list_newConversionValoresConcepto;
    	list<Concepto__c> 				list_oldConversionValoresConcepto;

		Concepto__c						obj_oldConcepto;
		

     	system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.on_beforeUpdate>> map_inOldMap: ' + map_inOldMap + ' map_inNewMap: ' + map_inNewMap );
     	
     	for( Concepto__c obj : map_inOldMap.values() )
			system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.on_beforeUpdate>> obj.Precio__c: ' + obj.Precio__c );
     	
     	if( map_inOldMap==null || map_inOldMap.isEmpty() || map_inNewMap==null || map_inNewMap.isEmpty() )
    		return;     		
     	
     	list_newConversionValoresConcepto 		= new list<Concepto__c>();
     	list_oldConversionValoresConcepto 	= new list<Concepto__c>();
     	   	
    	for( Concepto__c obj_c : map_inNewMap.values() )
    	{
    		
    		obj_oldConcepto = map_inOldMap.containsKey( obj_c.Id ) ? map_inOldMap.get( obj_c.Id ) : null;
    		
    		if( ( /*obj_c.Pais_Cliente__c!=null &&*/
    		 	( 	( obj_c.Valor_de_F__c!=null && obj_c.Valor_de_F__c!=obj_oldConcepto.Valor_de_F__c ) || 
    		 		( obj_c.GNF_ValorK__c!=null && obj_c.GNF_ValorK__c!=obj_oldConcepto.GNF_ValorK__c ) || 
    		 		( obj_c.Precio__c!=null && obj_c.Precio__c!=obj_oldConcepto.Precio__c ) )
		 		) /*|| obj_c.GNF_controlEjecucionConversion__c*/
		 		)
    		{
    			list_newConversionValoresConcepto.add( obj_c );
    			list_oldConversionValoresConcepto.add( obj_oldConcepto );
    		}
    	}
    
    	if( !list_newConversionValoresConcepto.isEmpty() && !list_oldConversionValoresConcepto.isEmpty() ) 
    		GNF_integracionData_cls.conversionValoresConceptos( list_oldConversionValoresConcepto, list_newConversionValoresConcepto );
    }//ends on_beforeUpdate	
}//ends GNF_ConceptoTriggerHandler_cls