/**
* VASS
* @author 			Luis Igualada luis.igualada@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase GNF_matrizclasicaClick_ctr
*
*
* Changes( Version )
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-06-08		Luis Igualada( LI )		Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_matrizclasicaClick_ctr {

	public Opportunity obj_opp {get; set;}
	public Map<String,Map<String,Decimal>> matriz {get; set;}
	public Map<Id,Consumo_Click__c> consumosxId {get; set;}
	public list<integer> listorden {get; set;}
	public Map<String,Map<Integer,String>> ordenperiodos {get; set;}
	public Set<String> setPeriodNames {get; set;}
	public Map<String, String> mapPSByContractProduct				{get; set;}

	public GNF_matrizclasicaClick_ctr( ApexPages.StandardController stdController )
		{
			system.debug( '<<GNF_matrizclasicaClick_ctr>> :' + stdController );

			obj_opp =(  Opportunity  ) stdController.getRecord();
			consumosxId = new Map<Id,Consumo_Click__c>();
			mthd_getMatriz();
		}

	public Void mthd_getMatriz(){

		/* BEGIN - Manuel Medina - Llamado a clase GNF_PreciosVistaClasica_ctr para obtener funcionalidad de filtrado - 23082017 */
		GNF_PreciosVistaClasica_ctr ctrPreciosVistaClasica			= new GNF_PreciosVistaClasica_ctr();
		/* END - Manuel Medina - Llamado a clase GNF_PreciosVistaClasica_ctr para obtener funcionalidad de filtrado - 23082017 */

		list<Consumo_Click__c>  listacotiz =  new list<Consumo_Click__c>();

		listacotiz	= [Select Cotizacion_de_periodo__r.Name,
				Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,
				Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c,
				Cotizacion_de_periodo__r.Periodo__c
				from Consumo_Click__c
				where Cotizacion_de_periodo__r.Opportunity__c =: obj_opp.Id
			/* BEGIN - Manuel Medina - Filtrado por oportunidad puntos de suministro - 23082017 */
		AND Oportunidad_Punto_de_Suministro__c IN: ctrPreciosVistaClasica.getOpportunitySupplies( new List<Id>{ obj_opp.Id } )
		ORDER BY Cotizacion_de_periodo__r.Periodo__c ASC
		/* END - Manuel Medina - Filtrado por oportunidad puntos de suministro - 23082017 */
		];

		ordenperiodos = new Map<String,Map<Integer,String>>();
		mapPSByContractProduct = new Map<String, String>();

		listorden= new list<Integer>();

		setPeriodNames							= new Set<String>();

		for( Consumo_Click__c cotizperiodo : listacotiz ) {

			mapPSByContractProduct.put( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c, cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c );

			if( !ordenperiodos.containskey( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c ) ){
				Map<Integer,String> ordenporperiodo = new Map<Integer,String>();

				ordenporperiodo.put( integer.valueof( cotizperiodo.Cotizacion_de_periodo__r.Periodo__c ),cotizperiodo.Cotizacion_de_periodo__r.Name );
				ordenperiodos.put( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,ordenporperiodo );

				setPeriodNames.add(  cotizperiodo.Cotizacion_de_periodo__r.Name  );

			} else if( ordenperiodos.containskey( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c ) ){
				Map<Integer,String> ordenporperiodo = new Map<Integer,String>( ordenperiodos.get( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c ) );

				ordenporperiodo.put( integer.valueof( cotizperiodo.Cotizacion_de_periodo__r.Periodo__c ),cotizperiodo.Cotizacion_de_periodo__r.Name );
				ordenperiodos.put( cotizperiodo.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,ordenporperiodo );

				setPeriodNames.add(  cotizperiodo.Cotizacion_de_periodo__r.Name  );
			}

			listorden.add( integer.valueof( cotizperiodo.Cotizacion_de_periodo__r.Periodo__c ) );

		}

		//Lista ordenada
		listorden.sort();

		//Matriz a mostrar en la VisualForce
		matriz = new map<String,Map<String,Decimal>>();

		list<AggregateResult> consumos = new list <AggregateResult>();

		consumos = [Select SUM( Q_comprometida_kWh__c ) sumatorio,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c SS,Cotizacion_de_periodo__r.Name CP
				FROM Consumo_Click__c where Cotizacion_de_periodo__r.Opportunity__c =:obj_opp.Id
		GROUP BY Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,Cotizacion_de_periodo__r.Name];

		for( AggregateResult consumo : consumos ){
			if( !matriz.containskey( string.valueof( consumo.get( 'SS' ) ) ) ){
				Map<String,Decimal> consumoxperiodo = new Map<String,Decimal>();

				consumoxperiodo.put( string.valueof( consumo.get( 'CP' ) ),Decimal.valueof( string.valueof( consumo.get( 'sumatorio' ) ) ) );

				matriz.put( string.valueof( consumo.get( 'SS' ) ),consumoxperiodo );

			} else if( matriz.containskey( string.valueof( consumo.get( 'SS' ) ) ) ){
				Map<String,Decimal> consumoxperiodo = new Map<String,Decimal>( matriz.get( string.valueof( consumo.get( 'SS' ) ) ) );

				consumoxperiodo.put( string.valueof( consumo.get( 'CP' ) ),Decimal.valueof( string.valueof( consumo.get( 'sumatorio' ) ) ) );

				matriz.put( string.valueof( consumo.get( 'SS' ) ),consumoxperiodo );

			}
		}

		//consumosxId= new Map<Id,Consumo_Click__c>( [SELECT Id,Name, Oportunidad_Punto_de_Suministro__c,Cotizacion_de_periodo__c,
		//Cotizacion_de_periodo__r.Name,Oportunidad_Punto_de_Suministro__r.Sector_Suministro__c,
		//Oportunidad_Punto_de_Suministro__r.Sector_Suministro__r.NIS__r.Name
		//from Consumo_Click__c where Cotizacion_de_periodo__r.Opportunity__c =:obj_opp.Id] );

	}

	public Map<String, Map<String, String>> getMatriz2(){
		Map<String, Map<String, String>> mapMatriz	= new Map<String, Map<String, String>>();

		for( String strSS : ordenperiodos.keySet() ){
			Map<String, String> mapSumByCP	= new Map<String, String>();

			for( String strCP : ordenperiodos.get( strSS ).values() ){
				mapSumByCP.put( strCP, ( matriz.containsKey( strSS ) && matriz.get( strSS ).containsKey( strCP ) ? matriz.get( strSS ).get( strCP ).format() : '0' ) );
			}

			mapMatriz.put( strSS, mapSumByCP );
		}

		for( String strSS : mapMatriz.keySet() ){
			for( String strCP : getPeriodNames() ){
				if( !mapMatriz.get( strSS ).containsKey( strCP ) ){
					mapMatriz.get( strSS ).put( strCP, '0' );
				}
			}
		}

		return mapMatriz;
	}

	public Set<String> getPeriodNames(){
		Set<String> setPeriods					= new Set<String>();

		for( String strSS : ordenperiodos.keySet() ){
			for( String strCP : ordenperiodos.get( strSS ).values() ){
				setPeriods.add( strCP );
			}
		}

		return setPeriods;
	}
}