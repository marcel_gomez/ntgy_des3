/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase mock web service que simula las respuestas para la clase de prueba de GNF_PDFRequest_ws.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-02-12		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
global class GNF_PDFRequest_mws implements HttpCalloutMock{
	
	/**
	* @Method: 		respond
	* @param: 		HTTPRequest req
	* @Description: Metodo que implementa la respuestas de los servicios web de GNF_PDFRequest_ws.
	* @author 		Manuel Medina - 12022018
	*/
	global HTTPResponse respond( HTTPRequest req ) {
		HttpResponse httpRES									= new HttpResponse();
		
		if( req.getHeader( 'SOAPAction' ) != null ){
			httpRES.setBody( 
				'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:enterprise.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
					'<soapenv:Body>' +
						'<loginResponse>' +
							'<result>' +
								'<metadataServerUrl>https://appgestorgnf--DES2.cs89.my.salesforce.com/services/Soap/m/41.0/00D0E0000004lBw</metadataServerUrl>' +
								'<passwordExpired>false</passwordExpired>' +
								'<sandbox>true</sandbox>' +
								'<serverUrl>https://appgestorgnf--DES2.cs89.my.salesforce.com/services/Soap/c/41.0/00D0E0000004lBw</serverUrl>' +
								'<sessionId>00D0E0000004lBw!AQcAQLIbln1SLPq9auyameRMOQRUr755I3wIFlyaMFWuSNTq8NlfyfP_FMRaxrmCanLP6zjXDT5SXMXMQBCat25wm62mE6Zu</sessionId>' +
								'<userId>0050E000001IruPQAS</userId>' +
								'<userInfo>' +
									'<accessibilityMode>false</accessibilityMode>' +
									'<chatterExternal>false</chatterExternal>' +
									'<currencySymbol>€</currencySymbol>' +
									'<orgAttachmentFileSizeLimit>5242880</orgAttachmentFileSizeLimit>' +
									'<orgDefaultCurrencyIsoCode>EUR</orgDefaultCurrencyIsoCode>' +
									'<orgDefaultCurrencyLocale>es_ES_EURO</orgDefaultCurrencyLocale>' +
									'<orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments>' +
									'<orgHasPersonAccounts>false</orgHasPersonAccounts>' +
									'<organizationId>00D0E0000004lBwUAI</organizationId>' +
									'<organizationMultiCurrency>false</organizationMultiCurrency>' +
									'<organizationName>GNF EDELTA</organizationName>' +
									'<profileId>00e58000000yIqhAAE</profileId>' +
									'<roleId xsi:nil="true"/>' +
									'<sessionSecondsValid>7200</sessionSecondsValid>' +
									'<userDefaultCurrencyIsoCode xsi:nil="true"/>' +
									'<userEmail>manuel.medina@vass.es</userEmail>' +
									'<userFullName>Manuel Medina</userFullName>' +
									'<userId>0050E000001IruPQAS</userId>' +
									'<userLanguage>en_US</userLanguage>' +
									'<userLocale>es_ES</userLocale>' +
									'<userName>manuel.medina@edelta.com.des2</userName>' +
									'<userTimeZone>Europe/Paris</userTimeZone>' +
									'<userType>Standard</userType>' +
									'<userUiSkin>Theme3</userUiSkin>' +
								'</userInfo>' +
							'</result>' +
						'</loginResponse>' +
					'</soapenv:Body>' +
				'</soapenv:Envelope>'
			);
		}else{
			httpRES.setBody( 'OK' );
		}
		
		return httpRES;
	}
}