/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba encargada e crear los datos cargados en recursos estaticos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-09		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class SMC_ActuacionesContratactuales_tgr_TEST {

	static testMethod void scenarioOne() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();
		
		List<Condiciones_de_Cobertura__c> lstCoverageConditions					= new List<Condiciones_de_Cobertura__c>( [
																					SELECT Id,
																						Estado__c,
																						SMC_Oportunidad__c,
																						Fecha_de_fin__c
																					FROM Condiciones_de_Cobertura__c
																				] );
																				
		for( Condiciones_de_Cobertura__c objCoverageCondition : lstCoverageConditions ){
			objCoverageCondition.Estado__c										= System.Label.SMC_ACEstadoCB1;
			objCoverageCondition.SMC_Oportunidad__c								= SMC_TestData_cls.objOpportunity.Id;
			objCoverageCondition.Fecha_de_fin__c								= System.today().addDays( 100 );
		}
		
		update lstCoverageConditions;
		
		test.startTest();
		
		List<Actuacion_Contractual__c> lstContractualActions					= new List<Actuacion_Contractual__c>(
																					( List<Actuacion_Contractual__c> ) Database.query(
																						GNF_OpportunityTriggerHandler_cls.getSOQL(
																							'Actuacion_Contractual__c',
																							''
																						)
																					)
																				);
		
		List<Actuacion_Contractual__c> lstContractualActionsNew					= new List<Actuacion_Contractual__c>( lstContractualActions.deepClone( false, false, false ) );
		
		delete lstContractualActions;
		insert lstContractualActionsNew;
		
		SMC_ActContratactualesHandler_cls.getFutureConditionCoverage( new Map<Id, List<Condiciones_de_Cobertura__c>>{ SMC_TestData_cls.objOpportunity.Id => lstCoverageConditions } );
		
		test.stopTest();
	}
}