/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Ejecuta funcionalidades adicionales del objeto de PDF asincronos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-29		Manuel Medina (MM)		Definicion inicial de la clase
* @version	2.0		2018-02-01		Manuel Medina (MM)		Se realiza encapsulamiento de logica para ser usada de
*															forma sincrona o asincrona.
*********************************************************************************************************/
public class GNF_PDFAsincronosTriggerHandler_cls implements Queueable{
	
	public static Boolean blnAlreadyExecuted_attachPDF									= false;
	public static Boolean blnAlreadyExecuted_updateRecords								= false;
	public static Boolean blnExecutedSyncLogic											= false;
	
	public String strAsyncPDFToUpdate;
	
	/**
	* @Method: 		GNF_PDFAsincronosTriggerHandler_cls
	* @param: 		N/A
	* @Description:	Constructor por defecto.
	* @author 		Manuel Medina - 24102017
	*/
	public GNF_PDFAsincronosTriggerHandler_cls(){
		
	}
	
	/**
	* @Method: 		GNF_PDFAsincronosTriggerHandler_cls
	* @param: 		String strAsyncPDFToUpdate
	* @Description:	Constructor para acceder a funcionalida Queueable.
	* @author 		Manuel Medina - 24102017
	*/
	public GNF_PDFAsincronosTriggerHandler_cls( String strAsyncPDFToUpdate ){
		this.strAsyncPDFToUpdate														= strAsyncPDFToUpdate;
	}
	
	/**
	* @Method: 		execute
	* @param: 		QueueableContext context
	* @Description: Metodo asincrono para agendar generacion de html de documentos.
	* @author 		Manuel Medina - 25092017
	*/
	public void execute( QueueableContext context ){
		updatePDF( strAsyncPDFToUpdate );
	}
	
	/**
	* @Method: 		attachPDF
	* @param: 		Map<Id, GNF_PDFAsincronos__c> mapNewAsyncPDF
	* @Description:	Ajuntar PDF asincrono.
	* @author 		Manuel Medina - 29092017
	*/
	public static void attachPDF( Map<Id, GNF_PDFAsincronos__c> mapNewAsyncPDF ){
		List<GNF_PDFAsincronos__c> lstAsyncPDFToUpdate									= new List<GNF_PDFAsincronos__c>();
		Map<Id, GNF_PDFAsincronos__c> mapAsyncPDFById									= new Map<Id, GNF_PDFAsincronos__c>( [
																							SELECT Id,
																								GNF_Objeto__c,
																								GNF_IdRegistro__c,
																								GNF_Plantilla__c,
																								GNF_PDFAdjuntado__c,
																								GNF_Iterating__c,
																								(
																									SELECT Id
																									FROM Attachments
																								)
																							FROM GNF_PDFAsincronos__c
																							WHERE Id IN: mapNewAsyncPDF.keySet()
																						] );
		
		for( GNF_PDFAsincronos__c objAsyncPDF : mapNewAsyncPDF.values() ){
			if( !mapAsyncPDFById.get( objAsyncPDF.Id ).Attachments.isEmpty() && !objAsyncPDF.GNF_PDFAdjuntado__c && !objAsyncPDF.GNF_Iterating__c ){
				
				if( blnExecutedSyncLogic ){
					objAsyncPDF.GNF_PDFAdjuntado__c										= true;
					objAsyncPDF.GNF_Iterating__c										= false;
					objAsyncPDF.GNF_FinProcesamiento__c									= System.now();
				}
				
				lstAsyncPDFToUpdate.add( objAsyncPDF );
			}
		}
		
		if( !lstAsyncPDFToUpdate.isEmpty() && !System.isFuture() && !System.isBatch() && !blnExecutedSyncLogic ){
			updatePDFAsync( System.JSON.serialize( lstAsyncPDFToUpdate ) );
			
		}else if( !lstAsyncPDFToUpdate.isEmpty() && blnExecutedSyncLogic ){
			updatePDFSync( System.JSON.serialize( lstAsyncPDFToUpdate ) );
		}
	}
	
	/**
	* @Method: 		updatePDFAsync
	* @param: 		String strAsyncPDFToUpdate
	* @Description:	Actualizar PDF asincrono para desencadenar el adjunto al registro principal.
	* @author 		Manuel Medina - 29092017
	*/
	@future 
	public static void updatePDFAsync( String strAsyncPDFToUpdate ){
		GNF_PDFAsincronosTriggerHandler_cls clsPDFAsincronos							= new GNF_PDFAsincronosTriggerHandler_cls();
		clsPDFAsincronos.updatePDF( strAsyncPDFToUpdate );
	}
	
	/**
	* @Method: 		updatePDFSync
	* @param: 		String strAsyncPDFToUpdate
	* @Description:	Actualizar PDF asincrono para desencadenar el adjunto al registro principal.
	* @author 		Manuel Medina - 29092017
	*/
	public static void updatePDFSync( String strAsyncPDFToUpdate ){
		GNF_PDFAsincronosTriggerHandler_cls clsPDFAsincronos							= new GNF_PDFAsincronosTriggerHandler_cls();
		clsPDFAsincronos.updatePDF( strAsyncPDFToUpdate );
		blnAlreadyExecuted_attachPDF													= true;
	}
	
	/**
	* @Method: 		updatePDF
	* @param: 		String strAsyncPDFToUpdate
	* @Description:	Actualizar PDF asincrono para desencadenar el adjunto al registro principal.
	* @author 		Manuel Medina - 23102017
	*/
	public void updatePDF( String strAsyncPDFToUpdate ){
		Map<String, GNF_PDFAsincronos__c> mapAsyncPDFById								= new Map<String, GNF_PDFAsincronos__c>();
		List<GNF_PDFAsincronos__c> lstAsyncPDFToUpdate									= new List<GNF_PDFAsincronos__c>(
																							( List<GNF_PDFAsincronos__c> ) System.JSON.deserialize( strAsyncPDFToUpdate, List<GNF_PDFAsincronos__c>.class )
																						);
		
		for( GNF_PDFAsincronos__c objAsyncPDF : lstAsyncPDFToUpdate ){
			objAsyncPDF.GNF_PDFAdjuntado__c												= true;
			objAsyncPDF.GNF_Iterating__c												= false;
			objAsyncPDF.GNF_FinProcesamiento__c											= System.now();
			
			mapAsyncPDFById.put( objAsyncPDF.Id, objAsyncPDF );
		}
		
		if( !blnExecutedSyncLogic && !System.isBatch() ){
			update lstAsyncPDFToUpdate;
		}
		
		try{
			Map<String, GNF_PDFAsincronos__c> mapAsyncPDFToDeleteById					= new Map<String, GNF_PDFAsincronos__c>( [
																							SELECT Id,
																								GNF_Objeto__c,
																								GNF_IdRegistro__c,
																								GNF_Plantilla__c,
																								GNF_TiempoCreacion__c
																							FROM GNF_PDFAsincronos__c
																							WHERE GNF_TiempoCreacion__c >= 86400
																							AND GNF_Iterating__c = false
																						] );
			
			if( !mapAsyncPDFToDeleteById.isEmpty() ){
				delete mapAsyncPDFToDeleteById.values();
				Database.emptyRecycleBin( mapAsyncPDFToDeleteById.values() );
			}
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
		}
	}
	
	/**
	* @Method: 		updateRecordsAsync
	* @param: 		String strMapNewAsyncPDF
	* @Description:	Actualizar los registros relacionados a los PDF generados desde procesamiento asincrono.
	* @author 		Manuel Medina - 01022018
	*/
	@future 
	public static void updateRecordsAsync( String strMapNewAsyncPDF ){
		updateRecords( strMapNewAsyncPDF );
	}
	
	/**
	* @Method: 		updateRecordsSync
	* @param: 		String strMapNewAsyncPDF
	* @Description:	Actualizar los registros relacionados a los PDF generados desde procesamiento sincrono.
	* @author 		Manuel Medina - 01022018
	*/
	public static void updateRecordsSync( String strMapNewAsyncPDF ){
		updateRecords( strMapNewAsyncPDF );
		blnAlreadyExecuted_updateRecords												= true;
	}
	
	/**
	* @Method: 		updateRecords
	* @param: 		String strMapNewAsyncPDF
	* @Description:	Actualizar los registros relacionados a los PDF generados.
	* @author 		Manuel Medina - 25102017
	*/
	public static void updateRecords( String strMapNewAsyncPDF, Boolean blnUpdate ){
		blnExecutedSyncLogic															= false;
		updateRecords( strMapNewAsyncPDF );
	}
	
	/**
	* @Method: 		updateRecords
	* @param: 		String strMapNewAsyncPDF
	* @Description:	Actualizar los registros relacionados a los PDF generados.
	* @author 		Manuel Medina - 25102017
	*/
	public static void updateRecords( String strMapNewAsyncPDF ){
		Map<String, List<String>> mapRecordIdsBySObjectType								= new Map<String, List<String>>();
		Map<String, List<String>> mapTemplatesBySObjectType								= new Map<String, List<String>>();
		Map<String, GNF_Plantilla__c> mapTemplateBySObjectName;
		Set<String> setTemplateUniqueNames												= new Set<String>();
		Set<String> setRecordIds														= new Set<String>();
		Map<String, SObject> mapRecordsToUpdateById										= new Map<String, SObject>();
		
		Map<Id, GNF_PDFAsincronos__c> mapNewAsyncPDF									= new Map<Id, GNF_PDFAsincronos__c>();
		mapNewAsyncPDF																	= ( Map<Id, GNF_PDFAsincronos__c> ) JSON.deserialize( strMapNewAsyncPDF, Map<Id, GNF_PDFAsincronos__c>.class );
		
		for( GNF_PDFAsincronos__c objAsyncPDF : mapNewAsyncPDF.values() ){
			
			if( ( objAsyncPDF.GNF_PDFAdjuntado__c && objAsyncPDF.GNF_Iterating__c ) || !objAsyncPDF.GNF_Iterating__c ){
			
				if( mapRecordIdsBySObjectType.containsKey( objAsyncPDF.GNF_Objeto__c ) ){
					mapRecordIdsBySObjectType.get( objAsyncPDF.GNF_Objeto__c ).add( objAsyncPDF.GNF_IdRegistro__c );
					
				}else if( !mapRecordIdsBySObjectType.containsKey( objAsyncPDF.GNF_Objeto__c ) ){
					mapRecordIdsBySObjectType.put( objAsyncPDF.GNF_Objeto__c, new List<String>{ objAsyncPDF.GNF_IdRegistro__c } );
				}
				
				if( mapTemplatesBySObjectType.containsKey( objAsyncPDF.GNF_Objeto__c ) ){
					mapTemplatesBySObjectType.get( objAsyncPDF.GNF_Objeto__c ).add( objAsyncPDF.GNF_Plantilla__c );
					
				}else if( !mapTemplatesBySObjectType.containsKey( objAsyncPDF.GNF_Objeto__c ) ){
					mapTemplatesBySObjectType.put( objAsyncPDF.GNF_Objeto__c, new List<String>{ objAsyncPDF.GNF_Plantilla__c } );
				}
				
				setTemplateUniqueNames.add( objAsyncPDF.GNF_Plantilla__c );
				
			}
		}
		
		mapTemplateBySObjectName														= new Map<String, GNF_Plantilla__c>( [
																							SELECT Id,
																								GNF_CampoControlEmail__c,
																								GNF_NombreUnico__c,
																								GNF_ObjetoPrincipal__c
																							FROM GNF_Plantilla__c
																							WHERE GNF_NombreUnico__c IN: setTemplateUniqueNames
																						] );
		
		for( GNF_Plantilla__c objTemplate : mapTemplateBySObjectName.values() ){
			mapTemplateBySObjectName.put( objTemplate.GNF_ObjetoPrincipal__c, objTemplate );
		}
		
		for( String strSObjectType : mapRecordIdsBySObjectType.keySet() ){
			if( mapTemplateBySObjectName.containsKey( strSObjectType ) && String.isNotBlank( mapTemplateBySObjectName.get( strSObjectType ).GNF_CampoControlEmail__c ) ){
			
				for( String strRecordId : mapRecordIdsBySObjectType.get( strSObjectType ) ){
					SObject sObjMainObject							= Schema.getGlobalDescribe().get( strSObjectType ).newSObject();
					sObjMainObject.put( 'Id', strRecordId );
					sObjMainObject.put( mapTemplateBySObjectName.get( strSObjectType ).GNF_CampoControlEmail__c, true );
					
					mapRecordsToUpdateById.put( strRecordId, sObjMainObject );
				}
			}
		}
		
		try{
			if( !mapRecordsToUpdateById.isEmpty() && !blnExecutedSyncLogic ){
				update mapRecordsToUpdateById.values();
			}
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
		}
	}
	
	/**
	* @Method: 		updateRecordsSync
	* @param: 		List<updateRecordsSyncParameters> lstUpdateRecordsSyncParameters
	* @Description: Actualiza los registros relacionados una vez a terminado el procesamiento sincrono de documentos PDF.
	* @author 		Manuel Medina - 09022018
	*/
	@invocableMethod(label='Actualizar registros relacionados.')
	public static List<String> updateRecordsSync( List<updateRecordsSyncParameters> lstUpdateRecordsSyncParameters ){
		GNF_PDFAsincronos__c objAsyncPDF												= new GNF_PDFAsincronos__c();
		objAsyncPDF																		= [SELECT Id,
																								GNF_Objeto__c,
																								GNF_IdRegistro__c,
																								GNF_Plantilla__c,
																								GNF_TiempoCreacion__c,
																								GNF_PDFAdjuntado__c
																							FROM GNF_PDFAsincronos__c
																							WHERE Id =: lstUpdateRecordsSyncParameters.get( 0 ).strAsyncPDFId
																							LIMIT 1
																						];
																						
		updateRecords( JSON.serialize( new Map<Id, GNF_PDFAsincronos__c>{ objAsyncPDF.Id => objAsyncPDF } ), false );
		
		return new List<String>{ 'OK' };
	}
	
	/**
	* @Method: 		updateRecordsSyncParameters
	* @param: 		N/A
	* @Description: Define parametros invocables desde acciones de Salesforce.
	* @author 		Manuel Medina - 09022018
	*/
	public class updateRecordsSyncParameters{
		@invocableVariable( label='Id del registro de procesamiento de PDF.' )
		public String strAsyncPDFId;
	}
}