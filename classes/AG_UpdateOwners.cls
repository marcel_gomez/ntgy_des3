public class AG_UpdateOwners {
    private static Map<String, User> userMap; 
    // Input list contains the ID of the user
    @InvocableMethod(label='Execute Batch Update Owners')
    public static void executeUpdateOwners(List<ID> pListNewOwnerId) {
        System.debug('AG_UpdateOwners.executeUpdateOwners');
        ID pNewOwnerId = pListNewOwnerId.get(0);
        //Recuperamos el ID de Empleado para poder buscar en la tabla Empleados
        List<User> sUsers = [Select ID_Empleado__c, isActive From User Where Id = :pNewOwnerId Limit 1];
        if(sUsers.size() != 0)
        {
            User theUser = sUsers.get(0);
            String sEmpleadoId = theUser.ID_Empleado__c;
            userMap = new Map<String, User>();
            for(User aUser : [SELECT Id, ID_Empleado__c FROM User Where ID_Empleado__c != null and isActive  = true]) {
                User prevUser = userMap.put(aUser.ID_Empleado__c, aUser);
            }
            if (!theUser.isActive) {
                System.debug('AG_UpdateOwners.executeUpdateOwners: user has been deactivated. ID Empleado = ' + theUser.ID_Empleado__c);
                // User has been deactivated   
                Map<String, Empleado__c> mapEmployees = new Map<String, Empleado__c>();
                for (Empleado__c tmpEmployee: [Select Id, ID_Manager__c, ID_Empleado__c From Empleado__c]) {
                    mapEmployees.put(tmpEmployee.ID_Empleado__c, tmpEmployee);
                }  

                // get the user of the manager
                Empleado__c employee = mapEmployees.get(theUser.ID_Empleado__c);
                if (employee != null && employee.ID_Manager__c != null) {
                    System.debug('AG_UpdateOwners.executeUpdateOwners: ID Empleado of the Manager = ' + employee.ID_Manager__c);
                    User userOfManager = getUserOfManager(employee.ID_Manager__c, mapEmployees);
                    if (userOfManager != null) {
                        AG_Batch_UpdateOnwers processUpdateOwner = new AG_Batch_UpdateOnwers(userOfManager.ID, employee.Id, null);
                            ID batchprocessid = Database.executeBatch(processUpdateOwner);           
                            updateSubordinateEmployees(userOfManager.ID, sEmpleadoId);
                    }
                }
            }
            else {
                if (!String.isEmpty(sEmpleadoId)) { 
                    List<Empleado__c> sEmpleados = [Select Id From Empleado__c Where ID_Empleado__c = :sEmpleadoId Limit 1];  
                    if(sEmpleados.size() != 0)
                    {
                        System.debug('AG_UpdateOwners.executeUpdateOwners: launch update owners batch process');
                        AG_Batch_UpdateOnwers processUpdateOwner = new AG_Batch_UpdateOnwers(pNewOwnerId, sEmpleados.get(0).Id, null);
                        ID batchprocessid = Database.executeBatch(processUpdateOwner);       
                    }
                    // in addition, if the user is a manager, update owner id of the employees in its hierarchy without user
                    updateSubordinateEmployees(pNewOwnerId, sEmpleadoId);
                }
            }

        }
        
    }

    private static User getUserOfManager(String employee_id, Map<String, Empleado__c> mapEmployees) {
        System.debug('AG_UpdateOwners.getUserOfManager: ID empleado = ' + employee_id);
        User userOfManager = userMap.get(employee_id);
        if (userOfManager != null) {
            System.debug('AG_UpdateOwners.getUserOfManager: manager has user');
            return userOfManager;
        }
        else {
            
            Empleado__c employee = mapEmployees.get(employee_id);
            System.debug('AG_UpdateOwners.getUserOfManager: manager has NO user. Look for the user of  employee = ' + employee.ID_Manager__c);
            if (employee != null && employee.ID_Manager__c != null) {
                return getUserOfManager(employee.ID_Manager__c, mapEmployees);
            }   
        }
        return null ;
    }

    private static void updateSubordinateEmployees(Id newUserId, String mangerEmployeeId) {
        System.debug('AG_UpdateOwners.updateSubordinateEmployees: Manager Employee Id = ' + mangerEmployeeId);
        for (Empleado__c subordinate: [SELECT ID_Empleado__c From Empleado__c Where ID_Manager__c = :mangerEmployeeId]) {
            // Check if subordinate has user, if not, the owner of the Accunts, offers, etc managed by this employee
            // must the new user
            if (userMap.get(subordinate.ID_Empleado__c) == null) {
                // launch the batch process to update owners
                System.debug('AG_UpdateOwners.executeUpdateOwners: launch update owners batch process for subordinate employee = ' + subordinate.ID_Empleado__c);
                AG_Batch_UpdateOnwers processUpdateOwner = new AG_Batch_UpdateOnwers(newUserId, subordinate.Id, null);
                ID batchprocessid = Database.executeBatch(processUpdateOwner);       
                updateSubordinateEmployees(newUserId, subordinate.ID_Empleado__c);
            }
            
        }
    }
}