@isTest
private class GNF_cotizador_ctr_TEST {
	@isTest static void test_GNF_cotizador_ctr_mthd() {
		
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];
	
		GNF_cotizador_ctr controllerSite = new GNF_cotizador_ctr();
	}

	@isTest static void test_rmt_onloadpage_mthd() {

		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];

		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = GNF_cotizador_ctr.rmt_onloadpage(String.valueOf(listaopps.get(0).Id));
	}


	@isTest static void test_saveData_ctr_mthd() {

		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		Test.startTest();
		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];
		Opportunity opp= listaopps.get(0);

		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = getWrapperTestData(opp.Id);

		GNF_cotizador_ctr.saveData(wrapper, null);
		Test.stopTest();
	}

	@isTest static void test_updateDataInitial_ctr_mthd() {

		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		Test.startTest();
		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];
		Opportunity opp= listaopps.get(0);

		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = getWrapperTestData(opp.Id);
		// updateData(String paramObject_str, wrp_infoCotizador_cls paramWrapper_wrp, String paramDateTime_str)
		String strObjectCase = 'Initial'; // Initial | CalculoFormulasSite | StageCotizada
		GNF_cotizador_ctr.updateData(strObjectCase , wrapper, null);
		Test.stopTest();
	}

	@isTest static void test_updateDataCalculoFormulasSite_ctr_mthd() {

		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		Test.startTest();
		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];
		Opportunity opp= listaopps.get(0);

		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = getWrapperTestData(opp.Id);
		// updateData(String paramObject_str, wrp_infoCotizador_cls paramWrapper_wrp, String paramDateTime_str)
		String strObjectCase = 'CalculoFormulasSite'; // Initial | CalculoFormulasSite | StageCotizada
		GNF_cotizador_ctr.updateData(strObjectCase , wrapper, null);
		Test.stopTest();
	}

	@isTest static void test_updateDataStageCotizada_ctr_mthd() {

		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		Test.startTest();
		list<Opportunity> listaopps = [Select Id from Opportunity WHERE Name = 'DELTA_RENDIMIENTO un click sf1'];
		Opportunity opp= listaopps.get(0);

		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = getWrapperTestData(opp.Id);
		// updateData(String paramObject_str, wrp_infoCotizador_cls paramWrapper_wrp, String paramDateTime_str)
		String strObjectCase = 'StageCotizada'; // Initial | CalculoFormulasSite | StageCotizada
		GNF_cotizador_ctr.updateData(strObjectCase , wrapper, null);
		Test.stopTest();
	}
	
	@isTest static void test_rmt_onloadpage_two_mthd() {
		GNF_cotizador_ctr controllerSite = new GNF_cotizador_ctr();
		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = GNF_cotizador_ctr.rmt_onloadpage(null);
	}

	@isTest static void test_attachPDF_mthd() {
		GNF_cotizador_ctr.attachPDF('', false);
		GNF_cotizador_ctr.attachPDF('', true);
	}

	public static GNF_cotizador_ctr.wrp_infoCotizador_cls getWrapperTestData(Id param_IdOpportunity_Id){
		GNF_cotizador_ctr.wrp_infoCotizador_cls wrapper = new GNF_cotizador_ctr.wrp_infoCotizador_cls('001');
			wrapper.dc_comsumosKwh=0;
			wrapper.dc_comsumosMMbtu=0;
			wrapper.str_accName='MERCADONA SA';
			wrapper.str_formulaDestino='(0;45+0;001726*G1CMED603+0;002691*F1CMED603)* Pa5  + F';
			wrapper.str_idAcc='0014E00000eoiPlQAI';
			wrapper.str_idClick=param_IdOpportunity_id;
			wrapper.str_metodoAplicacion='Q Abs.';
			wrapper.str_noClick='CL00013208';
			wrapper.str_periodicidadPrecio='Único';
			wrapper.str_primaRiesgo='0.0000';
			wrapper.str_stageName='A cotizar';
			wrapper.str_targetPrice='false';
			wrapper.str_tipo_Operacion='Click';
			wrapper.str_tipoFijacion='SQ';
			wrapper.str_tipoPrecio='Indicativo';
			wrapper.str_tipoProducto='M';
			wrapper.str_tipoProductoDesc='Month';
			wrapper.str_tipoServicio='SWAP Hub';

			GNF_cotizador_ctr.wrp_infoConsumos_cls objConsumo = new GNF_cotizador_ctr.wrp_infoConsumos_cls(new Consumo_Click__c(), wrapper.map_consumoCentKwhXmesXperiodo, wrapper.map_consumoMmbtuwhXmesXperiodo);
				objConsumo.str_consumosKwh = '1212';
				objConsumo.str_consumosMMBTu = '4';
				objConsumo.str_contratoEspejoCompraSwap = 'GNC1801M150718';
				objConsumo.str_cvCliente = '0.0000';
				objConsumo.str_idConsumoClick = 'a0U4E000003tN5xUAE';
				objConsumo.str_idCotizacion = 'a0V4E000002GhccUAC';
				objConsumo.str_mes = 'Febrero';
				objConsumo.str_paridadReferenciaUsdEur = '0.000';
				objConsumo.str_periodo = 'M2-18';
				objConsumo.str_tradeGroupCompraSwap = 'HGNCOM180115_';
				objConsumo.uniqueKey = 'M2-18Febrero';

			wrapper.lst_infoConsumos.add(objConsumo);

		return wrapper;
	}
	
}