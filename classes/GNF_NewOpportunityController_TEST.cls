/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de NewOpportunityController.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-22		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_NewOpportunityController_TEST {
	
	static testMethod void test_NewOpportunityController_mthd() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Test.startTest();
		
			List<Account> lstAccount										= new List<Account>();
			lstAccount														= [SELECT Id
																				FROM Account
																			];
			Account acc = lstAccount.get(1);
			ApexPages.StandardSetController sCtr							= new ApexPages.StandardSetController( lstAccount );
			ApexPages.CurrentPage().getParameters().put('id',acc.Id);
			NewOpportunityController ctrNewOpportunity						= new NewOpportunityController( sCtr );
			System.debug(ctrNewOpportunity.typesList);
			
		
		Test.stopTest();
	}

	@isTest
	static void test_returnPage_mthd() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Test.startTest();
		
			List<Account> lstAccount										= new List<Account>();
			lstAccount														= [SELECT Id
																				FROM Account
																			];
			Account acc = lstAccount.get(0);
			ApexPages.StandardSetController sCtr							= new ApexPages.StandardSetController( lstAccount );
			ApexPages.CurrentPage().getParameters().put('id',acc.Id);
			NewOpportunityController ctrNewOpportunity						= new NewOpportunityController( sCtr );
			ctrNewOpportunity.returnPage();
		
		Test.stopTest();
	}

	@isTest
	static void test_getRecordTypeId_mthd() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Test.startTest();
		
			List<Account> lstAccount										= new List<Account>();
			lstAccount														= [SELECT Id
																				FROM Account
																			];
			Account acc = lstAccount.get(0);
			ApexPages.StandardSetController sCtr							= new ApexPages.StandardSetController( lstAccount );
			ApexPages.CurrentPage().getParameters().put('id',acc.Id);
			NewOpportunityController ctrNewOpportunity						= new NewOpportunityController( sCtr );
			ctrNewOpportunity.getRecordTypeId();
		
		Test.stopTest();
	}
}