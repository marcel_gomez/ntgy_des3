/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcionalidades requeridas por el trigger GNF_Contrato_tgr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-08		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
public class GNF_ContratoTriggerHandler_cls {
	
	/**
	* @Method: 		validateContract
	* @param: 		List<Contrato__c> lstContracts
	* @param: 		Map<Id, Contrato__c> mapOldContractsById
	* @Description: Valida los clientes y/o sectores suministro ficticios y genera la eliminacion cuando sea necesario.
	* @author 		Manuel Medina - 10052017
	*/
	public static void validateContract( List<Contrato__c> lstContracts, Map<Id, Contrato__c> mapOldContractsById ){
		Map<Id, Account> mapAccountsById						= new Map<Id, Account>();
		Map<Id, SSs__c> mapSupplySectorsById					= new Map<Id, SSs__c>();
		/*List<Account> lstAccountsToDelete						= new List<Account>();
		List<SSs__c> lstSupplySectorsToDelete					= new List<SSs__c>();*/
		
		for( Contrato__c objContract : lstContracts ){
			if( String.isNotEmpty( objContract.Cliente__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).Cliente__c ) && !objContract.Cliente__c.equals( mapOldContractsById.get( objContract.Id ).Cliente__c ) ){
				mapAccountsById.put( objContract.Cliente__c, new Account() );
				mapAccountsById.put( mapOldContractsById.get( objContract.Id ).Cliente__c, new Account() );
			}
			
			if( String.isNotEmpty( objContract.NISS__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).NISS__c ) && !objContract.NISS__c.equals( mapOldContractsById.get( objContract.Id ).NISS__c ) ){
				mapSupplySectorsById.put( objContract.NISS__c, new SSs__c() );
				mapSupplySectorsById.put( mapOldContractsById.get( objContract.Id ).NISS__c, new SSs__c() );
			}
		}
		
		mapAccountsById											= new Map<Id, Account>( [
																	SELECT Id,
																		Ficticio__c,
																		NIF_CIF__c
																	FROM Account
																	WHERE Id IN: mapAccountsById.keySet()
																] );
																
		mapSupplySectorsById									= new Map<Id, SSs__c>( [
																	SELECT Id,
																		Ficticio__c,
																		CUPS__c,
																		Direccion__c
																	FROM SSs__c
																	WHERE Id IN: mapSupplySectorsById.keySet()
																] );
																
		for( Contrato__c objContract : lstContracts ){
			if( String.isNotEmpty( objContract.Cliente__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).Cliente__c ) && mapAccountsById.containsKey( objContract.Cliente__c ) && mapAccountsById.get( objContract.Cliente__c ).Ficticio__c && !objContract.Cliente__c.equals( mapOldContractsById.get( objContract.Id ).Cliente__c ) && ( ( String.isNotBlank( mapAccountsById.get( mapOldContractsById.get( objContract.Id ).Cliente__c ).NIF_CIF__c ) && String.isNotBlank( mapAccountsById.get( objContract.Cliente__c ).NIF_CIF__c ) && !mapAccountsById.get( mapOldContractsById.get( objContract.Id ).Cliente__c ).NIF_CIF__c.equals( mapAccountsById.get( objContract.Cliente__c ).NIF_CIF__c ) ) ) ){
				objContract.Cliente__c.addError( System.Label.GNF_TgrContratoMensaje1 );
				
			}/*else if( String.isNotEmpty( objContract.Cliente__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).Cliente__c ) && mapAccountsById.containsKey( objContract.Cliente__c ) && !mapAccountsById.get( objContract.Cliente__c ).Ficticio__c && !objContract.Cliente__c.equals( mapOldContractsById.get( objContract.Id ).Cliente__c ) && ( ( String.isNotBlank( mapAccountsById.get( mapOldContractsById.get( objContract.Id ).Cliente__c ).NIF_CIF__c ) && String.isNotBlank( mapAccountsById.get( objContract.Cliente__c ).NIF_CIF__c ) && mapAccountsById.get( mapOldContractsById.get( objContract.Id ).Cliente__c ).NIF_CIF__c.equals( mapAccountsById.get( objContract.Cliente__c ).NIF_CIF__c ) ) ) ){
				lstAccountsToDelete.add( mapAccountsById.get( mapOldContractsById.get( objContract.Id ).Cliente__c ) );
			}*/
			
			if( String.isNotEmpty( objContract.NISS__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).NISS__c ) && mapSupplySectorsById.containsKey( objContract.NISS__c ) && mapSupplySectorsById.get( objContract.NISS__c ).Ficticio__c && !objContract.NISS__c.equals( mapOldContractsById.get( objContract.Id ).NISS__c ) && ( ( String.isNotBlank( mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).CUPS__c ) && String.isNotBlank( mapSupplySectorsById.get( objContract.NISS__c ).CUPS__c ) && !mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).CUPS__c.equals( mapSupplySectorsById.get( objContract.NISS__c ).CUPS__c ) ) || ( String.isNotBlank( mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).Direccion__c ) && String.isNotBlank( mapSupplySectorsById.get( objContract.NISS__c ).Direccion__c ) && !mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).Direccion__c.equals( mapSupplySectorsById.get( objContract.NISS__c ).Direccion__c ) ) ) ){
				objContract.NISS__c.addError( System.Label.GNF_TgrContratoMensaje2 );
				
			}/*else if( String.isNotEmpty( objContract.NISS__c ) && String.isNotEmpty( mapOldContractsById.get( objContract.Id ).NISS__c ) && mapSupplySectorsById.containsKey( objContract.NISS__c ) && !mapSupplySectorsById.get( objContract.NISS__c ).Ficticio__c && !objContract.NISS__c.equals( mapOldContractsById.get( objContract.Id ).NISS__c ) && ( ( String.isNotBlank( mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).CUPS__c ) && String.isNotBlank( mapSupplySectorsById.get( objContract.NISS__c ).CUPS__c ) && mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).CUPS__c.equals( mapSupplySectorsById.get( objContract.NISS__c ).CUPS__c ) ) || ( String.isNotBlank( mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).Direccion__c ) && String.isNotBlank( mapSupplySectorsById.get( objContract.NISS__c ).Direccion__c ) && mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ).Direccion__c.equals( mapSupplySectorsById.get( objContract.NISS__c ).Direccion__c ) ) ) ){
				lstSupplySectorsToDelete.add( mapSupplySectorsById.get( mapOldContractsById.get( objContract.Id ).NISS__c ) );
			}*/
		}
		
		/*if( ( !lstAccountsToDelete.isEmpty() || !lstSupplySectorsToDelete.isEmpty() ) && !System.isFuture() ){
			String strAccountsToDelete							= System.JSON.serialize( lstAccountsToDelete );
			String strSupplySectorsToDelete						= System.JSON.serialize( lstSupplySectorsToDelete );
			
			deleteOldRecords( strAccountsToDelete, strSupplySectorsToDelete );
		}*/
	}
	
	/**
	* @Method: 		deleteOldRecords
	* @param: 		String strAccountsToDelete
	* @param: 		String strSupplySectorsToDelete
	* @Description: Elimina el cliente y/o sector suministro ficticios de un contrato cuando se cambia por el registro de Delta.
	* @author 		Manuel Medina - 10052017
	*/
	@future 
	public static void deleteOldRecords( String strAccountsToDelete, String strSupplySectorsToDelete ){
		List<Account> lstAccountsToDelete						= new List<Account>();
		lstAccountsToDelete										= ( List<Account> ) System.JSON.deserialize( strAccountsToDelete, List<Account>.class );
		
		List<SSs__c> lstSupplySectorsToDelete					= new List<SSs__c>();
		lstSupplySectorsToDelete								= ( List<SSs__c> ) System.JSON.deserialize( strSupplySectorsToDelete, List<SSs__c>.class );
		
		if( !lstSupplySectorsToDelete.isEmpty() ){
			delete lstSupplySectorsToDelete;
		}
		
		if( !lstAccountsToDelete.isEmpty() ){
			delete lstAccountsToDelete;
		}
	}
}