public with sharing class AG_SecurityRecalculationProcessUtil {

    //Los objetos afectadas por la seguridad de los sharing.
    //Han de tener definido los campos: Cliente__c, Gestor_Cliente__c, Gestor__c, Gestor_external_key__c
    public static final List<String> BUSINESS_OBJECTS = 
        new List<String> {'Suministros__c', 'Actividad_Comercial__c','Ofertas__c', 'SRs__c','SSs__c'};

    /**
    * Lanza el proceso batch de borrado y recalculo de los  objectShare 
    * @param recalculateAll Recalcula todos los permisos y no solo de los objetos modificados
    * @param noClean Salta proceso de borrado de los permisos previos
    */
    public static Id executeBatchToRecalculateObjetShares(Boolean recalculateAll, Boolean noClean) {

        Id batchId = null;
        if (!noClean) {
            batchId = AG_Batch_CleanObjectShare.executeFullProcess(recalculateAll);
        } else {
            batchId = AG_Batch_RecalculateObjectShare.executeFullProcess(recalculateAll);
        }

        System.debug('**** Proceso de recalculo de objetos = ' + batchId);
        return batchId;
    }

    /**
    * Lanza el proceso batch de borrado y recalculo de los AccountShare
    * @param recalculateAll Recalcula todos los permisos y no solo de los objetos modificados
    * @param noClean Salta proceso de borrado de los permisos previos
    */
    public static Id executeBatchToRecalculateAccountShares(Boolean recalculateAll, Boolean noClean) {
        
        Id batchId = null;
        if (!noClean) {
            batchId = AG_Batch_CleanAccountShare.executeFullProcess(recalculateAll);
        } else {
            batchId = AG_Batch_RecalculateAccountShare.executeFullProcess(recalculateAll);
        }
        System.debug('**** Proceso de recalculo de objetos = ' + batchId);

        return batchId;
    }


    /**
    * Lanza el proceso batch de borrado y recalculo de los AccountShare y objectShare 
    * @param recalculateAll Recalcula todos los permisos y no solo de los objetos modificados
    * @param noClean Salta proceso de borrado de los permisos previos
    */
    public static Id[] executeBatchToRecalculateShares(Boolean recalculateAll, Boolean noClean) {

        Id accountBatchId = executeBatchToRecalculateAccountShares(recalculateAll, noClean);

        Id objectBatchId = executeBatchToRecalculateObjetShares(recalculateAll, noClean);
        
        return new List<Id> {accountBatchId, objectBatchId};

    }

    /**
    * Lanza el proceso batch de borrado y recalculo de los AccountShare y objectShare de los objetos modificados
    */
    public static Id[] executeBatchToRecalculateShares() {

        return executeBatchToRecalculateShares(false, false);
    }

}