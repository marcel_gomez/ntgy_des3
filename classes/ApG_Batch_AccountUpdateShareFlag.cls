/*
VASS 31052018 first implementation
Updates "Recalcular_Permisos__c" or "Recalcular_Permisos_Objeto__c" account's field to true. Called after sharing recalculation from AG_Batch_RecalculateAccountShare or AG_Batch_RecalculateObjectShare
*/
global with sharing class ApG_Batch_AccountUpdateShareFlag implements Database.Batchable<sObject>, Database.Stateful{
    private string accountFieldToUpdate;
    private list<id> accountToUpdateList;
    private boolean setTo;

    public ApG_Batch_AccountUpdateShareFlag(string accountFieldToUpdate, list<id> accountToUpdateList, boolean setTo){
    	this.accountFieldToUpdate = accountFieldToUpdate;
    	this.accountToUpdateList = accountToUpdateList;
    	this.setTo = setTo;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    	boolean notSetTo = !setTo;
		string query = 'SELECT Id, '+accountFieldToUpdate+' FROM Account WHERE '+accountFieldToUpdate+' =: notSetTo';
		if(accountToUpdateList!=null){
			query += ' AND Id IN: accountToUpdateList';
		}
		system.debug('lanzamiento del metodo start, query generada	' + query);
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		system.debug('Cuentas que se van a procesar en este bloque: ' +scope.size() + ' - ' + scope);
   		for (sObject i : scope) {
			i.put(accountFieldToUpdate, setTo);
   		}
		update scope;

	}
	
	global void finish(Database.BatchableContext BC) {
		system.debug('proceso finalizado');
	}
}