/**
 * Created by luis.igualada on 04/09/2017.
 */
@istest
private class GNF_consolidadosClick_ctr_TEST {

	static testMethod void GNF_consolidadosClick(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		Opportunity opp = [Select Id from Opportunity LIMIT 1];

		ApexPages.StandardController sctr = new ApexPages.StandardController(opp);
		GNF_consolidadosClick_ctr ctr = new GNF_consolidadosClick_ctr(sctr);
		ctr.mthd_getConsumosClick();
	}

}