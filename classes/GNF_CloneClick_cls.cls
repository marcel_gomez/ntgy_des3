/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase invocable desde boton para desencadenar el clonado de click
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-06-09		Manuel Medina (MM)		Definicion inicial de la clase.
* @version	2.0		2017-11-14		Manuel Medina (MM)		Se remueve @future para procesar los clones sincronamente.
* @version	3.0		2017-11-20		Manuel Medina (MM)		Se aplica logica para determinar procesamiento sincrono o asincrono
*															tomando como referencia la cantidad de PS respecto a valor parametrizado
*															en GNF_ValoresPredeterminados__c.getInstance().GNF_LimitePSSincronos__c.
* @version	4.0		2017-11-21		Manuel Medina (MM)		Se realiza implementacion de metodo Queueable para garantizar procesamiento
*															para clicks masivos.
**************************************************************************************************************************************/
global class GNF_CloneClick_cls implements Queueable {
	
	global Map<Id, Opportunity> mapOpportunityByIdCloned										{get; set;}
	global Map<Id, Oportunidad_Punto_de_Suministros__c>  mapOpportunitySuppliesByOppIdCloned	{get; set;}
	global Map<Id, Cotizacion_de_periodo__c>  mapQuotesByOppIdCloned							{get; set;}
	global Map<Id, Consumo_Click__c>  mapCClickByOppIdCloned									{get; set;}
	
	/**
	* @Method: 		GNF_CloneClick_cls
	* @param: 		N/A
	* @Description:	Constructor para inicializar variables requeridos por metodo Queueable.
	* @author 		Manuel Medina - 21112017
	*/
	global GNF_CloneClick_cls(){
		this.mapOpportunityByIdCloned							= new Map<Id, Opportunity>();
		this.mapOpportunitySuppliesByOppIdCloned				= new Map<Id, Oportunidad_Punto_de_Suministros__c>();
		this.mapQuotesByOppIdCloned								= new Map<Id, Cotizacion_de_periodo__c>();
		this.mapCClickByOppIdCloned								= new Map<Id, Consumo_Click__c>();
	}
	
	/**
	* @Method: 		execute
	* @param: 		QueueableContext context
	* @Description: Metodo asincrono para procesar clon/unclick.
	* @author 		Manuel Medina - 21112017
	*/
	global void execute( QueueableContext context ){
		processClone( mapOpportunityByIdCloned, mapOpportunitySuppliesByOppIdCloned, mapQuotesByOppIdCloned, mapCClickByOppIdCloned );
	}
	
	/**
	* @Method: 		cloneClick
	* @param: 		N/A
	* @Description:	Realizar clonacion de un click dado el Id del registro.
	* @author 		Manuel Medina - 09062017
	*/
	webservice static String cloneClick( String strOpportunityId, Boolean blnIsClick ){
		GNF_OpportunityTriggerHandler_cls.setMapRecordTypeIdByDevName();
		
		Opportunity objOpportunity								= new Opportunity();
		objOpportunity											= ( Opportunity ) Database.query(
																	GNF_OpportunityTriggerHandler_cls.getSOQL(
																		'Opportunity',
																		'WHERE Id = \'' + strOpportunityId + '\' LIMIT 1'
																	)
																);
																
		Savepoint sp											= Database.setSavepoint();

		try{
			if( objOpportunity.RecordTypeId.equals( GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) && objOpportunity.StageName.equals( 'Firmada' ) ){
				throw new AdminException( System.Label.GNF_ClsClonacion );
			}
			
			if( ( objOpportunity.RecordTypeId.equals( GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) && !hasUnclickSigned( new Set<Id>{ objOpportunity.Click_Asociado__c } ).get( objOpportunity.Click_Asociado__c ).Clicks__r.isEmpty() ) ){
				throw new AdminException( System.Label.GNF_TgrOppMensaje3 );
			}
			
			Opportunity objNewOppotunity						= new Opportunity();
			objNewOppotunity.RecordTypeId						= blnIsClick ? objOpportunity.RecordTypeId : GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id;
			objNewOppotunity.AccountId							= objOpportunity.AccountId;
			objNewOppotunity.StageName							= blnIsClick ? GNF_ValoresPredeterminados__c.getInstance().GNF_ClickEstadoInicial__c : GNF_ValoresPredeterminados__c.getInstance().GNF_UnclickEstadoInicial__c;
			objNewOppotunity.CloseDate							= System.today();
			objNewOppotunity.Name								= blnIsClick ? objOpportunity.Name + ' - ' + System.now().format( 'dd/MM/yyyy' ) : objOpportunity.Name;
			objNewOppotunity.Click_Asociado__c					= blnIsClick ? ( objOpportunity.RecordTypeId.equals( GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ? objOpportunity.Click_Asociado__c : null ) : objOpportunity.Id;
			objNewOppotunity.GNF_IsClone__c						= blnIsClick;
			objNewOppotunity.GNF_AccesoWizard__c				= false;
			insert objNewOppotunity;
			
			startClone( new Map<Opportunity, Opportunity>{ objOpportunity => objNewOppotunity }, blnIsClick );
			
			return objNewOppotunity.Id;
	    	
		}catch( AdminException e ){
			String strError										= '';
			System.debug( '\n\n\n\t<<<<<<<<< AdminExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			strError											= '\n\n<<<<<<<<< AdminExceptionType >>>>>>>>> \n@@--> getCause > ' + e.getCause() + '\n@@--> getLineNumber > ' + e.getLineNumber() + '\n@@--> getMessage > '+ e.getMessage() + '\n@@--> getStackTraceString > '+ e.getStackTraceString() + '\n@@--> getTypeName > ' + e.getTypeName();
			
			Database.rollback( sp );
			
			return e.getMessage();
			
		}catch( DMLException e ){
			String strError										= '';
			for( Integer i=0; i<e.getNumDml(); i++ ){
				System.debug( '\n\n\n\t<<<<<<<<< DMLException >>>>>>>>> \n\t\t@@--> getDmlFieldNames > ' + e.getDmlFieldNames(i) + '\n\t\t@@--> getDmlId > ' + e.getDmlId(i) + '\n\t\t@@--> getDmlMessage > '+ e.getDmlMessage(i) + '\n\t\t@@--> getDmlType > '+ e.getDmlType(i) + '\n\n' );
				strError										+= e.getDmlMessage(i);
			}
			
			Database.rollback( sp );
			
			return strError;
			
		}catch( Exception e ){
			String strError										= '';
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			strError											= '\n\n<<<<<<<<< ExceptionType >>>>>>>>> \n@@--> getCause > ' + e.getCause() + '\n@@--> getLineNumber > ' + e.getLineNumber() + '\n@@--> getMessage > '+ e.getMessage() + '\n@@--> getStackTraceString > '+ e.getStackTraceString() + '\n@@--> getTypeName > ' + e.getTypeName();
			
			Database.rollback( sp );
			
			return strError;
		}
		
		return 'OK';
	}
    
	/**
	* @Method: 		hasUnclickSigned
	* @param: 		Set<Id> strOpportunityId
	* @Description:	Valida si el click tiene unclicks firmados.
	* @author 		Manuel Medina - 10082017
	*/
	public static Map<Id, Opportunity> hasUnclickSigned( Set<Id> setOpportunityIds ){
		Map<Id, Opportunity> mapOpportunityById													= new Map<Id, Opportunity>( [
																									SELECT Id,
																										(
																											SELECT Id
																											FROM Clicks__r
																											WHERE RecordType.DeveloperName = 'Unclick'
																											AND StageName = 'Firmada'
																										)
																									FROM Opportunity
																									WHERE Id IN: setOpportunityIds
																								] );

		return mapOpportunityById;
	}
	
	/**
	* @Method: 		startClone
	* @param: 		Map<Opportunity, Opportunity> mapOpportunityByTargetOpportunity
	* @param: 		Boolean blnIsClick
	* @Description:	Realizar clonacion masiva de los registros asociados a una oportunidad.
	* @author 		Manuel Medina - 08062017
	*/
	public static void startClone( Map<Opportunity, Opportunity> mapOpportunityByTargetOpportunity, Boolean blnIsClick ){
		Map<Id, Opportunity> mapOpportunityById													= new Map<Id, Opportunity>();
		Map<Id, Opportunity> mapOpportunityByIdCloned											= new Map<Id, Opportunity>();
		Map<Id, Oportunidad_Punto_de_Suministros__c> mapOpportunitySuppliesByOppId				= new Map<Id, Oportunidad_Punto_de_Suministros__c>();
		Map<Id, Oportunidad_Punto_de_Suministros__c> mapOpportunitySuppliesByOppIdCloned		= new Map<Id, Oportunidad_Punto_de_Suministros__c>();
		Map<Id, Cotizacion_de_periodo__c> mapQuotesByOppId										= new Map<Id, Cotizacion_de_periodo__c>();
		Map<Id, Cotizacion_de_periodo__c> mapQuotesByOppIdCloned								= new Map<Id, Cotizacion_de_periodo__c>();
		Map<Id, Consumo_Click__c> mapCClickByOpp												= new Map<Id, Consumo_Click__c>();
		Map<Id, Consumo_Click__c> mapCClickByOppIdCloned										= new Map<Id, Consumo_Click__c>();
		Set<Id> setOpportunityIds																= new Set<Id>();
		Map<Id, Opportunity> mapOpportunityIdByParentOpportunityId								= new Map<Id, Opportunity>();
		Map<Id, Opportunity> mapOpportunityIdByTargetOpportunityId								= new Map<Id, Opportunity>();
		
		for( Opportunity objOpportunity : mapOpportunityByTargetOpportunity.keySet() ){
			if( !blnIsClick ){
				mapOpportunityById.put( objOpportunity.Id, new Opportunity() );
				mapOpportunityIdByParentOpportunityId.put( objOpportunity.Id, objOpportunity );
				mapOpportunityIdByTargetOpportunityId.put( objOpportunity.Id, mapOpportunityByTargetOpportunity.get( objOpportunity ) );
				
			}else if( blnIsClick ){
				mapOpportunityById.put( objOpportunity.Id, new Opportunity() );
				mapOpportunityIdByTargetOpportunityId.put( objOpportunity.Id, mapOpportunityByTargetOpportunity.get( objOpportunity ) );
			}
		}
		
		setOpportunityIds.addAll( mapOpportunityById.keySet() );
		
		if( !setOpportunityIds.isEmpty() ){
			mapOpportunityById																	= new Map<Id, Opportunity>( 
																									( List<Opportunity> ) Database.query(
																										GNF_OpportunityTriggerHandler_cls.getSOQL(
																											'Opportunity',
																											'WHERE Id IN: setOpportunityIds \n'
																										).replace( 'stagename,', '' ).replace( 'stagename', '' )
																									)
																								);
		
			if( !mapOpportunityById.isEmpty() ){
			
				for( Opportunity objOpportunity : mapOpportunityById.values() ){
					Opportunity objOpportunityCloned											= new Opportunity();
					objOpportunityCloned														= objOpportunity.clone( false, true, false, false );
					objOpportunityCloned.GNF_AccesoWizard__c									= false;
					objOpportunityCloned.Id														= mapOpportunityIdByTargetOpportunityId.get( objOpportunity.Id ).Id;
					objOpportunityCloned.GNF_IdClonado__c										= objOpportunity.Id;
					objOpportunityCloned.RecordTypeId											= blnIsClick ? ( mapOpportunityIdByTargetOpportunityId.get( objOpportunity.Id ).RecordTypeId.equals( GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ? GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id : GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Click_Opportunity' ).Id ) : GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id;
					objOpportunityCloned.GNF_IsClone__c											= blnIsClick;
					objOpportunityCloned.Name													= blnIsClick ? objOpportunity.Name + ' - ' + System.now().format( 'dd/MM/yyyy' ) : mapOpportunityIdByParentOpportunityId.get( objOpportunity.Id ).Name;
					objOpportunityCloned.Click_Asociado__c										= blnIsClick ? ( mapOpportunityIdByTargetOpportunityId.get( objOpportunity.Id ).RecordTypeId.equals( GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ? mapOpportunityIdByTargetOpportunityId.get( objOpportunity.Id ).Click_Asociado__c : null ) : objOpportunity.Id;
					//objOpportunityCloned.StageName												= blnIsClick ? GNF_ValoresPredeterminados__c.getInstance().GNF_ClickEstadoInicial__c : GNF_ValoresPredeterminados__c.getInstance().GNF_UnclickEstadoInicial__c;
					objOpportunityCloned.GNF_Enviado_Edelta__c									= false;
					objOpportunityCloned.Fecha_Cotizacion__c									= null;
					objOpportunityCloned.GNF_Aprobador1__c										= null;
					objOpportunityCloned.GNF_Aprobador2__c										= null;
					objOpportunityCloned.GNF_Aprobador3__c										= null;
					objOpportunityCloned.GNF_Aprobador4__c										= null;
					objOpportunityCloned.GNF_OfertasAprueba__c									= false;
					objOpportunityCloned.GNF_Ofertas_revisado__c								= false;
					objOpportunityCloned.GNF_Ventas_aprobado__c									= false;
					objOpportunityCloned.GNF_Ventas_aprobado_2__c								= false;
					objOpportunityCloned.GNF_Ventas_aprobado_3__c								= false;
					objOpportunityCloned.GNF_Ventas_aprobado_4__c								= false;
					objOpportunityCloned.GNF_VentasAprueba__c									= false;
					objOpportunityCloned.GNF_CondicionesCumplidas__c							= null;
					objOpportunityCloned.GNF_LogAsignacion__c									= null;
					objOpportunityCloned.GNF_RiesgosAprueba__c									= false;
					objOpportunityCloned.GNF_Riesgos_revisado__c								= false;
					objOpportunityCloned.GNF_Precio_CV_CLIENTE_medio_pond_c_KWh__c				= null;
					objOpportunityCloned.GNF_Precio_CVF2_cerrado_medio_pond_c_kWh__c			= null;
					objOpportunityCloned.GNF_Precio_CV_F2_medio_pond_c_KWh__c					= null;
					objOpportunityCloned.GNF_precio_CV_FRC_Final_medio_pond_MMBtu__c			= null;
					objOpportunityCloned.GNF_Precio_CV_FRC_Final_medio_pond_c_KWh__c			= null;
					objOpportunityCloned.GNF_Precio_CV_FRC_inicial_medio_pond__c				= null;
					objOpportunityCloned.GNF_PrecioCV_FRC_inicialMedioPond_MMBtu__c				= null;
					objOpportunityCloned.GNF_Media_pond_de_Brent_bbl__c							= null;
					objOpportunityCloned.Media_pond_de_Paridad_eur__c							= null;
					objOpportunityCloned.GNF_MediaPonderadaCVClientePrima__c					= null;
					objOpportunityCloned.GNF_RegistroBloqueado__c								= false;
					objOpportunityCloned.GNF_FechaLimiteValidez__c								= null;
					objOpportunityCloned.GNF_Trader__c											= null;
					objOpportunityCloned.GNF_F_Anexo__c											= null;
					objOpportunityCloned.GNF_Spread__c											= null;
					objOpportunityCloned.GNF_Spread_Cliente__c									= null;
					objOpportunityCloned.GNF_PDFEnviarEmail__c									= false;
					objOpportunityCloned.GNF_AnexoAdjuntado__c									= false;
					objOpportunityCloned.CloseDate												= System.today();
					objOpportunityCloned.GNF_CalculosFinalizados__c								= false;
					
					if( blnIsClick ){
						objOpportunityCloned.Fecha_Inicio__c									= objOpportunity.Fecha_Inicio__c > System.Today() ? objOpportunity.Fecha_Inicio__c : System.Today().addMonths( 1 ).toStartOfMonth();
					}else{
						objOpportunityCloned.Fecha_Inicio__c									= mapOpportunityIdByParentOpportunityId.get( objOpportunity.Id ).Fecha_Inicio__c > System.Today() ? mapOpportunityIdByParentOpportunityId.get( objOpportunity.Id ).Fecha_Inicio__c : System.Today().addMonths( 1 ).toStartOfMonth();
					}
					
					mapOpportunityByIdCloned.put( objOpportunity.Id, objOpportunityCloned );
				}
				
				mapOpportunitySuppliesByOppId													= new Map<Id, Oportunidad_Punto_de_Suministros__c>(
																									( List<Oportunidad_Punto_de_Suministros__c> ) Database.query(
																										GNF_OpportunityTriggerHandler_cls.getSOQL(
																											'Oportunidad_Punto_de_Suministros__c',
																											'WHERE Oportunidad__c IN: setOpportunityIds \n'
																										)
																									)
																								);
																							
				for( Oportunidad_Punto_de_Suministros__c objOpportunitySupply : mapOpportunitySuppliesByOppId.values() ){
					Oportunidad_Punto_de_Suministros__c objOpportunitySupplyCloned				= new Oportunidad_Punto_de_Suministros__c();
					objOpportunitySupplyCloned													= objOpportunitySupply.clone( false, true, false, false );
					objOpportunitySupplyCloned.GNF_IdClonado__c									= objOpportunitySupply.Oportunidad__c;
					mapOpportunitySuppliesByOppIdCloned.put( objOpportunitySupply.Id, objOpportunitySupplyCloned );
				}
				
				mapQuotesByOppId																= new Map<Id, Cotizacion_de_periodo__c>(
																									( List<Cotizacion_de_periodo__c> ) Database.query(
																										GNF_OpportunityTriggerHandler_cls.getSOQL(
																											'Cotizacion_de_periodo__c',
																											'WHERE Opportunity__c IN: setOpportunityIds \n'  
																										)
																									)
																								);
																							
				for( Cotizacion_de_periodo__c objQuote : mapQuotesByOppId.values() ){
					Cotizacion_de_periodo__c objQuoteCloned										= new Cotizacion_de_periodo__c();
					objQuoteCloned																= objQuote.clone( false, true, false, false );
					objQuoteCloned.GNF_IdClonado__c												= objQuote.Opportunity__c;
					objQuoteCloned.Brent_de_referencia__c										= null;
					objQuoteCloned.CV_Cliente__c												= null;
					objQuoteCloned.GNF_FormulaDestinoFinal__c									= null;
					objQuoteCloned.GNF_FormulaDestinoInicial__c									= null;
					objQuoteCloned.CV_FRC_Final__c												= null;
					objQuoteCloned.CV_FRC_Final_c_KWh__c										= null;
					objQuoteCloned.CV_FRC_Inicial__c											= null;
					objQuoteCloned.CV_FRC_c_kWh__c												= null;
					objQuoteCloned.Paridad_de_referencia_USD_EUR__c								= null;
					objQuoteCloned.GNF_Precio_TV_Final_Cliente_c_kWh__c							= null;
					objQuoteCloned.GNF_TVAnexo__c												= null;
					objQuoteCloned.GNF_TradeGroupCompra__c										= null;
					objQuoteCloned.GNF_TradeGroupVenta__c										= null;
					objQuoteCloned.GNF_CVClientePrima_Click__c									= null;
					
					mapQuotesByOppIdCloned.put( objQuote.Id, objQuoteCloned );
				}
				
				mapCClickByOpp																	= new Map<Id, Consumo_Click__c>(
																									( List<Consumo_Click__c> ) Database.query(
																										GNF_OpportunityTriggerHandler_cls.getSOQL(
																											'Consumo_Click__c',
																											'WHERE Oportunidad_Punto_de_Suministro__r.Oportunidad__c IN: setOpportunityIds \n' +
																											'AND Cotizacion_de_periodo__r.Opportunity__c IN: setOpportunityIds'
																										)
																									)
																								);
																							
				for( Consumo_Click__c objCClick : mapCClickByOpp.values() ){
					Consumo_Click__c objCClickCloned											= new Consumo_Click__c();
					objCClickCloned																= objCClick.clone( false, true, false, false );
					objCClickCloned.GNF_IdClonadoClickPS__c										= objCClick.Oportunidad_Punto_de_Suministro__c;
					objCClickCloned.GNF_IdClonadoCotizacion__c									= objCClick.Cotizacion_de_periodo__c;
					objCClickCloned.Q_comprometida_kWh__c										= blnIsClick ? objCClick.Q_comprometida_kWh__c : 0;
					objCClickCloned.GNF_Qa_mes_kWh__c											= blnIsClick ? objCClick.GNF_Qa_mes_kWh__c : 0;
					objCClickCloned.Trade_Group__c												= null;
					objCClickCloned.GNF_TradeGroupFinal__c										= null;
					objCClickCloned.GNF_TradeGroupVenta__c										= null;
					objCClickCloned.Consumo_Cubierto_anterior_kWh__c							= null;
					objCClickCloned.Consumo_cubierto_total_PS_kWh__c							= null;
					objCClickCloned.Consumo_disponible_periodo_kWh__c							= null;
					objCClickCloned.Consumo_M_ximo_Mensual__c									= null;
					
					mapCClickByOppIdCloned.put( objCClick.Id, objCClickCloned );
				}
				
				if( mapOpportunitySuppliesByOppIdCloned.size() < Integer.valueOf( GNF_ValoresPredeterminados__c.getInstance().GNF_LimitePSSincronos__c ) ){
					processClone( mapOpportunityByIdCloned, mapOpportunitySuppliesByOppIdCloned, mapQuotesByOppIdCloned, mapCClickByOppIdCloned );
				}else{
					deepCloneAsync( mapOpportunityByIdCloned, mapOpportunitySuppliesByOppIdCloned, mapQuotesByOppIdCloned, mapCClickByOppIdCloned );
				}
			}
		}
	}
	
	/**
	* @Method: 		deepCloneAsync
	* @param: 		String strMapOpportunityByIdCloned
	* @param: 		String strMapOpportunitySuppliesByOppIdCloned
	* @param: 		String strMapQuotesByOppIdCloned
	* @param: 		String strMapCClickByOppIdCloned
	* @Description:	Envia los registros a clonar por procesamiento asincrono.
	* @author 		Manuel Medina - 20112017
	*/
	public static void deepCloneAsync( Map<Id, Opportunity> mapOpportunityByIdCloned, Map<Id, Oportunidad_Punto_de_Suministros__c>  mapOpportunitySuppliesByOppIdCloned, Map<Id, Cotizacion_de_periodo__c>  mapQuotesByOppIdCloned, Map<Id, Consumo_Click__c>  mapCClickByOppIdCloned ){
		GNF_CloneClick_cls clsCloneClick													= new GNF_CloneClick_cls();
		clsCloneClick.mapOpportunityByIdCloned												= mapOpportunityByIdCloned; 
		clsCloneClick.mapOpportunitySuppliesByOppIdCloned									= mapOpportunitySuppliesByOppIdCloned;
		clsCloneClick.mapQuotesByOppIdCloned												= mapQuotesByOppIdCloned;
		clsCloneClick.mapCClickByOppIdCloned												= mapCClickByOppIdCloned;
		
		Id idJob																			= System.enqueueJob( clsCloneClick );
	}
	
	/**
	* @Method: 		processClone
	* @param: 		String strMapOpportunityByIdCloned
	* @param: 		String strMapOpportunitySuppliesByOppIdCloned
	* @param: 		String strMapQuotesByOppIdCloned
	* @param: 		String strMapCClickByOppIdCloned
	* @Description:	Procesar clon en base de datos.
	* @author 		Manuel Medina - 20112017
	*/
	public static void processClone( Map<Id, Opportunity> mapOpportunityByIdCloned, Map<Id, Oportunidad_Punto_de_Suministros__c>  mapOpportunitySuppliesByOppIdCloned, Map<Id, Cotizacion_de_periodo__c>  mapQuotesByOppIdCloned, Map<Id, Consumo_Click__c>  mapCClickByOppIdCloned ){
		Savepoint dbSavePoint																= Database.setSavepoint();
		
		try{
			for( Opportunity objOpportunity : mapOpportunityByIdCloned.values() ){
				objOpportunity.GNF_IdClonado__c												= null;
				objOpportunity.GNF_AccesoWizard__c											= true;
			}
			
			upsert mapOpportunityByIdCloned.values();
			
			for( Oportunidad_Punto_de_Suministros__c objOpportunitySupply : mapOpportunitySuppliesByOppIdCloned.values() ){
				objOpportunitySupply.Oportunidad__c											= mapOpportunityByIdCloned.get( objOpportunitySupply.GNF_IdClonado__c ).Id;
				objOpportunitySupply.GNF_IdClonado__c										= null;
			}
			
			insert mapOpportunitySuppliesByOppIdCloned.values();
			
			for( Cotizacion_de_periodo__c objQuote : mapQuotesByOppIdCloned.values() ){
				objQuote.Opportunity__c														= mapOpportunityByIdCloned.get( objQuote.GNF_IdClonado__c ).Id;
				objQuote.GNF_IdClonado__c													= null;
			}
			
			insert mapQuotesByOppIdCloned.values();
			
			for( Consumo_Click__c objCClick : mapCClickByOppIdCloned.values() ){
				objCClick.Oportunidad_Punto_de_Suministro__c								= mapOpportunitySuppliesByOppIdCloned.get( objCClick.GNF_IdClonadoClickPS__c ).Id;
				objCClick.Cotizacion_de_periodo__c											= mapQuotesByOppIdCloned.get( objCClick.GNF_IdClonadoCotizacion__c ).Id;
				objCClick.GNF_IdClonadoClickPS__c											= null;
				objCClick.GNF_IdClonadoCotizacion__c										= null;
			}
			
			insert mapCClickByOppIdCloned.values();
			
		}catch( Exception e ){
			Database.rollback( dbSavePoint );
			
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
		}
	}
	
	/**
	* @Method: 		AdminExceptions
	* @param: 		N/A
	* @Description:	Desencadenar excepciones personalizadas.
	* @author 		Manuel Medina - 10082017
	*/
	public class AdminException extends Exception{}
}