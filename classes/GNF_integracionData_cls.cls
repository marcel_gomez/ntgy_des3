/**
* VASS
* @author           Juan Cardona juan.cardona@vass.es
* Project:          Gas Natural Fenosa
* Description:      Desencadenador sobre el objeto Concepto
*                   
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-07-12      Juan Cardona (JSC)      Definicion inicial del trigger.
*********************************************************************************************************/

public class GNF_integracionData_cls 
{
    
    public static final String EC_COMMODITY_SPLIT_SEPARATOR = '&&'; 


    /*
    */
    public static void conversionValoresConceptos(  list<Concepto__c> list_inOldConceptos, list<Concepto__c> list_inNewConceptos )
    {
        
        map< string, set<string > >     map_conConceptosXpais;  
        map< string, Concepto__c >      map_oldConceptosXId;        
        map< string, Concepto__c >      map_newConceptosXId;
        map< string, string >			map_paisXcontrato;

        set<Id>                         set_contratos;
        set<Id>                         set_contratosFicticios;
          
        Concepto__c                     obj_oldConcepto;
        
        Double                          db_factorConversion;
        Integer                         i; 
 
 
        system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.conversionValoresConceptos>> list_inOldConceptos: ' + list_inOldConceptos + ' list_inNewConceptos: ' + list_inNewConceptos );
        
        if( list_inNewConceptos==null || list_inNewConceptos.isEmpty() )
            return;
        
        map_conConceptosXpais   = new map< string, set<string > >();
        map_oldConceptosXId     = new map< string, Concepto__c >();     
        map_newConceptosXId     = new map< string, Concepto__c >();
        map_paisXcontrato		= new map< string, string >();
        set_contratos           = new set<Id>();
        set_contratosFicticios  = new set<Id>();
                
        i                       = 0;    
        
        db_factorConversion     =  GNF_ValoresPredeterminados__c.getInstance().GNF_factorConversionNumericoNIITtoSFDC__c;       
            
        for( GNF_AllowedConceptoTypes__c obj_confConcept : GNF_AllowedConceptoTypes__c.getall().values() )
            if( obj_confConcept.Name!=null && obj_confConcept.GNF_codigoConceptosDescPrcnt__c!=null )
                map_conConceptosXpais.put( obj_confConcept.Name, new Set<String>( obj_confConcept.GNF_codigoConceptosDescPrcnt__c.split( EC_COMMODITY_SPLIT_SEPARATOR ) ) );

        system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.conversionValoresConceptos>> db_factorConversion: ' + db_factorConversion + ' map_conConceptosXpais: ' + map_conConceptosXpais );

        if( list_inOldConceptos!=null )
            for( Concepto__c obj_concep : list_inOldConceptos )
                if( obj_concep.id!=null )
                    map_oldConceptosXId.put( obj_concep.id, obj_concep );       

        if( list_inNewConceptos!=null )
            for( Concepto__c obj_concep : list_inNewConceptos )
            {
                if( obj_concep.id!=null )
                    map_newConceptosXId.put( obj_concep.id, obj_concep );   
                else{
                    map_newConceptosXId.put( '' + i++, obj_concep ); 

                }       
        
                if( obj_concep.Contrato__c!=null )
                    set_contratos.add( obj_concep.Contrato__c );
                    
                                        
            }

        system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.conversionValoresConceptos>> set_contratos: ' + set_contratos ); 

        for( Contrato__c obj_contrato : [ Select Id, Ficticio__c, Cliente__c, Cliente__r.Pais__c From Contrato__c Where Id IN : set_contratos ] )
        {
        	if( obj_contrato.Ficticio__c )
            	set_contratosFicticios.add( obj_contrato.Id );  
            
            if( obj_contrato.Cliente__c!=null && obj_contrato.Cliente__r.Pais__c!=null )	
				map_paisXcontrato.put( obj_contrato.id, obj_contrato.Cliente__r.Pais__c );     
        }   
           
        system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.conversionValoresConceptos>> set_contratosFicticios: ' + set_contratosFicticios + ' map_paisXcontrato: ' + map_paisXcontrato );   
                    
        for( Concepto__c obj_newConcepto : map_newConceptosXId.values() )
        {
            if( obj_newConcepto.id!=null && map_oldConceptosXId.containsKey( obj_newConcepto.id ) )
                obj_oldConcepto = map_oldConceptosXId.get( obj_newConcepto.id );   
            else
                obj_oldConcepto = null;
                
            system.debug( '\n <<GNF_ConceptoTriggerHandler_cls.conversionValoresConceptos>> obj_oldConcepto: ' + obj_oldConcepto );             
            
            if(  db_factorConversion!=null && /*obj_newConcepto.Pais_Cliente__c!=null &&*/ map_paisXcontrato.containsKey( obj_newConcepto.Contrato__c ) && map_conConceptosXpais.containsKey( map_paisXcontrato.get( obj_newConcepto.Contrato__c ) ) &&  !map_conConceptosXpais.get( map_paisXcontrato.get( obj_newConcepto.Contrato__c ) ).contains( obj_newConcepto.Name ) && !set_contratosFicticios.contains( obj_newConcepto.Contrato__c ) )
            {           	         	
                if( obj_newConcepto.Valor_de_F__c!=null && obj_newConcepto.Valor_de_F__c>0 && ( obj_oldConcepto==null || ( obj_oldConcepto!=null && obj_oldConcepto.Valor_de_F__c!=obj_newConcepto.Valor_de_F__c ) /*|| !obj_newConcepto.GNF_controlEjecucionConversion__c*/ ) )
                    obj_newConcepto.Valor_de_F__c   = obj_newConcepto.Valor_de_F__c*db_factorConversion;
                    
                if( obj_newConcepto.GNF_ValorK__c!=null && obj_newConcepto.GNF_ValorK__c>0 && ( obj_oldConcepto==null || ( obj_oldConcepto!=null && obj_oldConcepto.GNF_ValorK__c!=obj_newConcepto.GNF_ValorK__c ) /*|| !obj_newConcepto.GNF_controlEjecucionConversion__c*/ ) )                      
                    obj_newConcepto.GNF_ValorK__c   = obj_newConcepto.GNF_ValorK__c*db_factorConversion;
                
                if( obj_newConcepto.Precio__c!=null && obj_newConcepto.Precio__c>0 && ( obj_oldConcepto==null || ( obj_oldConcepto!=null && obj_oldConcepto.Precio__c!=obj_newConcepto.Precio__c ) /*|| !obj_newConcepto.GNF_controlEjecucionConversion__c*/ ) )           
                    obj_newConcepto.Precio__c       =  obj_newConcepto.Precio__c*db_factorConversion;
                    
                    
                /*if( obj_oldConcepto.Valor_de_F__c!=null && obj_oldConcepto.Valor_de_F__c>0 && obj_newConcepto.GNF_controlEjecucionConversion__c )
                    obj_newConcepto.Valor_de_F__c   = obj_oldConcepto.Valor_de_F__c*db_factorConversion;
                    
                if( obj_oldConcepto.GNF_ValorK__c!=null && obj_oldConcepto.GNF_ValorK__c>0 && obj_newConcepto.GNF_controlEjecucionConversion__c )                      
                    obj_newConcepto.GNF_ValorK__c   = obj_oldConcepto.GNF_ValorK__c*db_factorConversion;
                
                if( obj_oldConcepto.Precio__c!=null && obj_oldConcepto.Precio__c>0 && obj_newConcepto.GNF_controlEjecucionConversion__c )           
                    obj_newConcepto.Precio__c       =  obj_oldConcepto.Precio__c*db_factorConversion;  */             
                    
                /*if( obj_newConcepto.VASS_ejecutarConversion__c )
					obj_newConcepto.VASS_ejecutarConversion__c = false;*/ 
					
				obj_newConcepto.GNF_controlEjecucionConversion__c = true;       
            }
        }
    
    }//ends conversionValoresConceptos  
}//ends GNF_integracionData_cls