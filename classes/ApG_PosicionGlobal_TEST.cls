@isTest
private class ApG_PosicionGlobal_TEST {
    
    @testSetup
	private static void testSetup(){
		map<string, id> profileMap = ApG_TestUtil.getProfileMap(new set<string>{'Custom Standar Gestores'}); 
		map<string, id> roleMap = ApG_TestUtil.getRoleMap(new set<string>{'MAYORISTAS', 'DV_IBERIA_IBERIA'});
		User usr0, usr1, usr2;
		system.runAs(new User(Id = userInfo.getUserId())){
			usr0 = ApG_TestUtil.getUser('fName0', 'lName0', profileMap.get('Custom Standar Gestores'), roleMap.get('MAYORISTAS'));
			usr1 = ApG_TestUtil.getUser('fName1', 'lName1', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
			usr2 = ApG_TestUtil.getUser('fName2', 'lName2', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
			usr0.ID_Gestor__c = 'Desesperado0';
			usr1.ID_Gestor__c = 'Desesperado1';
			usr2.ID_Gestor__c = 'Desesperado2';
			list<User> userList = new list<User>{usr0, usr1, usr2};
			insert userList;
		}
		Empleado__c emp0 = ApG_TestUtil.getEmpleado('Desesperado0','Desesperado0','','theHigh','España');
		Empleado__c emp1 = ApG_TestUtil.getEmpleado('Desesperado1','Desesperado1','Desesperado0','theMiddle','España');
		Empleado__c emp2 = ApG_TestUtil.getEmpleado('Desesperado2','Desesperado2','Desesperado1','theLow','España');
		list<Empleado__c> empList = new list<Empleado__c>{emp0, emp1, emp2};
		insert empList;

		Account acc0;
		acc0 = ApG_TestUtil.getAccount('acc0', 'España', 'acc0');
		acc0.Gestor__c = emp2.id;
		insert acc0;
		SRs__c srs0 = ApG_TestUtil.getSRs(emp2.id, acc0.id);
		SRs__c srs1 = ApG_TestUtil.getSRs(emp2.id, acc0.id);
		srs0.name = '00';
		srs0.Estado__c = 'Cerrada';
		srs1.name = '01';
		srs1.Estado__c = 'Cerrada';
		list<SRs__c> SRsList = new list<SRs__c>{srs0, srs1};
		insert SRsList;
	}
	
	@isTest
	private static void testForGetGlobalPositionPage(){
		ApG_PosicionGlobalController pageInstance = new ApG_PosicionGlobalController();
		system.assert(pageInstance.userId == userInfo.getUserId(), '	Page instantiation failed	' + pageInstance.userId);

		list<ApG_GlobalPositionGroup__mdt> retList = ApG_PosicionGlobalController.getGlobalPositionPage('status');
		system.assert(!retList.isEmpty(), '	no custom metadata types have been retrieved '+retList);

		User usr0 = [SELECT Id FROM User WHERE firstName = 'fName0'];
		system.runAs(usr0){
			ApG_GlobalPositionItem__mdt sRsItemStatus = [SELECT Id FROM ApG_GlobalPositionItem__mdt WHERE DeveloperName =: 'ApG_Status_SrsActivas'];
			list<map<string, string>> statusSRsList = ApG_PosicionGlobalController.getTreeContent(sRsItemStatus.id, '', '');
			system.assert(statusSRsList[0].get('lvl0')=='Cerrada' && statusSRsList[1].get('lvl0')=='Cerrada', statusSRsList);

			ApG_GlobalPositionItem__mdt sRsItemHierarchy = [SELECT Id FROM ApG_GlobalPositionItem__mdt WHERE DeveloperName =: 'ApG_Jer_SrsActivas'];
			list<map<string, string>> hierarchySRsList = ApG_PosicionGlobalController.getTreeContent(sRsItemHierarchy.id, '', '');
			system.assert(hierarchySRsList[0].get('lvl0')=='theMiddle theMiddle' && hierarchySRsList[1].get('lvl0')=='theMiddle theMiddle' , hierarchySRsList);
		}
	}

	
	@isTest
	private static void testForGetReport(){
		list<map<string, string>> retList = new list<map<string, string>>();
		ApG_GlobalPositionItem__mdt sRsItemStatus = [SELECT Id FROM ApG_GlobalPositionItem__mdt WHERE DeveloperName =: 'ApG_Status_SrsActivas'];
		map<id, SRs__c> srsMap = new map<id, SRs__c>([SELECT Id FROM SRs__c]);
		list<id> idList = new list<id>();
		idList.addAll(srsMap.keySet());
		list<map<string, string>> retListMap = ApG_PosicionGlobalController.getReport(idList, sRsItemStatus.id);
		system.assert(retListMap.size()==3, '	Report has not been properly retrieved	' + retListMap);
	}

	@isTest
	private static void testForCSVgeneration(){
		string retString = ApG_PosicionGlobalController.createTableChuncks('cacho0', 'idTabla0', false, 'tablaName');
		list<PosicionGlobalTempFile__c> theCachoList = [SELECT Id, TableChunck__c FROM PosicionGlobalTempFile__c];
		system.assert(theCachoList[0].TableChunck__c =='cacho0' && retString == 'NOT YET', '	Chunck not created	 '+theCachoList);
		
		retString = ApG_PosicionGlobalController.createTableChuncks('cacho1', 'idTabla0', true, 'tablaName');
		theCachoList = [SELECT Id, TableChunck__c FROM PosicionGlobalTempFile__c];
		ContentVersion theContentVersion = [SELECT Id FROM ContentVersion WHERE Title =: 'tablaName.csv'];
		system.assert(theCachoList.size() ==0 && retString == theContentVersion.id, '	Chuncks have been deleted and a contentVersion have been created	 '+retString);

	}

	@isTest
	private static void testForFailCSVgeneration(){
		string retString;
		try{
			retString = ApG_PosicionGlobalController.createTableChuncks('cacho0', null, false, 'tableName');
		}catch(exception e){}
		system.assert(retString.contains(label.ApG_CSVexportFailMessage), '	Error message is not the expected	'+retString);
	}
	

	@isTest
	private static void testdashboard(){

		string strRetornDashboard = ApG_PosicionGlobalController.getdashboard('Vision_Comercial_por_Clientes');

		
	}

}