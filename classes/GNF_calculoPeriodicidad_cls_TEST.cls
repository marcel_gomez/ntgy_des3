/**
 * Created by luis.igualada on 25/09/2017.
 */
@isTest
private class GNF_calculoPeriodicidad_cls_TEST {

	static testmethod void generarPerdiodicidad() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		list<Concepto__c> lstconceptos= new list<Concepto__c>();
		list<Opportunity> listaopps = new list<Opportunity>();
		list<Condiciones_de_Cobertura__c> listacondcobertura = new list<Condiciones_de_Cobertura__c>();
	    list<string> listaidconceptos = new list<string>();
		list<Contrato__c> listacontratos = new list<Contrato__c>();
		
		listaopps = [SELECT Id from Opportunity where Tipo_Producto__c != null LIMIT 1];
		
		for (Opportunity opp : listaopps){
			opp.Fecha_Inicio__c = Date.valueOf('2018-10-01');
			opp.Fecha_Fin__c = Date.valueOf('2019-10-25');
		}
		
		update listaopps;
		
		listacondcobertura = [SELECT Id from Condiciones_de_Cobertura__c LIMIT 1];
		
		for (Condiciones_de_Cobertura__c condcobertura : listacondcobertura){
			condcobertura.Fecha_de_inicio__c = Date.valueOf('2018-10-01');
			condcobertura.Fecha_de_fin__c = Date.valueOf('2019-10-25');
		}
		
	    update listacondcobertura;
	    	    
	    listacontratos=[SELECT Id from Contrato__c LIMIT 2];
		
		Set<Id> setContractId = new Set<Id>();
		
		for(Contrato__c contrato :listacontratos){
			contrato.Fecha_de_Inicio__c= Date.valueOf('2018-10-01');
			contrato.Fecha_de_Fin__c =  Date.valueOf('2019-10-25');
			contrato.Ficticio__c = false;
			setContractId.add( contrato.Id );
		}
		
		update listacontratos;
	    
	    lstconceptos = [SELECT Id from Concepto__c WHERE Contrato__c IN: setContractId LIMIT 2];
	    
		for ( Concepto__c concepto : lstconceptos){
			listaidconceptos.add(concepto.Id);
			concepto.Fecha_Inicio__c = Date.valueOf('2018-10-01');
			concepto.Fecha_Fin__c = Date.valueOf('2019-10-25');
		}
		
		update lstconceptos;
		
		Test.startTest();
		
		GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad(listacondcobertura[0].Id,listaidconceptos,listaopps[0].Id);
		
		Test.stopTest();
	}
}