/*
VASS 01062018 Apex class to support test classes.
Contains a bundle of methods, where each one returns a single record ready to be inserted or a collection of a setup object
*/

public class ApG_TestUtil {
	
	/*Returns a ready to insert account*/
	public static Account getAccount(string Name, string Country, string documentNumber){
		Account retAcc = new Account(
			Name = Name,
			Pais__c = Country,
			NIF_CIF__c = documentNumber
		);
		return retAcc;
	}
	
	/*Returns a ready to insert suministro record*/
	public static Suministros__c getSuministro(string accId, string deltaManager, string nis){
		Suministros__c retSum = new Suministros__c(
			Cliente__c = accId,
			Gestor__c = deltaManager,
			Name = nis
		);
		return retSum;
	}
	
	/*Returns a ready to insert user*/
	public static User getUser(string fName, string lName, string profId, string roleId){
		User retUsr = new User(
			firstName = fName,
			lastName = lName,
			email = fName+lName+'@gasNatural.com',
			userName = fName+lName+'@gasNatural.com',
			CommunityNickname = fName,
			alias = fName,
			ProfileId = profId,
			UserRoleId = roleId,
			TimeZoneSidKey = 'Europe/Paris', 
			LocaleSidKey ='es_ES', 
			EmailEncodingKey = 'ISO-8859-1', 
			LanguageLocaleKey = 'es'
		);
		return retUsr;
	}
	
	/*Returns a map of profile name, profile id with the profiles which names has been passed as attribute*/
	public static map<string, id> getProfileMap(set<string> profileNames){
		list<Profile> profList = [SELECT Id, Name FROM Profile WHERE Name IN: profileNames];
		map<string, id> retMap = new map<string, id>();
		for(Profile i : profList){
			retMap.put(i.Name, i.id);
		}
		return retMap;
	}
	
	/*Returns a map of role developername, role id with the roles which names has been received as attribute*/
	public static map<string, id> getRoleMap(set<string> roleNames){
		list<UserRole> roleList = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName IN: roleNames];
		map<string, id> retMap = new map<string, id>();
		for(UserRole i : roleList){
			retMap.put(i.DeveloperName, i.id);
		}
		return retMap;
	}

	/*Returns an employee*/
	public static Empleado__c getEmpleado(string employeeCode, string employeeId, string managerId, string firstName, string country){
		Empleado__c theEmployee = new Empleado__c(
			Codigo_Empleado__c = employeeCode,
			ID_Empleado__c = employeeId,
			ID_Manager__c = managerId,
			FirstName__c = firstName,
			LastName__c = firstName,
			Name = firstName + ' ' + firstName,
			Pais__c = country
		);
		return theEmployee;

	}

	/*Retunr an SR*/
	public static SRs__c getSRs(string employeeId, string accountId){
		SRs__c theSR = new SRs__c(
			Cliente__c = accountId,
			Gestor__c = employeeId
		);
		return theSR;
	}
    
}