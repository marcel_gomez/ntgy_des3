global with sharing class AG_Batch_UpdateOnwers  implements Database.Batchable<sObject>, Database.stateful {   
    private final Id newOwnerId;
    private final Id gestorId;
    private final String objectName;
    private Integer totalObjectsUpdated;
    global AG_Batch_UpdateOnwers(Id pNewOwnerId, Id pGestorId, String pObjectName) {
        newOwnerId = pNewOwnerId;
        gestorId = pGestorId;
        if (pObjectName == null) {
            objectName = getNextObjectToUpdate(pObjectName);
        }
        else {
            objectName = pObjectName;   
        }
        
        totalObjectsUpdated = 0;
        System.debug('AG_Batch_UpdateOnwers: newOwnerId = ' + newOwnerId + ',  gestorId = ' + gestorId + ', object name = ' +  objectName);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select OwnerId, Name From ' + objectName + ' Where Gestor__c = \'' + gestorId + '\''; 
        System.debug('AG_Batch_UpdateOnwers.start: Query = ' + query);

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject obj : scope) {
            obj.put('OwnerId', newOwnerId);
        }
        totalObjectsUpdated += scope.size();
        update scope;
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('AG_Batch_UpdateOnwers.finish. Total ' + objectName + ' = ' + totalObjectsUpdated);
        String nextObject = getNextObjectToUpdate(objectName);
        System.debug('AG_Batch_UpdateOnwers.finish. Next object = ' + nextObject);
        if (nextObject == null) {
            // send an email to user
            String body = label.ApG_SignUpSuccessNewUserBody;
            String subject = label.ApG_SignUpSuccessNewUserSubject;
            sendMail(newOwnerId, subject, body);

            System.debug('AG_Batch_UpdateOnwers.finish. Next object is null, end of the proccess. An email has been sent to user');
        }
        else {
            AG_Batch_UpdateOnwers processUpdateAccountOwner = new AG_Batch_UpdateOnwers(newOwnerId, gestorId, nextObject);
            ID batchprocessid = Database.executeBatch(processUpdateAccountOwner);
        }
        
    }

    private String getNextObjectToUpdate(String currentObjectName) {
        if (currentObjectName == null || currentObjectName == '') {
            return 'Account';
        }
        else if (currentObjectName == 'Account') {
            return 'Actividad_Comercial__c';
        }
        else if (currentObjectName == 'Actividad_Comercial__c') {
            return 'Ofertas__c';
        }
        else if (currentObjectName == 'Ofertas__c') {
            return 'SSs__c';
        }
        else if (currentObjectName == 'SSs__c') {
            return 'SRs__c';
        }
        else if (currentObjectName == 'SRs__c') {
            return 'Suministros__c';
        }
        else if (currentObjectName == 'Suministros__c') {
            return null;
        }
        return null;
    }

    public static void sendMail(Id snewOwnerId, String subject, String body) {
        // Create an email message object
        System.debug('AG_Batch_UpdateOnwers. SendMail: The new owner is: ' + snewOwnerId);
        List<User> sUsers = [Select Email From User Where Id = :snewOwnerId Limit 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {sUsers.get(0).Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        Messaging.SendEmailResult[] results = Messaging.sendEmail(
                                 new Messaging.SingleEmailMessage[] { mail });
        

        for (Messaging.SendEmailResult res : results) {
            if (res.isSuccess()) {
                System.debug('AG_Batch_UpdateOnwers. SendMail: Email sent successfully');
            }
            else {
                System.debug('AG_Batch_UpdateOnwers. SendMail: The following errors occurred: ' + res.getErrors());                 
            }
        }
    }
    
}