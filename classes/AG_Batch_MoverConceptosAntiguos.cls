global class AG_Batch_MoverConceptosAntiguos implements Database.Batchable<sObject> {

	Date filterDate;

	final static private String[] CAMPOS_CONCEPTO = new List<String>
		{ 	'ID_Concepto__c',  'Name', 		  'Descripcion__c', 'Fecha_Fin__c', 
			'Fecha_Inicio__c', 'Contrato__c', 'Tipo_Tarifa__c', 'Precio__c'};

	private String query;
	
	/*
	 * Proceso de mover todos los conceptos anteriores a una fecha a Conceptos_Historico__c
	 * @param diasCaducidad -  Numero de días anteriores a fecha actual para establecer  
	 * la fecha máxima de los conceptos a mover a Concepto_Historico__v:
	 * 			Concepto__c.fecha_fin__c <= Today() - diasCaducidad
	*/
	global AG_Batch_MoverConceptosAntiguos(String camposConcepto, Integer diasCaducidad) {

		this.filterDate = Date.Today().addDays(-diasCaducidad);
		query = 'SELECT ' + camposConcepto + 
				' FROM Concepto__c ' +
				//' WHERE Fecha_Fin__c <= :filterDate';
				' WHERE Fecha_Fin__c < LAST_N_DAYS:' + diasCaducidad;

		System.debug('Query:' + query );
	}


	/*
	* Proceso de mover todos los conceptos anteriores a los dos últimos años a Conceptos_Historico__c
	*/
	global AG_Batch_MoverConceptosAntiguos() {

		this(String.join(CAMPOS_CONCEPTO,','), 365*2);
	}

	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		List<Concepto_Historico__c> newListConceptoHist = new List<Concepto_Historico__c>();
   		for(sObject concepto : scope) {
   			Concepto_Historico__c newConceptoHist = (Concepto_Historico__c)copyObject(concepto, new Concepto_Historico__c());
   			newListConceptoHist.add(newConceptoHist);
   		}
   		insert newListConceptoHist;
   		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private sObject copyObject(sObject source, sObject target) {

		Map<String, Object> sourceFields = source.getPopulatedFieldsAsMap();

		for (String fName : sourceFields.keySet()) {
			if (!'Id'.equalsIgnoreCase(fName)) {
				Object value = sourceFields.get(fName);
				target.put(fName, value);
			}
		}

		return target;
	}


}