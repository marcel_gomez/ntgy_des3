public class AG_LogicSuministro {


    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicSuministro() {}

    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<Suministros__c>}
     */
    public void assignOwnerFromEmpleado(final List<Suministros__c> newList, final Map<Id, Suministros__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: start' + newList.size());

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();

        if (oldMap==null) oldMap = new Map<Id, Suministros__c>(); //just to avoid null checks

        // Iterate over suministros and find and assign owner based on its empleado
        for(Suministros__c theSuministro : newList) {

          if(theSuministro.Gestor_External_Key__c <> null) {

            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theSuministro.Id), theSuministro)) {
              
              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theSuministro.managers__c = managersEmpleados.get(theSuministro.Gestor_External_Key__c);

              String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theSuministro.Gestor_External_Key__c,empleadoTable,userTable);
              if(assignedOwner <> null) {
                theSuministro.OwnerId = assignedOwner;
              } else {
                //TODO: What todo in if not find a valid user?
                final String errMsg = '#ERROR# No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_supply=' + theSuministro.ID_Supply__c + ' - gestor_external_key='+ theSuministro.Gestor_External_Key__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug(loggingLevel.Error, errMsg);
                //throw new SuministroLogicException(errMsg);
              }
            }
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '#ERROR# No esta informada el gestor para el suministro :' + theSuministro.ID_Supply__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new SuministroLogicException('No esta informada el gestor para el suministro :' + theSuministro.ID_Supply__c);
          }
        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }
    
    /**
     * Add sharing roles to Suministros__c and Account to allow visibility to role hierachy.
     */
    public void setCustomSharing(final List<Suministros__c> newList, final Map<Id, Suministros__c> newMap,
                                 final List<Suministros__c> oldList, final Map<Id, Suministros__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ SetCustomSharing: start');

        //List of suministro sharing records to be inserted
        List<Suministros__Share> suministroSharingList = new List<Suministros__Share>();

        //List of account sharing records to be inserted
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, Suministros__c>(); //just to avoid null checks

        //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();

        //Iterate over collection
        for(Suministros__c theSuministro : newList) {

            Id supplyId = theSuministro.Id;
            Suministros__c oldSupply = oldMap.get(supplyId);
            Suministros__c newSupply = theSuministro;

            //Se comenta debug(Victor Velandia (VV))
            /*System.debug('@@@@ ID suministro:' + supplyId);
            System.debug('@@@@ Old suministro:' + oldSupply);
            System.debug('@@@@ New suministro:' + newSupply);*/

            //Get only new records or records whose cliente has changed or owner has changed
            if (hasChangedCliente(oldSupply, newSupply)) {

              if (oldSupply!=null)  //si es insert no hay previos que borrar
                Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(oldSupply.Id);

              //get the role of the user assigned to the record account
              Id ownerSuministro = theSuministro.ownerId;
              Id ownerRelatedAccount = theSuministro.Gestor_Cliente__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug('@@@@ Supply gestion_cliente__C:' + ownerRelatedAccount);

              //Si owners de Account y suministro son el mismo, no hacen falta Sharings
              if(ownerSuministro!=ownerRelatedAccount) {  
                List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount, allUsers, allRoles, allGroups);
                for (Id grpOrUsrId : gruposRoleSuperiores) {
                  Suministros__Share clienteAbobeRoleSharing = createSuministroSharedRecord(theSuministro, grpOrUsrId);
                  suministroSharingList.add(clienteAbobeRoleSharing);
                }
              }
            }

            //Si es un nuevo Suministro, calculamos los permisops para el Account en el trigger,
            //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.

            //El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
            if (oldSupply!=null && hasChangedOwner(oldSupply, newSupply)) {   

              accountToRecalculate.add(theSuministro.Cliente__c);

            //Se trata de un alta de suministro --> calculamos permisos a dar al Account
            } else if (oldSupply==null) { 

                if(!String.isBlank(theSuministro.Cliente__c)) {   //No hay nada que hacer, no hay cliente

                  Id accountOwnerId = theSuministro.Gestor_Cliente__c;
                  Id supplyOwnerId = theSuministro.OwnerId;

                  //Si owner del cliente es el mismo que el del objeto, no es necesario dar más permisos
                  if (accountOwnerId!=supplyOwnerId) {

                    //Se comenta debug(Victor Velandia (VV))
                    /*System.debug('@@@Account Propietrario suministro:' + supplyOwnerId);
                    System.debug('@@@Account Propietrario cuenta:' + accountOwnerId);
                    System.debug('@@@Account Cliente:' + theSuministro.cliente__r.name);*/

                    List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(supplyOwnerId, allUsers, allRoles, allGroups);
                    for (Id grpOrUsrId : grpOrUsrIdToShare) {
                      AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(theSuministro.Cliente__c, grpOrUsrId);
                      accountSharingList.add(accountSharing);
                    }

                    //Se comenta debug(Victor Velandia (VV))
                    //System.debug('###Account Account Share:' + accountSharingList);
                  }
                }
              }
        } //for

        if (!accountToRecalculate.isEmpty()) {
          AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!suministroSharingList.isEmpty())
          upsert suministroSharingList;


        if(!accountSharingList.isEmpty())
          upsert accountSharingList;

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS

   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldSuministro is null.
    */
    @testVisible
    private boolean hasChangedGestor(Suministros__c oldSuministro, Suministros__c newSuministro) {

      if (oldSuministro==null) return true;   //new object
      return (oldSuministro.Gestor__c != newSuministro.Gestor__c);
    }      

    /*
     * @return true if the field value "Cliente__c" has changed or oldSuministro is null.
    */
    @testVisible
    private boolean hasChangedCliente(Suministros__c oldSuministro, Suministros__c newSuministro) {

      if (oldSuministro==null) return true;   //new object
      return (oldSuministro.cliente__c != newSuministro.Cliente__c);
    }      

    /*
    * @return true if the owner has changed or oldSuministro is null.
    */
    @testVisible
    private boolean hasChangedOwner(Suministros__c oldSuministro, Suministros__c newSuministro) {

      if (oldSuministro==null) return true;   //new object
      return (oldSuministro.ownerId != newSuministro.OwnerId);
    }      

    /**
     * Crea  Suministro__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible     
    private Suministros__Share createSuministroSharedRecord(final Suministros__c suministro, Id userOrGroup) {
        return new Suministros__Share(
                  ParentId = suministro.Id,
                  UserOrGroupId = userOrGroup, //suministro.Gestor_Cliente__c,
                  AccessLevel = SHARE_MODE_READ,
                  RowCause = Schema.Suministros__Share.RowCause.Apex_Sharing__c);
    }


    /*
    * Remove all the sharing added from Apex.
    */
    @testVisible
    private Integer deleteSuministroShares(Id suministrosId) {

      List<Suministros__Share> shareList = [SELECT Id FROM Suministros__Share WHERE ParentId = :suministrosId AND RowCause = :Schema.Suministros__Share.RowCause.Apex_Sharing__c];
      Integer count = shareList.size();
      if (count>0)
        delete shareList;

      return count;
    }
   

    //-- CUSTOM EXCEPTION
    public class SuministroLogicException extends Exception {}


    /* 
     * Instancia shares para suministros a partir de lista de grupos 
    public List<Suministros__share> createSuministrosShareList(Suministros__c theSuministro, Id[] gruposRoleSuperiores) {

      List<Suministros__Share> suministroSharingList = new List<Suministros__Share>();

      System.debug('@@@@ Result1' + gruposRoleSuperiores );
      for (Id grpOrUsrId : gruposRoleSuperiores) {
        Suministros__Share clienteAbobeRoleSharing = createSuministroSharedRecord(theSuministro, grpOrUsrId);
        suministroSharingList.add(clienteAbobeRoleSharing);
      }
      return suministroSharingList;
    }
     */

    /*
     * Borra los permisos de los objetos (suministros) asociados al Account informado por parametro
    public void removeSharedSuministrosByAccount(Account theAccount) {
      String ApexSharing = 'Apex_Sharing';
      Id accountId = theAccount.Id;
      for (List<Suministros__share> sharesToDel : [select id from Suministros__share WHERE RowCause= :ApexSharing And ParentId IN (SELECT ID FROM Suministros__c WHERE Cliente__c = :accountId)])
      {
        delete sharesToDel;
      }
    }
     */


}