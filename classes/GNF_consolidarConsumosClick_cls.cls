/**
* VASS
* @author 			Juan Cardona juan.cardona@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase --
*					
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-24		Juan Cardona (JSC)		Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_consolidarConsumosClick_cls 
{
    
	public map<string, Integer>		map_meses 					= new map<string, Integer>{	'Enero'=>01, 'Febrero'=>02, 'Marzo'=>03, 
																				'Abril'=>04, 'Mayo'=>05, 'Junio'=>06,
																				'Julio'=>07, 'Agosto'=>08, 'Septiembre'=>09, 
																				'Octubre'=>10, 'Noviembre'=>11, 'Diciembre'=>12};   

	public static map<Integer, string>		map_mesesNombre 	= new map<Integer, string>{	01=>'Enero', 02=>'Febrero', 03=>'Marzo', 
																				04=>'Abril', 05=>'Mayo', 06=>'Junio',
																				07=>'Julio', 08=>'Agosto', 09=>'Septiembre', 
																				10=>'Octubre', 11=>'Noviembre', 12=>'Diciembre'}; 

	public static map<string, Integer>	map_periodos 				= new map<string, Integer>{ 'M'=> 1, 'Q'=>3, 'S'=>6, 'C'=>12 };

	private map<Id, Opportunity> 													map_opp;
	private map<string, list<Oportunidad_Punto_de_Suministros__c>>					map_pSuminitroXopp;
	private map<String, map<Integer, map<Integer, Consumo_Click__c>>>				map_cClickXpSuministro;
	private map<String, list<Cotizacion_de_periodo__c>>								map_cClickXcPeriodo;
	private map<String, map<Integer, list<Consumo_Click__c>>>						map_cClickAnteriorXsSuministro;	
	private map<String, map<Integer, map<Integer, list<Consumo_Click__c>>>>			map_cClickAnteriorXanoXmesXsSuministro;
	private map<String, map<Integer, map<Integer, Consumo_Click__c>>>				map_cClickUnclickXsuministro;
	
	private Integer																	int_uninadesDcm;

	public GNF_consolidarConsumosClick_cls()
	{
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> No inputs' );		
		
	}//ends GNF_consolidarConsumosClick_cls
	
	
	public GNF_consolidarConsumosClick_cls ( list<string> lst_idClick, set<string> set_idOppPuntoSuministro, Boolean blnApplyFilter )
	{
		/* BEGIN - Manuel Medina - Variables requeridas para el filtrado - 23082017 */
		GNF_PreciosVistaClasica_ctr ctrPreciosVistaClasica			= new GNF_PreciosVistaClasica_ctr();
		List<Consumo_Click__c> lstConsumosClick						= new List<Consumo_Click__c>();
		/* END - Manuel Medina - Variables requeridas para el filtrado - 23082017 */

		Datetime		dtm_creacionClick;	

		set<string>		set_IdClickAsociado		= new set<string>(); 
		set<string>		set_IdContratos			= new set<string>(); 

		map_pSuminitroXopp						= new map<string, list<Oportunidad_Punto_de_Suministros__c>>();	
		
		map_cClickXpSuministro					= new map<String, map<Integer, map<Integer, Consumo_Click__c>>>();
		map_cClickXcPeriodo						= new map<String, list<Cotizacion_de_periodo__c>>();
		map_cClickAnteriorXsSuministro			= new map<String, map<Integer, list<Consumo_Click__c>>>();
		map_cClickAnteriorXanoXmesXsSuministro 	= new map<String, map<Integer, map<Integer, list<Consumo_Click__c>>>>();
				
		
		map_cClickUnclickXsuministro			= new map<String, map<Integer, map<Integer, Consumo_Click__c>>>();		
		
		
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> lst_idClick :' + lst_idClick );		
		
		
		int_uninadesDcm = Integer.valueOf( GNF_ValoresPredeterminados__c.getInstance().GNF_unidadesDecimal__c );
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> int_uninadesDcm :' + int_uninadesDcm );			
 		
		if( lst_idClick!=null && lst_idClick.size()>0 )		
			map_opp = new map<Id, Opportunity>( [Select Id,CreatedDate,Click_Asociado__c From Opportunity Where Id IN : lst_idClick] );
		
		for( Opportunity obj_opp : map_opp.values() ){
			
			if( dtm_creacionClick==null || obj_opp.createdDate>dtm_creacionClick )
				dtm_creacionClick = obj_opp.createdDate;			
			
			if( obj_opp.Click_Asociado__c!=null )
				set_IdClickAsociado.add( obj_opp.Click_Asociado__c );
		}

		if( set_idOppPuntoSuministro!= null && !set_idOppPuntoSuministro.isEmpty() )
			map_opp = new map<Id, Opportunity>();
			
		if( map_opp!=null /*&& !map_opp.isEmpty()*/ )
			for( Oportunidad_Punto_de_Suministros__c ps : [Select Id,Oportunidad__c, Contrato__c, Contrato__r.Consumo_Contratado__c, Contrato__r.Producto__c,Contrato__r.NISS__c, Contrato__r.Codigo_Contrato__c, Contrato__r.NISS__r.CUPS__c,Contrato__r.NISS__r.Name  From Oportunidad_Punto_de_Suministros__c Where ( Oportunidad__c IN : map_opp.keySet() OR Id IN :set_idOppPuntoSuministro ) And Contrato__c!=null] )
				if( ps.Oportunidad__c!=null && map_pSuminitroXopp.containsKey( ps.Oportunidad__c ) )
					map_pSuminitroXopp.get( ps.Id ).add( ps );
				else if( ps.Oportunidad__c!=null )
					map_pSuminitroXopp.put( ps.Id, new list<Oportunidad_Punto_de_Suministros__c>{ ps } );
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> map_pSuminitroXopp :' + map_pSuminitroXopp );			
			
			
		if( map_pSuminitroXopp!=null && !map_pSuminitroXopp.isEmpty() )
		
			/* BEGIN - Manuel Medina - Logica para permitir filtrado de consumos por puntos de suministro - 23082017 */
			lstConsumosClick											= new List<Consumo_Click__c>( [
																			Select Id,
																				GNF_FechaInicioValidez_Anio__c,
																				Mes_de_aplicacion__c,
																				Q_comprometida_kWh__c,
																				GNF_Qa_mes_kWh__c,
																				Cotizacion_de_periodo__c,
																				Cotizacion_de_periodo__r.Year__c,
																				Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c,
																				Cotizacion_de_periodo__r.Periodo_Formula__c,
																				Cotizacion_de_periodo__r.Tipo_de_periodo__c,
																				Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c,
																				Cotizacion_de_periodo__r.Opportunity__r.Fecha_Inicio__c,
																				Oportunidad_Punto_de_Suministro__c,
																				Oportunidad_Punto_de_Suministro__r.Name,
																				Oportunidad_Punto_de_Suministro__r.Contrato__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Contratado__c,
																				Oportunidad_Punto_de_Suministro__r.Contrato__r.Producto__c
																			From Consumo_Click__c
																			Where Oportunidad_Punto_de_Suministro__c IN : ( blnApplyFilter ? ctrPreciosVistaClasica.getOpportunitySupplies( map_opp.keySet() ) : map_pSuminitroXopp.keySet() )
																			AND Cotizacion_de_periodo__r.Year__c != null
																			AND Mes_de_aplicacion__c != null
																		] );
			/* END - Manuel Medina - Logica para permitir filtrado de consumos por puntos de suministro - 23082017 */
		
			for( Consumo_Click__c cc : lstConsumosClick )
				if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) && map_cClickXpSuministro.containsKey( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ) )
					if( map_cClickXpSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).containsKey( Integer.valueOf( cc.GNF_FechaInicioValidez_Anio__c ) ) )
						map_cClickXpSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( Integer.valueOf( cc.GNF_FechaInicioValidez_Anio__c ) ).put( map_meses.get( cc.Mes_de_aplicacion__c ), cc );
					else
						map_cClickXpSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).put( Integer.valueOf( cc.GNF_FechaInicioValidez_Anio__c ), new map<Integer, Consumo_Click__c>{ map_meses.get( cc.Mes_de_aplicacion__c ) => cc } );					
				else if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) )
					map_cClickXpSuministro.put( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,  new map<Integer, map<Integer, Consumo_Click__c>>{ Integer.valueOf( cc.GNF_FechaInicioValidez_Anio__c ) => new map<Integer, Consumo_Click__c>{ map_meses.get( cc.Mes_de_aplicacion__c )=>cc } } );		
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> map_cClickXpSuministro :' + map_cClickXpSuministro );


 		if( map_pSuminitroXopp!=null )
 			for( string key : map_pSuminitroXopp.keyset() )
 				for( Oportunidad_Punto_de_Suministros__c os : map_pSuminitroXopp.get(key) )
 					set_IdContratos.add( os.Contrato__c );
    	
		if( set_IdContratos!=null )
			for( Consumo_Click__c cc : [Select Id,Mes_de_aplicacion__c,Q_comprometida_kWh__c,GNF_Qa_mes_kWh__c,Cotizacion_de_periodo__c,Cotizacion_de_periodo__r.Periodo_Formula__c,Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c,Cotizacion_de_periodo__r.Tipo_de_periodo__c,Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c,Oportunidad_Punto_de_Suministro__c,Oportunidad_Punto_de_Suministro__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Contratado__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__r.CreatedDate From Consumo_Click__c Where Oportunidad_Punto_de_Suministro__r.Contrato__c IN : set_IdContratos AND Oportunidad_Punto_de_Suministro__r.Oportunidad__c!=NULL AND Oportunidad_Punto_de_Suministro__r.Oportunidad__c NOT IN : lst_idClick AND Oportunidad_Punto_de_Suministro__r.Oportunidad__r.stageName = 'Firmada' AND Oportunidad_Punto_de_Suministro__r.Oportunidad__r.CreatedDate<:dtm_creacionClick] )
				if( cc.Mes_de_aplicacion__c!=null && map_meses.containsKey( cc.Mes_de_aplicacion__c ) && map_cClickAnteriorXsSuministro.containsKey( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ) )
					if( map_cClickAnteriorXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).containsKey( map_meses.get( cc.Mes_de_aplicacion__c ) ) )
						map_cClickAnteriorXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( map_meses.get( cc.Mes_de_aplicacion__c ) ).add( cc );
					else
						map_cClickAnteriorXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).put(map_meses.get( cc.Mes_de_aplicacion__c ), new list<Consumo_Click__c>{cc} );
				else if( cc.Mes_de_aplicacion__c!=null && map_meses.containsKey( cc.Mes_de_aplicacion__c ) )
					map_cClickAnteriorXsSuministro.put( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name, new map<Integer, list<Consumo_Click__c>>{ map_meses.get( cc.Mes_de_aplicacion__c )=> new list<Consumo_Click__c>{cc} } );
					
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> map_cClickAnteriorXsSuministro :' + map_cClickAnteriorXsSuministro );			

		if( set_IdContratos!=null )
			for( Consumo_Click__c cc : [Select Id,Mes_de_aplicacion__c,Q_comprometida_kWh__c,GNF_Qa_mes_kWh__c,Cotizacion_de_periodo__c,Cotizacion_de_periodo__r.Periodo_Formula__c,Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c,Cotizacion_de_periodo__r.Tipo_de_periodo__c,Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c,Cotizacion_de_periodo__r.Year__c,Oportunidad_Punto_de_Suministro__c,Oportunidad_Punto_de_Suministro__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Contratado__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__r.CreatedDate From Consumo_Click__c Where Oportunidad_Punto_de_Suministro__r.Contrato__c IN : set_IdContratos AND Oportunidad_Punto_de_Suministro__r.Oportunidad__c!=NULL AND Oportunidad_Punto_de_Suministro__r.Oportunidad__c NOT IN : lst_idClick AND Oportunidad_Punto_de_Suministro__r.Oportunidad__r.stageName = 'Firmada' AND Oportunidad_Punto_de_Suministro__r.Oportunidad__r.CreatedDate<:dtm_creacionClick] )
				if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) && map_cClickAnteriorXanoXmesXsSuministro.containsKey( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ) )
				
					if( map_cClickAnteriorXanoXmesXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).containsKey( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ) )
						if( map_cClickAnteriorXanoXmesXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ).containsKey( map_meses.get( cc.Mes_de_aplicacion__c ) ) )
							map_cClickAnteriorXanoXmesXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ).get( map_meses.get( cc.Mes_de_aplicacion__c ) ).add( cc );
						else
							map_cClickAnteriorXanoXmesXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ).put( map_meses.get( cc.Mes_de_aplicacion__c ), new list<Consumo_Click__c>{ cc } );
					else
						map_cClickAnteriorXanoXmesXsSuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).put( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ), new map<Integer, list<Consumo_Click__c>>{ map_meses.get( cc.Mes_de_aplicacion__c ) => new list<Consumo_Click__c>{cc} } );
											
				else if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) )
					map_cClickAnteriorXanoXmesXsSuministro.put( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,  new map<Integer, map<Integer, list<Consumo_Click__c>>>{ Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) => new map<Integer, list<Consumo_Click__c>>{ map_meses.get( cc.Mes_de_aplicacion__c )=>new list<Consumo_Click__c>{cc} } } );				
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> map_cClickAnteriorXanoXmesXsSuministro :' + map_cClickAnteriorXanoXmesXsSuministro );   
    	
    	
		if( set_IdClickAsociado!=null )
			for( Consumo_Click__c cc : [Select Id,Mes_de_aplicacion__c,Q_comprometida_kWh__c,GNF_Qa_mes_kWh__c,Cotizacion_de_periodo__c,Cotizacion_de_periodo__r.Periodo_Formula__c,Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c,Cotizacion_de_periodo__r.Tipo_de_periodo__c,Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c,Cotizacion_de_periodo__r.Year__c,Oportunidad_Punto_de_Suministro__c,Oportunidad_Punto_de_Suministro__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Contratado__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__c,Oportunidad_Punto_de_Suministro__r.Oportunidad__r.CreatedDate From Consumo_Click__c Where Oportunidad_Punto_de_Suministro__r.Oportunidad__c!=NULL AND Oportunidad_Punto_de_Suministro__r.Oportunidad__c IN : set_IdClickAsociado] )
				if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) && map_cClickUnclickXsuministro.containsKey( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ) )
					if( map_cClickUnclickXsuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).containsKey( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ) )
						map_cClickUnclickXsuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).get( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) ).put( map_meses.get( cc.Mes_de_aplicacion__c ), cc );
					else
						map_cClickUnclickXsuministro.get( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name ).put( Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ), new map<Integer, Consumo_Click__c>{ map_meses.get( cc.Mes_de_aplicacion__c ) => cc } );					
				else if( map_meses.containsKey( cc.Mes_de_aplicacion__c ) )
					map_cClickUnclickXsuministro.put( cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,  new map<Integer, map<Integer, Consumo_Click__c>>{ Integer.valueOf( cc.Cotizacion_de_periodo__r.Year__c ) => new map<Integer, Consumo_Click__c>{ map_meses.get( cc.Mes_de_aplicacion__c )=>cc } } );	
					
    	system.debug('<<GNF_consolidarConsumosClick_cls.GNF_consolidarConsumosClick_cls>> map_cClickUnclickXsuministro :' + map_cClickUnclickXsuministro );	    								
	}//ends GNF_consolidarConsumosClick_cls     


	/**
	* @Method: 		mthd_generarPerdiodicidad
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/	    
    //Consumo Cubierto Mes (Qc)
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_consumoCubiertosMes()
	{
		map<Integer, map<Integer, Consumo_Click__c>>		map_cclickMesXano;
		map<Integer, Consumo_Click__c> 						map_cclickXmes;
		
		GNF_consolidarConsumosClick_cls.wrp_resultado		obj_wrpResultado;		
		Consumo_Click__c 									cClick;
		
		Date												dt_fechaInicioConsumo;

		list<GNF_consolidarConsumosClick_cls.wrp_resultado>	lst_wrp_resultado	= new list<GNF_consolidarConsumosClick_cls.wrp_resultado>();		
		list<Integer> 										lst_orderAno 		= new list<Integer>();	
		list<Integer> 										lst_orderMes 		= new list<Integer>();
				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoCubiertosMes>> map_cClickXpSuministro :' + map_cClickXpSuministro );				
				
		if( map_cClickXpSuministro==null || map_cClickXpSuministro.isEmpty() )
			return null;
		
		for( string sIdPs : map_cClickXpSuministro.keyset() )
		{
			map_cclickMesXano 	= map_cClickXpSuministro.get(sIdPs);
			
			lst_orderAno.clear();			
			
			lst_orderAno.addAll( map_cclickMesXano.keyset() );
			
			lst_orderAno.sort();
					
			for( Integer iAno : lst_orderAno )
			{	
				lst_orderMes.clear();
				
				map_cclickXmes 		= map_cclickMesXano.get(iAno);
						
				lst_orderMes.addAll(map_cclickXmes.keyset());
						
				lst_orderMes.sort();	
				
				system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoCubiertosMes>> map_cclickXmes :' + map_cclickXmes );	
						
				for( Integer iMes : lst_orderMes)
				{
					cClick 												= map_cclickXmes.get(iMes);					
					
					dt_fechaInicioConsumo								= null;
					
					obj_wrpResultado 									= new GNF_consolidarConsumosClick_cls.wrp_resultado();
					
					obj_wrpResultado.str_cClickId						= cClick.Id;
					
					
					if(cClick.Oportunidad_Punto_de_Suministro__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__c!=null)
					{
						if(cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Producto__c != null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Producto__c == 'GNL')
							obj_wrpResultado.str_codigoContratoCUPS			= cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c !=null ? cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c : null;
						else
							obj_wrpResultado.str_codigoContratoCUPS			= cClick.Oportunidad_Punto_de_Suministro__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c!=null ? cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c : null;
					}

					obj_wrpResultado.str_puntoSuministroName			= cClick.Oportunidad_Punto_de_Suministro__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c!=null ? cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.CUPS__c : null;					
					obj_wrpResultado.str_condigoContrato				= cClick.Oportunidad_Punto_de_Suministro__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__c!=null && cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c!=null ? cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c : null;
					obj_wrpResultado.str_puntoSuministroId				= cClick.Oportunidad_Punto_de_Suministro__c!=null ? cClick.Oportunidad_Punto_de_Suministro__r.Id : null;
					obj_wrpResultado.str_sectorSuministroId	            = cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name!=null ? cClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name : null;			
					obj_wrpResultado.str_periodo						= cClick.Cotizacion_de_periodo__c!=null ? cClick.Cotizacion_de_periodo__r.Periodo_Formula__c : null;
					obj_wrpResultado.str_Ano							= cClick.Cotizacion_de_periodo__r.Year__c;
					obj_wrpResultado.str_mes							= cClick.Mes_de_aplicacion__c;
					
					dt_fechaInicioConsumo								= date.parse( '01/' + map_meses.get( cClick.Mes_de_aplicacion__c ) + '/' + cClick.Cotizacion_de_periodo__r.Year__c ); 
					
					if (  dt_fechaInicioConsumo!=null && cClick.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Inicio__c!=null && dt_fechaInicioConsumo<cClick.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Inicio__c )
						obj_wrpResultado.bln_isDisabled									= true;
								
					obj_wrpResultado.str_ConsumoCubiertoMes 			= cClick.Q_comprometida_kWh__c!=null ? String.valueof( cClick.Q_comprometida_kWh__c ) : null;
					obj_wrpResultado.str_ConsumoEstimado				= cClick.GNF_Qa_mes_kWh__c!=null ? String.valueof( cClick.GNF_Qa_mes_kWh__c.setscale( int_uninadesDcm ) ) : null;
					
					obj_wrpResultado.str_ConsumoCubiertoTotalPeriodo 	= cClick.Cotizacion_de_periodo__c!=null ? String.valueOf( cClick.Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c ) : null;
					
					lst_wrp_resultado.add( obj_wrpResultado );			
				}			
			
			}
			
		}

		system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoCubiertosMes>> lst_wrp_resultado :' + lst_wrp_resultado );
		system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoCubiertosMes>> lst_wrp_resultado.size() :' + lst_wrp_resultado.size() );

		 return lst_wrp_resultado;		
	}//ends mthd_consumoCubiertosMes


	/**
	* @Method: 		mthd_ConsumoCubiertototalPS
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/	    
    //Consumo Cubierto Mes (Qc)
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_ConsumoCubiertototalPS( list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos )
	{	
		map<Integer, map<Integer, Consumo_Click__c>> map_cclickMesXano;
			
		Decimal dec_consumoClick;
				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_ConsumoCubiertototalPS>> map_cClickXpSuministro :' + map_cClickXpSuministro );				
				
		if( lst_consumos==null || lst_consumos.isEmpty() )
			return null;
		
		for( GNF_consolidarConsumosClick_cls.wrp_resultado wrp : lst_consumos )
		{
			dec_consumoClick = 0;
		
			if( map_cClickXpSuministro.containsKey( wrp.str_sectorSuministroId ) )
			{
				map_cclickMesXano = map_cClickXpSuministro.get( wrp.str_sectorSuministroId );
				
				for( Integer iAno :  map_cclickMesXano.keySet() )			
					for( Consumo_Click__c cClick : map_cclickMesXano.get( iAno ).values() )
						if( cClick.Q_comprometida_kWh__c!=null )
							dec_consumoClick += cClick.Q_comprometida_kWh__c;
			
				wrp.str_ConsumoCubiertototalPS = String.valueof( dec_consumoClick.setscale( int_uninadesDcm ) );
			}	
	
		}

		 return lst_consumos;		
	}//ends mthd_consumoCubiertosMes


	/**
	* @Method: 		mthd_ConsumoCubiertoAnterior
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/    
    //mthd_ConsumoCubiertototalPS
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_ConsumoCubiertoAnterior( list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos )
	{		
		
		Decimal 			dec_consumoClick;
				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_ConsumoCubiertoAnterior>> map_cClickAnteriorXsSuministro :' + map_cClickAnteriorXsSuministro );				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_ConsumoCubiertoAnterior>> map_cClickAnteriorXanoXmesXsSuministro :' + map_cClickAnteriorXanoXmesXsSuministro );					
				
		if( lst_consumos==null || lst_consumos.isEmpty() || map_cClickAnteriorXanoXmesXsSuministro==null || map_cClickAnteriorXanoXmesXsSuministro.isEmpty() )
			return null;

		for( GNF_consolidarConsumosClick_cls.wrp_resultado wrp : lst_consumos )
		{
			dec_consumoClick = 0;

    		system.debug('<<GNF_consolidarConsumosClick_cls.mthd_ConsumoCubiertoAnterior>> wrp :' + wrp );	

			if( map_cClickAnteriorXanoXmesXsSuministro.containsKey( wrp.str_sectorSuministroId ) && 
				map_cClickAnteriorXanoXmesXsSuministro.get( wrp.str_sectorSuministroId ).containsKey( Integer.valueOf( wrp.str_Ano ) ) && 
				map_meses.containsKey( wrp.str_mes ) && 
				map_cClickAnteriorXanoXmesXsSuministro.get( wrp.str_sectorSuministroId ).get( Integer.valueOf( wrp.str_Ano ) ).containsKey( map_meses.get( wrp.str_mes ) ) )
			{
				for(Consumo_Click__c  cClick : map_cClickAnteriorXanoXmesXsSuministro.get( wrp.str_sectorSuministroId ).get( Integer.valueOf( wrp.str_Ano ) ).get( map_meses.get( wrp.str_mes ) ) )
					if( cClick.Q_comprometida_kWh__c!=null )
						dec_consumoClick += cClick.Q_comprometida_kWh__c;
								
    			system.debug('<<GNF_consolidarConsumosClick_cls.mthd_ConsumoCubiertoAnterior>> dec_consumoClick :' + dec_consumoClick );
    								
				wrp.str_consumoCubiertoAnterior = String.valueof( dec_consumoClick.setscale( int_uninadesDcm ) );
			}
			
		}
		
		 return lst_consumos;	
	}//ends mthd_consumoCubiertosMes	


	/**
	* @Method: 		mthd_ConsumoCubiertoAnterior
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/    
    //mthd_ConsumoCubiertototalPS
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_consumoDisponiblePeriodo( list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos )
	{		
		Decimal 			dec_consumoClick;
				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoDisponiblePeriodo>> mthd_ConsumoCubiertoAnterior :' + map_cClickAnteriorXsSuministro );				
				
		if( lst_consumos==null || lst_consumos.isEmpty() )
			return null;

		for( GNF_consolidarConsumosClick_cls.wrp_resultado wrp : lst_consumos )
		{
			dec_consumoClick = 0;

    		system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoDisponiblePeriodo>> wrp :' + wrp );	

			if( wrp.str_consumoEstimado!=null && Decimal.valueOf( wrp.str_consumoEstimado )>0 && ( wrp.str_consumoCubiertoAnterior==null || Decimal.valueOf( wrp.str_consumoCubiertoAnterior )>=0 ) )
			{
				if( wrp.str_consumoCubiertoAnterior == null )
					wrp.str_consumoCubiertoAnterior = '0';
				
				dec_consumoClick = Decimal.valueOf( wrp.str_consumoEstimado ) - Decimal.valueOf( wrp.str_consumoCubiertoAnterior ); 
				
				wrp.str_consumoDisponiblePeriodo = String.valueOf( dec_consumoClick.setscale( int_uninadesDcm ) );
								
    			system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoDisponiblePeriodo>> wrp.str_consumoDisponiblePeriodo :' + wrp.str_consumoDisponiblePeriodo );
			}
			
		}
		
		 return lst_consumos;	
	}//ends mthd_consumoCubiertosMes		


	/**
	* @Method: 		mthd_ConsumoCubiertoAnterior
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/    
    //mthd_ConsumoCubiertototalPS
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_consumoDisponiblePeriodoUnclick( list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos )
	{		
		Decimal 			dec_consumoClick;
		
		map<Integer, map<Integer, Consumo_Click__c>> map_cclickMesXano;	
		map<Integer, Consumo_Click__c> map_cclickXmes;					
				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoDisponiblePeriodoUnclick>> map_cClickUnclickXsuministro :' + map_cClickUnclickXsuministro );				
				
		if( lst_consumos==null || lst_consumos.isEmpty() )
			return null;

		for( GNF_consolidarConsumosClick_cls.wrp_resultado wrp : lst_consumos )
		{
			dec_consumoClick = 0;
		
			if( map_cClickUnclickXsuministro.containsKey( wrp.str_sectorSuministroId ) )
			{
				map_cclickMesXano = map_cClickUnclickXsuministro.get( wrp.str_sectorSuministroId );
				
				if( map_cclickMesXano.containsKey( Integer.valueOf( wrp.str_Ano ) ) )
				{				
					map_cclickXmes = map_cclickMesXano.get( Integer.valueOf( wrp.str_Ano ) );
				
					if( map_cclickXmes.containskey( map_meses.get( wrp.str_mes ) ) && map_cclickXMes.get( map_meses.get( wrp.str_mes ) ).Q_comprometida_kWh__c!=null )
						wrp.str_consumoDisponiblePeriodo = String.valueof( map_cclickXMes.get( map_meses.get( wrp.str_mes ) ).Q_comprometida_kWh__c.setscale( int_uninadesDcm ) );
				}
			}	
	
		}

    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_consumoDisponiblePeriodoUnclick>> lst_consumos :' + lst_consumos );
		
		 return lst_consumos;	
	}//ends mthd_consumoCubiertosMes


	/**
	* @Method: 		mthd_ConsumoCubiertoAnterior
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/    
    //mthd_ConsumoCubiertototalPS
    public list<GNF_consolidarConsumosClick_cls.wrp_resultado_2> mthd_numeroClicksEjecutados( )
	{		
		Integer 				int_nroClick;
		wrp_resultado_2			obj_wrp;

		list<wrp_resultado_2>   list_wraper 		= new list<wrp_resultado_2>();		
		set<string>				set_idOportunidad 	= new set<string>();
 				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_numeroClicksEjecutados>> map_cClickAnteriorXsSuministro :' + map_cClickAnteriorXsSuministro );				
    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_numeroClicksEjecutados>> map_pSuminitroXopp :' + map_pSuminitroXopp );					
				
		/*if( map_cClickAnteriorXsSuministro==null || map_cClickAnteriorXsSuministro.isEmpty() )
			return null;*/

		for( string s : map_pSuminitroXopp.keySet() )
			for( Oportunidad_Punto_de_Suministros__c ops : map_pSuminitroXopp.get( s ) )
			{
				int_nroClick 	= 0;
				obj_wrp			= new wrp_resultado_2();
	
	    		system.debug('<<GNF_consolidarConsumosClick_cls.mthd_numeroClicksEjecutados>> obj_wrp :' + obj_wrp );	
	
				if( map_cClickAnteriorXsSuministro.containsKey( ops.Contrato__r.NISS__r.Name ) )
					for( list<Consumo_Click__c> list_cClick : map_cClickAnteriorXsSuministro.get( ops.Contrato__r.NISS__r.Name ).values() )
					{
						for( Consumo_Click__c cClick : list_cClick )
						{
							if( cClick.Oportunidad_Punto_de_Suministro__r.Oportunidad__c!=null && !set_idOportunidad.contains( cClick.Oportunidad_Punto_de_Suministro__c ) )
							{
								int_nroClick ++;
								set_idOportunidad.add( cClick.Oportunidad_Punto_de_Suministro__c );
						
							}
			
						}
		
					}

				if(ops.Contrato__r.Producto__c == 'GNL')
					obj_wrp.str_sectorSuministroDenominacion 	= ops.Contrato__r.Codigo_Contrato__c;
				else
					obj_wrp.str_sectorSuministroDenominacion 	= ops.Contrato__r.NISS__r.CUPS__c;		
				obj_wrp.str_sectorSuministroName 			= ops.Contrato__r.NISS__r.Name;
				obj_wrp.str_condigoContrato					= ops.Contrato__r.Codigo_Contrato__c;				 
				obj_wrp.str_noClicksEjecutados 				= String.valueOf( int_nroClick );
										
				system.debug('<<GNF_consolidarConsumosClick_cls.mthd_numeroClicksEjecutados>> obj_wrp.str_noClicksEjecutados :' + obj_wrp.str_noClicksEjecutados );			
				
				list_wraper.add( obj_wrp );
			}
		
		 return list_wraper;	
	}//ends mthd_numeroClicksEjecutados	


	/*
	*/
	public map<string, GNF_consolidarConsumosClick_cls.wrp_resultado> mthd_getConsumosMap( list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos )
	{

		map<string, GNF_consolidarConsumosClick_cls.wrp_resultado> map_consolidadosXidConsumo;


    	system.debug('<<GNF_consolidarConsumosClick_cls.mthd_getConsumosMap>> lst_consumos :' + lst_consumos );			
	
		if( lst_consumos==null || lst_consumos.isEmpty() )
			return null;
		
 		map_consolidadosXidConsumo = new map<string, GNF_consolidarConsumosClick_cls.wrp_resultado>();
	
		for( wrp_resultado obj_wrp :  lst_consumos )
			if( obj_wrp.str_cClickId!=null )
				map_consolidadosXidConsumo.put( obj_wrp.str_cClickId, obj_wrp );
			
		return map_consolidadosXidConsumo;
	}//ends mthd_getConsumosMap


	/**
	* @Method: 		wrp_resultado
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/	      
	public class wrp_resultado
	{
		public string str_cClickId                  			{get; set;}
		
		public string 	str_puntoSuministroName					{get; set;}
		public string 	str_codigoContratoCUPS					{get; set;}		
		public string	str_condigoContrato						{get; set;}
		public string 	str_puntoSuministroId					{get; set;}	
		public string 	str_sectorSuministroId	    			{get; set;}	
		public string 	str_periodo								{get; set;}
		public string 	str_Ano									{get; set;}		
		public string 	str_mes									{get; set;}
		public string 	str_consumoCubiertoMes					{get; set;}	
		public string 	str_consumoEstimado						{get; set;}
		public string 	str_consumoCubiertototalPS				{get; set;}
		public string 	str_consumoCubiertoTotalPeriodo			{get; set;}
		public string 	str_consumoCubiertoAnterior				{get; set;}		
		public string 	str_consumoDisponiblePeriodo			{get; set;}	
	
		public boolean 	bln_isDisabled							{get; set;}	
	
		public wrp_resultado(){
			
			bln_isDisabled = false;
		
		}
	}//ends wrp_resultado
	
	
		/**
	* @Method: 		wrp_resultado_2
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/	      
	public class wrp_resultado_2
	{
		public string str_sectorSuministroName			{get; set;}
		public string str_condigoContrato				{get; set;}		
		public string str_sectorSuministroId	   		{get; set;}	
		public string str_noClicksEjecutados			{get; set;}	
		public string str_sectorSuministroDenominacion	{get; set;}	

		public wrp_resultado_2(){
		
		}
	}//ends wrp_resultado_2
}//ends GNF_consolidarConsumosClick_cls