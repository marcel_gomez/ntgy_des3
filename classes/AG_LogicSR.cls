public class AG_LogicSR {


    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicSR() {}

    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<SRs__c>}
     */
    public void assignOwnerFromEmpleado(final List<SRs__c> newList, final Map<Id, SRs__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        System.debug('Asignacion De Owner Desde Empleado  ' + newList);
        System.debug('se van a procesar estas SRs  ' + newList.size());
        System.debug('oldMap  ' + oldMap);

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();
        system.debug('ManagersEmpleados    ' +managersEmpleados);
        if (oldMap==null) oldMap = new Map<Id, SRs__c>(); //just to avoid null checks

        // Iterate over srs and find and assign owner based on its empleado
        for(SRs__c theSR : newList) {

          if(theSR.Gestor_External_Key__c <> null) {

            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theSR.Id), theSR)) {
                system.debug('ha cambiado de empleado o es nuevo registro  '+ theSR.Gestor_External_Key__c);
              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theSR.managers__c = managersEmpleados.get(theSR.Gestor_External_Key__c);
                system.debug('campo managers__c' + theSR.managers__c);
              String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theSR.Gestor_External_Key__c,empleadoTable,userTable);
              system.debug('Owner que se va a assignar     '+assignedOwner);
              if(assignedOwner <> null) {
                theSR.OwnerId = assignedOwner;
              } else {
                //TODO: What todo in if not find a valid user?
                final String errMsg = '#ERROR# No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_sr=' + theSR.ID_SR__c + ' - gestor_external_key='+ theSR.Gestor_External_Key__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug(loggingLevel.Error, errMsg);
                //throw new SRLogicException(errMsg);
              }
            }   
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '#ERROR# No esta informada el gestor para la sr :' + theSR.ID_SR__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new SRLogicException('No esta informada el gestor para la sr :' + theSR.ID_SR__c);
          }

        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }
    
    /**
     * Add sharing roles to SRs__c and Account to allow visibility to role hierachy.
     */
    public void setCustomSharing(final List<SRs__c> newList, final Map<Id, SRs__c> newMap,
                                 final List<SRs__c> oldList, final Map<Id, SRs__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        System.debug('Se van a procesar estas srS   ' + newList);
        System.debug('Se van a procesar estas srS   ' + oldList);

        //List of sr sharing records to be inserted
        List<SRs__Share> srSharingList = new List<SRs__Share>();

        //List of account sharing records to be inserted 
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, SRs__c>(); //just to avoid null checks
 
        //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();

        //Iterate over collection
        for(SRs__c theSR : newList) {

          Id srId = theSR.Id;
          SRs__c oldSR = oldMap.get(srId);
          SRs__c newSR = theSR;

          //Se comenta debug(Victor Velandia (VV))
          /*System.debug('@@@@ ID sr:' + srId);
          System.debug('@@@@ Old sr:' + oldSR);
          System.debug('@@@@ New sr:' + newSR);*/

          //Get only new records or records whose cliente has changed or owner has changed
          if (hasChangedCliente(oldSR, newSR)) {
            system.debug('la SR ha cambiado de cliente   ' + newSR.Cliente__c);
            if (oldSR!=null) //si es insert no hay previos que borrar
              Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(oldSR.Id);

            //get the role of the user assigned to the record account
            Id ownerSR  = theSR.ownerID;
            Id ownerRelatedAccount = theSR.Gestor_Cliente__c;
            system.debug('owner de la SR    ' + theSR.ownerID);
            system.debug('owner de la cuenta asociada    ' + theSR.Gestor_Cliente__c);
            //Se comenta debug(Victor Velandia (VV))
            /*System.debug('@@@@ SR gestion_cliente__C:' + ownerRelatedAccount);
            System.debug('TTTT owners SR' + ownerSR);
            System.debug('TTTT owners cliente SR' + ownerRelatedAccount);*/
            if (ownerSR!=ownerRelatedAccount){
              List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount,allUsers,allRoles,allGroups);
              system.debug('listado de grupos grupos de roles superiores     ' + gruposRoleSuperiores);
              for (Id grpOrUsrId : gruposRoleSuperiores) {
                SRs__Share clienteAbobeRoleSharing = createSRSharedRecord(theSR, grpOrUsrId);
                srSharingList.add(clienteAbobeRoleSharing);
              }
              system.debug('lista para insertar  ' + srSharingList);
          //Se comenta debug(Victor Velandia (VV))
          //System.debug('TTTT sharinglist:' + srSharingList);
            }
          }

          //Si es un nuevo SR, calculamos los permisops para el Account en el trigger,
          //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.

          //El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
          if (oldSR!=null && hasChangedOwner(oldSR, newSR)) {

              accountToRecalculate.add(theSR.Cliente__c);
              system.debug('nueva cuenta para recalcular   ' + theSR.Cliente__c);
          //Se trata de un alta de SR --> calculamos permisos al dar alta SR
          } else if (oldSR==null) {

              if(!String.isBlank(theSR.Cliente__c)) { //No hay nada que hacer, no hay cliente

                Id accountOwnerId = theSR.Gestor_Cliente__c;
                Id srOwnerId = theSR.OwnerId;

                if (accountOwnerId!=srOwnerId) {

                  //Se comenta debug(Victor Velandia (VV))
                  System.debug('@@@Account Propietrario sr:' + srOwnerId);
                  System.debug('@@@Account Propietrario cuenta:' + accountOwnerId);
                  System.debug('@@@Account Cliente:' + theSR.cliente__r.name);

                  List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(srOwnerId,allUsers,allRoles,allGroups);
                  for (Id grpOrUsrId : grpOrUsrIdToShare) {
                    AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(theSR.Cliente__c, grpOrUsrId);
                    accountSharingList.add(accountSharing);
                  }

                  System.debug('###Account Account Share:' + accountSharingList);
                }
              }
          }
        }

        if (!accountToRecalculate.isEmpty()) {
          AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!srSharingList.isEmpty())
          upsert srSharingList;


        if(!accountSharingList.isEmpty())
          upsert accountSharingList;

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS

   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldSR is null.
    */
    @testVisible
    private boolean hasChangedGestor(SRs__c oldSR, SRs__c newSR) {

      if (oldSR==null) return true;   //new object
      return (oldSR.Gestor__c <> newSR.Gestor__c);
    }      

    /*
     * @return true if the field value "Cliente__c" has changed or oldSR is null.
    */
    @testVisible
    private boolean hasChangedCliente(SRs__c oldSR, SRs__c newSR) {

      if (oldSR==null) return true;   //new object
      return (oldSR.cliente__c <> newSR.Cliente__c);
    }      

    /*
    * @return true if the owner has changed or oldSR is null.
    */
    @testVisible
    private boolean hasChangedOwner(SRs__c oldSR, SRs__c newSR) {

      if (oldSR==null) return true;   //new object
      return (oldSR.ownerId <> newSR.OwnerId);
    }      

    /**
     * Crea  SR__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible
    private SRs__Share createSRSharedRecord(final SRs__c sr, Id userOrGroup) {
        return new SRs__Share(
                  ParentId = sr.Id,
                  UserOrGroupId = userOrGroup, //sr.Gestor_Cliente__c,
                  AccessLevel = SHARE_MODE_READ,
                  RowCause = Schema.SRs__Share.RowCause.Apex_Sharing__c);
    }


    /*
    * Remove all the sharing added from Apex.
    */
    @testVisible
    private Integer deleteSRShares(Id srId) {

      List<SRs__Share> shareList = [SELECT Id FROM SRs__Share WHERE ParentId = :srId AND RowCause = :Schema.SRs__Share.RowCause.Apex_Sharing__c];
      Integer count = shareList.size();
      if (count>0)
        delete shareList;

      return count;
    }
   

    //-- CUSTOM EXCEPTION
    public class SRLogicException extends Exception {}

}