@isTest
private class AG_TestOrganizationModelUtil {
	
	static Map<String,user> mockUSers ;
	static Map<String,UserRole> mockRoles;
	static Map<String,Group> mockGroups ;
	static Map<Id,Group> mockGroupsById ;
	static Map<String,Account> mockAccounts;

  	static Map<String, Empleado__c> allEmpleados ;
	static Map<String,User> allUsers ;
	static Map<Id,UserRole> allRoles ;
	static Map<Id,User> allUsersById ;
	static Map<Id,Group> allRoleGroups;

	private static void initOMCollections() {

		mockUSers = AG_TestUtil.getMockUsers();
		mockRoles = AG_TestUtil.getMockUserRoles();
		mockGroups = AG_TestUtil.getMockGroups();
		mockGroupsById = AG_TestUtil.getMockGroupsById();
		mockAccounts = AG_TestUtil.getMockAccounts();

	  	allEmpleados =  AG_OrganizationModelUtil.getAllEmpleados();
		allUsers = AG_OrganizationModelUtil.getAllUsers();
		allRoles = AG_OrganizationModelUtil.getAllRolesById();
		allUsersById = AG_OrganizationModelUtil.getAllUsersById();
		allRoleGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

	}

	static {
		initOMCollections();
	}


	@testSetup
	private static void setup() {
		System.debug('Test setup');

		/*	Se crean 7 roles en dos ramas con raiz comun (testRole)
			--testRole
			|	|_testRole2
			|		|_testRole3
			|			|_testRole4		--> User1
			|___testRoleA
					|_testRoleB
						|_testRoleC	--> User2
		*/


		UserRole topRole = new UserRole(name='RoleT');
		insert topRole;
		UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
		insert userRole2;
		UserRole userRole3 = new UserRole(name='Role3', parentRoleId = userRole2.Id);
		insert userRole3;
		UserRole userRole4 = new UserRole(name='Role4', parentRoleId = userRole3.id);
		insert userRole4;
		System.debug('userRole:' + userRole.id + '- userRrole2:' + userRole2.id + '- userRrole3:' + userRole3.id + '- userRole4:' + userRole4.id);

		UserRole userRoleA = new UserRole(name='RoleA', parentRoleId = topRole.id);
		insert userRoleA;
		UserRole userRoleB = new UserRole(name='RoleB', parentRoleId = userRoleA.id);
		insert userRoleB;
		UserRole userRoleC = new UserRole(name='RoleC', parentRoleId = userRoleB.id);
		insert userRoleC;

		/* Se crean 2 usuarios
			User1 y User2) asignados a los roles Role4 y RoleC y a los
			empleados__c 'Empleado1' (k=111) y 'Empleado2' (k=222) respectivamente.
		*/

		System.debug('Look for Standard USer profile ');
  		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
  
		User theUser1 = new User(
			LastName = 'User1',					FirstName='Test',	
			Alias = 'tu1',                     	Email = 'test.user1@edelta.com',
			Username = 'test.user1@edelta.com',	ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',				LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',			LocaleSidKey = 'en_US',
            UserRoleId= userRole4.Id,			ID_Empleado__c=	'111',isActive=true);

 
		insert theUser1;
		System.debug('theUser1:' + theUser1);
		System.debug('theUser1 active:' + theUser1.isActive);

		User theUser2 = new User(
			LastName = 'User2',               	FirstName='Test',
			Alias = 'tu',                     	Email = 'test.user2@edelta.com',
            Username = 'test.user2@edelta.com',	ProfileId = profileId.id,
           	TimeZoneSidKey = 'GMT',				LanguageLocaleKey = 'en_US',
			EmailEncodingKey = 'UTF-8',			LocaleSidKey = 'en_US', 
            UserRoleId= userRoleC.Id,			ID_Empleado__c='222', isActive=true);

		insert theUser2;
		System.debug('theUser2:' + theUser2);
		System.debug('theUser2 active:' + theUser2.isActive);

		User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
		System.runAs(adminUser) {

			//add 2 empleados
			Empleado__c empleado1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='111');
			insert empleado1;
			Empleado__c empleado2 = new Empleado__c(name='Empleado2', OwnerId=theUSer2.id, ID_Empleado__c='222');
			insert empleado2;

			//add 2 accounts
			Account cliente1 = new Account(name='Account1', Gestor__c=empleado1.Id, OwnerId=theUser1.id);
			insert cliente1;
			System.debug('@#@# Account1:' + cliente1);

			Account cliente2 = new Account(name='Account2', Gestor__c=empleado2.Id, OwnerId=theUser2.id);
			insert cliente2;
			System.debug('@#@# Account2:' + cliente2);
		}	
	}

//-----------------------------------------------------------------

   /*
    * Read all empleados in a map indexed by field 'ID_Empleado__c'
    */
    @isTest public static void getAllEmpleados_test() {

    	 System.assert(allEmpleados.size()==2);

    	 System.assert(allEmpleados.containsKey('111'));
    	 System.assert(allEmpleados.containsKey('222'));
	}

	@isTest public static void getAllUsers_test() {

		//ID_Empleado key
		System.assert(allUsers.containsKey('111'));
		System.assert(allUsers.containsKey('222'));
	}

	@isTest public static void getAllUsersById_Test() {

		System.assert(allUsersById.size()>0);	//just check load something 
		System.assert(allUsersById.containsKey(mockUsers.get('User1').Id));
		System.assert(allUsersById.containsKey(mockUsers.get('User2').Id));

	}

	@isTest public static void getAllRolesById_Test() {
		
		System.assert(allRoles.size()>0);	//just check load something
		for (UserRole ur : mockRoles.values()) 
			System.assert(allRoles.containsKey(ur.id));

	}

    @isTest public static void recursiveUserFind_test1() {

    	User theUser = allUsers.get('111');
    	Id result = AG_OrganizationModelUtil.recursiveUserFind(theUser.ID_Empleado__c, allEmpleados, allUsers);

    	System.assertEquals(result, theUser.Id);

	}

    @isTest public static void recursiveUserFind_test2() {
   	
    	User user1 = allUsers.get('111');
    	
    	//Sesactivamos user 2
    	User user2 = allUsers.get('222');
    	uSer2.isActive=false;
    	update user2;

		User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
		System.runAs(adminUser) {
	    	
	    	//Empleado1 pasa a ser manager de Empleado2
	    	Empleado__c empleado2 = allEmpleados.get('222');
	    	empleado2.ID_Manager__c = '111';
	    	update empleado2;
	    }

    	Id result  = AG_OrganizationModelUtil.recursiveUserFind(user2.ID_Empleado__c, allEmpleados,  allUsers);

    	System.assertEquals(result, user1.Id);
	}

	
	@isTest static void getUserRole_test() {

		//prepare
		User theUser = [SELECT Id, FirstName, LastName, UserRoleId FROM User WHERE Username='test.user1@edelta.com' LIMIT 1];
		System.debug('the User:' + theUser);

		//call
		UserRole userRole =  AG_OrganizationModelUtil.getUserRole(theUser.id);

		//check
		System.assertEquals(userRole.id, theUser.UserRoleId);

	}

	@isTest static void getUserRoleFromId_test() {

		//preapare
		UserRole ur = [SELECT Id, Name FROM UserRole Where name='RoleT'];
		System.debug('User role:' + ur);

		//call
		UserRole userRole = AG_OrganizationModelUtil.getUserRoleFromId(ur.Id);

		//check
		System.assertEquals(userRole.Id, ur.Id);
		System.assertEquals(userRole.name, ur.name);
	}


	@isTest static void getGroupFromUserRoleId_test() {

		//preapare
		UserRole ur = [SELECT Id, Name FROM UserRole Where name='RoleT'];
		System.debug('User role:' + ur);

		//call
		Group g =  AG_OrganizationModelUtil.getGroupFromUserRoleId(ur.Id);
		System.Debug('Group:' + g);

		//check
		System.assertEquals(g.RelatedId, ur.Id);
		System.assertEquals(g.Type, 'Role');
	}


	@isTest static void  getAncestorRoles_test() {

		//prepare
		UserRole ur = [SELECT Id, Name, ParentRoleId FROM UserRole Where name='Role4'];
		System.debug('User role:' + ur);

		//call
		List<UserRole> ancestors =  AG_OrganizationModelUtil.getAncestorRoles(ur);
		
		//check: remember the role used in argument it is not included in result
		System.assertEquals(ancestors.size(), 3);	
		for (UserRole ancestor : ancestors) {
			System.assert(ancestor.name.startsWith('Role'));
		}
	}


	@isTest static void getAncestorRoles2_Test() {

		List<UserRole> rolesAbove = AG_OrganizationModelUtil.getAncestorRoles2(mockRoles.get('RoleC'), allRoles);

		System.assert(rolesAbove.size()>0);

	}


	@isTest static void getRoleGroupsByRoleId_Test() {

		for (UserRole r : mockRoles.values())
			System.assert(allRoleGroups.containsKey(r.Id));

	}

	@isTest static void getPublicGroupsOfRoleList_test() {

		//preapare
		List<USerRole> userRoleList = [SELECT Id, Name, parentRoleId FROM UserRole WHERE name like 'Role_'];
		System.debug('User role list:' + userRoleList);

		//call
		List<Group> groups =  AG_OrganizationModelUtil.getPublicGroupsOfRoleList(userRoleList);

		//check
		System.AssertEquals(groups.size(), userRoleList.size());
	}

	@isTest static void getPublicGroupsOfRoleList2_Test() {

		List<Group> groups = AG_OrganizationModelUtil.getPublicGroupsOfRoleList(mockRoles.values());

		System.assertEquals(mockRoles.size(), groups.size());

	}

	@isTest public static void calculateMembersIdsToSharesEx_Test() {

		System.debug('@@@@ calculateMembersIdsToSharesEx_Test: start');
		System.debug('@@@@ GRoups:' + mockGroupsById.size() + ' - ' + mockGroupsById.keySet());


		System.debug('Users: ' + allUsersById.size() + ' - ' + allUsersById.keySet());
		System.debug('Roles: ' + allRoles.size() + ' - ' + allRoles.keySet());
		System.debug('Groups: ' + allRoleGroups.size() + ' - ' + allRoleGroups.keySet());

		System.debug('User1:' + mockUsers.get('User1').id);
		System.debug('User2:' + mockUsers.get('User2').id);

		List<Id> ids2 = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(mockUsers.get('User2').id, allUsersById, allRoles, allRoleGroups);
		List<Id> ids1 = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(mockUsers.get('User1').id, allUsersById, allRoles, allRoleGroups);

		System.debug('@@@@ Check result id1:' + mockUsers.get('User1').id );
		for (Id id1 : ids1) {
			System.debug('@@@@ Check:' + id1);
			System.Assert( (mockGroupsById.containsKey(id1)) || (allUsersById.containsKey(id1)) );
		}

		System.debug('@@@@ Check result id2:' + mockUsers.get('User2').id);
		for (Id id2 : ids2) {
			System.debug('@@@@ Check:' + id2);
			System.Assert( (mockGroupsById.containsKey(id2)) || (allUsersbyId.containsKey(id2)) );
		}
		
	}


	@isTest public static void calculateMembersIdsToShares3_Test() {

		User u1 = mockUsers.get('User1');
		System.debug('@@@@ USer1:' + u1);
		User u2 = mockUsers.get('User2');
		System.debug('@@@@ USer2:' + u2);

		List<Id> ids1 =AG_OrganizationModelUtil.calculateMembersIdsToShare(u1.id);
		List<Id> ids2 =AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(u1.id, allUsersById, allRoles, allRoleGroups);

		System.assertEquals(ids1.size(),ids2.size());

		Set<Id> setIds1 = new Set<Id>(ids1);
		System.assert(setIds1.containsAll(ids2));
	}

	 @isTest static void createAccountSharedRecord_Test() {

	 	Account account1 = mockAccounts.get('Account1');
	 	User user1 = mockUsers.get('User1');

	 	AccountShare share = AG_OrganizationModelUtil.createAccountSharedRecord(account1.Id, user1.id);

	 	System.assert(share!=null);
	 	System.assertEquals(share.userOrGroupId , user1.id);
	 	System.assertEquals(share.AccountId , account1.id);
	 }

	 @isTest static void createAccountSharedRecordArrayArray_Test() {

	 	Account account1 = mockAccounts.get('Account1');
	 	Id[] usersId = new List<Id>(allUsersById.keySet());
	 	AccountShare[] shares = AG_OrganizationModelUtil.createAccountSharedRecord(account1.Id, usersId);

	 	System.assert(shares!=null);
	 	System.assertEquals(shares.size(), allUsersById.size());

	 	for (AccountShare ah : shares) {
	 		System.assert(allUsersById.containsKey(ah.UserOrGroupId));
	 		System.assertEquals(ah.AccountId, account1.id);
	 	}
	 }
	
	 @isTest static void removeAccountSharedRecords_Test() {

        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado2 = allEmpleados.get('222');
        User user1 = allUsers.get('111');

        //Forzamos el inser de un objeto para que se creen share's sobre su Account (account1)
        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado2.id);
        insert supply1;

	 	List<AccountShare> sharesBefore =[SELECT Id FROM AccountShare WHERE accountId = :account1.id AND RowCause='Manual'];
	 	System.assert(sharesBefore.size()>0);		//Verificamos que tiene algun share asignado.

	 	AG_OrganizationModelUtil.removeAccountSharedRecords(account1.id);

	 	List<AccountShare> sharesAfter =[SELECT Id FROM AccountShare WHERE accountId = :account1.id AND RowCause='Manual'];
	 	System.assertEquals(sharesAfter.size(), 0);		//Despues del borrado no queda nada
	 }
	
	 @isTest static void removeAccountSharedRecords2_Test() {

        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado2 = allEmpleados.get('222');

        //Forzamos el inser de un objeto para que se creen share's sobre su Account (account1)
        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado2.id);
        insert supply1;

		//Verificamos que tiene algun share asignado.
		Id account1Id = account1.id;
	 	List<AccountShare> sharesBefore =[SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE accountId = :account1Id AND RowCause='Manual'];
	 	System.assert(sharesBefore.size()>0);		
	 	Integer numSharesBefore = sharesBefore.size();

	 	//Lanzamos  borrado del primer share = SharesBefore.get(0)
	 	AG_OrganizationModelUtil.removeAccountSharedRecords(sharesBefore.get(0).AccountId, sharesBefore.get(0).UserOrGroupId);

		//Verificamos que despues del borrado hay un permiso menos
	 	List<AccountShare> sharesAfter =[SELECT Id FROM AccountShare WHERE accountId = :account1.Id AND RowCause='Manual'];
	 	Integer numSharesAfter = sharesAfter.size();
	 	System.assertEquals(numSharesAfter, numSharesBefore-1);		

	 	//Lanzamos el borrado de una combinación que no existe.
		AG_OrganizationModelUtil.removeAccountSharedRecords(sharesBefore.get(0).AccountId, UserInfo.getUserId());
	 	List<AccountShare> sharesAfter2 =[SELECT Id FROM AccountShare WHERE accountId = :account1.Id AND RowCause='Manual'];
	 	System.assertEquals(sharesAfter2.size(), numSharesAfter);		//El numero de shares no ha cambiado
	 }
	
	 @isTest static void removeShares_Test() {  	 

        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado2 = allEmpleados.get('222');

        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado2.id);
        insert supply1;

	 	List<Suministros__Share> sharesBefore =[SELECT Id FROM Suministros__Share WHERE ParentId = :supply1.id AND RowCause='Apex_Sharing__c'];
	 	System.assert(sharesBefore.size()>0);		//Verificamos que tiene algun share asignado.

	 	AG_OrganizationModelUtil.removeShares(supply1.id);

	 	List<Suministros__Share> sharesAfter=[SELECT Id FROM Suministros__Share WHERE ParentId = :supply1.id AND RowCause='Apex_Sharing__c'];
	 	System.assertEquals(sharesAfter.size(),0);		//Verificamos que ya no tiene ningun share asignado.
	 }

  	 @isTest static void createSharedRecord_Test() {

        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado1 = allEmpleados.get('111');
        User user1 = allUsers.get('111');

        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado1.id);
        insert supply1;

        SObject share = AG_OrganizationModelUtil.createSharedRecord(supply1.id, user1.id);
        Suministros__Share supplyShare = (Suministros__Share)share;
		
		System.assert(supplyShare!=null);
        System.assertEquals(supplyShare.parentID, supply1.id);
        System.assertEquals(supplyShare.userOrGroupId, user1.id);
  	 }

 	 @isTest static void createSharedRecords_Test() {


        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado1 = allEmpleados.get('111');

        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado1.id);
        insert supply1;

        Id[] allUsersId = new List<Id>(allUsersById.keySet());

        SObject[] shares = AG_OrganizationModelUtil.createSharedRecords(supply1.id, allUsersId);

        for (sObject obj : shares) {
        	Suministros__Share supplyShare = (Suministros__Share)obj;
        	System.assert(allUsersById.containsKey(supplyShare.UserOrGroupId));
        	System.assertEquals(supply1.id, supplyShare.ParentId);
        }
 	 }

	 @isTest static void getClassName_Test() {

        Account account1 = mockAccounts.get('Account1');
        Empleado__c empleado1 = allEmpleados.get('111');

        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado1.id);
        insert supply1;

        String accountClass = AG_OrganizationModelUtil.getClassName(account1.Id);
        System.assertEquals(accountClass, 'Account');

		String suministrosClass = AG_OrganizationModelUtil.getClassName(supply1.Id);
		System.assertEquals(suministrosClass, 'Suministros__c');

		String empleadoClass = AG_OrganizationModelUtil.getClassName(empleado1.Id);
		System.assertEquals(empleadoClass, 'Empleado__c');
	 }

 	 @isTest static void updateAccountsToRecalculate_Test() {

        Account account1 = mockAccounts.get('Account1');
        Account account2 = mockAccounts.get('Account1');
        Id[] accountIds = new List<Id>{account1.id, account2.id};

        //check the flag is false before call
        system.assertEquals(Account1.recalcular_permisos__c,false);
        system.assertEquals(Account2.recalcular_permisos__c,false);

        AG_OrganizationModelUtil.updateAccountsToRecalculate(accountIds);

        Map<String,Account> mapAccountes = AG_TestUtil.getMockAccounts();
		account1 = mapAccountes.get('Account1');
        account2 = mapAccountes.get('Account1');

        system.assertEquals(Account1.recalcular_permisos__c,true);
        system.assertEquals(Account2.recalcular_permisos__c,true);


 	 }

	
}