/*
VASS 31052018
Test class for AG_Batch_RecalculateAccountShare and ApG_Batch_AccountUpdateShareFlag
*/

@isTest
private class AG_TestBatch_RecalculateAccountShare {
	
	@testSetup
	private static void testSetup(){
		map<string, id> profileMap = ApG_TestUtil.getProfileMap(new set<string>{'Custom Standar Gestores'}); 
		map<string, id> roleMap = ApG_TestUtil.getRoleMap(new set<string>{'MAYORISTAS', 'DV_IBERIA_IBERIA'});

		User usr0 = ApG_TestUtil.getUser('fName0', 'lName0', profileMap.get('Custom Standar Gestores'), roleMap.get('MAYORISTAS'));
		User usr1 = ApG_TestUtil.getUser('fName1', 'lName1', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
		User usr2 = ApG_TestUtil.getUser('fName2', 'lName2', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
		list<User> userList = new list<User>{usr0, usr1, usr2};
		insert userList;
		
		system.runAs(userList[1]){
			Account acc0 = ApG_TestUtil.getAccount('acc0', 'España', 'acc0');
			acc0.Recalcular_Permisos__c = true;
			insert acc0;
			Suministros__c sum0 = ApG_TestUtil.getSuministro(acc0.id, null, 'Sum0');
			sum0.OwnerId = usr2.id;
			insert sum0;
		}
	}
	
	@isTest
	private static void testForClassBatch(){
		Account accBefore = [SELECT id, Recalcular_Permisos__c FROM Account WHERE Name = 'acc0'];
		integer amountOfSharesBefore = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		test.startTest();
			
			AG_Batch_RecalculateAccountShare recalcBatch = new AG_Batch_RecalculateAccountShare(0, new Map<Id,Set<Id>>(), false,true);
			Database.executeBatch(recalcBatch, 200);
		test.stopTest();
		integer amountOfSharesAfter = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		Account accAfter = [SELECT id, Recalcular_Permisos__c FROM Account WHERE Name = 'acc0'];
		system.assert(amountOfSharesAfter == amountOfSharesBefore,	'the Account has not been shared as espected	' + amountOfSharesAfter + ' ' + amountOfSharesBefore);
		system.assert(accBefore.Recalcular_Permisos__c == true && accAfter.Recalcular_Permisos__c == false, 'Recalculation field has not been updated	'+ accAfter.Recalcular_Permisos__c + '	'+ accBefore.Recalcular_Permisos__c);
	}
	
	@isTest
	private static void testForFullProcess(){
		Account accBefore = [SELECT id, Recalcular_Permisos__c FROM Account WHERE Name = 'acc0'];
		integer amountOfSharesBefore = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		test.startTest();
			AG_Batch_RecalculateAccountShare.executeFullProcess(true);
		test.stopTest();
		integer amountOfSharesAfter = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		system.assert(amountOfSharesAfter == amountOfSharesBefore,	'the Account has not been shared as espected	' + amountOfSharesAfter + ' ' + amountOfSharesBefore);
	}
	
	@isTest
	private static void testForClassOneTable(){
		Account accBefore = [SELECT id, Recalcular_Permisos__c FROM Account WHERE Name = 'acc0'];
		integer amountOfSharesBefore = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		test.startTest();
			AG_Batch_RecalculateAccountShare.executeProcessOneTable(0, true);
		test.stopTest();
		integer amountOfSharesAfter = [SELECT Count() FROM AccountShare WHERE id  =: accBefore.id];
		system.assert(amountOfSharesAfter == amountOfSharesBefore,	'the Account has not been shared as espected	' + amountOfSharesAfter + ' ' + amountOfSharesBefore);
	}

	@isTest
	private static void testForException(){
		try{
			AG_Batch_RecalculateAccountShare.executeProcessOneTable(703, true);
		}catch(exception e){
			system.assert(e.getMessage()=='Table index out of bounds', 'Not expected error message');
		}
	}

	@isTest
	private static void testForAccountShareFlag(){
		list<Account> accListBefore = [SELECT Id, Recalcular_Permisos__c FROM Account];
		accListBefore[0].Recalcular_Permisos__c = false;
		update accListBefore;
		test.startTest();
		list<Account> accList = [SELECT Id FROM Account];
		list<id> idList = new list<id>();
		for(Account i : accList){
			idList.add(i.id);
		}
		Id batchJobId = Database.executeBatch(new ApG_Batch_AccountUpdateShareFlag('Recalcular_Permisos__c', idList, true), 200);
		test.stopTest();
		list<Account> accListAfter = [SELECT Id, Recalcular_Permisos__c FROM Account];
		system.assert(accListBefore[0].Recalcular_Permisos__c==false && accListAfter[0].Recalcular_Permisos__c==true, 'The accounts has not been updated as expected	'+accListAfter);

	}
	
}