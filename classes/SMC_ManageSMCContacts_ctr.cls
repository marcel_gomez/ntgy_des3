/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Visualforce encargada de gestionar los contactos ejecutores asignados al SMC.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-10-20		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
public without sharing class SMC_ManageSMCContacts_ctr {
	
	public List<Schema.FieldSetMember> lstFieldSetMembers						{get; set;}
	public List<SMCContact> lstCurrentSMCContacts								{get; set;}
	public List<SMCContact> lstAvailableContacts								{get; set;}
	
	public Set<String> setCurrentSMCContacts									{get; set;} 
	
	public Map<String, String> mapFieldLabelByFieldName							{get; set;}
	public Map<Id, Contact> mapContactById										{get; set;}
	
	public Opportunity objOpportunity											{get; set;}
	
	public String strMsgError													{get; set;}
	public String strRenderMsgError												{get; set;}
	public String strShowCurrentContacts										{get; set;}
	
    /**
	* @Method: 		SMC_ManageSMCContacts_ctr
	* @param: 		ApexPages.StandardController stdOpportunity
	* @Description:	Constructor StandardController para Opportunity.
	* @author 		Manuel Medina - 22112017
	*/
	public SMC_ManageSMCContacts_ctr( ApexPages.StandardController stdOpportunity ){
		if( !Test.isRunningTest() ){
			stdOpportunity.addFields(
				new List<String>{
					'Id',
					'AccountId'
				}
			);
		}
		
		this.objOpportunity														= ( Opportunity ) stdOpportunity.getRecord();
		
		this.lstAvailableContacts												= new List<SMCContact>();
		
		this.setCurrentSMCContacts												= new Set<String>();
		
		this.strRenderMsgError													= 'visibility: hidden; position: absolute;';
		this.strShowCurrentContacts												= 'visibility: hidden; position: absolute;';
		
		setFieldSet();
		loadContacts();
		loadCurrentSMCContacts();
	}
	
	/**
	* @Method: 		loadCurrentSMCContacts
	* @param: 		N/A
	* @Description:	Carga los contactos SMC asociados actualmente.
	* @author 		Manuel Medina - 04122017
	*/
	public void loadCurrentSMCContacts(){
		this.lstCurrentSMCContacts												= new List<SMCContact>();
		List<SMC_ContactosSMC__c> lstCurrentSMCContacts							= new List<SMC_ContactosSMC__c>( [
																					SELECT Id,
																						SMC_CartaClick__c,
																						SMC_Contacto__c,
																						SMC_IdentificadorUnico__c
																					FROM SMC_ContactosSMC__c
																					WHERE SMC_CartaClick__c =: objOpportunity.Id
																				] );
																				
		for( SMC_ContactosSMC__c objCurrentSMCContact : lstCurrentSMCContacts ){
			SMCContact wpNEWSMCContact											= new SMCContact( objOpportunity.Id, mapContactById.get( objCurrentSMCContact.SMC_Contacto__c ) );
			wpNEWSMCContact.objSMCContact.Id									= objCurrentSMCContact.Id;
			this.lstCurrentSMCContacts.add( wpNEWSMCContact );
			
			setCurrentSMCContacts.add( String.valueOf( wpNEWSMCContact.objContact.Id ).left( 15 ) );
		}
		
		if( !lstCurrentSMCContacts.isEmpty() ){
			this.strShowCurrentContacts											= 'visibility: visible;';
		}
	}
	
	/**
	* @Method: 		loadContacts
	* @param: 		N/A
	* @Description:	Carga los contactos disponibles.
	* @author 		Manuel Medina - 22112017
	*/
	public void loadContacts(){
		try{
			Set<Id> setAccountIds												= new Set<Id>( getAccountIds() );
			
			List<Contact> lstContacts											= new List<Contact>( 
																					( List<Contact> ) Database.query(
																						'SELECT Id, ' +
																							String.join( new List<String>( mapFieldLabelByFieldName.keySet() ), ',' ) + ' ' +
																						'FROM Contact ' +
																						'WHERE AccountId IN: setAccountIds ' +
																						'ORDER BY Name ASC'
																					)
																				);
			
			for( Contact objContact : lstContacts ){
				this.lstAvailableContacts.add( new SMCContact( objContact ) );
			}
			
			this.mapContactById													= new Map<Id, Contact>( lstContacts );
			
			this.strRenderMsgError												= 'visibility: hidden; position: absolute;';
			
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			
			this.strMsgError													= e.getMessage();
			this.strRenderMsgError												= 'visibility: visible';
		}
	}
	
	/**
	* @Method: 		getAccountIds
	* @param: 		N/A
	* @Description:	Obtiene los Id de cuentas relacionados al SMC.
	* @author 		Manuel Medina - 22112017
	*/
	public Set<Id> getAccountIds(){
		Set<Id> setAccountIds													= new Set<Id>();
		
		List<Account> lstAccounts												= new List<Account>();
		lstAccounts																= [SELECT Id,
																						Name,
																						Ficticio__c,
																						(
																							SELECT Id,
																								Name,
																								Ficticio__c
																							FROM ChildAccounts
																						)
																					FROM Account
																					WHERE Id =: objOpportunity.AccountId
																				];
		
		for( Account objAccount : lstAccounts ){
			for( Account objChildAccount : objAccount.ChildAccounts ){
				setAccountIds.add( objChildAccount.Id );
			}
		}
		
		setAccountIds.add( objOpportunity.AccountId );
		
		return setAccountIds;
	}
	
	/**
	* @Method: 		addContacts
	* @param: 		N/A
	* @Description:	Asocia los contactos al SMC.
	* @author 		Manuel Medina - 22112017
	*/
	public void addContacts(){
		try{
			List<SMC_ContactosSMC__c> lstSMCContacts							= new List<SMC_ContactosSMC__c>();
			Boolean blnAtLeastOneWithoutEmail									= false;
			
			for( SMCContact wpSMCContact : lstAvailableContacts ){
				if( wpSMCContact.blnContactSelected && setCurrentSMCContacts.contains( String.valueOf( wpSMCContact.objContact.Id ).left( 15 ) ) ){
					wpSMCContact.blnContactSelected								= false;
					
					throw new AdminException( System.Label.SMC_MCMensaje1 );
				}
				
				if( wpSMCContact.blnContactSelected && !setCurrentSMCContacts.contains( String.valueOf( wpSMCContact.objContact.Id ).left( 15 ) ) && String.isBlank( wpSMCContact.objContact.Email ) ){
					wpSMCContact.blnContactSelected								= false;
					blnAtLeastOneWithoutEmail									= true;
				}
			}
			
			if( blnAtLeastOneWithoutEmail ){
				throw new AdminException( System.Label.SMC_MCMensaje2 );
			}
			
			for( SMCContact wpSMCContact : lstAvailableContacts ){
				if( wpSMCContact.blnContactSelected && !setCurrentSMCContacts.contains( String.valueOf( wpSMCContact.objContact.Id ).left( 15 ) ) ){
					wpSMCContact.blnContactSelected								= false;
					
					SMCContact wpNEWSMCContact									= new SMCContact( objOpportunity.Id, wpSMCContact.objContact );
					this.lstCurrentSMCContacts.add( wpNEWSMCContact );
					
					lstSMCContacts.add( wpNEWSMCContact.objSMCContact );
					
					this.setCurrentSMCContacts.add( String.valueOf( wpSMCContact.objContact.Id ).left( 15 ) );
				}
			}
			
			if( !lstSMCContacts.isEmpty() ){
				upsert lstSMCContacts SMC_IdentificadorUnico__c;
				
				this.strShowCurrentContacts										= 'visibility: visible;';
			}
			
			this.strRenderMsgError												= 'visibility: hidden; position: absolute;';
			
		}catch( AdminException e ){
			System.debug( '\n\n\n\t<<<<<<<<< AdminExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			
			this.strMsgError													= e.getMessage();
			this.strRenderMsgError												= 'visibility: visible';
		}
	}
	
	/**
	* @Method: 		addContacts
	* @param: 		N/A
	* @Description:	Asocia los contactos al SMC.
	* @author 		Manuel Medina - 22112017
	*/
	public void deleteSMCContact(){
		try{
			String strIdSMCContact												= ApexPages.currentPage().getParameters().get( 'idSMCContact' );
			String strIdContact													= ApexPages.currentPage().getParameters().get( 'idContact' );
			
			delete new SMC_ContactosSMC__c( Id = strIdSMCContact );
			
			this.setCurrentSMCContacts.remove( strIdContact.left( 15 ) );
			
			loadCurrentSMCContacts();
			
			if( !lstCurrentSMCContacts.isEmpty() ){
				this.strShowCurrentContacts										= 'visibility: visible;';
				
			}else{
				this.strShowCurrentContacts										= 'visibility: hidden; position: absolute;';
			}
			
		}catch( DMLException e ){
			String strDMLErrors													= '';
			
			for( Integer i=0; i<e.getNumDml(); i++ ){
				System.debug( '\n\n\t<<<<<<<<< DMLException >>>>>>>>> \n\t\t@@--> getDmlFieldNames > ' + e.getDmlFieldNames(i) + '\n\t\t@@--> getDmlId > ' + e.getDmlId(i) + '\n\t\t@@--> getDmlMessage > '+ e.getDmlMessage(i) + '\n\t\t@@--> getDmlType > '+ e.getDmlType(i) + '\n\n' );
				strDMLErrors													+= e.getDmlMessage(i) + '\n';
			}
			
			this.strMsgError													= strDMLErrors;
			this.strRenderMsgError												= 'visibility: visible';
		}
	}
	
	/**
	* @Method: 		setFieldSet
	* @param: 		N/A
	* @Description:	Obtener el conjunto de campos SMC_CamposVFSMC.
	* @author 		Manuel Medina - 22112017
	*/
	public void setFieldSet(){
		this.mapFieldLabelByFieldName											= new Map<String, String>();
		Map<String, Schema.SObjectType> mapSFDCObjects							= Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult sObjResult									= mapSFDCObjects.get( 'Contact' ).getDescribe();
		this.lstFieldSetMembers													= sObjResult.fieldSets.getMap().get( 'SMC_CamposVFSMC' ).getFields();
		
		for( FieldSetMember objFieldSetMember : lstFieldSetMembers ){
			this.mapFieldLabelByFieldName.put( objFieldSetMember.getFieldPath(), objFieldSetMember.getLabel() );
		}
	}
	
	/**
	* @Method: 		getFieldSet
	* @param: 		N/A
	* @Description:	Obtener el conjunto de campos SMC_CamposVFSMC.
	* @author 		Manuel Medina - 22112017
	*/
	public List<Schema.FieldSetMember> getFieldSet(){
		return lstFieldSetMembers;
	}
	
	/**
	* @Method: 		SMCContact
	* @param: 		N/A
	* @Description:	Estructura de datos para adminsitrar los datos del objeto SMC_ContactosSMC__c.
	* @author 		Manuel Medina - 22112017
	*/
	public class SMCContact{
		
		public SMC_ContactosSMC__c objSMCContact								{get; set;}
		public Contact objContact												{get; set;}
		
		public Boolean blnContactSelected										{get; set;}
		
		public SMCContact( Contact objContact ){
			this.objSMCContact													= new SMC_ContactosSMC__c();
			this.objContact														= new Contact();
			this.objContact														= objContact;
			
			this.blnContactSelected												= false;
		}
		
		public SMCContact( Id idOpportunity, Contact objContact ){
			this.objContact														= new Contact();
			this.objContact														= objContact;
			
			this.objSMCContact													= new SMC_ContactosSMC__c();
			this.objSMCContact.SMC_CartaClick__c								= idOpportunity;
			this.objSMCContact.SMC_Contacto__c									= objContact.Id;
			this.objSMCContact.SMC_IdentificadorUnico__c						= String.valueOf( idOpportunity ) + String.valueOf( objContact.Id );
		}
	}
	
	/**
	* @Method: 		AdminException
	* @param: 		N/A
	* @Description: Desencadenar excepciones personalizadas.
	* @author 		Manuel Medina - 05122017
	*/
	public class AdminException extends Exception{}
}