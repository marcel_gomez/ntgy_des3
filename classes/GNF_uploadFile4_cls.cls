/**
* VASS
* @author           Luis Igualada Flors luis.igualada@vass.es
* Project:          Gas Natural Fenosa
* Description:      Clase que genera el CSV a nivel de Consumo Click y lo envía a DELTA
*   
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-06-13      Luis Igualada (LI)      Definicion inicial de la clase.
*********************************************************************************************************/
public class GNF_uploadFile4_cls {

    /**
    * @Method:      uploadFile
    * @param:       List<SObject> lstRecords
    * @Description: Método que crear el fichero CSV y lo sube a DELTA.
    * @author       Luis Igualada Flors
    */


    public static string idlog;
    public static integer exitoso=0;
    public static integer fallido=0;

    public static void GeneraCSV(Set<Id> oppts){

         String csv = 'ID_CLICK;' +
                      'NISS;' +
                      'NISC;' +
                      'URL;' +
                      'NIF/CIF;'+
                      'TRADE_GROUP;' +
                      'NOMBRE;'+
                      'PAIS;' +
                      'CUPS_CUI;' +
                      'FX_COTIZACION;' +
                      'FX_FIRMA;' +
                      'TIPO_OPERACION;' +
                      'TIPO_COBERTURA;' +
                      'METODO_APLICACION;' +
                      'TIPO_FIJACION;'+
                      'TIPO_PRODUCTO;' +
                      'TIPO_SERVICIO;' +
                      'FRC;' +
                      'FORMULA_CLIENTE;' +
                      'FX_INICIO_PERIODO;' +
                      'FX_FIN_PERIODO;' +
                      'CV_FRC_FINAL(c€/kWh);' +
                      'CV_FRC_FINAL($/MMBTu);' +
                      'CV_CLIENTE_PERIODO;'+
                      'CV\'_CLIENTE_PERIODO(c€/kWh);'+
                      'CV\'_CLIENTE_PERIODO+F(c€/kWh);' +
                      'CV\'_CLIENTE_PERIODO+K(c€/kWh);' +
                      'SPREAD(c€/kWh);' +
                      'SPREAD_CLIENTE(c€/kWh);' +
                      'PERIODO_APLICACION;' +
                      'QC_KWH;' +
                      'QC_MMBTU;' +
                      'N_CONTRATO_ESPEJO;'+
                      'DESCUENTO_ABS;' +
                      'DESCUENTO_PORCENTAJE;' +
                      'FEE_GESTION(c€/kWh);' +
                      'PRIMA_COMERCIAL(c€/kWh);' +
                      'PRIMA_RIESGO(c€/kWh);' +
                      'FORMULA_DESTINO;'+
                      'TOP_TIPO_CLICKS;' +
                      'TOP_AGREGADO;' +
                      'TOP_COD_AGRUPADOR;' +
                      'Fee flexibilidad(c€/kWh);' +
                      'PF_Anexo;' +
                      'F;'+
                      'Liquidación ToP;'+
                      'Banda superior de consumo(%);' +
                      'Banda inferior de consumo(%);'+
                      'Num de operacion'+'\n';

        //String RecordTypeClickID = [Select RecordTypeId from Opportunity where RecordType.DeveloperName ='Click' limit 1].RecordTypeId;

        Set<Id> opptys = new Set<Id>();
        //Creamos un Set<Id> dentro del método para poder usarlo en el método dentro de la Query
        opptys = oppts;
        System.debug('Ids Oportunidades'+opptys);

        String consultaconsumos='SELECT Name,N_Clicks_Unclicks_ejecutados__c,GNF_fechaFinConsumo__c,GNF_fechaInicioConsumo__c,GNF_ConsumoTotalizadoPeriodo_kWh__c,GNF_Consumo_Totalizado_Periodo_Informado__c,Cotizacion_de_periodo__c,Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name,' +
                'Oportunidad_Punto_de_Suministro__r.Contrato__r.Name,Trade_Group__c, Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Name,' +
                'Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.NIF_CIF__c, Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Pais__c,' +
                'Oportunidad_Punto_de_Suministro__r.CUPS__c, Cotizacion_de_periodo__r.Opportunity__r.Fecha_Cotizacion__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Fecha_Firma__c, Cotizacion_de_periodo__r.Opportunity__r.RecordTypeId,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c, Cotizacion_de_periodo__r.Opportunity__r.Metodo_aplicacion__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Tipo_Fijacion__c, Cotizacion_de_periodo__r.Opportunity__r.Tipo_Producto__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c, Cotizacion_de_periodo__r.Opportunity__r.FRC__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Formula__c,Mes_de_aplicacion__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Periodicidad_ToP__c,Cotizacion_de_periodo__r.Name,'+
                'Cotizacion_de_periodo__r.CV_FRC_Final__c,Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c,'+
                'Q_comprometida_kWh__c, Contrato_Espejo__c, Cotizacion_de_periodo__r.Opportunity__r.Prima_de_gestion__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Prima_comercial__c,Cotizacion_de_periodo__r.Opportunity__r.Prima_de_riesgo__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Fee_flexibilidad__c,Cotizacion_de_periodo__r.Opportunity__r.Banda_superior_de_consumo__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Banda_inferior_de_consumo__c,Cotizacion_de_periodo__r.Opportunity__r.N_Maximo_Operaciones__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.Formula_Destino__c,Cotizacion_de_periodo__r.Opportunity__r.Tipo_ToP_Clicks__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.ToP_agregado__c,Cotizacion_de_periodo__r.Opportunity__r.Codigo_Agrupador_ToP__c,' +
                'Cotizacion_de_periodo__r.Fecha_limite_de_validez__c,Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c,Cotizacion_de_periodo__r.CV_FRC_c_kWh__c,' +
                'Cotizacion_de_periodo__r.CV_FRC_Inicial__c,Cotizacion_de_periodo__r.CV_Cliente__c,Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c,GNF_IDTransaccion__c,' +
                'GNF_ValorF__c,Cotizacion_de_periodo__r.GNF_ValorK__c,Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c,' +
                'GNF_descuentoClickAbs__c,GNF_descuentoClickPrcnt__c,PFAnexo__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c,Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c,' +
                'Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName,Cotizacion_de_periodo__r.Opportunity__r.ID_Opportunity__c,Cotizacion_de_periodo__r.Opportunity__r.Id '+
                'FROM Consumo_Click__c ' +
                'WHERE Oportunidad_Punto_de_Suministro__r.Concepto__r.Tipo_Tarifa__c != \'Cantidades comprometidas (POR TRAMOS)\' and ' +
                'Cotizacion_de_periodo__r.Opportunity__r.Id IN: opptys ORDER BY Cotizacion_de_periodo__r.Opportunity__r.id,Oportunidad_Punto_de_Suministro__r.CUPS__c,CreatedDate Asc, GNF_fechaInicioConsumo__c Asc';

        //Lista de Consumos Click
        List<Consumo_Click__c> consumosclick = new list<Consumo_Click__c>();
        consumosclick = database.query(consultaconsumos);

        System.debug('Tamaño Consulta Lista Consumos click: '+consumosclick.size());

        Datetime dt;
        String FechaInicioValidezCotizacion;
        String FechaLimiteValidezCotizacion;

        Datetime dia = system.Datetime.now();
        List<Consumo_Click__c> consumosDELTA = new List<Consumo_Click__c>();

        Map<Id,Opportunity> MapaOportunidades = new Map<Id, Opportunity>();

        //Campo usado para el objeto LOG
        string fechasubida = System.now().format('yyyyMMddHHmmssSSS');

        //F
        double f = 0;

        //K
        double k = 0;

        //Q Comprometida KWh
        double QComprometidaKWh = 0;

        //Consumo Totalizado periodo KWh
        double ConsumoTotalizadoPeriodoKWh = 0;
        
        //CV_Cliente_c
        double CVPrimaClienteC = 0;

        //GNF_CVClientePrima_Click__c + f
        double cvclientemasf = 0;

        //GNF_CVClientePrima_Click__c +f +k
        double cvclientemask = 0;
        
        //Recorremos la lista de todos los Consumos Click
        for ( Consumo_Click__c cc : consumosclick ) {

            //Actualizamos el Flag de la Oportunidad
            Opportunity oferta = New Opportunity(Id= cc.Cotizacion_de_periodo__r.Opportunity__r.Id,GNF_Enviado_Edelta__c = true,
            GNF_IDTransaccion__c=fechasubida);

            //Actualizamos las Oportunidades
            MapaOportunidades.put(cc.Cotizacion_de_periodo__r.Opportunity__c,oferta);

            //Activamos el Flag de Enviado Edelta a nivel de Oportunidad
            //cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c= true;

            //Guardamos la fecha del Log en Consumos Click
            cc.GNF_IDTransaccion__c=fechasubida;
            //Guardamos la fecha del Log en la Oportunidad
            cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_IDTransaccion__c=fechasubida;

            //Agregamos Consumos click a la lista DELTA para luego hacer un update
            consumosDELTA.add(cc);
            
            //Extraemos la fecha del DateTime de la fecha límite de validez de la cotización y le damos formato
            if (cc.Cotizacion_de_periodo__r.Fecha_limite_de_validez__c != null){
                Datetime fechalimval = cc.Cotizacion_de_periodo__r.Fecha_limite_de_validez__c;
                FechaLimiteValidezCotizacion = fechalimval.format('dd/MM/yyyy');
            }

            //Extraemos la fecha de Inicio de validez de la cotización y le damos formato
            if(cc.Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c != null){
                Date fechainicioval=cc.Cotizacion_de_periodo__r.GNF_FechaInicioValidez__c;
                Time myTime = Time.newInstance(12, 0, 0, 0);
                Datetime finicioval = DateTime.newInstance(fechainicioval, myTime);
                FechaInicioValidezCotizacion = finicioval.format('dd/MM/yyyy');
            }

            //Cálculo de la F
            if(cc.GNF_ValorF__c != null){
                f=Double.valueOf(cc.GNF_ValorF__c);
            }

            //Cálculo de la K
            if(cc.Cotizacion_de_periodo__r.GNF_ValorK__c != null){
                k=Double.valueOf(cc.Cotizacion_de_periodo__r.GNF_ValorK__c);
            }
            
            //Comprobamos que CV_Cliente_c no sea null
            if (cc.Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c !=null){
                CVPrimaClienteC = Double.valueOf(cc.Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c);
            }

            if(cc.Q_comprometida_kWh__c != null){
                QComprometidaKWh = cc.Q_comprometida_kWh__c;
            }

            if (cc.GNF_ConsumoTotalizadoPeriodo_kWh__c != null &&  cc.GNF_Consumo_Totalizado_Periodo_Informado__c != null){
                ConsumoTotalizadoPeriodoKWh = cc.GNF_Consumo_Totalizado_Periodo_Informado__c;
            }

            //Calculamos si el Click es de España o Portugal para el cálculo de la K y la F
            if(cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Pais__c == 'Portugal'){
                cvclientemasf = 0;
                cvclientemask= CVPrimaClienteC+k;
            }
            else{
                //Es España
                cvclientemasf=CVPrimaClienteC+f;
                cvclientemask=0;
            }

            //Transformación fechas
            string fechacotizacion='';
            string fechafirma='';
            string fechainicioconsumo='';
            string fechafinconsumo='';

            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Cotizacion__c != null){
                //Datetime
                fechacotizacion =cc.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Cotizacion__c.format('dd/MM/yyyy');
            }

            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Firma__c != null){
                //Date
                Date fechadefir= cc.Cotizacion_de_periodo__r.Opportunity__r.Fecha_Firma__c;
                Time myTime = Time.newInstance(12, 0, 0, 0);
                Datetime firma = DateTime.newInstance(fechadefir, myTime);
                fechafirma = firma.format('dd/MM/yyyy');
            }

            if (cc.GNF_fechaInicioConsumo__c != null){
                //Date
                Date fechainicioco = cc.GNF_fechaInicioConsumo__c;
                Time myTime = Time.newInstance(12, 0, 0, 0);
                Datetime finicioconsumo = Datetime.newInstance(fechainicioco,myTime);
                fechainicioconsumo = finicioconsumo.format('dd/MM/yyyy');
            }
            if(cc.GNF_fechaFinConsumo__c != null){
                //Date
                Date fechafco = cc.GNF_fechaFinConsumo__c;
                Time myTime = Time.newInstance(12, 0, 0, 0);
                Datetime ffinconsumo = Datetime.newInstance(fechafco,myTime);
                fechafinconsumo = ffinconsumo.format('dd/MM/yyyy');
            }

            /*string fechaBD = '2017-06-29T08:19:38.000+0000';
            //fechaBD= '2017-05-01';
            String fechaformato='';
            if (fechaBD.length() > 10){
                Datetime fechahora;
                fechahora = date.valueOf(fechaBD);

                fechaformato = fechahora.format('dd/MM/yyyy');
            //fecha.day()+'/'+fecha.month()+'/'+fecha.year();
                System.debug(fechaformato);
            }
            else{
                Date fecha = date.valueOf(fechaBD);


                Date myDate = Date.newInstance(fecha.year(), fecha.Month(), fecha.day());
                Time myTime = Time.newInstance(12, 0, 0, 0);
                DateTime dt = DateTime.newInstance(myDate, myTime);
                fechaformato=dt.format('dd/MM/yyyy');
                System.debug(dt);
                }
            */


            /*System.debug( '\n\n\n@@--> Valores  \n' +
                '\t\n --> QComprometidaKWh > ' + QComprometidaKWh +
                '\t\n --> QComprometidaKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c > ' + QComprometidaKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c +
                '\t\n --> QComprometidaKWh.divide( 1, 0 ) > ' + QComprometidaKWh.divide( 1, 0 ) +
                '\t\n --> QComprometidaKWh.divide( GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c, 0 ) > ' + QComprometidaKWh.divide( GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c, 0 ) +
                '\t\n --> Math.round( QComprometidaKWh ) > ' + Math.round( QComprometidaKWh ) +
                '\t\n --> Math.round( QComprometidaKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c ) > ' + Math.round( QComprometidaKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c )
            );*/

            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Periodicidad_ToP__c!='Trimestral'){
                //Creamos el CSV
                csv += cc.Cotizacion_de_periodo__r.Opportunity__r.ID_Opportunity__c + ';' +
                        cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name+';'+
                        cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Name+';'+
                        'https://'+System.URL.getSalesforceBaseUrl().getHost()+'/ui/support/servicedesk/ServiceDeskPage#/'+cc.Cotizacion_de_periodo__r.Opportunity__r.Id+';'+
                        cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.NIF_CIF__c+';'+
                        cc.Trade_Group__c+';'+ +'"'+
                        cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Name+'";'+
                        cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Pais__c+';'+
                        cc.Oportunidad_Punto_de_Suministro__r.CUPS__c+';'+
                        fechacotizacion+';'+
                        fechafirma+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Metodo_aplicacion__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_Fijacion__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_Producto__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.FRC__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Formula__c+';'+
                        fechainicioconsumo+';'+
                        fechafinconsumo+';'+
                        cc.Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c+';'+
                        cc.Cotizacion_de_periodo__r.CV_FRC_Final__c+';'+
                        cc.Cotizacion_de_periodo__r.CV_Cliente__c+';'+
                        string.valueOf(Decimal.valueOf(CVPrimaClienteC).divide(1,4))+';'+
                        string.valueof(Decimal.valueOf(cvclientemasf).divide(1,4))+';'+
                        string.valueof(Decimal.valueOf(cvclientemask).divide(1,4))+';'+
                        string.valueof(cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c)+';'+
                        string.valueof(cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c)+';'+
                        cc.Mes_de_aplicacion__c+';'+
                        Math.round(QComprometidaKWh) +';'+
                        Math.round(QComprometidaKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c) +';'+
                        cc.Contrato_Espejo__c+';'+
                        cc.GNF_descuentoClickAbs__c+';'+
                        cc.GNF_descuentoClickPrcnt__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_de_gestion__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_comercial__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_de_riesgo__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Formula_Destino__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_ToP_Clicks__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.ToP_agregado__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Codigo_Agrupador_ToP__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Fee_flexibilidad__c+';'+
                        cc.PFAnexo__c+';'+
                        f+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Periodicidad_ToP__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Banda_superior_de_consumo__c+';'+
                        cc.Cotizacion_de_periodo__r.Opportunity__r.Banda_inferior_de_consumo__c+';'+
                        (cc.N_Clicks_Unclicks_ejecutados__c != null ? String.valueOf(cc.N_Clicks_Unclicks_ejecutados__c) : '0')+'\n';
            }
            else{ //Aplicamos una lógica diferente para sacar únicamente un registro mensualizado del trimestre
                if (cc.GNF_ConsumoTotalizadoPeriodo_kWh__c != null){
                    csv += cc.Cotizacion_de_periodo__r.Opportunity__r.ID_Opportunity__c + ';' +
                            cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.Name+';'+
                            cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Name+';'+
                            'https://'+System.URL.getSalesforceBaseUrl().getHost()+'/ui/support/servicedesk/ServiceDeskPage#/'+cc.Cotizacion_de_periodo__r.Opportunity__r.Id+';'+
                            cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.NIF_CIF__c+';'+
                            cc.Trade_Group__c+';'+ +'"'+
                            cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Name+'";'+
                            cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.Cliente__r.Pais__c+';'+
                            cc.Oportunidad_Punto_de_Suministro__r.CUPS__c+';'+
                            fechacotizacion+';'+
                            fechafirma+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Metodo_aplicacion__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_Fijacion__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_Producto__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.FRC__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Formula__c+';'+
                            FechaInicioValidezCotizacion+';'+
                            FechaLimiteValidezCotizacion+';'+
                            cc.Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c+';'+
                            cc.Cotizacion_de_periodo__r.CV_FRC_Final__c+';'+
                            cc.Cotizacion_de_periodo__r.CV_Cliente__c+';'+
                            string.valueOf(Decimal.valueOf(CVPrimaClienteC).divide(1,4))+';'+
                            string.valueof(Decimal.valueOf(cvclientemasf).divide(1,4))+';'+
                            string.valueof(Decimal.valueOf(cvclientemask).divide(1,4))+';'+
                            string.valueof(cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c)+';'+
                            string.valueof(cc.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c)+';'+
                            string.valueof(cc.Cotizacion_de_periodo__r.Name).substring(0,2)+';'+
                            Math.round(ConsumoTotalizadoPeriodoKWh) +';'+
                            Math.round(ConsumoTotalizadoPeriodoKWh/GNF_ValoresPredeterminados__c.getInstance().GNF_FactorConversionKWhMMBTU__c) +';'+
                            cc.Contrato_Espejo__c+';'+
                            cc.GNF_descuentoClickAbs__c+';'+
                            cc.GNF_descuentoClickPrcnt__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_de_gestion__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_comercial__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Prima_de_riesgo__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Formula_Destino__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_ToP_Clicks__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.ToP_agregado__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Codigo_Agrupador_ToP__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Fee_flexibilidad__c+';'+
                            cc.PFAnexo__c+';'+
                            f+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Periodicidad_ToP__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Banda_superior_de_consumo__c+';'+
                            cc.Cotizacion_de_periodo__r.Opportunity__r.Banda_inferior_de_consumo__c+';'+
                            (cc.N_Clicks_Unclicks_ejecutados__c != null ? String.valueOf(cc.N_Clicks_Unclicks_ejecutados__c) : '0')+'\n';
                }

            }


        }

        //Cambios las comas del CSV por puntos y coma y los NULL por cadena vacías
        //csv= csv.replace('null','').replace(',',';');
        csv= csv.replace('null','');

        //Llamada a metodo futuro
        envioficherobus(csv,fechasubida);

        //Actualizamos Consumos Click con el Flag
        update consumosDELTA;

        //Actualizamos las Oportunidades
        Update MapaOportunidades.values();



    }

    public static void envioficherobus(String csv, string fechasubida){
        //Envio de fichero a DELTA
        Blob file_body = Blob.valueof(csv);
        String file_name = 'ConsumosClick'+'-'+fechasubida+'.csv';


        String boundary = '----------------------------741e90d31eff';

        String header = '--'+boundary+'\r\nContent-Disposition: form-data; name="file"; filename="'+file_name+'"\r\nContent-Type: text/csv'; // added '\r's removed ';' see Tim Smith's comment
        String footer = '\r\n--'+boundary+'--';

        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('='))
            {
                header+=' ';
                headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
            }

        String bodyJSON = '--' + boundary + '\r\n';
        bodyJSON+= 'Content-Disposition: form-data; name="param1"\r\n\r\n';
        //bodyJSON+= 'Content-Type: application/json; charset=utf-8\r\n\r\n';

        //Generamos un GUID/UUID único cada vez que se vaya a enviar el fichero a DELTA
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);

        bodyJSON+= '{"GNFHeader":';
        //bodyJSON+= '{"IDServicio":"GRABACION","IDPeticion":"5be28b20-714a-9dcb-9d30-efd87ce277b3","IDOperacion":"WSL","IDCliente":"SALESFORCE"},';
        bodyJSON+= '{"IDServicio":"GRABACION","IDPeticion":"'+guid+'","IDOperacion":"DELTA","IDCliente":"SALESFORCE"},';
        //bodyJSON+= '"extension":"csv"';
        bodyJSON+= '"Nombrefichero":"'+file_name+'"';
        bodyJSON+= '}';

        String bodyJSONEncoded = EncodingUtil.base64Encode(Blob.valueOf(bodyJSON+'\r\n\r\n'));
        while(bodyJSONEncoded.endsWith('='))
            {
                bodyJSON+=' ';
                bodyJSONEncoded = EncodingUtil.base64Encode(Blob.valueOf(bodyJSON+'\r\n\r\n'));
            }


        String bodyEncoded = EncodingUtil.base64Encode(file_body);
        String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));

        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
        if(last4Bytes.endsWith('='))
        {
            Blob decoded4Bytes = EncodingUtil.base64Decode(last4Bytes);
            HttpRequest tmp = new HttpRequest();
            tmp.setBodyAsBlob(decoded4Bytes);
            String last4BytesFooter = tmp.getBody()+footer;
            bodyBlob = EncodingUtil.base64Decode(bodyJSONEncoded+headerEncoded+bodyEncoded.substring(0,bodyEncoded.length()-4)+EncodingUtil.base64Encode(Blob.valueOf(last4BytesFooter)));
        }
        else
        {
            bodyBlob = EncodingUtil.base64Decode(bodyJSONEncoded+headerEncoded+bodyEncoded+footerEncoded);
        }

        system.debug('body : \n' + bodyBlob.toString());

        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setMethod('POST');
        req.setEndpoint(GNF_ValoresPredeterminados__c.getInstance().GNF_Envio_Delta_Endpoint__c);
        req.setBodyAsBlob(bodyBlob);
        req.setTimeout(120000);

        //Generamos un nuevo Log
        GNF_Log__c[] logs = new List<GNF_Log__c>();

        Http http = new Http();
        HTTPResponse res = http.send(req);
        Attachment objattachment = new Attachment();

        //SI EL ENVÍO DEL FICHERO A DELTA HA SIDO CORRECTO
        if (res.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            // Cast the values in the 'animals' key as a list
            System.debug('Conexión exitosa con DELTA:' + results);

            //Si el Custom Setting de Guardar Fichero está a true
            if (GNF_ValoresPredeterminados__c.getInstance().GNF_LogGuardarFichero__c) {
                objattachment.Body = Blob.valueOf(csv);
                objattachment.ParentId = idlog;/* variable id log */
                objattachment.Name=fechasubida+'-OK'+'.csv';

                //Insertamos el Attachment. Con esto ya se añadiría al Registro Log
                insert objattachment;
            }

            //Incrementamos la variable de exitoso
            exitoso++;

        }//SI EL ENVÍO DEL FICHERO A DELTA HA SIDO INCORRECTO
            else{

                //Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                // Cast the values in the 'animals' key as a list
                //System.debug('Fallo Conexión DELTA:' + results);
                
                system.debug('Fallo Conexión res.getBody():' + res.getBody());              

                //Si el Custom Setting de Guardar Fichero está a true
                if (GNF_ValoresPredeterminados__c.getInstance().GNF_LogGuardarFichero__c){
                    objattachment.Body=Blob.valueOf(csv);
                    objattachment.ParentId=idlog;
                    objattachment.Name=fechasubida+'-KO'+'.csv';
                    //Insertamos el Attachment. Con esto ya se añadiría al Registro Log
                    insert objattachment;
                }

                //Incrementamos la variable de fallidos
                fallido++;
            }

        //Si el Custom Setting está a true
        //if (GNF_ValoresPredeterminados__c.getInstance().GNF_LogGuardarFichero__c){
            //GUARDAMOS EL FICHERO EN SALESFORCE
        //  ContentVersion file = new ContentVersion( title = 'ConsumosClick.csv', versionData = Blob.valueOf( csv ), pathOnClient = '/ConsumosClick.csv');
        //  insert file;
        //}
    }

}