/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_PDFGenerator_ctr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-07		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_PDFGenerator_ctr_TEST {
	
	public static void createData(){
		List<sObject> lstPlantillas											= Test.loadData( GNF_Plantilla__c.sobjectType, 'GNF_PlantillaTST' );
		List<sObject> lstSecciones											= Test.loadData( GNF_Seccion__c.sobjectType, 'GNF_SeccionTST' );
		List<sObject> lstComponentes										= Test.loadData( GNF_Componente__c.sobjectType, 'GNF_ComponentesTST' );
		List<sObject> lstCondiciones										= Test.loadData( GNF_Condiciones__c.sobjectType, 'GNF_CondicionesTST' );
		List<sObject> lstPDFSettings										= Test.loadData( GNF_PDFSettings__c.sobjectType, 'GNF_PDFSettingsTST' );
	}
	
	static testMethod void scenarioOne_sync(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id,
																					GNF_EsIterable__c
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
																			
			objTemplate.GNF_EsIterable__c									= true;
			update objTemplate;
			
			List<GNF_Componente__c> lstComponents							= new List<GNF_Componente__c>( [
																				SELECT Id,
																					GNF_ObjetoReferencia__c
																				FROM GNF_Componente__c
																				WHERE GNF_Seccion__r.GNF_Plantilla__c =: objTemplate.Id
																				AND RecordType.DeveloperName IN ( 'GNF_Parrafo', 'GNF_TextoDescriptivo' )
																			] );
																			
			for( GNF_Componente__c objComponent : lstComponents ){
				objComponent.GNF_ObjetoReferencia__c						= 'GNF_Seccion__c';
			}
			
			update lstComponents;
			
			ApexPages.StandardController sCtrTemplate						= new ApexPages.StandardController( objTemplate );
			ApexPages.currentPage().getParameters().put( 'attach', 'true' );
			GNF_PDFTemplates_ctr ctrPDFTemplates							= new GNF_PDFTemplates_ctr( sCtrTemplate );
			ctrPDFTemplates.setSectionRichHTML();
			
			GNF_PDFTemplates_cls.getPDFTemplate( objTemplate.Id );
			GNF_PDFTemplates_cls.clonePDFTemplate( objTemplate.Id );
			
			GNF_PDFGenerator_ctr ctrPDFGeneratorIterable					= new GNF_PDFGenerator_ctr( 'PDF_TestClass', objTemplate.Id, 'GNF_Plantilla__c' );
			ctrPDFGeneratorIterable.strSObjectType							= 'Opportunity';
			ctrPDFGeneratorIterable.getReferenceTo( 'Account' );
			
			GNF_PDFGenerator_ctr ctrPDFGenerator							= new GNF_PDFGenerator_ctr();
			ctrPDFGenerator.strTemplateUniqueName							= 'PDF_TestClass';
			ctrPDFGenerator.strMainRecordId									= objTemplate.Id;
			ctrPDFGenerator.strSObjectType									= 'GNF_Plantilla__c';
			ctrPDFGenerator.blnIsAsync										= false;
			ctrPDFGenerator.attachPDF();
			
		Test.stopTest();
	}
	
	static testMethod void scenarioOne_sync2(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id,
																					GNF_EsIterable__c
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
																			
			objTemplate.GNF_EsIterable__c									= true;
			update objTemplate;
			
			List<GNF_Componente__c> lstComponents							= new List<GNF_Componente__c>( [
																				SELECT Id,
																					GNF_ObjetoReferencia__c
																				FROM GNF_Componente__c
																				WHERE GNF_Seccion__r.GNF_Plantilla__c =: objTemplate.Id
																				AND RecordType.DeveloperName IN ( 'GNF_Parrafo', 'GNF_TextoDescriptivo' )
																			] );
																			
			for( GNF_Componente__c objComponent : lstComponents ){
				objComponent.GNF_ObjetoReferencia__c						= 'GNF_Seccion__c';
			}
			
			update lstComponents;
			
			ApexPages.StandardController sCtrTemplate						= new ApexPages.StandardController( objTemplate );
			ApexPages.currentPage().getParameters().put( 'attach', 'true' );
			
			GNF_PDFGenerator_ctr ctrPDFGenerator							= new GNF_PDFGenerator_ctr( 'PDF_TestClass', objTemplate.Id, 'GNF_Plantilla__c' );
			ctrPDFGenerator.blnIsAsync										= false;
			ctrPDFGenerator.attachPDF();
			
			GNF_PDFAsincronos__c objAsyncPDF								= new GNF_PDFAsincronos__c();
			objAsyncPDF														= [SELECT Id,
																					GNF_Objeto__c,
																					GNF_IdRegistro__c,
																					GNF_Plantilla__c,
																					GNF_TiempoCreacion__c,
																					GNF_PDFAdjuntado__c
																				FROM GNF_PDFAsincronos__c
																				LIMIT 1
																			];
																			
			objAsyncPDF.GNF_AsyncApexJobId__c								= '';
			objAsyncPDF.GNF_PDFAdjuntado__c									= true;
			update objAsyncPDF;
			
		Test.stopTest();
	}
	
	static testMethod void scenarioOne_async(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id,
																					GNF_EsIterable__c
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
																			
			objTemplate.GNF_EsIterable__c									= true;
			update objTemplate;
			
			List<GNF_Componente__c> lstComponents							= new List<GNF_Componente__c>( [
																				SELECT Id,
																					GNF_ObjetoReferencia__c
																				FROM GNF_Componente__c
																				WHERE GNF_Seccion__r.GNF_Plantilla__c =: objTemplate.Id
																				AND RecordType.DeveloperName IN ( 'GNF_Parrafo', 'GNF_TextoDescriptivo' )
																			] );
																			
			for( GNF_Componente__c objComponent : lstComponents ){
				objComponent.GNF_ObjetoReferencia__c						= 'GNF_Seccion__c';
			}
			
			update lstComponents;
			
			ApexPages.StandardController sCtrTemplate						= new ApexPages.StandardController( objTemplate );
			GNF_PDFTemplates_ctr ctrPDFTemplates							= new GNF_PDFTemplates_ctr( sCtrTemplate );
			ctrPDFTemplates.setSectionRichHTML();
			
			GNF_PDFTemplates_cls.getPDFTemplate( objTemplate.Id );
			GNF_PDFTemplates_cls.clonePDFTemplate( objTemplate.Id );
			
			GNF_PDFGenerator_ctr ctrPDFGeneratorIterable					= new GNF_PDFGenerator_ctr( 'PDF_TestClass', objTemplate.Id, 'GNF_Plantilla__c' );
			ctrPDFGeneratorIterable.strSObjectType							= 'Opportunity';
			ctrPDFGeneratorIterable.getReferenceTo( 'Account' );
			
			GNF_PDFGenerator_ctr ctrPDFGenerator							= new GNF_PDFGenerator_ctr();
			ctrPDFGenerator.strTemplateUniqueName							= 'PDF_TestClass';
			ctrPDFGenerator.strMainRecordId									= objTemplate.Id;
			ctrPDFGenerator.strSObjectType									= 'GNF_Plantilla__c';
			ctrPDFGenerator.blnIsAsync										= true;
			ctrPDFGenerator.startAsync();
			
		Test.stopTest();
	}
	
	static testMethod void scenarioTwo(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id,
																					GNF_EsIterable__c
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
																			
			objTemplate.GNF_EsIterable__c									= false;
			objTemplate.GNF_AdjuntarPDF__c									= true;
			update objTemplate;
			
			ApexPages.StandardController sCtrTemplate						= new ApexPages.StandardController( objTemplate );
			GNF_PDFTemplates_ctr ctrPDFTemplates							= new GNF_PDFTemplates_ctr( sCtrTemplate );
			ctrPDFTemplates.setSectionRichHTML();
			
			GNF_PDFTemplates_cls.getPDFTemplate( objTemplate.Id );
			GNF_PDFTemplates_cls.clonePDFTemplate( objTemplate.Id );
			
			GNF_PDFGenerator_ctr ctrPDFGenerator							= new GNF_PDFGenerator_ctr();
			ctrPDFGenerator.strTemplateUniqueName							= 'PDF_TestClass';
			ctrPDFGenerator.strMainRecordId									= objTemplate.Id;
			ctrPDFGenerator.strSObjectType									= 'GNF_Plantilla__c';
			ctrPDFGenerator.blnIsAsync										= true;
			ctrPDFGenerator.startAsync();
			
		Test.stopTest();
	}
	
	static testMethod void scenarioThree_sync(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
			
			GNF_PDFRequest_ws.sendPDFRequest( objTemplate.Id, false, false );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioThree_async(){
		createData();
		
		Test.startTest();
		
			GNF_Plantilla__c objTemplate									= new GNF_Plantilla__c();
			objTemplate														= [SELECT Id
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
			
			Test.setMock( HttpCalloutMock.class, new GNF_PDFRequest_mws() );
			GNF_PDFRequest_ws.sendPDFRequest( objTemplate.Id, true, true );
			GNF_PDFRequest_ws.sendPDF( objTemplate.Id, true, true );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioThree_async2(){
		createData();
		
		Test.startTest();
		
			GNF_Componente__c objComponent									= new GNF_Componente__c();
			objComponent													= [SELECT Id
																				FROM GNF_Componente__c
																				WHERE GNF_Seccion__r.GNF_Plantilla__r.GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
			
			Test.setMock( HttpCalloutMock.class, new GNF_PDFRequest_mws() );
			GNF_PDFRequest_ws.attachPDFError( objComponent.Id, 'FakeError' );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioFour(){
		createData();
		
		GNF_Plantilla__c objTemplate										= new GNF_Plantilla__c();
		objTemplate															= [SELECT Id
																				FROM GNF_Plantilla__c
																				WHERE GNF_NombreUnico__c = 'PDF_TestClass'
																				LIMIT 1
																			];
		
		GNF_PDFAsincronos__c objAsyncPDF									= new GNF_PDFAsincronos__c();
		objAsyncPDF.GNF_Identificador__c									= 'PDF_TestClass' + String.valueOf( objTemplate.Id ).left( 15 ) + 'GNF_Plantilla__c';
		objAsyncPDF.GNF_Plantilla__c										= 'PDF_TestClass';
		objAsyncPDF.GNF_IdRegistro__c										= objTemplate.Id;
		objAsyncPDF.GNF_Objeto__c											= 'GNF_Plantilla__c';
		objAsyncPDF.GNF_PDFAdjuntado__c										= false;
		objAsyncPDF.GNF_Iterating__c										= true;
		objAsyncPDF.GNF_AsyncApexJobId__c									= String.valueOf( 'TST' ).left( 15 );
		upsert objAsyncPDF GNF_Identificador__c;
		
		Attachment objAttachment											= new Attachment();
		objAttachment.Name													= 'PDF_TestClass' + String.valueOf( objTemplate.Id ).left( 15 ) + 'GNF_Plantilla__c_0';
		objAttachment.ParentId												= objAsyncPDF.Id;
		objAttachment.Body													= Blob.valueOf( 'TST' );
		insert objAttachment;
		
		Test.startTest();
		
			GNF_PDFGenerator_ctr ctrPDFGeneratorIterable					= new GNF_PDFGenerator_ctr( 'PDF_TestClass', objTemplate.Id, 'GNF_Plantilla__c' );
			ctrPDFGeneratorIterable.getAsyncPDF();
			
			GNF_PDFGenerator_bch bchPDFGenerator							= new GNF_PDFGenerator_bch( 'PDF_TestClass', objTemplate.Id, 'GNF_Plantilla__c' );
			bchPDFGenerator.ctrPDFGenerator									= ctrPDFGeneratorIterable;
			
			Database.executeBatch( bchPDFGenerator, 1 );
			
		Test.stopTest();
	}
}