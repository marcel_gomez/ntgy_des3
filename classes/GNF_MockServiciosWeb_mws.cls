@isTest
global class GNF_MockServiciosWeb_mws implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest httpREQ) {
        HttpResponse httpRespAliado = new HttpResponse();
        
        httpRespAliado.setHeader('Content-Type', 'application/json');
        httpRespAliado.setStatusCode(200);
        httpRespAliado.setBody('{"rta":"200"}');
        return httpRespAliado;

      
    }
}