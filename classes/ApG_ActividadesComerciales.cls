/**
* VASS Latam
* @author           Victor Velandia victor.velandia@vass.es
* Project:          Gas Natural Fenosa - AppGestor
* Description:      Clase invocable para VisualForce ApG_PosicionGlobal.page a partir de una ficha "Posicion Global"
*                   Esta clase se llama en ApG_PosicionGlobalController
*
*            No.     Date            Author                  Description
*            -----   ----------      --------------------    ---------------
* @version   1.0     2018-06-27      Victor Velandia (VV)    Clase para mostrar las Actividades Comerciales en informacion Comercial   
**************************************************************************************************************************************/

public with sharing class ApG_ActividadesComerciales implements ApG_PosicionGlobalReportInterface {
	public ApG_ActividadesComerciales() {
		system.debug('Ejecutando ApG_ActividadesComerciales');
    }

    public list<map<string, string>> getsObjectInformation(string itemToGetFrom, string pageType, string forceUser){

        boolean blnisHierarchy = !pageType.equalsIgnoreCase('status');
        /*Recoger la custom metadata type, sacar sus campos, poner los campos en el mapa según el nivel especificado y si es jerarquía, primero la jerarquía, si es estado, primero el estado.*/
        ApG_GlobalPositionItem__mdt sqlGPI = [SELECT Id, lvl0__c, lvl1__c, lvl2__c, lvl3__c, lvl4__c 
                                                    FROM ApG_GlobalPositionItem__mdt 
                                                    WHERE DeveloperName =: 'SrsActivasEstado'];
        system.debug('globalPositionItem recuperado '+sqlGPI);

        map<string, string>                 mapfields           = fillFieldList(sqlGPI);
        list<map<string, string>>           listMapReturn       = new list<map<string, string>>();
        map<string, map<string, string>>    maphierarchy        = ApG_PosicionGlobalController.getHierarchyMap(forceUser);
        list<Actividad_Comercial__c>        listActComercial    = new list<Actividad_Comercial__c>();

        listActComercial = [SELECT Id, Estado__c, CreatedDate, Name, Gestor__c, Gestor__r.ID_Empleado__c
                                FROM Actividad_Comercial__c 
                                    WHERE Name >: itemToGetFrom AND Gestor__r.ID_Empleado__c IN: maphierarchy.keySet()
                                    ORDER BY Name ASC 
                                    LIMIT 7000];
        system.debug('lista de Actividad_Comercial' + listActComercial);

        for(Actividad_Comercial__c objActComercial : listActComercial){
            map<string, string> mapinst = new map<string, string>();
            if(blnisHierarchy){
                mapinst.putAll(maphierarchy.get(objActComercial.Gestor__r.ID_Empleado__c));
                integer intlevelNumber = mapinst.keySet().size();
                for(string strfield : mapfields.keySet()){
                    string strtheLevel = 'lvl' +intlevelNumber+'__c';
                    mapinst.put(strtheLevel, string.valueOf(objActComercial.get(mapfields.get(strfield))));
                    intlevelNumber++;
                }
            }else{
                integer intlevelNumber = 0;
                for(string strfield : mapfields.keySet()){
                    string strtheLevel = 'lvl' +intlevelNumber+'__c';
                    mapinst.put(strtheLevel, string.valueOf(objActComercial.get(mapfields.get(strfield))));
                    intlevelNumber++;
                }
                for(string strfield : maphierarchy.get(objActComercial.Gestor__r.ID_Empleado__c).keySet()){
                    string strtheLevel = 'lvl' +intlevelNumber+'__c';
                    mapinst.put(strtheLevel, maphierarchy.get(objActComercial.Gestor__r.ID_Empleado__c).get(strfield));
                    intlevelNumber++;
                }
            }
            mapinst.put('fieldToGetFrom', string.valueOf(objActComercial.Name));
            mapinst.put('id', string.valueOf(objActComercial.id));
            listMapReturn.add(mapinst);
        }
        system.debug('lista de mapas que va de vuelta   '+listMapReturn);
        return listMapReturn;
    }
  
    private map<string, string> fillFieldList(ApG_GlobalPositionItem__mdt sqlGPI){
        map<string, string> mapReturn = new map<string, string>();
        for(integer i = 0; i<5; i++){
            string fieldName = 'lvl'+i+'__c';
            if(sqlGPI.get(fieldName)!= null){
                mapReturn.put(fieldName, string.valueOf(sqlGPI.get(fieldName)));
            }
        }
        return mapReturn;
    }
    

}