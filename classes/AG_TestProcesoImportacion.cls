@isTest
private class AG_TestProcesoImportacion {

//Crear Proceso_Improtacion__c directamente sobre BBDD
//-------------------------------------------------------
	
	@isTest static void insertProcesoImportacion1() {
		
		//Ántes de empezar el proceso de importación, doy de alta Proceso_Improtacion__c
		//Pro defecto, queda en estado: Planificado.
		Proceso_Importacion__c pImp1 = new Proceso_Importacion__c();
		pImp1.Descripcion__c= 'Proceso de improtacion 1 ';
		pImp1.Comentarios__c = 'Comentarios sobre como va el proceso importación';
		pImp1.Recalcular_permisos_cliente__c = 'Sólo modificados';	//este ya es el valor por defecto
		pImp1.Recalcular_permisos_objeto__c = 'Sólo modificados';	//este ya es el valor por defecto
		insert pImp1;
		pImp1 = getAndLogProcesoImportacion (pImp1.id);

		//Cuando empieza, verdaderamente, lo pongo 'En curso' y marco hora inicio.
		pImp1.Estado__c = 'En curso';
		pImp1.Inicio__c = DateTime.now();
		update pImp1;
		pImp1 = getAndLogProcesoImportacion (pImp1.id);

		//Cuando finaliza, lo pongo en 'Finalizado con éxito' o 'Finalizado con errores'
		//y marco la hora de final.
		//En este punto, se inician los procesos batch (tanto si termina con éxito como si termina con errores)
		pImp1.Estado__c = 'Finalizado con éxito';
		pImp1.Final__c = DateTime.now();
		update pImp1;

		pImp1 = getAndLogProcesoImportacion (pImp1.id);
	}
	
	@isTest static void insertProcesoImportacion2() {
		
		//Ántes de empezar el proceso de importación, doy de alta Proceso_Improtacion__c
		//Pro defecto, queda en estado: Planificado.
		Proceso_Importacion__c pImp1 = new Proceso_Importacion__c();
		pImp1.Descripcion__c= 'Proceso de improtacion 1';
		pImp1.Comentarios__c = 'Comentarios sobre como va el proceso importación';
		pImp1.Recalcular_permisos_cliente__c = 'Ninguno';
		pImp1.Recalcular_permisos_objeto__c = 'Ninguno';
		insert pImp1;
		pImp1 = getAndLogProcesoImportacion (pImp1.id);

		//Cuando empieza, verdaderamente, lo pongo 'En curso' y marco hora inicio.
		pImp1.Estado__c = 'En curso';
		pImp1.Inicio__c = DateTime.now();
		update pImp1;
		pImp1 = getAndLogProcesoImportacion (pImp1.id);

		//Cuando finaliza, lo pongo en 'Finalizado con éxito' o 'Finalizado con errores'
		//y marco la hora de final.
		//En este punto, se inician los procesos batch (tanto si termina con éxito como si termina con errores)
		pImp1.Estado__c = 'Finalizado con éxito';
		pImp1.Final__c = DateTime.now();
		update pImp1;

		pImp1 = getAndLogProcesoImportacion (pImp1.id);
	}

// Crear Proceso_Importacion__c mediante el pequeño API de AG_ProcesoImportacionService
//-----------------------------------------------------------------------------------------


	@isTest static void crearObtenerProcesoTest() {

		Id procId = AG_ProcesoImportacionService.crearProceso('Nuevo proceso importación.');

		System.assert(procId!=null);

		Proceso_Importacion__c proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assert(proc!=null);
		System.assertEquals(proc.Estado__c, 'Planificado');
		System.assertEquals(proc.Inicio__c,null);
		System.assertEquals(proc.Final__c,null);
		System.assertEquals(proc.Recalcular_permisos_cliente__c,'Sólo modificados');
		System.assertEquals(proc.Recalcular_permisos_objeto__c,'Sólo modificados');

		delete proc;
		Proceso_Importacion__c procNotExist = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assertEquals(procNotExist, null);
	}


	@isTest static void iniciarFinalizarProcesoTest() {

		Id procId = AG_ProcesoImportacionService.crearProceso('Nuevo proceso importación.');

		System.assert(procId!=null);

		Proceso_Importacion__c proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assert(proc!=null);
		System.assertEquals(proc.Estado__c, 'Planificado');
		System.assertEquals(proc.Inicio__c,null);
		System.assertEquals(proc.Final__c,null);

		AG_ProcesoImportacionService.iniciarProceso(procId);

		proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assert(proc!=null);
		System.assertEquals(proc.Estado__c, 'En curso');
		System.assert(proc.Inicio__c<=System.now());
		System.assertEquals(proc.Final__c,null);

		AG_ProcesoImportacionService.finalizarProceso(procId, true);

		proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assert(proc!=null);
		System.assertEquals(proc.Estado__c, 'Finalizado con éxito');
		System.assert(proc.Inicio__c<=System.now());
		System.assert(proc.Final__c<=System.now());
	}

	@isTest static void anyadorComentarioProcesoTest() {

		Id procId = AG_ProcesoImportacionService.crearProceso('Nuevo proceso importación.');

		System.assert(procId!=null);

		Proceso_Importacion__c proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assert(proc!=null);
		System.assertEquals(proc.comentarios__c, null);

		String comentario1 = 'Primer comentario';
		String c1 = AG_ProcesoImportacionService.anyadirComentarioProceso(procId, comentario1);
		System.assertEquals(comentario1,c1);
		proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assertEquals(comentario1, proc.comentarios__c);

		String comentario2 = 'Segundo comentario';
		String c2 = AG_ProcesoImportacionService.anyadirComentarioProceso(procId, comentario2);
		System.assertEquals(comentario1 + '\n' + comentario2, c2);

		proc = AG_ProcesoImportacionService.obtenerProcesoImportacion(procId);
		System.assertEquals(comentario1 + '\n' + comentario2, proc.comentarios__c);

	}

	private static Proceso_Importacion__c getAndLogProcesoImportacion(Id procesoImportacionId) {

		Proceso_Importacion__c procImp = 
			[SELECT Id, name, Descripcion__c, Comentarios__c, Estado__c, Final__c, Inicio__c, Recalcular_permisos_cliente__c, Recalcular_permisos_objeto__c 
			 FROM Proceso_Importacion__c 
			 WHERE Id = :procesoImportacionId];

		System.debug ('Proceso Importacion: ' + procImp.name + '(' + procImp.Id + ')');
		System.debug ('\t Descripcion: ' + procImp.Descripcion__c);
		System.debug ('\t Comentarios: ' + procImp.Comentarios__c);
		System.debug ('\t Estado: ' + procImp.Estado__c);
		System.debug ('\t Inicio - Final: ' + procImp.Inicio__c + ' - ' + procImp.Final__c);
		System.debug ('\t Recalcular cliente: ' + procImp.Recalcular_permisos_cliente__c);
		System.debug ('\t Recalcular objeto: ' + procImp.Recalcular_permisos_objeto__c);

		return procImp;
	}



}