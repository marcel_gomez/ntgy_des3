/**
 * Created by luis.igualada on 19/06/2017.
 */
global class GNF_uploadFile4_sch implements Schedulable {

	global void execute(SchedulableContext sc) {
		GNF_uploadFile4_bch b = new GNF_uploadFile4_bch();
		//database.executebatch(b,100);
        database.executebatch(b,integer.valueOf(GNF_ValoresPredeterminados__c.getInstance().GNF_Envio_Delta_Lotes__c));
		/* GNF_uploadFile4_sch ia		= new GNF_uploadFile4_sch();
		DateTime fechaActual	= System.now().addMinutes(1);
		Integer minutos			= fechaActual.minute();
		Integer hora     		= fechaActual.hour();
		Integer dia				= fechaActual.day();
		integer mes				= fechaActual.month();
		Integer anio			= fechaActual.year();
		String sch 				= '0 '+minutos+' '+hora+' '+dia+' '+mes+' ? '+anio;
		Id tarea				= System.schedule('Actualizar opp '+System.now(), sch, ia);
		Faltaría programar un Apex Scheduler para que se ejecute todos los días a cierta hora
		o integrar el código de arriba.
		*/

	}

}