public interface ApG_PosicionGlobalReportInterface {
    list<map<string, string>> getsObjectInformation(string itemToGetFrom, string forceUser, boolean isHierarchy);
}