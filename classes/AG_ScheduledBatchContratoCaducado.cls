global with sharing class AG_ScheduledBatchContratoCaducado implements Database.Batchable<sObject>,
                                                                       Schedulable {
    
    //VARIABLES - implement interface statful to maintain state
    final String query;

    //SCHEDULABLE METHODS
    global void execute(SchedulableContext sc) {
        executeBatch();
    }

    //BATCHABLE METHODS
    global AG_ScheduledBatchContratoCaducado() {
        //Get all Contratos matching the criteria
        query = 'SELECT Id, Caducada__c FROM Contrato__c where Caducada__c = true';
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Contrato__c> contratosToUpdate = new List<Contrato__c>();
        if (!scope.isEmpty()) {
            for(sObject scopeSObject :scope) {
                Contrato__c theContrato = (Contrato__c) scopeSObject;
                if(theContrato.Caducada__c == true) {
                    theContrato.EstadoBis__c = system.label.ApG_Caducada;                   
                }
                contratosToUpdate.add(theContrato);
            }
        }
        if(!contratosToUpdate.isEmpty())
            update contratosToUpdate;
    }

    global void finish(Database.BatchableContext BC) {
        
    }

    //-- PRIVATE METHODS
    /**
     *
     * 
     * @return {@code Boolean} 
     */
    private static void executeBatch() {
        Database.Batchable<sObject> batchable = new AG_ScheduledBatchContratoCaducado();
        Database.executeBatch(batchable, 200);
    }

}