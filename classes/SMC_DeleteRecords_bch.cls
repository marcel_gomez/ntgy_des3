/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Encargado de eliminar los registros ficticios.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-11-29		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
global class SMC_DeleteRecords_bch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
	
	global String strSOQL;
	global String strObjectAPIName;
	global Map<String, SMC_BorradoFicticios__c> mapObjectsToDelete;
	
	/**
	* @Method:		SMC_DeleteRecords_bch
	* @Description:	Batch constructor that defines which object will be deleted.
	* @author		Manuel Medina - 29112017
	*/
	global SMC_DeleteRecords_bch(){
		this.mapObjectsToDelete										= new Map<String, SMC_BorradoFicticios__c>();
	}
	
	/**
	* @Method:		start
	* @param:		Database.BatchableContext DB
	* @author		Manuel Medina - 29112017
	*/
	global Database.QueryLocator start( Database.BatchableContext DB ){
		String strSELECTComplement									= '';
		
		for( String strChildObject : mapObjectsToDelete.get( strObjectAPIName ).SMC_ObjetosHijo__c.split( ',' ) ){
			strSELECTComplement										+= 		'( SELECT Id FROM ' + strChildObject + '),';
		}
		
		this.strSOQL												= 'SELECT Id, ' +
																			strSELECTComplement.substringBeforeLast( ',' ) +
																		'FROM ' + strObjectAPIName + ' ' +
																		'WHERE ' + mapObjectsToDelete.get( strObjectAPIName ).SMC_CampoFicticio__c + ' = true ' +
																		'LIMIT 5000';
		
		return Database.getQueryLocator( strSOQL );
	}
	
	/**
	* @Method:		execute
	* @param:		Database.BatchableContext DB
	* @param:		List<sObject> scope
	* @author		Manuel Medina - 29112017
	*/
	global void execute( Database.BatchableContext DB, List<sObject> scope ){
		List<SObject> lstRecordsToDelete							= new List<SObject>();
		
		for( SObject sObjRecord : scope ){
			Boolean blnDeleteRecord									= true;
			
			for( String strChildObject : mapObjectsToDelete.get( strObjectAPIName ).SMC_ObjetosHijo__c.split( ',' ) ){
				if( sObjRecord.getSObjects( strChildObject ) != null && !sObjRecord.getSObjects( strChildObject ).isEmpty() ){
					blnDeleteRecord									= false;
				}
			}
			
			if( blnDeleteRecord ){
				lstRecordsToDelete.add( sObjRecord );
			}
		}
		
		if( !lstRecordsToDelete.isEmpty() ){
			delete lstRecordsToDelete;
			
			//Database.emptyRecycleBin( lstRecordsToDelete );
		}
	}
	
	/**
	* @Method:		finish
	* @param:		Database.BatchableContext DB
	* @author		Manuel Medina - 29112017
	*/
	global void finish( Database.BatchableContext DB ){
		SMC_DeleteRecords_sch schDeleteRecords;
		Datetime dttNow												= System.now().addMinutes( 1 );
		String sch													= '0 ' + dttNow.minute() + ' ' + dttNow.hour() + ' ' + dttNow.day() + ' ' + dttNow.month() + ' ? ' + dttNow.year();
		
		Boolean blnAllObjectsHasBeenDeleted							= true;
		
		SMC_BorradoFicticios__c cfgBorradoFicticiosCompleted		= new SMC_BorradoFicticios__c();
		cfgBorradoFicticiosCompleted								= mapObjectsToDelete.get( strObjectAPIName );
		cfgBorradoFicticiosCompleted.SMC_Borrar__c					= false;
		
		mapObjectsToDelete.put( cfgBorradoFicticiosCompleted.Name, cfgBorradoFicticiosCompleted );
	
		update mapObjectsToDelete.values();
		
		for( String strObject : mapObjectsToDelete.keySet() ){
			if( mapObjectsToDelete.get( strObject ).SMC_Borrar__c ){
				schDeleteRecords									= new SMC_DeleteRecords_sch( strObject );
				blnAllObjectsHasBeenDeleted							= false;
				break;
			}
		}
		
		if( !blnAllObjectsHasBeenDeleted ){
			Id tarea												= System.schedule( 'Delete records from ' + schDeleteRecords.strObjectToDeleted + ' ' + dttNow.format( 'dd-MM-yyyy HH:mm' ), sch, schDeleteRecords );
			
		}else{
			for( String strObject : mapObjectsToDelete.keySet() ){
				SMC_BorradoFicticios__c cfgBorradoFicticios			= new SMC_BorradoFicticios__c();
				cfgBorradoFicticios									= mapObjectsToDelete.get( strObject );
				cfgBorradoFicticios.SMC_Borrar__c					= true;
				
				mapObjectsToDelete.put( cfgBorradoFicticios.Name, cfgBorradoFicticios );
			}
			
			update mapObjectsToDelete.values();
		}
	}
}