@isTest
public class AG_TestLogicAccount{

    static Map<String,User> users;
    static Map<String,EMpleado__c> empleados;
    static Map<String,Account> accounts ;

    @testSetup
    private static void setup() {

        UserRole topRole = new UserRole(name='RoleT');
        insert topRole;
        UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
        insert userRole2;
        UserRole userRole3 = new UserRole(name='Role3', parentRoleId = userRole2.Id);
        insert userRole3;
        UserRole userRole4 = new UserRole(name='Role4', parentRoleId = userRole3.id);
        insert userRole4;

        System.debug('Look for Standard USer profile ');
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

        User theUser1 = new User(
            LastName = 'User1',                 FirstName='Test',   
            Alias = 'tu1',                      Email = 'test.user1@edelta.com',
            Username = 'test.user1@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole4.Id,           ID_Empleado__c= '111',isActive=true);
        insert theUser1;

        User theUser2 = new User(
            LastName = 'User2',                 FirstName='Test',
            Alias = 'tu',                       Email = 'test.user2@edelta.com',
            Username = 'test.user2@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US', 
            UserRoleId= userRole3.Id,           ID_Empleado__c='222', isActive=true);
        insert theUser2;

        User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
        System.runAs(adminUser) {

            Empleado__c empleado1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='111');
            insert empleado1;
            Empleado__c empleado2 = new Empleado__c(name='Empleado2', OwnerId=theUSer2.id, ID_Empleado__c='222');
            insert empleado2;

            Account account1 = new Account(name='Account1', Gestor__c=empleado1.Id, OwnerId=theUser1.id);
            insert account1;
            System.debug('@#@# Account1:' + account1);

            Account account2 = new Account(name='Account2', Gestor__c=empleado2.Id, OwnerId=theUser2.id);
            insert account2;
            System.debug('@#@# Account2:' + account1);

            List<Account> xx = [SELECT Id, name, OwnerId FROM Account WHERE name like 'Account_'];
            System.debug('@#@# List accounts:' + xx);
        }

    }

    static {

        //Complete static collections
        users = AG_TestUtil.getMockUsers();
        empleados = AG_TestUtil.getMockEmpleados();
        accounts = AG_TestUtil.getMockAccounts();

    }

    @isTest
    static void canRemoveAccountSharedRecordsTest() {

        Account account1 = accounts.get('Account1');
        Account account2 = accounts.get('Account2');
        Empleado__c empleado1 = empleados.get('Empleado1');
        Empleado__c empleado2 = empleados.get('Empleado2');
        User user1 = users.get('User1');
        User user2 = users.get('User2');

        Suministros__c supply1 = new Suministros__c(name='Supply 1', Cliente__c = account1.id, Gestor__c =empleado1.id);
        insert supply1;

        Suministros__c supply2 = new Suministros__c(name='Supply 2', Cliente__c = account2.id, Gestor__c =empleado2.id);
        insert supply2;

        //No se pueden borra ya que Suministro1/2 tienen ese usuatio como owner y apuntan a Accomut1/2 respectivamente
        System.assert( !AG_LogicAccount.canRemoveAccountSharedRecords(account1.id, user1.id) );
        System.assert( !AG_LogicAccount.canRemoveAccountSharedRecords(account2.id, user2.id) );
        //Si se puede borrar
        System.assert( AG_LogicAccount.canRemoveAccountSharedRecords(account1.id, user2.id) );
        System.assert( AG_LogicAccount.canRemoveAccountSharedRecords(account2.id, user1.id) );
    }
 
    @isTest
    static void assignOwnerFromEmpleadoTest() {

        Empleado__c empleado1 = empleados.get('Empleado1'); //tiense
        Empleado__c empleado2 = empleados.get('Empleado2');
        User user1 = users.get('User1');
        User user2 = users.get('User2');

        Account accountT1 = new Account(name='Account Test1', Gestor__c=empleado1.Id);
        insert accountT1;

        Account accountT2 = new Account(name='Account Test2', Gestor__c=empleado2.Id);
        insert accountT2;

        //accountT1 = [SELECT Id, OwnerId from Account WHERE Name like 'Account Test1'];
        //System.assertEquals(accountT1.OwnerId, user1.Id);

        //accountT2 = [SELECT Id, OwnerId from Account WHERE Name like 'Account Test2'];
        //System.assertEquals(accountT2.OwnerId, user2.Id);

    }

    @isTest
    static void hasChangedGestorTest() {

    }

    @isTest
    static void hasChangedOwnerTest() {

    }


     @isTest
     static void checkOnly10FavByGestorTest() {

        Account account1 = accounts.get('Account1');
        System.debug('TTTT account1=' + account1);
        List<Account> newAccounts = new List<Account>();
        for (integer i=0; i<15;i++) {
            Account accountN = account1.clone();
            accountN.name = 'New Accounts - ' + i;
            accountN.favorito__c = true;
            newAccounts.add(accountN);
        }
        insert newAccounts;
        System.debug('TTTT nuevas:' + newAccounts);
        System.debug('TTTT nuevas:' + newAccounts.size());

        //account1.idioma__c = 'Esperanto';
        account1.Favorito__c = true;
        Boolean expectedExceptionThrown=false;
        try {
            update account1;
        } catch(Exception e) {
            expectedExceptionThrown =  e.getMessage().contains('El gestor asignado a esta cuenta ya tiene 10 clientes favoritos') ? true : false;

        }
        System.AssertEquals(expectedExceptionThrown, true);
        
     }


   @isTest static void onUpdateTrigger_Test() {

        Map<String, Account> mapMockAccounts = AG_TestUtil.getMockAccounts();
        Account ac1 = mapMockAccounts.get('Account1');
        ac1.description = ac1.description + '__';
        update ac1;

        System.assert(true);

        delete ac1;

        System.assert(true);
    }



}