global class AG_Batch_CleanObjectShare implements Database.Batchable<sObject> {

	//List of objects affected by security 
	public  static final List<String> BUSINESS_OBJECTS = AG_SecurityRecalculationProcessUtil.BUSINESS_OBJECTS;
    private final Integer tableIndex; //Index when loop over BUSINESS_OBJECTS list

    //Main batch process query Template: Loop over all Object__Share created by us (RowCause=Apex_Sharing__c)
	private final String QUERY_TEMPLATE =
		' SELECT id, RowCause FROM #TABLE_SHARE# ' + 
		' WHERE RowCause=\'Apex_Sharing__c\' #RECALCULATE_ALL_CONDITION#';

	//Filter the custom objects shares that has their associated Account modified: Cliente__r.recalcular_permisos_obj=true
	private final String RECALCULATE_ALL_CONDITION = 
		'AND ParentId IN (SELECT ID FROM #TABLE# WHERE Cliente__r.recalcular_permisos_objetos__c = true)';
	
	private String query;

	private final Boolean cleanAll;			//clean all and not just ObjectShare with associated Account modified.
	private final Boolean recalculateAfterClean;	//execute AG_Batch_RecalculareObjectShare at the end of the process
	private final Boolean justOneObject;	//just clean ObjectShare of one object 
	
	/**
	* Instantiate batch process to clean <n>ObjectShare</n>.
	* @param tableIndex 
	* @param cleanAll 
	* @param recalculateAfterClean 
	*/
	@testVisible
	private AG_Batch_CleanObjectShare(Integer tableIndex, Boolean cleanAll, Boolean recalculateAfterClean, Boolean justOneObject) {

		this.cleanAll = cleanAll;
		this.tableIndex = tableIndex;
		this.recalculateAfterClean = recalculateAfterClean;
		this.justOneObject = justOneObject;

		String tableName = BUSINESS_OBJECTS.get(tableIndex);
		String tableShareName = tableName.replace('__c','__share');

		//Monta Query reemplazando Object share, object y filtro segun se quiera reporcesar todo o no.
		query = QUERY_TEMPLATE.replace('#TABLE_SHARE#', tableShareName);
		if (cleanAll) {
			query = query.replace('#RECALCULATE_ALL_CONDITION#', ' ');
		} else {
			query = query.replace('#RECALCULATE_ALL_CONDITION#', RECALCULATE_ALL_CONDITION);
			query = query.replace('#TABLE#', tableName);
		}
	}

	/*
	* Starts batch process
	*/	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/*
	 * Process chunk of data
	*/
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> Batch.execute');

		delete scope;

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Records deleted: ' + scope.size());

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- Batch.execute');
	}
	
	/*
	 * Post processing operations
	*/
	global void finish(Database.BatchableContext BC) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> BatchProcess.finish');

		/* Se pueden dar 4 casos (2 variables: justOneObject y recalculateAfterClean)
		 * 		justOneObject 	recalculateAfterclean
		 *		-------------	---------------------
		 *  1: 		true			true				Borra permisos de todas las tablas y despues lanza proceso recalculo
		 *	2:		true			false				Borra permisos de todas las tablas y termina
		 * 	3:		false			true				Borra permisos de una tabla (tableIndex) y despues lanza recalculo de esta tabla
		 * 	4:		false			false				Borra permisos de una tabla (tableIndex) y termina
		 */

		if (!this.justOneObject) {	//(1) y (2)

			Integer nextTableIndex = tableIndex + 1;	//Siguiente tabla
		
			if (nextTableIndex<BUSINESS_OBJECTS.size()) { 
				//Se comenta debug(Victor Velandia (VV))
				//System.debug ('**** Siguiente tabla: NextTableIndex=' + nextTableIndex + ' - table Name=' + BUSINESS_OBJECTS.get(nextTableIndex));

				AG_Batch_CleanObjectShare batchProc = new AG_Batch_CleanObjectShare(nextTableIndex, this.cleanAll, this.recalculateAfterClean, false);
				Id batchId = Database.executeBatch(batchProc);
				
				//Se comenta debug(Victor Velandia (VV))
				//System.debug ('*** Siguiente batch lanzado:' + batchId);
			} else {
				//Una vez procesadas borradas todas las tablas, lanzamos proceso recalculo, si procede
				if (this.recalculateAfterClean) {	//(1)
					AG_Batch_RecalculateObjectShare.executeFullProcess(cleanAll);
				} else {	//(2)
					//Se comenta debug(Victor Velandia (VV))
					//System.debug('**** Fin del proceso de borrado de todas las tablas');
				}
			}
		} else {		
			if (this.recalculateAfterClean) {	//(3)
				AG_Batch_RecalculateObjectShare.ExecuteProcessOneTable(tableIndex, cleanAll);
			} else {	//(4)
				//Se comenta debug(Victor Velandia (VV))
				//System.debug('**** Fin del proceso de borrado de la tabla:' + tableIndex);
			}
		}

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- BatchProcess.finish');
	}

	/**
	* Lanza proceso batch de borrado de objetos Object_Share 
	* @param cleanAll Para borrar todos los permisos (Object_share), y no únicamnente la de objeto con Account asociado modificado.
	*/
	public static Id executeFullProcess(Boolean cleanAll) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Start batch process to recalculate Object Shares');

		AG_Batch_CleanObjectShare batchProcess = new AG_Batch_CleanObjectShare(0, cleanAll, true, false);
		Id batchId = Database.executeBatch(batchProcess);

		System.debug ('*** BatchProcess.finish: ends. Id=' + batchId);

		return batchId;
	}

	/*
	* Lanza proceso batch de borrado de Shares de todos los objetos. 
	* Por parametros indicamos si miramos marcas y si encadenamos con proceso de recalculo.
	* En BUSINESS_OBJECTS tenemos la lista de tipos de objetos.
	*/
	public static Id executeProcess(Boolean cleanAll, Boolean recalculateAfterClean) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> executeProcessOneTableStart: cleanAll=' + cleanAll);

		AG_Batch_CleanObjectShare batchProcess = new AG_Batch_CleanObjectShare(0, cleanAll, recalculateAfterClean, false);
		Id batchId = Database.executeBatch(batchProcess);
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- executeProcessOneTableStart: Batch.Id:' + batchId);
		
		return batchId;
	}

	/*
	* Lanza proceso batch de borrado de Share para un tip de objeto (TableIndex) en particular.
	* En BUSINESS_OBJECTS tenemos la lista de tipos de objetos.
	*/
	public static Id executeProcessOneTable(Integer tableIndex, Boolean cleanAll, Boolean recalculateAfterClean) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> executeProcessOneTableStart: cleanAll=' + cleanAll);

		AG_Batch_CleanObjectShare batchProcess = new AG_Batch_CleanObjectShare(tableIndex, cleanAll, recalculateAfterClean, true);
		Id batchId = Database.executeBatch(batchProcess);
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- executeProcessOneTableStart: Batch.Id:' + batchId);
		
		return batchId;
	}
}