/**
 * Created by luis.igualada on 12/07/2017.
    Lógica de la Visualforce de Precios en la Oportunidad
 */
public class GNF_PreciosVistaClasica_ctr {

    public Opportunity obj_opp {get; set;}
    public List<wrp_precios> wrapperprecios {get; set;}
    public Boolean valordef {get; set;}
    public Boolean valordek {get; set;}
    public Boolean GNF_Spread {get; set;}
    public Boolean GNF_SpreadCliente {get; set;}
    public Boolean cvprimacliente {get; set;}
    public Boolean GNF_F_Anexo {get; set;}
    public Boolean PF_Total_Click {get; set;}
    
    /**
    * @Method:      GNF_PreciosVistaClasica_ctr
    * @param:       N/A
    * @Description: Constructor para acceder a funcionalidades del controlador.
    * @author       Manuel Medina - 23082017
    */
    public GNF_PreciosVistaClasica_ctr(){}


    public GNF_PreciosVistaClasica_ctr(ApexPages.StandardController stdController) {


        obj_opp = ( Opportunity) stdController.getRecord();

        mthd_getResumenPreciosClick(obj_opp);

    }
    
    /**
    * @Method:      getOpportunitySupplies
    * @param:       List<String> setOpportunityId
    * @Description: Obtiene un set de id de oportunidades punto de suministro para filtrar otros query.
    * @author       Manuel Medina - 23082017
    */
    public Set<String> getOpportunitySupplies( Set<Id> setOpportunityId ){
        return getOpportunitySupplies( new List<Id>( setOpportunityId ) );
    }
    
    /**
    * @Method:      getOpportunitySupplies
    * @param:       List<Id> lstOpportunityId
    * @Description: Obtiene un set de id de oportunidades punto de suministro para filtrar otros query.
    * @author       Manuel Medina - 23082017
    */
    public Set<String> getOpportunitySupplies( List<Id> lstOpportunityId ){
        Map<String, Oportunidad_Punto_de_Suministros__c> mapOpportunitySuppliesById     = new Map<String, Oportunidad_Punto_de_Suministros__c>( [
                                                                                            SELECT Id
                                                                                            FROM Oportunidad_Punto_de_Suministros__c
                                                                                            WHERE Oportunidad__c IN: lstOpportunityId
                                                                                            LIMIT: Integer.valueOf( GNF_ValoresPredeterminados__c.getInstance().GNF_VistaClasicaLimitePS__c )
                                                                                        ] );
                                                                                        
        return mapOpportunitySuppliesById.keySet();
    }

    public void mthd_getResumenPreciosClick(Opportunity Oportunidad) {
        
        List<Consumo_Click__c> resumenprecios = [SELECT Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c, Cotizacion_de_periodo__r.Name,
                                                Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c,
                                                GNF_ValorF__c,Mes_Aplicacion__c,GNF_fechaInicioConsumo__c,PFAnexo__c,
                                                Cotizacion_de_periodo__r.GNF_ValorK__c,
                                                Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.Id,
                                                GNF_descuentoClickPrcnt__c,
                                                GNF_descuentoClickAbs__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.Precio_final_objetivo__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.Pais__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c,
                                                Cotizacion_de_periodo__r.Opportunity__r.GNF_F_Anexo__c,
                                                Oportunidad_Punto_de_Suministro__r.GNF_PDF_F_Anexo__c, //LSANCHO 20171214 (REQ000010554622)
                                                Cotizacion_de_periodo__r.Brent_de_referencia__c,
                                                Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c
                                                FROM Consumo_Click__c
                                                WHERE Cotizacion_de_periodo__r.Opportunity__r.Id= :Oportunidad.Id
                                                /* BEGIN - Manuel Medina - Filtrado por oportunidad puntos de suministro - 23082017 */
                                                AND Oportunidad_Punto_de_Suministro__c IN: getOpportunitySupplies( new List<Id>{ Oportunidad.Id } )
                                                /* END - Manuel Medina - Filtrado por oportunidad puntos de suministro - 23082017 */
                                                ORDER BY Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c,Cotizacion_de_periodo__r.Periodo__c asc, GNF_posicionConsumoClick__c asc ];

        Map <String,Set <String>> suministroperiodo = new Map <String,Set <String>>();

        for (Consumo_Click__c cc : resumenprecios) {

            if (suministroperiodo.containsKey(cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c)){
                suministroperiodo.get(cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c).add(cc.Mes_Aplicacion__c);
            }else{
                suministroperiodo.put(cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c, new set<String>{cc.Mes_Aplicacion__c});
            }

            //System.debug('Mapasuministroperiodo'+suministroperiodo);

            //Validaciones Mostrar/Ocultar Columnas
            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Pais__c == 'España') {valordef = true;} else {valordef = false;}

            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Pais__c == 'España') {valordek = false;} else {valordek = true;}

            if (cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c == 'SWAP Fórmula' || cc.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c == 'SWAP Hub') {
                GNF_Spread = true;
                GNF_SpreadCliente = true;
                cvprimacliente=false;
                GNF_F_Anexo = true;
                PF_Total_Click = false;
            } else {
                GNF_Spread = false;
                GNF_SpreadCliente = false;
                cvprimacliente=true;
                GNF_F_Anexo = false;
                PF_Total_Click = true;
            }


        }

        wrapperprecios = new List<wrp_precios>();
        integer contador = 0;

        for (Consumo_Click__c cc : resumenprecios) {
            if (suministroperiodo.containsKey(cc.Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c)) {
                wrapperprecios.add(new wrp_precios(cc));
                contador++;
            }
        }

        //System.debug('Contadoooooor'+contador);

        //El wrapperprecios.add habría que hacerlo dentro de un bucle que recorriera las cotizaciones de periodo por cups

        //wrp_precios wrapper = new wrp_precios(suministroperiodo,resumenprecios,Oportunidad.Id);

    }

        public class wrp_precios
        {
            public string Cups                      {get; set;}
            public string mesano                    {get; set;}
            public double strcvprimaclienteclick    {get; set;}
            public double strvalordef               {get; set;}
            public double strvalordek               {get; set;}
            public double strdtoenvalorabsoluto     {get; set;}
            public double strdtoenporcentaje        {get; set;}
            public double strpreciototalclick       {get; set;}
            public double strspread                 {get; set;}
            public double strspreadcliente          {get; set;}
            public double strpfanexo                {get; set;}
            public double strparidad                {get; set;}
            public double strbrent                  {get; set;}

            public wrp_precios(Consumo_Click__c consumo){

                //Consumo total del click
                Double totalpfanexoconsumoclick=0;
                Double cvprim = 0;
                /*Consumo_Click__c Consumopfanexo = [Select PFAnexo__c from Consumo_Click__c
                        where Cotizacion_de_periodo__r.Opportunity__r.Id = :idoportunidad and Cotizacion_de_periodo__r.Id = :idcotizacionperiodo];*/

                /*AggregateResult[] groupedresults= [Select sum(PFAnexo__c) from Consumo_Click__c
                                                   where Cotizacion_de_periodo__r.Opportunity__r.Id = :idoportunidad ];*/

                /*for (AggregateResult ar: groupedresults) {
                totalpfanexoconsumoclick= (Double)ar.get('expr0');
                }*/

                //totalpfanexoconsumoclick = (Consumopfanexo.PFAnexo__c != null ? (Double)Consumopfanexo.PFAnexo__c : 0);
                totalpfanexoconsumoclick= (consumo.PFAnexo__c != null ? (Double)consumo.PFAnexo__c : 0);
                cvprim=(consumo.Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c != null ? consumo.Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c : 0);

                cups = consumo.Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c;
                mesano =                consumo.Mes_Aplicacion__c;
                strvalordef=            (consumo.GNF_ValorF__c != null ? consumo.GNF_ValorF__c : 0);
                strvalordek =           (consumo.Cotizacion_de_periodo__r.GNF_ValorK__c != null ? consumo.Cotizacion_de_periodo__r.GNF_ValorK__c : 0);
                strdtoenvalorabsoluto = (consumo.GNF_descuentoClickAbs__c != null ? consumo.GNF_descuentoClickAbs__c : 0);
                strdtoenporcentaje =    (consumo.GNF_descuentoClickPrcnt__c != null ? consumo.GNF_descuentoClickPrcnt__c : 0);
                strspread =             consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c != null ? consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread__c : 0;
                strspreadcliente =      consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c != null ? consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_Spread_Cliente__c : 0;
                strcvprimaclienteclick  = cvprim;
                //LSANCHO 20180313 - Ini (Solicitado por Ofertas)
                strbrent =  consumo.Cotizacion_de_periodo__r.Brent_de_referencia__c != null ? consumo.Cotizacion_de_periodo__r.Brent_de_referencia__c : 0;
                strparidad =  consumo.Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c != null ? consumo.Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c : 0;
                //LSANCHO 20180313 - Fin
                //LSANCHO 20171214 - Ini (REQ000010554622)
                //strpfanexo =            consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_F_Anexo__c != null ? consumo.Cotizacion_de_periodo__r.Opportunity__r.GNF_F_Anexo__c : 0;
                strpfanexo =            consumo.Oportunidad_Punto_de_Suministro__r.GNF_PDF_F_Anexo__c != null ? consumo.Oportunidad_Punto_de_Suministro__r.GNF_PDF_F_Anexo__c : 0;
                // LSANCHO 20171214 - Fin
                strpreciototalclick =   totalpfanexoconsumoclick;

            }

        }

}