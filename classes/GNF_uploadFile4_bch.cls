/**
 * Created by luis.igualada on 19/06/2017.
 */
Global class GNF_uploadFile4_bch implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {

	string consultaconsumos;
	integer exitosos=0;
	integer fallidos=0;
	List<GNF_Log__c> listalogs;
	GNF_Log__c registrolog;

	//Constructor
	global GNF_uploadFile4_bch(){
		//Batch Constructor
		listalogs = new List<GNF_Log__c>();
		registrolog = new GNF_Log__c();
	}

	//START
	global Database.QueryLocator start(Database.BatchableContext BC) {
		// collect the batches of records or objects to be passed to execute

		AggregateResult[] idsClicks= [Select Cotizacion_de_periodo__r.Opportunity__r.Id FROM Consumo_Click__c WHERE
		Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c = false and
		Cotizacion_de_periodo__r.Opportunity__r.StageName =: ( Test.isRunningTest() ? 'En Elaboración' : 'Firmada' ) and
		Cotizacion_de_periodo__r.Opportunity__r.QA_Contratada__c > 0 and
		Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName='Click' and
		Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_precio__c not in ('Indicativo') and
		Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c not in ('SWAP Fórmula','SWAP Hub') and
		Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c not in ('Interna') and
		Oportunidad_Punto_de_Suministro__r.Concepto__r.Tipo_Tarifa__c != 'Cantidades comprometidas (POR TRAMOS)'
		GROUP BY Cotizacion_de_periodo__r.Opportunity__r.Id];

		set<Id> idsClicksCond = new set<Id>();

		for (AggregateResult ar : idsClicks){
			idsClicksCond.add(string.valueOf(ar.get('Id')));
		}

		//string consultabuena = 'Select Id FROM Opportunity WHERE Id IN :idsClicksCond';

		//Nos quedamos con los IDs únicos de las oportunidades
		string consultaoportunidades = 'Select Id from Opportunity where Id IN :idsClicksCond ORDER BY CreatedDate Asc';

		/*string consultaoportunidades = 'Select Id from Opportunity where GNF_Enviado_Edelta__c = false and '+
										'StageName = \'Firmada\' and '+
										'QA_Contratada__c > 0 and '+
										'RecordType.DeveloperName=\'Click\' and '+
										'Tipo_de_precio__c not in (\'Indicativo\') and '+
										'Tipo_de_servicio__c not in'+'(\'SWAP Fórmula\',\'SWAP Hub\') and '+
										'Tipo_de_cobertura__c not in (\'Interna\') '+
										'and Id in (Select Oportunidad__c from Oportunidad_Punto_de_Suministros__c where Concepto__r.Tipo_Tarifa__c != \'Cantidades comprometidas (POR TRAMOS)\')'+
										'ORDER BY CreatedDate Asc';*/

		//Campo usado para el objeto LOG
		string fechasubida = System.now().format('yyyyMMddHHmmssSSS');

		//Creamos el Log
		//System.debug('Contador Exitoso'+GNF_uploadFile4_cls.exitoso);
		//System.debug('Contador Fallido'+GNF_uploadFile4_cls.fallido);

		if (GNF_uploadFile4_cls.exitoso==1){
			//Rellenamos el objeto LOG OK
			registrolog = new GNF_Log__c(GNF_Estado__c = 'OK', GNF_IDTransaccion__c = fechasubida, GNF_Fecha_Inicio__c = System.now());
			exitosos=exitosos+GNF_uploadFile4_cls.exitoso;
		}
		else{
			//Rellenamos el objeto LOG FALLIDO
			registrolog = new GNF_Log__c(GNF_Estado__c='OK',GNF_IDTransaccion__c=fechasubida, GNF_Fecha_Inicio__c = System.now());
			fallidos=fallidos+GNF_uploadFile4_cls.fallido;
		}

		//Guardamos el Registro del Log
		insert registrolog;

		//GETQUERYLOCATOR
		return Database.GetQueryLocator(consultaoportunidades);
	}

	//EXECUTE
	/*global void execute(Database.BatchableContext BC, List<Consumo_Click__c> scope){
		// process each batch of records

		List<Consumo_Click__c> consumosclick = new List<Consumo_Click__c>();

		//Asignamos el ID del log a la variable Global de la clase
		GNF_uploadFile4_cls.idlog=registrolog.id;

		GNF_uploadFile4_cls.GeneraCSV(scope);
		exitosos = exitosos + GNF_uploadFile4_cls.exitoso;
		fallidos = fallidos + GNF_uploadFile4_cls.fallido;
	}*/

	global void execute(Database.BatchableContext BC, List<Opportunity> scope){
		// process each batch of records

		Set <Id> opptsunicas = new Set <Id>();

		//Asignamos el ID del log a la variable Global de la clase
		GNF_uploadFile4_cls.idlog=registrolog.id;

		for (Opportunity opp : scope){
			opptsunicas.add(opp.Id);
		}

		GNF_uploadFile4_cls.GeneraCSV(opptsunicas);

		exitosos = exitosos + GNF_uploadFile4_cls.exitoso;
		fallidos = fallidos + GNF_uploadFile4_cls.fallido;
	}


	//FINISH
	global void finish(Database.BatchableContext BC){
		// execute any post-processing operations
		//Actualizamos los contadores de exitosos y fallidos del LOG
		//for (GNF_Log__c log :listalogs){
		registrolog.GNF_Exitosos__c=exitosos;
		registrolog.GNF_Fallidos__c=fallidos;
		registrolog.GNF_Fecha_Fin__c=System.now();
		//}

		update registrolog;

	}

}