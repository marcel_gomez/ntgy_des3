/** 
 * =====================================================================================
 * @className        GNF_NewConceptoController
 * @description      Class to create a new concept
 * @author           Lorena Sancho
 * @version          V1.0
 * @date created     2017/08/04
 * @last change      2017/08/04
 * =====================================================================================
 **/
public with sharing class GNF_NewConceptoController {    
    
    private ApexPages.StandardSetController standardController;
    public String contratoId {get; set; }
    public String contratoName {get; set; }
    public List<Contrato__c> contratos {get; set;}
    
    public GNF_NewConceptoController (ApexPages.StandardSetController controller) {   
        this.standardController = controller;

        if (String.IsNotBlank(ApexPages.currentPage().getParameters().get('id'))) {
            contratoId = ApexPages.currentPage().getParameters().get('id');
            System.debug('contratoId: ' + contratoId);
        }
    }
    
    public PageReference returnPage() {

        if (String.IsNotBlank(ApexPages.currentPage().getParameters().get('id'))) {
            contratoId = ApexPages.currentPage().getParameters().get('id');
            contratoName = ApexPages.currentPage().getParameters().get('Name');
            System.debug('contratoId: ' + contratoId);
            System.debug('contratoName: ' + contratoName);
            
            contratos =[SELECT Id, Name
                FROM Contrato__c
                WHERE Id= :contratoId];

            for (Contrato__c cc : contratos ){
                contratoName= cc.Name;

                
            }
            
            System.debug('contratoNameQuery: ' + contratoName);
        
         }
            
        //This method only applies in Classic Mode
        Schema.DescribeSObjectResult result = Concepto__c.sObjectType.getDescribe();
        String prefix = result.getKeyPrefix();
        
        GNF_ValoresPredeterminados__c mhc = GNF_ValoresPredeterminados__c.getInstance();
        //contratoId = mhc.ContratoId__c;
        
        Concepto__c objConcepto = new Concepto__c();
        objConcepto.Name = 'CONCEP1064';
        objConcepto.Contrato__c = contratoId;
        objConcepto.Codigo_Concepto_Ficticio__c = '';
        insert objConcepto;

         //PageReference np = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Concepto&retURL=%2F006%2Fo'  + '&save_new_url=%2F006%2Fe%3FretURL%3D%252F006%252Fo');
        //PageReference np = new PageReference('/' + result.getKeyPrefix() + '/e'+ '?Name=CONCEP1064' + '&' + contratoId + '='+contratoName);
        PageReference np = new PageReference('/' + objConcepto.Id + '/e?retURL=%2F' + objConcepto.Id );

        np.setRedirect(true);
        return np;
       
        
    }
    
    public PageReference getRecordTypeId() { return null; }

}