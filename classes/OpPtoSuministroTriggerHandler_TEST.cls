@isTest
private class OpPtoSuministroTriggerHandler_TEST {

	@isTest
	public static void createData(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		/*List<sObject> lstValoresPredeterminados								= Test.loadData(GNF_ValoresPredeterminados__c.sobjectType, 'GNF_ValoresPredeterminados');
		List<sObject> lstPlantillas											= Test.loadData(GNF_Plantilla__c.sobjectType, 'GNF_Plantillas');
		List<sObject> lstSecciones											= Test.loadData(GNF_Seccion__c.sobjectType, 'GNF_Seccion');
		List<sObject> lstComponentes										= Test.loadData(GNF_Componente__c.sobjectType, 'GNF_Componente');
		List<sObject> lstCondiciones										= Test.loadData(GNF_Condiciones__c.sobjectType, 'GNF_Condiciones');
		List<sObject> lstAsignacionAprobadores								= Test.loadData(GNF_AsignacionAprobadores__c.sObjectType, 'GNF_AsignacionAprobadores');
		List<sObject> lstCondicionesAprobacion								= Test.loadData(GNF_CondicionesAprobacion__c.sObjectType, 'GNF_CondicionesAprobacion');
		
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName = 'DV_IBERIA_IBERIA'
																				AND IsActive = true
																				LIMIT 1
																			];
		
		System.runAs( objUser ){
			List<sObject> lstAccounts										= Test.loadData( Account.sObjectType, 'GNF_Accounts' );
			List<sObject> lssectorsuministro 								= Test.loadData(SSs__c.sObjectType, 'GNF_SectorSuministro');
			
			List<sObject> lstContratos										= Test.loadData( Contrato__c.sObjectType, 'GNF_Contratos' );
			List<sObject> lstConceptos										= Test.loadData( Concepto__c.sObjectType, 'GNF_Conceptos' );
			List<sObject> lstOpportunities									= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
			List<sObject> lstCotizacionesPeriodo							= Test.loadData( Cotizacion_de_periodo__c.sObjectType, 'GNF_CotizacionesPeriodo' );
			List<sObject> lstOportunidadesPuntoSuministro					= Test.loadData( Oportunidad_Punto_de_Suministros__c.sObjectType, 'GNF_OportunidadPuntodeSuministro' );
			List<sObject> lstConsumosClick									= Test.loadData( Consumo_Click__c.sObjectType, 'GNF_ConsumoClick' );
		}*/
	}

	@isTest static void test_validateOpportunitySupplies_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity LIMIT 10];
		Opportunity opp = queryOpp.get(4);
		List<Account> listAcc = [SELECT Id FROM Account WHERE Id !=: opp.AccountId];
		List<Contrato__c> listContratosOld = new List<Contrato__c>();
		Integer index = 0;
		while(index < listAcc.size() && listContratosOld.size() == 0){
			Account acc = listAcc.get(index);
			listContratosOld = [SELECT Id, Ficticio__c FROM Contrato__c WHERE Cliente__c =: acc.Id];
			index ++;
		}
		Contrato__c con = listContratosOld.get(0);
		List<String> listSectorSum = new List<String>();
		List<Oportunidad_Punto_de_Suministros__c> listOPS = [SELECT Id, Oportunidad__c, Contrato__r.NISS__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c =: opp.Id];
		for(Oportunidad_Punto_de_Suministros__c ops : listOPS){
			listSectorSum.add(ops.Contrato__r.NISS__c);
			ops.Contrato__c = con.Id;
		}
		//update listOPS;

		Test.stopTest();
	}

	@isTest static void test_validateOpportunitySupplies_Alternative_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity LIMIT 10];
		Opportunity opp = queryOpp.get(5);
		Opportunity newOPP = queryOpp.get(4);
		List<Contrato__c> listContratosOld = [SELECT Id, Ficticio__c FROM Contrato__c WHERE Cliente__c =: opp.AccountId];
		for(Contrato__c c : listContratosOld){
			//c.Ficticio__c = true;
			//c.Ficticio__c = false;
		}
		System.debug('[RFJ listContratosOld]' + listContratosOld);
		update listContratosOld;
		List<Contrato__c> listContratos = [SELECT Id, Ficticio__c FROM Contrato__c WHERE Cliente__c =: newOPP.AccountId];
		for(Contrato__c c : listContratos){
			//c.Ficticio__c = true;
			c.Ficticio__c = false;
			c.Servicio_Contratado__c='GNF';
		}
		System.debug('[RFJ listContratos]' + listContratos);
		update listContratos;
		List<Oportunidad_Punto_de_Suministros__c> listOPS = [SELECT Id, Oportunidad__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c =: opp.Id];
		for(Oportunidad_Punto_de_Suministros__c ops : listOPS){
			ops.Contrato__c = listContratos.get(0).Id;
		}
		//update listOPS;

		List<Oportunidad_Punto_de_Suministros__c> listOPSnewOPP = [SELECT Id, Oportunidad__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c =: newOPP.Id];
		for(Oportunidad_Punto_de_Suministros__c ops : listOPSnewOPP){
			ops.Contrato__c = listContratosOld.get(0).Id;
		}
		//update listOPSnewOPP;
		//List<Oportunidad_Punto_de_Suministros__c> listOPS = [SELECT Id, Oportunidad__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c =: opp.Id];
		//for(Oportunidad_Punto_de_Suministros__c ops : listOPS){
		//	ops.Contrato__c = listContratos.get(0).Id;
		//}
		//update listOPS;
		Test.stopTest();
	}

	@isTest static void test_deleteOldRecords_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity LIMIT 10];
		Opportunity opp = queryOpp.get(4);
		List<Account> listAcc = [SELECT Id FROM Account WHERE Id !=: opp.AccountId];
		List<Contrato__c> listContratosOld = new List<Contrato__c>();
		Integer index = 0;
		List<String> listSectorSum = new List<String>();
		while(index < listAcc.size() && listContratosOld.size() == 0){
			Account acc = listAcc.get(index);
			listContratosOld = [SELECT Id, Ficticio__c, NISS__c FROM Contrato__c WHERE Cliente__c =: acc.Id];
			index ++;
		}
		if(listContratosOld.size() > 0){
			for(Contrato__c c : listContratosOld){
				listSectorSum.add(c.NISS__c);
			}
			delete ([SELECT Id, Contrato__r.NISS__c FROM Oportunidad_Punto_de_Suministros__c WHERE Contrato__r.NISS__c in: listSectorSum]);
			List<sSS__c> listSS = [SELECT Id FROM sSS__c WHERE Id in: listSectorSum];
			String strContractsToDelete								= System.JSON.serialize( listContratosOld );
			String strSupplySectorsToDelete							= System.JSON.serialize( listSS );
			
			//OpPtoSuministroTriggerHandler.deleteOldRecords( strContractsToDelete, strSupplySectorsToDelete );
		}
		Test.stopTest();
	}

	//@isTest static void test_getInstance_mthd() {
	//	createData();
	//}
	
}