@isTest
private class AG_TestBatch_CleanObjectShare {

	static Map<String,Account> mockAccounts = AG_TestUtil.getMockAccounts();
	static Map<String,Empleado__c> mockEmpleados = AG_TestUtil.getMockEmpleados();

  	@testSetup
    private static void setup() {

    	//Jerarquía de Roles: RoleT <- Role2 <- Role3 <- Role4
        UserRole topRole = new UserRole(name='RoleT');
        insert topRole;
        UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
        insert userRole2;
        UserRole userRole3 = new UserRole(name='Role3', parentRoleId = userRole2.Id);
        insert userRole3;
        UserRole userRole4 = new UserRole(name='Role4', parentRoleId = userRole3.id);
        insert userRole4;

        //Recuperamos profile: Standard User
        System.debug('Look for Standard USer profile ');
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

        //Dos usuarios: User1 (Role4 y Empleado=111) y User2(Role3 y Empleado=222)
        User theUser1 = new User(
            LastName = 'User1',                 FirstName='Test',   
            Alias = 'tu1',                      Email = 'test.user1@edelta.com',
            Username = 'test.user1@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole4.Id,           ID_Empleado__c= '111',isActive=true);
        insert theUser1;

        User theUser2 = new User(
            LastName = 'User2',                 FirstName='Test',
            Alias = 'tu',                       Email = 'test.user2@edelta.com',
            Username = 'test.user2@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US', 
            UserRoleId= userRole3.Id,           ID_Empleado__c='222', isActive=true);
        insert theUser2;

        User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
        System.runAs(adminUser) {


            Empleado__c empleado1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='111');
            insert empleado1;
            Empleado__c empleado2 = new Empleado__c(name='Empleado2', OwnerId=theUSer2.id, ID_Empleado__c='222');
            insert empleado2;

            //Dos Accounts: Account1 (Gestor=Empleado1, Owner=User1), Account2 (Gestor=Empleado2, Owner=User2)
            Account account1 = new Account(name='Account1', Gestor__c=empleado1.Id, OwnerId=theUser1.id);
            insert account1;
            System.debug('@#@# Account1:' + account1);

            Account account2 = new Account(name='Account2', Gestor__c=empleado2.Id, OwnerId=theUser2.id);
            insert account2;
            System.debug('@#@# Account2:' + account1);

            List<Account> xx = [SELECT Id, name, OwnerId FROM Account WHERE name like 'Account_'];
            System.debug('@#@# List accounts:' + xx);
        }
    }

    private static void initData() {

    	//Recuperamos las accounts creadas
		Account account1 = mockAccounts.get('Account1');
		Map<Id, AccountShare> account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		Account account2 = mockAccounts.get('Account2');
		Map<Id, AccountShare> account2Shares = AG_TestUtil.getMapAccountShares(account2.id);

		//Actualizamos flag como si se hibieses tocado
		account1.recalcular_permisos__c =true;
		update account1;
		account2.recalcular_permisos__c =true;
		update account2;

		System.debug('TTTT Account1 shares:' + account1Shares);
		System.debug('TTTT Account2 shares:' + account2Shares);

		Empleado__c empleado1 = mockEmpleados.get('Empleado1');
		Empleado__c empleado2 = mockEmpleados.get('Empleado2');

		//Creamos suministro, por lo que se generan shares.
		//Se crean con el Account y Empleado cruzados (Account1 con Empleado2 y Account2 con Empleado1) 
		//ya que si Account y Suministro tienen mismo owner, no se crean Shares
		Suministros__c supply1 = createSuministro('Supply1',account1, empleado2);
		account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		System.assert(account1shares.size()>0);

		Suministros__c supply2 = createSuministro('Supply2',account2, empleado1);
		account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		System.assert(account2shares.size()>0);

		List<Suministros__share> supply1Shares = AG_TestUtil.getSuministrosShare(supply1.id);
		System.assert(supply1Shares.size()>0);
		List<Suministros__share> supply2Shares = AG_TestUtil.getSuministrosShare(supply2.id);
		System.assert(supply1Shares.size()>0);
    }


	@isTest static void cleanObjectShareModified_Test() {

		initData();

		//Recuperamos las Accounts y las marcamos para que se actualicen sus objetos relacionados.
		Account account1 = mockAccounts.get('Account1');
		account1.recalcular_permisos_objetos__c = true;

		Account account2 = mockAccounts.get('Account2');
		account2.recalcular_permisos_objetos__c = true;

		update new List<Account> {account1, account2 };

		//Preaparamos ejecución
		Test.startTest();

		//Ejecutamos batch: clanAll=false, recalculateAfter=false
		Id batchId = AG_Batch_CleanObjectShare.executeProcess(false, false);
		System.debug('Batch Id:' + batchId);

		Map<String,Suministros__c> mapSuministros = getMockSuministros();
		Suministros__c supply1 = mapSuministros.get('Supply1');
		Suministros__c supply2 = mapSuministros.get('Supply2');

		Test.stopTest();

		//Despues del proceso de borrado, no tendrían que haber shares
		List<Suministros__share> supply1Shares = AG_TestUtil.getSuministrosShare(supply1.id);
		System.debug('TTTT Supply 1 shares:' + supply1Shares);
		System.assertEquals(supply1Shares.size(),0);

		List<Suministros__share> supply2Shares = AG_TestUtil.getSuministrosShare(supply2.id);
		System.debug('TTTT Supply 2 shares:' + supply2Shares);
		System.assertEquals(supply2Shares.size(),0);

		//Verificamos que la marca recalcular_permisos__c se mantiene ya que no se ha recalculado
		mockAccounts = AG_TestUtil.getMockAccounts();	//Recargamos Acocunts de la BD.
		account1 = mockAccounts.get('Account1');
		account2 = mockAccounts.get('Account2');

		System.assertEquals(account1.recalcular_permisos_objetos__c,true);
		System.assertEquals(account2.recalcular_permisos_objetos__c,true);
	}


	@isTest static void cleanAllObjectShareModified_Test() {

		initData();

		//Recuperamos las Accounts verificamos que no tienen la marca, 
		//ya que ahora lanzamos el proceso para todos los accounts.
		Account account1 = mockAccounts.get('Account1');
		System.assertEquals(account1.recalcular_permisos_objetos__c ,false);

		Account account2 = mockAccounts.get('Account2');
		System.assertEquals(account2.recalcular_permisos_objetos__c ,false);

		//Preaparamos ejecución
		Test.startTest();

		//Ejecutamos batch: clanAll=false, recalculateAfter=false
		Id batchId = AG_Batch_CleanObjectShare.executeProcess(true, false);
		System.debug('Batch Id:' + batchId);

		Map<String,Suministros__c> mapSuministros = getMockSuministros();
		Suministros__c supply1 = mapSuministros.get('Supply1');
		Suministros__c supply2 = mapSuministros.get('Supply2');

		Test.stopTest();

		//Despues del proceso de borrado, no tendrían que haber shares
		List<Suministros__share> supply1Shares = AG_TestUtil.getSuministrosShare(supply1.id);
		System.debug('TTTT Supply 1 shares:' + supply1Shares);
		System.assertEquals(supply1Shares.size(),0);

		List<Suministros__share> supply2Shares = AG_TestUtil.getSuministrosShare(supply2.id);
		System.debug('TTTT Supply 2 shares:' + supply2Shares);
		System.assertEquals(supply2Shares.size(),0);
	}

	@isTest static void cleanAndRecalculateObjectShare() {

		initData();

		Empleado__c empleado1 = mockEmpleados.get('Empleado1');
		Empleado__c empleado2 = mockEmpleados.get('Empleado2');

		//Recuperamos las Accounts y las marcamos para que se actualicen sus objetos relacionados.
		Account account1 = mockAccounts.get('Account1');
		account1.gestor__c = empleado2.id;

		Account account2 = mockAccounts.get('Account2');
		account2.gestor__c = empleado1.id;
		update new List<Account> {account1, account2 };

		account1.gestor__c = empleado1.id;
		account2.gestor__c = empleado2.id;
		update new List<Account> {account1, account2 };

		//Vamos a recargar para verificar que la marca de recalcular_permisos_objetos__c se ha activado.
		mockAccounts = AG_TestUtil.getMockAccounts();
		
		account1 = mockAccounts.get('Account1');
		System.assert(!account1.recalcular_permisos_objetos__c);
		account2 = mockAccounts.get('Account2');
		System.assert(!account2.recalcular_permisos_objetos__c);

		//Preaparamos ejecución
		Test.startTest();

		//Ejecutamos batch: clanAll=true, recalculateAfter=false
		Id batchId = AG_Batch_CleanObjectShare.executeFullProcess(false);
		System.debug('Batch Id:' + batchId);

		Map<String,Suministros__c> mapSuministros = getMockSuministros();
		Suministros__c supply1 = mapSuministros.get('Supply1');
		Suministros__c supply2 = mapSuministros.get('Supply2');

		Test.stopTest();

		//Despues del proceso de borrado, si ha de haber permisos ya que s ehan recalculado
		List<Suministros__share> supply1Shares = AG_TestUtil.getSuministrosShare(supply1.id);
		System.debug('TTTT Supply 1 shares:' + supply1Shares);
		System.assert(supply1Shares.size()>0);

		List<Suministros__share> supply2Shares = AG_TestUtil.getSuministrosShare(supply2.id);
		System.debug('TTTT Supply 2 shares:' + supply2Shares);
		System.assert(supply2Shares.size()>0);

		//Verificamos que la marca recalcular_permisos__c se ha eliminado ya que se ha recalculado todo
		mockAccounts = AG_TestUtil.getMockAccounts();	//Recargamos Acocunts de la BD.
		account1 = mockAccounts.get('Account1');
		account2 = mockAccounts.get('Account2');
		System.assertEquals(account1.recalcular_permisos_objetos__c,false);
		System.assertEquals(account2.recalcular_permisos_objetos__c,false);
	}

	//////////////////////////

    private static Suministros__c createSuministro(String supplyName, Account cliente, Empleado__c gestor) {

        Suministros__C supply1 = new Suministros__C(
            name= supplyName, 
            Cliente__C= cliente.id,
            gestor__c=gestor.id);
        insert supply1;

        return [SELECT Id, name, Cliente__c, Gestor__c, OwnerId, Gestor_cliente__c, Gestor_external_Key__c FROM Suministros__c WHERE Id = :supply1.id];

        return supply1;
    }

    private static Map<String,Suministros__c> getMockSuministros() {

		Map<String,Suministros__c> mapSupplies = new Map<String,Suministros__c>();
    	Suministros__c[] supplies = [SELECT Id, name, Gestor__c, Gestor_external_Key__c, cliente__c, Gestor_Cliente__c, OwnerId FROM Suministros__c];
    	for (Suministros__c supply : supplies) {
    		mapSupplies.put(supply.name,supply);
    	}
    	return mapSupplies;
    }



}