@isTest
private class GNF_OpportunityTriggerHandler_cls_TEST {
  
    @isTest
    static void test_method_one() {
        /* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
        SMC_TestData_cls.createData();
        /* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
        
        Test.startTest();
        GNF_OpportunityTriggerHandler_cls.getSOQL( 'Opportunity', 'WHERE Id != null' );
        
        String strIdAcc                            = (String) [Select AccountId FROM Opportunity LIMIT 1].AccountId;
        List<Account> lstAccountsToDelete          = [Select Id FROM Account WHERE Id =: strIdAcc];
        String strAccountsToDelete                 = System.JSON.serialize( lstAccountsToDelete );
        
        //GNF_OpportunityTriggerHandler_cls.updateConsumosClickAsync( JSON.serialize( [SELECT Id FROM Consumo_Click__c LIMIT 1] ) );
        
        SMC_TestData_cls.createSMC();
        
        Condiciones_de_Cobertura__c objCoverageCondition = new Condiciones_de_Cobertura__c();
        objCoverageCondition = [SELECT 
                                    Id,
                                    SMC_Oportunidad__c,
                                    Estado__c,
                                    SMC_ReadOnly__c
                                FROM 
                                    Condiciones_de_Cobertura__c
                                LIMIT 1];
        
        objCoverageCondition.SMC_Oportunidad__c           = SMC_TestData_cls.objOpportunity.Id;
        objCoverageCondition.Estado__c                    = 'Histórico';
        objCoverageCondition.SMC_ReadOnly__c              = false;
        objCoverageCondition.Tipo_ToP_Clicks__c           = null;
        update objCoverageCondition;
        
        update SMC_TestData_cls.objOpportunity;     
        
        Test.stopTest();
    }
    
    @isTest
    static void test_method_two() {
        Test.startTest();
        
        SMC_TestData_cls.createData();
                
        Account account = [SELECT 
                                Id,
                                Name,
                                Ficticio__c
                           FROM 
                                Account
                           LIMIT 1];
        
        Account accountChild = new Account();
        accountChild.Name = 'Child Account';
        accountChild.ParentId = account.Id;
        
        insert accountChild;
        
        GNF_OpportunityTriggerHandler_cls.validateParentAccount(new Set<Id> { account.Id });
        
        Test.stopTest();
    }
}