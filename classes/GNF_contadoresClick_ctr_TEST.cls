/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_contadoresClick_ctr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-08		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_contadoresClick_ctr_TEST {
	
	static testMethod void scenarioOne() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Opportunity objOppotunity											= [SELECT Id
																				FROM Opportunity
																				WHERE RecordType.DeveloperName = 'Click'
																				AND Name = 'SCAMARA_TARGET_PRICE'
																				LIMIT 1
																			];
		
		Test.startTest();
		
			ApexPages.StandardController sCtrOpportunity					= new ApexPages.StandardController( objOppotunity );
			GNF_contadoresClick_ctr ctrContadoresClick						= new GNF_contadoresClick_ctr( sCtrOpportunity );
			ctrContadoresClick.mthd_getContadoresClick();
			
		Test.stopTest();
	}
}