public class AG_LogicActividad_Comercial {


    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicActividad_Comercial() {}

    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<Actividad_Comercial__c>}
     */
    public void assignOwnerFromEmpleado(final List<Actividad_Comercial__c> newList, final Map<Id, Actividad_Comercial__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: start' + newList.size());

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();

        if (oldMap==null) oldMap = new Map<Id, Actividad_Comercial__c>(); //just to avoid null checks

        // Iterate over actividad_comercials and find and assign owner based on its empleado
        for(Actividad_Comercial__c theActividad_Comercial : newList) {

          if(theActividad_Comercial.Gestor_External_Key__c <> null) {

            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theActividad_Comercial.Id), theActividad_Comercial)) {

              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theActividad_Comercial.managers__c = managersEmpleados.get(theActividad_Comercial.Gestor_External_Key__c);

              String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theActividad_Comercial.Gestor_External_Key__c,empleadoTable,userTable);
              if(assignedOwner <> null) {
                theActividad_Comercial.OwnerId = assignedOwner;
              } else {
                //TODO: What todo in if not find a valid user?
                final String errMsg = 'No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_actividad_comercial=' + theActividad_Comercial.ID_Codigo_Actividad__c + ' - gestor_external_key='+ theActividad_Comercial.Gestor_External_Key__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug(loggingLevel.Error, errMsg);
                //throw new Actividad_ComercialLogicException(errMsg);
              }
            }   
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '#ERROR# No esta informada el gestor para el actividad_comercial :' + theActividad_Comercial.ID_Codigo_Actividad__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new Actividad_ComercialLogicException('No esta informada el gestor para el actividad_comercial :' + theActividad_Comercial.ID_Codigo_Actividad__c);
          }

        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }
    
    /**
     * Add sharing roles to Actividad_Comercial__c and Account to allow visibility to role hierachy.
     */
    public void setCustomSharing(final List<Actividad_Comercial__c> newList, final Map<Id, Actividad_Comercial__c> newMap,
                                 final List<Actividad_Comercial__c> oldList, final Map<Id, Actividad_Comercial__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ SetCustomSharing: start');

        //List of actividad_comercial sharing records to be inserted
        List<Actividad_Comercial__Share> actividad_comercialSharingList = new List<Actividad_Comercial__Share>();

        //List of account sharing records to be inserted
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, Actividad_Comercial__c>(); //just to avoid null checks

         //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();

        //Iterate over collection
        for(Actividad_Comercial__c theActividad_Comercial : newList) {

            Id actividad_comercialId = theActividad_Comercial.Id;
            Actividad_Comercial__c oldActividad_Comercial = oldMap.get(actividad_comercialId);
            Actividad_Comercial__c newActividad_Comercial = theActividad_Comercial;

            //Se comenta debug(Victor Velandia (VV))
            /*System.debug('@@@@ ID actividad_comercial:' + actividad_comercialId);
            System.debug('@@@@ Old actividad_comercial:' + oldActividad_Comercial);
            System.debug('@@@@ New actividad_comercial:' + newActividad_Comercial);*/

            //Get only new records or records whose cliente has changed or owner has changed
            if (hasChangedCliente(oldActividad_Comercial, newActividad_Comercial)) {

                if (oldActividad_Comercial!=null)  //si es insert no hay previos que borrar
                  Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(oldActividad_Comercial.Id);

                //get the role of the user assigned to the record account
                Id ownerActividadComercial = theActividad_Comercial.ownerId;
                Id ownerRelatedAccount = theActividad_Comercial.Gestor_Cliente__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug('@@@@ Actividad_Comercial gestion_cliente__C:' + ownerRelatedAccount);

                if (ownerActividadComercial!= ownerRelatedAccount) {
                  //List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToShare(ownerRelatedAccount);
                  List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount,allUsers,allRoles,allGroups);

                  for (Id grpOrUsrId : gruposRoleSuperiores) {
                    Actividad_Comercial__Share clienteAbobeRoleSharing = createActividad_ComercialSharedRecord(theActividad_Comercial, grpOrUsrId);
                    actividad_comercialSharingList.add(clienteAbobeRoleSharing);
                  }
                }
              }

            //Si es un nuevo Oferta comercial, calculamos los permisops para el Account en el trigger,
            //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.

            //El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
            if (oldActividad_Comercial!=null && hasChangedOwner(oldActividad_Comercial, newActividad_Comercial)) {

              accountToRecalculate.add(theActividad_Comercial.Cliente__c);

            //Se trata de un alta de Oferta Comercial --> calculamos permisos a dar al Account
            } else if (oldActividad_Comercial==null) { 
                
                if(!String.isBlank(theActividad_Comercial.Cliente__c)) {  //No hay nada que hacer, no hay cliente

                  Id accountOwnerId = theActividad_Comercial.Gestor_Cliente__c;
                  Id actividad_comercialOwnerId = theActividad_Comercial.OwnerId;

                  if (accountOwnerId!=actividad_comercialOwnerId) {

                    //Se comenta debug(Victor Velandia (VV))
                    /*System.debug('@@@Account Propietrario actividad_comercial:' + actividad_comercialOwnerId);
                    System.debug('@@@Account Propietrario cuenta:' + accountOwnerId);
                    System.debug('@@@Account Cliente:' + theActividad_Comercial.cliente__r.name);*/

                    List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(actividad_comercialOwnerId,allUsers,allRoles,allGroups);
                    for (Id grpOrUsrId : grpOrUsrIdToShare) {
                      AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(theActividad_Comercial.Cliente__c, grpOrUsrId);
                      accountSharingList.add(accountSharing);
                    }

                    //Se comenta debug(Victor Velandia (VV))
                    //System.debug('###Account Account Share:' + accountSharingList);
                  }
                }
            }
        }

        if (!accountToRecalculate.isEmpty()) {
          AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!actividad_comercialSharingList.isEmpty())
          upsert actividad_comercialSharingList;


        if(!accountSharingList.isEmpty())
          upsert accountSharingList;

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS

   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldActividad_Comercial is null.
    */
    @testVisible
    private boolean hasChangedGestor(Actividad_Comercial__c oldActividad_Comercial, Actividad_Comercial__c newActividad_Comercial) {

      if (oldActividad_Comercial==null) return true;   //new object
      return (oldActividad_Comercial.Gestor__c <> newActividad_Comercial.Gestor__c);
    }      

    /*
     * @return true if the field value "Cliente__c" has changed or oldActividad_Comercial is null.
    */
    @testVisible
    private boolean hasChangedCliente(Actividad_Comercial__c oldActividad_Comercial, Actividad_Comercial__c newActividad_Comercial) {

      if (oldActividad_Comercial==null) return true;   //new object
      return (oldActividad_Comercial.cliente__c <> newActividad_Comercial.Cliente__c);
    }      

    /*
    * @return true if the owner has changed or oldActividad_Comercial is null.
    */
    @testVisible
    private boolean hasChangedOwner(Actividad_Comercial__c oldActividad_Comercial, Actividad_Comercial__c newActividad_Comercial) {

      if (oldActividad_Comercial==null) return true;   //new object
      return (oldActividad_Comercial.ownerId <> newActividad_Comercial.OwnerId);
    }      

    /**
     * Crea  Actividad_Comercial__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible
    private Actividad_Comercial__Share createActividad_ComercialSharedRecord(final Actividad_Comercial__c actividad_comercial, Id userOrGroup) {
        return new Actividad_Comercial__Share(
                  ParentId = actividad_comercial.Id,
                  UserOrGroupId = userOrGroup, //actividad_comercial.Gestor_Cliente__c,
                  AccessLevel = SHARE_MODE_READ,
                  RowCause = Schema.Actividad_Comercial__Share.RowCause.Apex_Sharing__c);
    }


    /*
    * Remove all the sharing added from Apex.
    */
    @testVisible
    private Integer deleteActividad_ComercialSharing(Id actividad_comercialId) {

      List<Actividad_Comercial__Share> shareList = [SELECT Id FROM Actividad_Comercial__Share WHERE parentId = :actividad_comercialId AND RowCause = :Schema.Actividad_Comercial__Share.RowCause.Apex_Sharing__c];
      Integer count = shareList.size();
      if (count>0)
        delete shareList;

      return count;
    }
   

    //-- CUSTOM EXCEPTION
    public class Actividad_ComercialLogicException extends Exception {}

}