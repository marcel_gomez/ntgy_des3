/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_ShareRecords_ctr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-08		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_ShareRecords_ctr_TEST {
	
	static testMethod void scenarioOne() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Test.startTest();
		
			List<Account> lstAccounts										= [SELECT Id
																				FROM Account
																			];
																			
			List<User> lstUsers												= [SELECT Id
																				FROM User
																				WHERE IsActive = true
																				AND UserRole.DeveloperName != 'DV_IBERIA_IBERIA'
																				AND ( 
																					UserRole.DeveloperName LIKE 'GV%'
																					OR UserRole.DeveloperName LIKE 'JV%'
																					OR UserRole.DeveloperName LIKE 'DV%'
																				)
																			];
			
			List<String> lstUsersId											= new List<String>();
			for( User obUser : lstUsers ){
				lstUsersId.add( obUser.Id ); 
			}
																			
			GNF_ShareRecords_ctr ctrShareRecordsCmp							= new GNF_ShareRecords_ctr();
			ctrShareRecordsCmp.strSObjectType								= 'Account';
			ctrShareRecordsCmp.objRecordList								= lstAccounts;
			ctrShareRecordsCmp.load();

			ApexPages.StandardSetController sCtrAccounts					= new ApexPages.StandardSetController( lstAccounts );
			GNF_ShareRecords_ctr ctrShareRecords							= new GNF_ShareRecords_ctr( sCtrAccounts );
			ctrShareRecords.loadComponent();
			ctrShareRecords.strSelectedType									= 'Usuarios';
			ctrShareRecords.changeType();
			ctrShareRecords.lstSelected2Add									= lstUsersId;
			
		Test.stopTest();
	}
	
	static testMethod void scenarioTwo() {
		SMC_TestData_cls.createData();
		
		Test.startTest();
			List<Account> lstAccounts										= [SELECT Id
																				FROM Account
																			];
																			
			List<User> lstUsers												= [SELECT Id
																				FROM User
																				WHERE IsActive = true
																				AND UserRole.DeveloperName != 'DV_IBERIA_IBERIA'
																				AND ( 
																					UserRole.DeveloperName LIKE 'GV%'
																					OR UserRole.DeveloperName LIKE 'JV%'
																					OR UserRole.DeveloperName LIKE 'DV%'
																				)
																			];
			
			List<String> lstUsersId											= new List<String>();
			for( User obUser : lstUsers ){
				lstUsersId.add( obUser.Id ); 
			}
		
			GNF_ShareRecords_ctr ctrShareRecordsNoStd						= new GNF_ShareRecords_ctr();
			ctrShareRecordsNoStd.strSObjectType								= 'Account';
			ctrShareRecordsNoStd.load();
			ctrShareRecordsNoStd.lstRecords									= lstAccounts;
			ctrShareRecordsNoStd.loadComponent();
			ctrShareRecordsNoStd.strSelectedType							= 'Usuarios';
			ctrShareRecordsNoStd.changeType();
			ctrShareRecordsNoStd.lstSelected2Add							= lstUsersId;
			ctrShareRecordsNoStd.add();
			ctrShareRecordsNoStd.lstSelected2Quit							= new List<String>{ lstUsersId.get( 0 ), lstUsersId.get( 1 ) };
			ctrShareRecordsNoStd.remove();
			ctrShareRecordsNoStd.shareRecords();
			ctrShareRecordsNoStd.getFieldSet();
			List<SelectOption> lstTypeList									= ctrShareRecordsNoStd.lstTypeList;
		
		Test.stopTest();
	}
}