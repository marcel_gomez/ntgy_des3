/**
* VASS
* @author           Juan Cardona juan.cardona@vass.es
* Project:          Gas Natural Fenosa
* Description:      Clase controladora 
*                   
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-06-26      Juan Cardona (JSC)      Definicion inicial de la clase.
*********************************************************************************************************/

public without sharing class GNF_cotizador_ctr 
{
    
    public String                           str_idOpp                   {get; Set;}
    public List<Cotizacion_de_periodo__c>   listCotPeriodos             {get; Set;}

	public GNF_cotizador_ctr(){
        this.str_idOpp = ApexPages.currentPage().getParameters().get('id');

        listCotPeriodos = new List<Cotizacion_de_periodo__c>();
    }

    /**/
    @RemoteAction   
    public static wrp_infoCotizador_cls rmt_onloadpage(String str_idOpp)
    {
        system.debug('<< GNF_cotizador_ctr.rmt_onloadpage >> str_idOpp: ' + str_idOpp);   
        
        if(str_idOpp==null)
            return null;
            
        return new wrp_infoCotizador_cls(str_idOpp);
    }

    /**/
    public class wrp_infoCotizador_cls
    {
        public String                           str_idClick;
        public String                           str_idAcc;

        public String                           str_stageName;
        public String                           str_noClick;
        public String                           str_accName;
        public String                           str_tipo_Operacion;             
        public Datetime                         dttm_fechaPresentacion;    
        public String                           str_tipoPrecio;
        public String                           str_tipoServicio;
        public String                           str_tipoFijacion;               
        public String                           str_tipoProducto;
        public String                           str_tipoProductoDesc;
        public String                           str_metodoAplicacion; 
        public String                           str_periodicidadPrecio; 
        public String                           str_formulaCliente;
        public String                           str_formulaDestino;
        public String                           str_formulaACubrir;
        public String                           str_primaRiesgo;
        public String                           str_targetPrice;
        public String                           str_cvObjetivo;
        public String                           str_cvFinalObjetivo;
        public Datetime                         dttm_fechaFinOrdenCierre;
        public Datetime                         dttm_fechaLimiteValidez;
        public String                           str_trader;
        public Decimal                          intCantidadPS;
		public Boolean 							bln_calculosFinalizados;

        public Decimal dc_comsumosKwh;
        public Decimal dc_comsumosMMbtu;    
        
        public List<wrp_infoConsumos_cls>       lst_infoConsumos;
        
        public Map<String, Map<String, String>> map_consumoCentKwhXmesXperiodo = new Map<String, Map<String, String>>();
        public Map<String, Map<String, String>> map_consumoMmbtuwhXmesXperiodo = new Map<String, Map<String, String>>();

        public wrp_infoCotizador_cls(String str_inIdOpp) {
            wrp_infoConsumos_cls        obj_infoConsumos;

            Map<String, Set<String>>    map_MesXperiodoCotizacion;   

            system.debug('<< GNF_cotizador_ctr.wrp_infoCotizador_cls >> str_inIdOpp: ' + str_inIdOpp);    
            
            if(str_inIdOpp==null)
                return;
            
            this.lst_infoConsumos        = new List<wrp_infoConsumos_cls>();
			this.bln_calculosFinalizados = false;
            map_MesXperiodoCotizacion    = new Map<String, Set<String>>();

            for(Opportunity obj_opp : GNF_cotizador_cls.getObjOpp(new List<String>{ str_inIdOpp })) {
                this.str_idClick                = obj_opp.Id != NULL ? obj_opp.Id : null;
                this.str_idAcc                  = obj_opp.AccountId != NULL ? obj_opp.AccountId : null;
                this.str_stageName              = obj_opp.StageName != NULL ? obj_opp.StageName : null;
                this.str_noClick                = obj_opp.ID_Opportunity__c != null ? obj_opp.ID_Opportunity__c : null;
                this.str_accName                = obj_opp.Account.Name != null ? obj_opp.Account.Name : null;
                this.str_tipo_Operacion         = obj_opp.recordTypeId != NULL ? obj_opp.recordType.Name : null;  
                this.dttm_fechaPresentacion     = obj_opp.Fecha_Cotizacion__c != NULL ? obj_opp.Fecha_Cotizacion__c : null;
                this.str_tipoPrecio             = obj_opp.Tipo_de_precio__c != NULL ? obj_opp.Tipo_de_precio__c : null;
                this.str_tipoServicio           = obj_opp.Tipo_de_servicio__c != NULL ? obj_opp.Tipo_de_servicio__c : null;           
                this.str_tipoProducto           = obj_opp.Tipo_Producto__c != NULL ? obj_opp.Tipo_Producto__c : null; 
                this.str_tipoProductoDesc       = obj_opp.Tipo_Producto_Mail__c != NULL ? obj_opp.Tipo_Producto_Mail__c : null;   
                this.str_tipoFijacion           = obj_opp.Tipo_Fijacion__c != NULL ? obj_opp.Tipo_Fijacion__c : null;
                this.str_metodoAplicacion       = obj_opp.Metodo_aplicacion__c != NULL ? obj_opp.Metodo_aplicacion__c : null;
                this.str_periodicidadPrecio     = obj_opp.Periodicidad_del_precio__c != NULL ? obj_opp.Periodicidad_del_precio__c : null;
                this.str_formulaCliente         = obj_opp.Formula__c != NULL ? obj_opp.Formula__c : null;     
                this.str_formulaACubrir         = obj_opp.Formula_a_cubrir__c != NULL ? obj_opp.Formula_a_cubrir__c : null;       
                this.str_formulaDestino         = obj_opp.Formula_destino_Descripcion__c != NULL ? obj_opp.Formula_destino_Descripcion__c : null; 
        
                this.str_primaRiesgo            = obj_opp.Prima_de_riesgo__c != NULL ? String.valueOf(obj_opp.Prima_de_riesgo__c) : null;
                this.str_targetPrice            = obj_opp.Target_Price__c != NULL ? String.valueOf(obj_opp.Target_Price__c) : null;     
                this.str_cvObjetivo             = obj_opp.Precio_final_objetivo__c != NULL ? String.valueOf(obj_opp.Precio_final_objetivo__c) : null;
                this.str_cvFinalObjetivo        = obj_opp.GNF_cvFinalObjetivoKwh__c != NULL ? String.valueOf(obj_opp.GNF_cvFinalObjetivoKwh__c) : null;                         
                this.dttm_fechaFinOrdenCierre   = obj_opp.Fecha_Fin_Orden_de_Cierre__c != NULL ? obj_opp.Fecha_Fin_Orden_de_Cierre__c : null;
                this.dttm_fechaLimiteValidez    = obj_opp.GNF_FechaLimiteValidez__c != NULL ? obj_opp.GNF_FechaLimiteValidez__c : null;   
                this.str_trader                 = obj_opp.GNF_Trader__c != NULL ? obj_opp.GNF_Trader__c : null;
                this.intCantidadPS              = obj_opp.SMC_CantidadPS__c!= null ? obj_opp.SMC_CantidadPS__c : 0.0;
            }           
                        
            dc_comsumosKwh = 0;
            dc_comsumosMMbtu = 0;

            for(Consumo_Click__c cClick : GNF_cotizador_cls.getObjConsumosClick(new List<String> { str_inIdOpp })) {
                if(cClick.Mes_de_aplicacion__c != NULL && cClick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL) {
                    
                    if(cClick.Q_comprometida_kWh__c != NULL && cClick.Q_comprometida_kWh__c > 0) {
                        Boolean mapkWh_ContainsFormula = map_consumoCentKwhXmesXperiodo.containsKey(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c);

                        if(mapkWh_ContainsFormula) {
                            Map<String, String> mesXperiodo_Map = map_consumoCentKwhXmesXperiodo.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c);

                            if(mesXperiodo_Map.containsKey(cClick.Mes_de_aplicacion__c)){
                                dc_comsumosKwh = cClick.Q_comprometida_kWh__c + Decimal.valueOf(mesXperiodo_Map.get(cClick.Mes_de_aplicacion__c));
                                mesXperiodo_Map.put(cClick.Mes_de_aplicacion__c, String.valueOf(dc_comsumosKwh));
                            }
                            else {
                                mesXperiodo_Map.put(cClick.Mes_de_aplicacion__c, String.valueOf(cClick.Q_comprometida_kWh__c));                      
                            }
                        }
                        else {                      
                            map_consumoCentKwhXmesXperiodo.put(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c, 
                                                               new Map<String,String> { 
                                                                    cClick.Mes_de_aplicacion__c => String.valueOf(cClick.Q_comprometida_kWh__c)  
                                                               });
                        }
                    }
                    
                    if(cClick.Q_comprometida_MMbtu__c != NULL && cClick.Q_comprometida_MMbtu__c > 0) {
                        Boolean mapBTU_ContainsFormula = map_consumoMmbtuwhXmesXperiodo.containsKey(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c);

                        if(mapBTU_ContainsFormula) {
                            if(map_consumoMmbtuwhXmesXperiodo.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).containsKey(cClick.Mes_de_aplicacion__c)) {
                                dc_comsumosMMbtu = cClick.Q_comprometida_MMbtu__c + Decimal.valueOf(map_consumoMmbtuwhXmesXperiodo.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).get(cClick.Mes_de_aplicacion__c));
                                map_consumoMmbtuwhXmesXperiodo.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).put(cClick.Mes_de_aplicacion__c, String.valueOf(dc_comsumosMMbtu));
                            }
                            else {
                                map_consumoMmbtuwhXmesXperiodo.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).put(cClick.Mes_de_aplicacion__c, String.valueOf(cClick.Q_comprometida_MMbtu__c));                        
                            }
                        }
                        else{                      
                            map_consumoMmbtuwhXmesXperiodo.put(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c, new Map<String,String> { cClick.Mes_de_aplicacion__c => String.valueOf(cClick.Q_comprometida_MMbtu__c)  });                    
                        }
                    }
                }
            }           
            
            for(Consumo_Click__c cClick : GNF_cotizador_cls.getObjConsumosClick(new List<String> { str_inIdOpp })) {
                Boolean mapContainsFormula = map_MesXperiodoCotizacion.containsKey(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c);

                if(cClick.Mes_de_aplicacion__c != NULL && cClick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL &&
                   (!mapContainsFormula || 
                    (mapContainsFormula && !map_MesXperiodoCotizacion.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).contains(cClick.Mes_de_aplicacion__c)) 
                   )
                ) {
                    if(mapContainsFormula && !map_MesXperiodoCotizacion.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).contains(cClick.Mes_de_aplicacion__c))
                        map_MesXperiodoCotizacion.get(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c).add(cClick.Mes_de_aplicacion__c);
                    else
                        map_MesXperiodoCotizacion.put(cClick.Cotizacion_de_periodo__r.Periodo_Formula__c, new Set<String> { cClick.Mes_de_aplicacion__c });
                                    
                    obj_infoConsumos = new wrp_infoConsumos_cls(cClick, map_consumoCentKwhXmesXperiodo, map_consumoMmbtuwhXmesXperiodo);
                    lst_infoConsumos.add(obj_infoConsumos);               
                }
            }
        }

        /*
        */
        public String getDefaultValuesbyField(String str_sObject, String str_fieldSelected)
        {
            String                              str_defaultValue;

            system.debug('<<GNF_Wizard_CTR.getDefaultValuesbyField>> str_sObject: ' + str_sObject + ' str_fieldSelected: ' + str_fieldSelected);

         
            Schema.SObjectType                  objType                     = schema.getGlobalDescribe().get(str_sObject);
            sObject                             obj_sObject                 = objType.newSObject(null, true);
            Schema.DescribeSObjectResult        objDescribe                 = objType.getDescribe();  
            Map<String, Schema.SObjectField>    mapFieldsByName             = new Map<String, Schema.SObjectField>(objDescribe.fields.getMap());

            //if(mapFieldsByName.containsKey(str_fieldSelected) && mapFieldsByName.get(str_fieldSelected).getDescribe().isDefaultedOnCreate())
            //    str_defaultValue = '' + mapFieldsByName.get(str_fieldSelected).getDescribe().getDefaultValue();

            if(mapFieldsByName.containsKey(str_fieldSelected) && obj_sObject.get(str_fieldSelected) != NULL)
                str_defaultValue = '' + obj_sObject.get(str_fieldSelected);

            system.debug('<<GNF_Wizard_CTR.getDefaultValuesbyField>> str_defaultValue: ' + str_defaultValue);

            return str_defaultValue;
        }//ends getDefaultValuesbyField 
    }//ends wrp_infoCotizador_cls
    
    
    @RemoteAction
    public static Map<String, Database.SaveResult[]> saveData(wrp_infoCotizador_cls wrapper, String strDateTime){
        Database.SaveResult[] results = new List<Database.SaveResult>();
        Map<String, Database.SaveResult[]> mapaResults = new Map<String, List<Database.SaveResult>>();
        
        Opportunity opp = new Opportunity();
        opp.Id          = wrapper.str_idClick;

        //opp.GNF_FechaLimiteValidez__c = wrapper.dttm_fechaLimiteValidez;
        opp.GNF_Trader__c               = wrapper.str_trader; 
        opp.StageName                   = wrapper.str_stageName;
        opp.Tipo_de_servicio__c         = wrapper.str_tipoServicio;
        opp.GNF_calculosFinalizados__c  = false;    

        if(strDateTime != null) 
            opp.GNF_FechaLimiteValidez__c = Datetime.valueOf(strDateTime);
        
        List<Cotizacion_de_periodo__c> listCot = new List<Cotizacion_de_periodo__c>();
        Set<Id> SetCot = new Set<Id>();
        
        for (wrp_infoConsumos_cls consumo :  wrapper.lst_infoConsumos){
            Cotizacion_de_periodo__c cot = new Cotizacion_de_periodo__c();
            cot.Id = consumo.str_idCotizacion;

            cot.CV_Cliente__c               = String.isNotBlank(consumo.str_cvCliente) ? Decimal.valueOf(consumo.str_cvCliente) : 0;
            cot.CV_FRC_c_kWh__c             = String.isNotBlank(consumo.str_cvFrcInicialKWh) ? Decimal.valueOf(consumo.str_cvFrcInicialKWh) : 0;
            cot.CV_FRC_Inicial__c           = String.isNotBlank(consumo.str_cvFrcInicialMMBTu) ? Decimal.valueOf(consumo.str_cvFrcInicialMMBTu) : 0;
            cot.CV_FRC_Final_c_KWh__c       = String.isNotBlank(consumo.str_cvFrcFinalKWh) ? Decimal.valueOf(consumo.str_cvFrcFinalKWh) : 0;
            cot.CV_FRC_Final__c             = String.isNotBlank(consumo.str_cvFrcFinalMMBtu) ? Decimal.valueOf(consumo.str_cvFrcFinalMMBtu) : 0;
            cot.GNF_FormulaDestinoFinal__c  = String.isNotBlank(consumo.str_formulaDestinoFinalKwh) ? Decimal.valueOf(consumo.str_formulaDestinoFinalKwh) : 0;
            cot.GNF_TradeGroupCompra__c     = consumo.str_tradeGroupCompraSwap;
            cot.GNF_ContratoEspejoCompra__c = consumo.str_contratoEspejoCompraSwap;
            cot.GNF_TradeGroupVenta__c      = consumo.str_tradeGroupVentaSwap;
            cot.GNF_ContratoEspejoVenta__c  = consumo.str_contratoEspejoVentaSwap;
            cot.Brent_de_referencia__c      = String.isNotBlank(consumo.str_brentReferenciaBbl) ? Decimal.valueOf(consumo.str_brentReferenciaBbl) : 0;
            cot.Paridad_de_referencia_USD_EUR__c = String.isNotBlank(consumo.str_paridadReferenciaUsdEur) ? Decimal.valueOf(consumo.str_paridadReferenciaUsdEur) : 0;
            cot.GNF_FormulaDestinoInicial__c     = String.isNotBlank(consumo.str_cvFormulaDestinoinicialKwh) ? Decimal.valueOf(consumo.str_cvFormulaDestinoinicialKwh) : 0;

            if(!SetCot.contains(cot.Id)){
                SetCot.add(cot.Id);

                listCot.add(cot);
            }
        }
        
        results.add(Database.Update(opp, false));       
        results = Database.Update(listCot, false);
        
        mapaResults.put('First', results);

        GNF_cotizador_cls cls = new GNF_cotizador_cls();
        cls.CalculoFormulasSite(opp.Id);

        opp.GNF_Precio_CV_FRC_Final_medio_pond_c_KWh__c = formatoDecimal(cls.preciocvfrcfinalmedioponderado, 4);
        opp.GNF_Precio_CV_CLIENTE_medio_pond_c_KWh__c   = formatoDecimal(cls.preciocvclientemedioponderado, 4);
        opp.GNF_Precio_CV_F2_medio_pond_c_KWh__c        = formatoDecimal(cls.preciocvf2medioponderado, 4);
        
        if(opp.Tipo_de_servicio__c == 'SWAP Fórmula' || opp.Tipo_de_servicio__c == 'SWAP Hub')
            opp.GNF_Spread__C = Decimal.valueOf(cls.spread).divide(1,4);

        if(opp.Tipo_de_servicio__c == 'SWAP Fórmula' || opp.Tipo_de_servicio__c == 'SWAP Hub')
            opp.GNF_Spread_Cliente__c = Decimal.valueOf(cls.spreadofertado).divide(1,4);

        opp.GNF_Precio_CV_FRC_inicial_medio_pond__c = formatoDecimal(cls.preciocvfrcinicialmedioponderado, 4);
        opp.GNF_Media_pond_de_Brent_bbl__c          = formatoDecimal(cls.MediaPonderadaBrent, 4);
        opp.Media_pond_de_Paridad_eur__c            = formatoDecimal(cls.MediaPonderadaParidad, 4);
        //opp.Diferencial_cierre_FRC_Medio__c       = cls.diferencialdecierrefrcmedio;
        //opp.Diferencial_de_cierre_F2_medio_c_kWh__c = cls.diferencialdecierref2medio;
        opp.GNF_Precio_CVF2_cerrado_medio_pond_c_kWh__c = formatoDecimal(cls.preciocvf2cerradomedioponderado, 4);
        opp.GNF_MediaPonderadaCVClientePrima__c         = formatoDecimal(cls.MediaPonderadaCVPrimaClienteClick, 4);

        opp.GNF_precio_CV_FRC_Final_medio_pond_MMBtu__c = formatoDecimal(cls.preciocvfrcfinalmedioponderadommbtu, 4);
        opp.GNF_PrecioCV_FRC_inicialMedioPond_MMBtu__c  = formatoDecimal(cls.preciocvfrcinicialmedioponderadommbtu, 4);

        opp.GNF_F_Anexo__c = Decimal.valueOf(cls.fanexo).divide(1,4);
        for(Cotizacion_de_periodo__c cot :listCot){
            if (cls.tvanexoperiodo.containsKey(cot.Id)){
                cot.GNF_TVAnexo__c=Decimal.valueOf(cls.tvanexoperiodo.get(cot.Id)).divide(1,4);
            }
            //cot.GNF_TVAnexo__c = cls.tvanexo;
        }
        
        if(opp.stageName != NULL && opp.stageName.equalsIgnoreCase('Cotizada definitiva'))
            opp.Margen_Negociaci_n_MEd_Pond_Operaci_n__c =  formatoDecimal( cls.db_margenNegociacionMediaPondOperacion, 4);

        for(Cotizacion_de_periodo__c cot :listCot)
            system.debug('cot.GNF_TVAnexo__c'+cot.GNF_TVAnexo__c);
                
        opp.GNF_calculosFinalizados__c  = true;     
        
        results = Database.Update(listCot, false);
        results.add(Database.Update(opp, false));
        
        //updateFuture(opp.Id);
        
        mapaResults.put('Second', results);

        return mapaResults; 
    }

	/*
    */
	@RemoteAction
    public static Map<String, List<String>> updateData(String paramObject_str, wrp_infoCotizador_cls paramWrapper_wrp, String paramDateTime_str){
		Map<String, List<String>> 	mapResults = new Map<String, List<String>>();

		if(paramObject_str == 'Initial'){

			Opportunity objOpportunity = new Opportunity();

			objOpportunity.Id                        = paramWrapper_wrp.str_idClick;
			objOpportunity.GNF_Trader__c             = paramWrapper_wrp.str_trader;
			objOpportunity.StageName                 = paramWrapper_wrp.str_stageName;
			objOpportunity.Tipo_de_servicio__c       = paramWrapper_wrp.str_tipoServicio;
			objOpportunity.GNF_calculosFinalizados__c  = paramWrapper_wrp.bln_calculosFinalizados;

			if(paramDateTime_str != null)
				objOpportunity.GNF_FechaLimiteValidez__c = Datetime.valueOf(paramDateTime_str);

			paramObject_str += ' objOpportunity';

			for(Database.Error objErrorDML: Database.update(objOpportunity, false).getErrors()){
				if(mapResults.containsKey(paramObject_str))
					mapResults.get(paramObject_str).add(objErrorDML.getMessage());
				else
					mapResults.put(paramObject_str, new List<String>{ objErrorDML.getMessage() });
			}

			paramObject_str = 'Initial';

			List<Cotizacion_de_periodo__c> 	listCotizacionesPerido 	= new List<Cotizacion_de_periodo__c>();
			Set<Id> 						SetCot 		= new Set<Id>();

			for (wrp_infoConsumos_cls consumo :  paramWrapper_wrp.lst_infoConsumos){

				Cotizacion_de_periodo__c objCotizacion = new Cotizacion_de_periodo__c();

				objCotizacion.Id 								= consumo.str_idCotizacion;
				objCotizacion.CV_Cliente__c 					= String.isNotBlank(consumo.str_cvCliente) ? Decimal.valueOf(consumo.str_cvCliente) : 0;
				objCotizacion.CV_FRC_c_kWh__c 					= String.isNotBlank(consumo.str_cvFrcInicialKWh) ? Decimal.valueOf(consumo.str_cvFrcInicialKWh) : 0;
				objCotizacion.CV_FRC_Inicial__c 				= String.isNotBlank(consumo.str_cvFrcInicialMMBTu) ? Decimal.valueOf(consumo.str_cvFrcInicialMMBTu) : 0;
				objCotizacion.Brent_de_referencia__c 			= String.isNotBlank(consumo.str_brentReferenciaBbl) ? Decimal.valueOf(consumo.str_brentReferenciaBbl) : 0;
				objCotizacion.Paridad_de_referencia_USD_EUR__c 	= String.isNotBlank(consumo.str_paridadReferenciaUsdEur) ? Decimal.valueOf(consumo.str_paridadReferenciaUsdEur) : 0;
				objCotizacion.GNF_FormulaDestinoInicial__c 		= String.isNotBlank(consumo.str_cvFormulaDestinoinicialKwh) ? Decimal.valueOf(consumo.str_cvFormulaDestinoinicialKwh) : 0;
				objCotizacion.CV_FRC_Final_c_KWh__c 			= String.isNotBlank(consumo.str_cvFrcFinalKWh) ? Decimal.valueOf(consumo.str_cvFrcFinalKWh) : 0;
				objCotizacion.CV_FRC_Final__c 					= String.isNotBlank(consumo.str_cvFrcFinalMMBtu) ? Decimal.valueOf(consumo.str_cvFrcFinalMMBtu) : 0;
				objCotizacion.GNF_FormulaDestinoFinal__c 		= String.isNotBlank(consumo.str_formulaDestinoFinalKwh) ? Decimal.valueOf(consumo.str_formulaDestinoFinalKwh) : 0;
				objCotizacion.GNF_TradeGroupCompra__c 			= consumo.str_tradeGroupCompraSwap;
				objCotizacion.GNF_ContratoEspejoCompra__c 		= consumo.str_contratoEspejoCompraSwap;
				objCotizacion.GNF_TradeGroupVenta__c 			= consumo.str_tradeGroupVentaSwap;
				objCotizacion.GNF_ContratoEspejoVenta__c 		= consumo.str_contratoEspejoVentaSwap;

				if(!SetCot.contains(objCotizacion.Id)){
					SetCot.add(objCotizacion.Id);
					listCotizacionesPerido.add(objCotizacion);
				}
			}
			paramObject_str += ' listCotizacionesPerido';

			for(Database.SaveResult objSaveResult : Database.update(listCotizacionesPerido, false))
				if(!objSaveResult.isSuccess()){
					for (Database.Error objDBerror: objSaveResult.getErrors()){
						if(mapResults.containsKey(paramObject_str))
							mapResults.get(paramObject_str).add(objDBerror.getMessage());
						else
							mapResults.put(paramObject_str, new List<String>{ objDBerror.getMessage() });
					}
				}

		}
        else if(paramObject_str == 'CalculoFormulasSite'){
			Set<Id> 			SetCot 	= new Set<Id>();
			Opportunity 		opp 	= new Opportunity();
			GNF_cotizador_cls 	cls 	= new GNF_cotizador_cls();

			cls.CalculoFormulasSite(paramWrapper_wrp.str_idClick);

			opp.Id                          = paramWrapper_wrp.str_idClick;
            opp.GNF_Trader__c               = paramWrapper_wrp.str_trader;
            opp.StageName                   = paramWrapper_wrp.str_stageName;
            opp.Tipo_de_servicio__c         = paramWrapper_wrp.str_tipoServicio;
            opp.GNF_calculosFinalizados__c  = paramWrapper_wrp.bln_calculosFinalizados;

			opp.GNF_Precio_CV_FRC_Final_medio_pond_c_KWh__c 	= formatoDecimal(cls.preciocvfrcfinalmedioponderado, 4);
			opp.GNF_Precio_CV_CLIENTE_medio_pond_c_KWh__c 		= formatoDecimal(cls.preciocvclientemedioponderado, 4);
			opp.GNF_Precio_CV_F2_medio_pond_c_KWh__c 			= formatoDecimal(cls.preciocvf2medioponderado, 4);
			opp.GNF_Precio_CV_FRC_inicial_medio_pond__c 		= formatoDecimal(cls.preciocvfrcinicialmedioponderado, 4);
			opp.GNF_Media_pond_de_Brent_bbl__c 					= formatoDecimal(cls.MediaPonderadaBrent, 4);
			opp.Media_pond_de_Paridad_eur__c 					= formatoDecimal(cls.MediaPonderadaParidad, 4);
			opp.GNF_Precio_CVF2_cerrado_medio_pond_c_kWh__c 	= formatoDecimal(cls.preciocvf2cerradomedioponderado, 4);
			opp.GNF_MediaPonderadaCVClientePrima__c 			= formatoDecimal(cls.MediaPonderadaCVPrimaClienteClick, 4);
			opp.GNF_precio_CV_FRC_Final_medio_pond_MMBtu__c 	= formatoDecimal(cls.preciocvfrcfinalmedioponderadommbtu, 4);
			opp.GNF_PrecioCV_FRC_inicialMedioPond_MMBtu__c  	= formatoDecimal(cls.preciocvfrcinicialmedioponderadommbtu, 4);

			opp.GNF_F_Anexo__c 									= Decimal.valueOf(cls.fanexo).divide(1,4);
                        
			if(opp.Tipo_de_servicio__c == 'SWAP Fórmula' || opp.Tipo_de_servicio__c == 'SWAP Hub')
				opp.GNF_Spread__c = Decimal.valueOf(cls.spread).divide(1,4);

			if(opp.Tipo_de_servicio__c == 'SWAP Fórmula' || opp.Tipo_de_servicio__c == 'SWAP Hub')
				opp.GNF_Spread_Cliente__c = Decimal.valueOf(cls.spreadofertado).divide(1,4);

			if(opp.stageName != NULL && opp.stageName.equalsIgnoreCase('Aceptada'))
				opp.Margen_Negociaci_n_MEd_Pond_Operaci_n__c = formatoDecimal( cls.db_margenNegociacionMediaPondOperacion, 4);

			List<Cotizacion_de_periodo__c> 	listCotizacionPeriodo	= new List<Cotizacion_de_periodo__c>();

			cls.CalculoFormulasSite(paramWrapper_wrp.str_idClick);

			for(wrp_infoConsumos_cls consumo :paramWrapper_wrp.lst_infoConsumos){
				Cotizacion_de_periodo__c cot = New Cotizacion_de_periodo__c ();
				cot.Id = consumo.str_idCotizacion;

				if (cls.tvanexoperiodo.containsKey(consumo.str_idCotizacion))
					cot.GNF_TVAnexo__c=Decimal.valueOf(cls.tvanexoperiodo.get(consumo.str_idCotizacion)).divide(1,4);

				if(!SetCot.contains(cot.Id)) {
					SetCot.add(cot.Id);
					listCotizacionPeriodo.add(cot);
				}
			}

			paramObject_str += ' listCotizacionPeriodo';

			for(Database.SaveResult objSaveResult : Database.update(listCotizacionPeriodo, false)) {
				if(!objSaveResult.isSuccess()) {
					for (Database.Error objDBerror: objSaveResult.getErrors()) {
						if(mapResults.containsKey(paramObject_str))
							mapResults.get(paramObject_str).add(objDBerror.getMessage());
						else
							mapResults.put(paramObject_str, new List<String>{ objDBerror.getMessage() });
					}
				}
            }

			paramObject_str = 'CalculoFormulasSite objOpportunity';

			for(Database.Error objErrorDML: Database.update(opp, false).getErrors()){
				if(mapResults.containsKey(paramObject_str))
					mapResults.get(paramObject_str).add(objErrorDML.getMessage());
				else
					mapResults.put(paramObject_str, new List<String>{ objErrorDML.getMessage() });
			}

		} else if(paramObject_str == 'StageCotizada'){

			Opportunity 	objOpportunity = new Opportunity();

			objOpportunity.Id 							= paramWrapper_wrp.str_idClick;
			objOpportunity.StageName 					= paramWrapper_wrp.str_stageName;
			objOpportunity.GNF_calculosFinalizados__c  	= paramWrapper_wrp.bln_calculosFinalizados;

			List<Consumo_Click__c> 		listConsumoClick	= [SELECT Id FROM Consumo_Click__c WHERE Oportunidad_Punto_de_Suministro__r.Oportunidad__c =: paramWrapper_wrp.str_idClick];

			paramObject_str += ' listConsumoClick';

			for(Database.SaveResult objSaveResult : Database.update(listConsumoClick, false))
				if(!objSaveResult.isSuccess()){
					for (Database.Error objDBerror: objSaveResult.getErrors()){
						if(mapResults.containsKey(paramObject_str))
							mapResults.get(paramObject_str).add(objDBerror.getMessage());
						else
							mapResults.put(paramObject_str, new List<String>{ objDBerror.getMessage() });
					}
				}

			paramObject_str = 'StageCotizada objOpportunity';

			for(Database.Error objErrorDML: Database.update(objOpportunity, false).getErrors()){
				if(mapResults.containsKey(paramObject_str))
					mapResults.get(paramObject_str).add(objErrorDML.getMessage());
				else
					mapResults.put(paramObject_str, new List<String>{ objErrorDML.getMessage() });
			}

		}

		return mapResults;
	}

    /*
    */
    public static Decimal formatoDecimal(Double dbl_inValor, integer int_escala)
    {
        
        system.debug('<< GNF_cotizador_ctr.formatoDecimal >> dbl_inValor: ' + dbl_inValor);
        
        if(dbl_inValor==null || int_escala==null)
            return null;    
        
        return Decimal.valueOf(dbl_inValor).divide(1, int_escala);
    }//ends formatoDecimal

	@remoteAction
    public static Object attachPDF(String str_idOpp, Boolean blnPDFAsync){
        
        try{
			//Boolean blnPDFAsync = Boolean.valueOf(ApexPages.currentPage().getParameters().get('flagPDFAsync'));
        	/* 
        		TODO: REQ_SyncPDFGenerator_02022018:
        		Crear logica que valide la cantidad de PS del click para:
        		
        		- Sí PS < Integer.valueOf(GNF_ValoresPredeterminados__c.getInstance().GNF_LimitePSSincronos__c) 
        		hacer llamado a GNF_PDFRequest_ws.sendPDFRequest(str_idOpp, true, true);

        		
        		- Si PS >= Integer.valueOf(GNF_ValoresPredeterminados__c.getInstance().GNF_LimitePSSincronos__c)
        		hacer llamdo a GNF_PDFRequest_ws.sendPDFRequest(str_idOpp, false, false);
        	*/
            //GNF_PDFGenerator_ctr.sendPDFRequest(str_idOpp);

			if(blnPDFAsync)
				GNF_PDFRequest_ws.sendPDFRequest(str_idOpp, true, true);
			else
				GNF_PDFRequest_ws.sendPDFRequest(str_idOpp, false, false);

			return null;
        }catch(Exception e){
			String errorMessage = '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n';
			System.debug(errorMessage);
			return errorMessage;
			//sendError(errorMessage);
        }

    }

    /*
    */
    @future
    public static void updateFuture(String str_idOpp)
    {
        
        Opportunity obj_opp;
        
        system.debug('<< GNF_cotizador_ctr.wrp_infoCotizador_cls >> str_idOpp: ' + str_idOpp);
        
        if(obj_opp==null)
            return; 
        
        obj_opp = [Select Id From Opportunity Where Id=:str_idOpp];     
    
        obj_opp.GNF_calculosFinalizados__c = true;
        
        update obj_opp;
    }//ends updateFuture


    /*
    */
    public class wrp_infoConsumos_cls
    {
        public String           uniqueKey                           {get; Set;} 
        public String           str_idConsumoClick                  {get; Set;}
        public String           str_periodo                         {get; Set;}
        public String           str_idCotizacion                    {get; Set;}
        public String           str_mes                             {get; Set;}
        public String           str_consumosKwh                     {get; Set;}
        public String           str_consumosMMBTu                   {get; Set;}
        public String           str_cvCliente                       {get; Set;}
        public String           str_cvFrcInicialKWh                 {get; Set;}
        public String           str_cvFrcInicialMMBTu               {get; Set;}
        public String           str_brentReferenciaBbl              {get; Set;}
        public String           str_paridadReferenciaUsdEur         {get; Set;}
        public String           str_cvFormulaDestinoinicialKwh      {get; Set;}
        public String           str_cvFrcFinalKWh                   {get; Set;}
        public String           str_cvFrcFinalMMBtu                 {get; Set;}
        public String           str_formulaDestinoFinalKwh          {get; Set;}
        public String           str_tradeGroupCompraSwap            {get; Set;}
        public String           str_contratoEspejoCompraSwap        {get; Set;}
        public String           str_tradeGroupVentaSwap             {get; Set;}
        public String           str_contratoEspejoVentaSwap         {get; Set;}

        public wrp_infoConsumos_cls(Consumo_Click__c obj_inCclick, Map<String, Map<String, String>> map_consumosKwhX, Map<String, Map<String, String>> map_consumosMMBtu)
        {
            
            system.debug('<< GNF_cotizador_ctr.wrp_infoConsumos_cls >> obj_inCclick: ' + obj_inCclick);   
            
            if(obj_inCclick==null)
                return;
            
            this.uniqueKey                      = obj_inCclick.Cotizacion_de_periodo__c != NULL && obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL && obj_inCclick.Mes_de_aplicacion__c != NULL ? obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c + obj_inCclick.Mes_de_aplicacion__c : null;    
            this.str_idConsumoClick             = obj_inCclick.Id != NULL ? obj_inCclick.Id : null;           
            this.str_periodo                    = obj_inCclick.Cotizacion_de_periodo__c != NULL && obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL ? obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c : null;    
            this.str_idCotizacion               = obj_inCclick.Cotizacion_de_periodo__c != NULL ? obj_inCclick.Cotizacion_de_periodo__c : null; 
            this.str_mes                        = obj_inCclick.Mes_de_aplicacion__c != NULL ? obj_inCclick.Mes_de_aplicacion__c : null;
             
            this.str_consumoskWh                =   obj_inCclick.Cotizacion_de_periodo__c != NULL && 
                                                    obj_inCclick.Mes_de_aplicacion__c != NULL &&
                                                    obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL &&
                                                    map_consumosKwhX.containsKey(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c) && 
                                                    map_consumosKwhX.get(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c).containsKey(obj_inCclick.Mes_de_aplicacion__c) 
                                                    ? map_consumosKwhX.get(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c).get(obj_inCclick.Mes_de_aplicacion__c) : null;
                                                    
            this.str_consumosMMBTu              =   obj_inCclick.Cotizacion_de_periodo__c != NULL && 
                                                    obj_inCclick.Mes_de_aplicacion__c != NULL &&
                                                    obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c != NULL &&
                                                    map_consumosMMBtu.containsKey(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c) && 
                                                    map_consumosMMBtu.get(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c).containsKey(obj_inCclick.Mes_de_aplicacion__c)                                                  
                                                    ? map_consumosMMBtu.get(obj_inCclick.Cotizacion_de_periodo__r.Periodo_Formula__c).get(obj_inCclick.Mes_de_aplicacion__c) : null;
            
            if(obj_inCclick.Cotizacion_de_periodo__c  != NULL) {
                this.str_cvCliente                  = obj_inCclick.Cotizacion_de_periodo__r.CV_Cliente__c                                != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.CV_Cliente__c)                      : null;
                this.str_cvFrcInicialKWh            = obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_c_kWh__c                              != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_c_kWh__c)                    : null;
                this.str_cvFrcInicialMMBTu          = obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Inicial__c                            != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Inicial__c)                  : null;
                this.str_brentReferenciaBbl         = obj_inCclick.Cotizacion_de_periodo__r.Brent_de_referencia__c                       != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.Brent_de_referencia__c)             : null;
                this.str_paridadReferenciaUsdEur    = obj_inCclick.Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c             != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c)   : null;
                this.str_cvFormulaDestinoinicialKwh = obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaDestinoInicial__c                 != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaDestinoInicial__c)       : null;            
                this.str_cvFrcFinalKWh              = obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c                        != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c)              : null; 
                this.str_cvFrcFinalMMBtu            = obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Final__c                              != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.CV_FRC_Final__c)                    : null; 
                this.str_formulaDestinoFinalKwh     = obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaDestinoFinal__c                   != NULL ? String.valueOf(obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaDestinoFinal__c)         : null; 
                this.str_tradeGroupCompraSwap       = obj_inCclick.Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Compra_SWAP__c       != NULL ? obj_inCclick.Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Compra_SWAP__c               : null;
                this.str_contratoEspejoCompraSwap   = obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulID_Contrato_Espejo_Compra_SWAP__c  != NULL ? obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulID_Contrato_Espejo_Compra_SWAP__c          : null;
                this.str_tradeGroupVentaSwap        = obj_inCclick.Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Venta_SWAP__c        != NULL ? obj_inCclick.Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Venta_SWAP__c                : null;
                this.str_contratoEspejoVentaSwap    = obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaID_Contrato_Espejo_Venta_SWAP__c  != NULL ? obj_inCclick.Cotizacion_de_periodo__r.GNF_FormulaID_Contrato_Espejo_Venta_SWAP__c          : null;
            }
        }//ends wrp_infoConsumos_cls            

    }//ends wrp_infoConsumos_cls

    /*public static Blob GenerarCSV(List<wrp_infoConsumos_cls> listainfoconsumos){
        String csv='Periodo;';
        Blob fichero = Blob.valueof(csv);

        for (wrp_infoConsumos_cls infocon : listainfoconsumos){
            infocon.
        }

        return fichero;

    }*/

	public void sendError(String body){
		String[] email = new String[] {'francisco.rojas@mad.vass.es'};
		String subject = 'Error Site Debug';
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.SetToAddresses(email);
		mail.SetSubject(subject);
		mail.SetPlainTextBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}         
}//ends GNF_cotizador_ctr