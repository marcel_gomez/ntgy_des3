/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba encargada e crear los datos cargados en recursos estaticos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-09		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class SMC_PDFStarterSF1_ctr_TEST {

	static testMethod void scenarioOne() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();

		List<sObject> lstPlantillas = Test.loadData(GNF_Plantilla__c.sobjectType, 'GNF_PlantillaTST');
		List<sObject> lstSecciones = Test.loadData(GNF_Seccion__c.sobjectType, 'GNF_SeccionTST');
		List<sObject> lstComponentes = Test.loadData(GNF_Componente__c.sobjectType, 'GNF_ComponentesTST');
		List<sObject> lstCondiciones = Test.loadData(GNF_Condiciones__c.sobjectType, 'GNF_CondicionesTST');

		Test.startTest();

		ApexPages.StandardController sCtrOpportunity =
				new ApexPages.StandardController(SMC_TestData_cls.objOpportunity);
		SMC_PDFStarterSF1_ctr ctrPDFStarterSF1 = new SMC_PDFStarterSF1_ctr(sCtrOpportunity);
		SMC_PDFStarterSF1_ctr.startPDFGenerator('PDF_TestClass', SMC_TestData_cls.objOpportunity.Id);

		Test.stopTest();
	}

	static testMethod void scenarioTwo() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();
		List<sObject> lstPlantillas											= Test.loadData( GNF_Plantilla__c.sobjectType, 'GNF_PlantillaTST' );
		List<sObject> lstSecciones											= Test.loadData( GNF_Seccion__c.sobjectType, 'GNF_SeccionTST' );
		List<sObject> lstComponentes										= Test.loadData( GNF_Componente__c.sobjectType, 'GNF_ComponentesTST' );
		List<sObject> lstCondiciones										= Test.loadData( GNF_Condiciones__c.sobjectType, 'GNF_CondicionesTST' );

		Test.startTest();
		GNF_ValoresPredeterminados__c customSet = [SELECT Id, SMC_LimitePSSincronos__c FROM GNF_ValoresPredeterminados__c LIMIT 1 ];
		customSet.SMC_LimitePSSincronos__c = 0;
		update customSet;
		Opportunity opp = [SELECT Id, SMC_CantidadPS__c FROM Opportunity WHERE Id =: SMC_TestData_cls.objOpportunity.Id];
		opp.SMC_CantidadPS__c = 1;
		update opp;
		ApexPages.StandardController sCtrOpportunity					= new ApexPages.StandardController( SMC_TestData_cls.objOpportunity );
		SMC_PDFStarterSF1_ctr ctrPDFStarterSF1							= new SMC_PDFStarterSF1_ctr( sCtrOpportunity );
		SMC_PDFStarterSF1_ctr.startPDFGenerator( 'PDF_TestClass', SMC_TestData_cls.objOpportunity.Id );

		Test.stopTest();
	}
}