public class AG_TriggerHandlerSS {

    //LOGIC
    private final AG_LogicSS logic;

    //CONSTRUCTOR
    private AG_TriggerHandlerSS() {
        this.logic = new AG_LogicSS();
    }

    //SINGLETON PATTERN
    private static AG_TriggerHandlerSS instance;
    public static AG_TriggerHandlerSS getInstance() {
        if (instance == null) instance = new AG_TriggerHandlerSS();
        return instance;
    }    
    
    //SUMINISTRO HANDLER      
    public void onBeforeInsert(final List<SSs__c> newList) {
        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<SSs__c> newList, final Map<Id, SSs__c> newMap,
                               final List<SSs__c> oldList, final Map<Id, SSs__c> oldMap) {
        //REVIEW: No need to re-calculate owner when the Gestor_External_Key__c has not change.
        this.logic.assignOwnerFromEmpleado(newList, oldMap);
    }

    public void onAfterInsert(final List<SSs__c> newList, final Map<Id, SSs__c> newMap){
       this.logic.setCustomSharing(newList,newMap,null,null);       
    }

    public void onAfterUpdate(final List<SSs__c> newList, final Map<Id, SSs__c> newMap,
                              final List<SSs__c> oldList, final Map<Id, SSs__c> oldMap){
       this.logic.setCustomSharing(newList,newMap,oldList,oldMap);
    }

    public void onAfterDelete(final List<SSs__c> oldList, final Map<Id, SSs__c> oldMap){
    }
}