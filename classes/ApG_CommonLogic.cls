public without sharing class ApG_CommonLogic {
   //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    private final static map<string, string> sObjectAndSelfObject = new map<string, string>{
		'Ofertas__c'=>'ID_Oferta__c',
		'Suministros__c'=>'ID_Supply__c',
		'SSs__c'=>'ID_SS__c',
		'SRs__c'=>'ID_SR__c',
		'Actividad_Comercial__c'=>'ID_Codigo_Actividad__c'
    };

    private final static map<string, string> sObjectAndSharingName = new map<string, string>{
		'Ofertas__c'=>'Ofertas__Share',
		'Suministros__c'=>'Suministros__Share',
		'SSs__c'=>'SSs__Share',
		'SRs__c'=>'SRs__Share',
		'Actividad_Comercial__c'=>'Actividad_Comercial__Share'
    };

    
    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<sObject>}
     */
    public static void assignOwnerFromEmpleado(final List<sObject> newList, final Map<Id, sObject> oldMap, string objectName) {
    	System.debug('Asignacion De Owner Desde Empleado  ' + newList);
        System.debug('se van a procesar estas SRs  ' + newList.size());
        System.debug('oldMap  ' + oldMap);
        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();

        if (oldMap==null) oldMap = new Map<Id, sObject>(); //just to avoid null checks

        // Iterate over ofertas and find and assign owner based on its empleado
        for(sObject i : newList) {
        	string gestorExternalKey = string.valueOf(i.get('Gestor_External_Key__c'));
        	string rId = string.valueOf(i.get('Id'));
        	string selfId = string.valueOf(i.get(sObjectAndSelfObject.get(objectName)));
        	if(!string.isBlank(gestorExternalKey)) {

            //if gestor has not change, it is not necessary to recalculate owner.
				if (hasChangedGestor(oldMap.get(rId), i)) {
	                system.debug('ha cambiado de empleado o es nuevo registro  '+ gestorExternalKey);
					//set a list with the managers of the Gestor__c in the Empleado__c hierachy
					i.put('managers__c', managersEmpleados.get(gestorExternalKey));
	                system.debug('campo managers__c' + i.get('managers__c'));
					String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(gestorExternalKey,empleadoTable,userTable);
		            system.debug('Owner que se va a assignar     '+assignedOwner);
					if(!string.isBlank(assignedOwner)) {
						i.put('OwnerId', assignedOwner);
					} /*else {
						//TODO: What todo in if not find a valid user?
						//final String errMsg = '#ERROR# No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_'+objectName +'=  ' + selfId + ' - gestor_external_key='+ gestorExternalKey;
						//throw new OfertaLogicException(errMsg);
					}*/
				}   
          	} /*else {
				//TODO: What to do if Gestor_External_key__c it is not set?
				//final String errMsg = '@@@@ No esta informada el gestor para la '+objectName+':  ' + selfId;
				//throw new OfertaLogicException('No esta informada el gestor para la oferta :' + i.ID_Oferta__c);
        	}*/
        }
    }
    
    /**
     * Shares records with the hierarchy of the owner of the account which the record is related with.
     */
    public static void setCustomSharing(final List<sObject> newList, final Map<Id, sObject> oldMap, string objectName) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ SetCustomSharing: start');
        System.debug('Se van a procesar estos sObjects   ' + newList);
        //List of oferta sharing records to be inserted
        List<sObject> objectSharingList = new List<sObject>();

        //List of account sharing records to be inserted
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, sObject>(); //just to avoid null checks

        //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();
        
        Set<Id> sharesToRemove = new Set<Id>();
        //Iterate over collection
        for(sObject i : newList) {

            id rId = string.valueOf(i.get('Id'));
            id clientId = string.valueOf(i.get('Cliente__c'));
		    id rOwner = string.valueOf(i.get('OwnerId'));
		    id ownerRelatedAccount = string.valueOf(i.get('Gestor_Cliente__c'));
            sObject oldRecord = oldMap.get(rId);

            //Get only new records or records whose cliente has changed or owner has changed
            if (hasChangedCliente(oldRecord, i)) {
	            system.debug('la SR ha cambiado de cliente   ' + clientId);
	           	if (oldRecord!=null){  //si es insert no hay previos que borrar
		            //Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(rId);
		            sharesToRemove.add(rId);
		        }
	            //get the role of the user assigned to the record account
				system.debug('owner de la SR    ' + rOwner);
	            system.debug('owner de la cuenta asociada    ' + ownerRelatedAccount);
	            if (rOwner!=ownerRelatedAccount) {
		            List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount,allUSers, allRoles, allGroups);
		            for (Id grpOrUsrId : gruposRoleSuperiores) {
			            objectSharingList.add(createRecordSharing(rId, grpOrUsrId, objectName));
		            }
	            }
	            system.debug('lista para insertar  ' + objectSharingList);

	        }

            //Si es un nuevo Suministro, calculamos los permisos para el Account en el trigger,
            //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.
            if (oldRecord!=null && hasChangedOwner(oldRecord, i)) {//El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
            	accountToRecalculate.add(clientId);
            	system.debug('nueva cuenta para recalcular   ' + clientId);

            }else if(oldRecord==null && !String.isBlank(clientId)) {//Se trata de un alta de suministro --> calculamos permisos a dar al Account
                //Si owner del cliente es el mismo que el del objeto, no es necesario dar más permisos
                system.debug('se propone compartir con: ' + ownerRelatedAccount + ' y el owner es:  '+ rOwner);
                if(ownerRelatedAccount!=rOwner) {
                    System.debug('@@@Account Propietrario sr:' + rOwner);
                  	System.debug('@@@Account Propietrario cuenta:' + ownerRelatedAccount);
                    List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(rOwner,allUSers,allRoles,allGroups);
                    for (Id grpOrUsrId : grpOrUsrIdToShare) {
                    	AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(clientId, grpOrUsrId);
                      	accountSharingList.add(accountSharing);
                    }
                }
                System.debug('###Account Account Share:' + accountSharingList);
            }
        }

        if(!sharesToRemove.isEmpty()){
        	removeShares(sharesToRemove, objectName);
        }

        if (!accountToRecalculate.isEmpty()) {
        	AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!objectSharingList.isEmpty()){
        	insert objectSharingList;
        }


        if(!accountSharingList.isEmpty()){
        	insert accountSharingList;
        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS
     /**
     * Elimina los shares de un objeto determinado. 
     * El objeto dejara de ser accesible para su Account.Owner.
     */
    private static void removeShares(set<id> parentsToUnshare, string objectName){
    	string rowCause = 'Apex_Sharing__c';
    	string theQuery = 'SELECT Id FROM ' + sObjectAndSharingName.get(objectName) + ' WHERE RowCause=: rowCause AND ParentId IN: parentsToUnshare';
    	list<sObject> sharesToDelete = database.query(theQuery);
    	if(!sharesToDelete.isEmpty()){
    		delete sharesToDelete;
    	}
    }
   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldRecord is null.
    */
    @testVisible
    private static boolean hasChangedGestor(sObject oldRecord, sObject i) {
		boolean retValue = false;
		if(oldRecord==null || string.valueOf(oldRecord.get('Gestor__c')) != string.valueOf(i.get('Gestor__c'))){
			retValue = true;
		}
		return retValue;
    }      
     

    /*
     * @return true if the field value "Cliente__c" has changed or oldRecord is null.
    */
    @testVisible
    private static boolean hasChangedCliente(sObject oldRecord, sObject i) {
		boolean retValue = false;
		if(oldRecord==null || string.valueOf(oldRecord.get('cliente__c')) != string.valueOf(i.get('cliente__c'))){
			retValue = true;
		}
		return retValue;
    }      

    /*
    * @return true if the owner has changed or oldRecord is null.
    */
    @testVisible
    private static boolean hasChangedOwner(sObject oldRecord, sObject i) {
		boolean retValue = false;
		if(oldRecord==null || string.valueOf(oldRecord.get('OwnerId')) != string.valueOf(i.get('OwnerId'))){
			retValue = true;
		}
		return retValue;
    } 


    /**
     * Crea  sObject__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible
    private static sObject createRecordSharing(string parentId, Id userOrGroup, string objectName) {
        sObject retObject = Schema.getGlobalDescribe().get(sObjectAndSharingName.get(objectName)).newSObject();
        retObject.put('ParentId', parentId);
        retObject.put('UserOrGroupId', userOrGroup);
        retObject.put('AccessLevel', SHARE_MODE_READ);
        retObject.put('RowCause', 'Apex_Sharing__c');
        return retObject;
    }


    /*
    * Remove all the sharing added from Apex.
    */

    //-- CUSTOM EXCEPTION
    public class OfertaLogicException extends Exception {}
}