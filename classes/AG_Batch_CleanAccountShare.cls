global class AG_Batch_CleanAccountShare implements Database.Batchable<sObject> {
	
	private final String query;

	private final Boolean cleanAll;		//clean all account_share and not just the Accounts liked to one object that has been modified
	private final Boolean recalculateAfterclean;

	/*+
	* Constructor del proces de borrado de los
	* @param cleanAll
	+ @param recalculateAfterClean
	*/
	@testVisible
	private AG_Batch_CleanAccountShare(Boolean cleanAll, Boolean recalculateAfterclean) {
		
		this.cleanAll = cleanAll;
		this.recalculateAfterclean = recalculateAfterclean;

		if (!this.cleanAll)
			query ='SELECT Id FROM AccountShare WHERE RowCause=\'Manual\' AND AccountId IN (SELECT Id FROM Account WHERE Recalcular_Permisos__c = true)';
		else 
			query ='SELECT Id FROM AccountShare WHERE RowCause=\'Manual\'';
	}


	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}


   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> AG_Batch_CleanAccountShare.execute');

   		delete scope;

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Núm. Registros borrados:' + scope.size());
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- AG_Batch_CleanAccountShare.execute');
	}

	global void finish(Database.BatchableContext BC) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> AG_Batch_CleanAccountShare.finish');		

		if (this.recalculateAfterclean) {
			Id batchId = AG_Batch_RecalculateAccountShare.executeFullProcess(this.cleanAll);
			System.debug('**** BatchProcess Id lanzado:' + batchId);
		}

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- AG_Batch_CleanAccountShare.finish');		
	}


	/*
	* Lanza proceso batch de borrado de Share sobre todos los objetos.
	* @cleanAll Borrar todos los permisos (share) otorgados, y no únicamnente la de los completamente
	*/
	public static Id executeFullProcess(Boolean cleanAll) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Start batch process to recalculare Account Shares');

		//Boolean cleanAll, 	Boolean recalculateAfterclean
		AG_Batch_CleanAccountShare batchProcess = new AG_Batch_CleanAccountShare(cleanAll, true);
		Id batchId = Database.executeBatch(batchProcess);

		System.debug ('*** BatchProcess.finish: ends. Id=' + batchId);

		return batchId;
	}

	/*
	* Lanza proceso batch de borrado de Share para un objeto (TableIndex) en particular
	*/
	public static Id executeProcess(Boolean cleanAll, Boolean recalculateAccount) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> executeProcessOneTableStart: cleanAll=' + cleanAll);

		AG_Batch_CleanAccountShare batchProcess = new AG_Batch_CleanAccountShare(cleanAll, recalculateAccount);
		Id batchId = Database.executeBatch(batchProcess);
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- executeProcessOneTableStart: Batch.Id:' + batchId);
		
		return batchId;
	}

}