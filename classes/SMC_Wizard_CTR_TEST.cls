/**
* VASS
* @author 			Francisco Rojas friancisco.rojas@mad.vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de TEST para la clase controller SMC_Wizard_CTR.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-12		Francisco Rojas (FR)	Definicion inicial de la clase.
*********************************************************************************************************/

@isTest
private class SMC_Wizard_CTR_TEST {	

	
	public static void createData(String strLimit){
		SMC_TestData_cls.strLoadUntil = strLimit;
		SMC_TestData_cls.createData();
		List<sObject> lsopportunity	= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
	}

	//@isTest
	//public static void test_InitializationGlobalVariables(){
	//	SMC_Wizard_CTR objSMC_WizardCTR 	= 	new SMC_Wizard_CTR();
	//}

	@isTest
	public static void test_InitializationGlobalVariables(){
		createData('Contact');
		List<sObject> lsopportunity	= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			SMC_Wizard_CTR objSMC_WizardCTR 	= 	new SMC_Wizard_CTR();
			objSMC_WizardCTR.opp 				= 	queryOpp.get(0);
        	objSMC_WizardCTR.strIdOpp 			= 	objSMC_WizardCTR.opp.Id;
        	objSMC_WizardCTR.strIdAcc			= 	objSMC_WizardCTR.opp.AccountId;
        	objSMC_WizardCTR.strPaisAcc			= 	objSMC_WizardCTR.opp.Pais__c;
        	objSMC_WizardCTR.strRecordTypeDev	=	objSMC_WizardCTR.opp.RecordType.DeveloperName;
        	objSMC_WizardCTR.strRecordType		=	objSMC_WizardCTR.opp.RecordTypeId;
        	objSMC_WizardCTR.strIsClone			=	String.valueOf(objSMC_WizardCTR.opp.GNF_IsClone__c);
        	objSMC_WizardCTR.condCobertura 		= 	new List<Condiciones_de_Cobertura__c>();			
		}
		Test.stopTest();

	}

	@isTest
	public static void test_recordPage_mthd(){
		createData('Contact');
		List<sObject> lsopportunity	= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			SMC_Wizard_CTR objSMC_WizardCTR 	= 	new SMC_Wizard_CTR();
			objSMC_WizardCTR.opp 				= 	queryOpp.get(0);
        	objSMC_WizardCTR.strIdOpp 			= 	objSMC_WizardCTR.opp.Id;
        	objSMC_WizardCTR.recordPage();
		}
		Test.stopTest();

	}

	@isTest
	public static void test_getWrapper_mthd(){
		createData('Contrato__c');
		List<sObject> lsopportunity	= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			Opportunity opp 				= 	queryOpp.get(0);

			SMC_Wizard_CTR.getWrapper(opp.AccountId, opp.Id);
		}
		Test.stopTest();

	}
	
	@isTest
	public static void test_saveDataAlt_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		List<Condiciones_de_Cobertura__c> queryCB = [SELECT Id, GNF_IsStandard__c FROM Condiciones_de_Cobertura__c WHERE GNF_IsStandard__c = false LIMIT 10];
		if(!queryOpp.isEmpty() && !queryCB.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			Condiciones_de_Cobertura__c cb 	= 	queryCB.get(0);

			SMC_Wizard_CTR.getPSInfo(opp.AccountId, opp.Id, cb.Id, cb.GNF_IsStandard__c, new List<String>(), false);
		}
		Test.stopTest();

	}

	@isTest
	public static void test_saveDataMod_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		List<Condiciones_de_Cobertura__c> queryCB = [SELECT Id, GNF_IsStandard__c FROM Condiciones_de_Cobertura__c WHERE GNF_IsStandard__c = false LIMIT 10];
		if(!queryOpp.isEmpty() && !queryCB.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			Condiciones_de_Cobertura__c cb 	= 	queryCB.get(0);

			SMC_Wizard_CTR.getPSInfo(opp.AccountId, opp.Id, cb.Id, cb.GNF_IsStandard__c, new List<String>(), true);
		}
		Test.stopTest();

	}

	@isTest
	public static void test_saveData_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		List<Condiciones_de_Cobertura__c> queryCB = [SELECT Id, GNF_IsStandard__c FROM Condiciones_de_Cobertura__c WHERE GNF_IsStandard__c = false LIMIT 10];
		List<String> setPS = new List<String>();
		for(Contrato__c objContrato : [SELECT Id FROM Contrato__c LIMIT 1]){
			setPS.add(objContrato.Id);
		}
		if(!queryOpp.isEmpty() && !queryCB.isEmpty() && !setPS.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			Condiciones_de_Cobertura__c cb 	= 	queryCB.get(0);

			SMC_Wizard_CTR.saveData(opp, cb, setPS, '2018-01-01', '2018-12-31', cb, false);
		}
		Test.stopTest();

	}

	@isTest
	public static void test_updateOpp_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(!queryOpp.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);

			SMC_Wizard_CTR.updateOpp(opp, '2018-12-31');
		}
		Test.stopTest();

	}

	@isTest
	public static void test_getPicklistValues_mthd(){
		Test.startTest();
			SMC_Wizard_CTR.getPicklistValues( 'Condiciones_de_Cobertura__c', 'Pais__c');
		Test.stopTest();

	}

	@isTest
	public static void test_getWarnings_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c, GNF_RevisarUO__c FROM Opportunity  LIMIT 10];
		List<Condiciones_de_Cobertura__c> queryCB = [SELECT Id, GNF_IsStandard__c FROM Condiciones_de_Cobertura__c WHERE GNF_IsStandard__c = false LIMIT 10];
		if(!queryOpp.isEmpty() && !queryCB.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			Condiciones_de_Cobertura__c cb 	= 	queryCB.get(0);

			SMC_Wizard_CTR.getWarnings(opp, cb, false);
		}
		Test.stopTest();

	}

	@isTest
	public static void test_loadPSInfo_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(!queryOpp.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			SMC_Wizard_CTR.loadPSInfo(opp.Id);
		}
		Test.stopTest();

	}

	@isTest
	public static void test_loadClickUnclickInfo_mthd(){
		createData('Opportunity');
		Test.startTest();
		List<String> set_idSuministro = new List<string>();
		List<Opportunity> queryOpp = [SELECT Id, AccountId, Pais__c, RecordTypeId, RecordType.DeveloperName, GNF_IsClone__c FROM Opportunity  LIMIT 10];
		if(!queryOpp.isEmpty()){
			Opportunity opp 				= 	queryOpp.get(0);
			for( Oportunidad_Punto_de_Suministros__c obj_oppXps : [ SELECT Id FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c =: opp.Id ] )
				set_idSuministro.add( obj_oppXps.Id );
			if(!set_idSuministro.isEmpty()){
				SMC_Wizard_CTR.loadClickUnclickInfo(opp.Id, set_idSuministro);
			}
		}
		Test.stopTest();

	}

	@isTest
	public static void test_wrp_cclickCunclick_cls_mthd(){
		
		Test.startTest();
			SMC_Wizard_CTR.wrp_cclickCunclick_cls wrapperFirmanteXPS = new SMC_Wizard_CTR.wrp_cclickCunclick_cls();
			wrapperFirmanteXPS.obj_inOpp = new Opportunity();
			wrapperFirmanteXPS.obj_inCobertura = new Condiciones_de_Cobertura__c();
			wrapperFirmanteXPS.map_inContratosxciff = new map <string, list<Contrato__c>>();
		Test.stopTest();

	}
	
	@isTest
	public static void test_loadClickUnclickInfo_cls_mthd(){
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();
		
		Test.startTest();
			SMC_Wizard_CTR.wrp_cclickCunclick_cls wrapperFirmanteXPS = new SMC_Wizard_CTR.wrp_cclickCunclick_cls();
			wrapperFirmanteXPS.obj_inOpp = new Opportunity();
			wrapperFirmanteXPS.obj_inCobertura = new Condiciones_de_Cobertura__c();
			wrapperFirmanteXPS.map_inContratosxciff = new map <string, list<Contrato__c>>();
			
			List<Oportunidad_Punto_de_Suministros__c> lstOppSupplies				= new List<Oportunidad_Punto_de_Suministros__c>( [
																						SELECT Id
																						FROM Oportunidad_Punto_de_Suministros__c
																					] );
																					
			List<String> lstOppSuppliesId											= new List<String>();
			for( Oportunidad_Punto_de_Suministros__c objOppSupply : lstOppSupplies ){
				lstOppSuppliesId.add( objOppSupply.Id );
			}
			
			SMC_Wizard_CTR.loadClickUnclickInfo( String.valueOf( SMC_TestData_cls.objOpportunity.Id ), lstOppSuppliesId );
			
		Test.stopTest();

	}

}