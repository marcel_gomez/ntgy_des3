public class GNF_StatusOpportunity_CTR {

    private Opportunity opportunity;
    public Boolean done {get; set;} 

    public List<Schema.PicklistEntry> getStageListValues(){     
	    return Opportunity.getSObjectType().getDescribe().fields.getMap().get('stagename').getDescribe().getPicklistValues();    
    }
    public GNF_StatusOpportunity_CTR(ApexPages.StandardController stdController) {
        this.done = false;
        /*this.opportunity = [SELECT Id, StageName, ProductCatalogueBatchId__c, ProductCatalogueValidationErrors__c FROM Opportunity WHERE Id =: stdController.getId()];*/
    	this.opportunity = [SELECT Id, StageName FROM Opportunity WHERE Id =: stdController.getId()];
    }

    /*public String getTranslatedStatus() {
        
        if (opportunity != null) { º
            
            System.debug('Opportunity status: ' + opportunity.StageName);
            // Custom Setting with the translations of the Opportunity status
            List<TranslationTable__c> statusTranslations = [SELECT English__c, French__c, Spanish__c FROM TranslationTable__c 
                                                            WHERE Name= :opportunity.StageName];

            for (TranslationTable__c tran: statusTranslations) {
                
                if (UserInfo.getLanguage().equals('es'))
                    return tran.Spanish__c; 
                else if (UserInfo.getLanguage().equals('fr'))
                    return tran.French__c;  
                else 
                    return tran.English__c;

            }
        
        }
        
        return null;
        
    }*/

   /* public void checkBatchStatus() {

        System.debug('Method checkBatchStatus with batchId: ' + opportunity.ProductCatalogueBatchId__c + ' and errors: ' + opportunity.ProductCatalogueValidationErrors__c);
        if (String.isBlank(opportunity.ProductCatalogueBatchId__c)) {
            done = true;
        } else {
            List<AsyncApexJob> apexJobs = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus
                                           FROM AsyncApexJob WHERE Id = :opportunity.ProductCatalogueBatchId__c];
            if ( apexJobs != null && apexJobs.size() > 0 && 'Completed'.equals(apexJobs[0].status) ) {
                done = true;	//Set done to true
                //System.debug('Done!');
            } else {
                done = false;
            }
        }
    }
    
    public void clearValidationError() {
        System.debug('Method clearValidationError');
        opportunity.ProductCatalogueBatchId__c = null;
        opportunity.ProductCatalogueValidationErrors__c  = null;
        update opportunity;
    }*/

}