/**
 * Created by luis.igualada on 26/09/2017.
 */
@isTest
private class GNF_consolidarConsumosClick_cls_TEST {

	static testmethod void consolidarConsumosClick() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
        
        user usuario = [SELECT Id FROM User WHERE Profile.Name in ('System Administrator','Administrador del sistema') AND IsActive = true LIMIT 1].get(0);

		list<string> lst_idClick = new list<string>();
		set<string> set_idOppPuntoSuministro = new set<string>();
		list<Opportunity> listaopps = new list<Opportunity>();
		list<Oportunidad_Punto_de_Suministros__c> listaopppuntosuministro = new list<Oportunidad_Punto_de_Suministros__c>();

		Opportunity objOppotunity											=  [SELECT Id
																				FROM Opportunity
																				WHERE RecordType.DeveloperName = 'Click'
																				AND Name = 'SCAMARA_TARGET_PRICE'
																				LIMIT 1
																				];

		Test.setCreatedDate(objOppotunity.Id, DateTime.newInstance(2000,10,10));

		Test.startTest();

		listaopps = [SELECT Id from Opportunity WHERE stageName !='Firmada' LIMIT 3];

		for (Opportunity opp :listaopps){
			lst_idClick.add(opp.Id);
		}

		listaopppuntosuministro = [SELECT Id from Oportunidad_Punto_de_Suministros__c];

		for (Oportunidad_Punto_de_Suministros__c opps :listaopppuntosuministro){
			set_idOppPuntoSuministro.add(opps.Id);
		}

		GNF_consolidarConsumosClick_cls consolidarConsumosClick = new GNF_consolidarConsumosClick_cls(lst_idClick,set_idOppPuntoSuministro,false);

		list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos = consolidarConsumosClick.mthd_consumoCubiertosMes();

		list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos2 = consolidarConsumosClick.mthd_ConsumoCubiertototalPS(lst_consumos);

		list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos3 = consolidarConsumosClick.mthd_ConsumoCubiertoAnterior(lst_consumos2);

		list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos4 = consolidarConsumosClick.mthd_consumoDisponiblePeriodo(lst_consumos3);

		list<GNF_consolidarConsumosClick_cls.wrp_resultado> lst_consumos5 = consolidarConsumosClick.mthd_consumoDisponiblePeriodoUnclick(lst_consumos4);

		list<GNF_consolidarConsumosClick_cls.wrp_resultado_2> lst_numeroClicksejecutados = consolidarConsumosClick.mthd_numeroClicksEjecutados();

		map<string, GNF_consolidarConsumosClick_cls.wrp_resultado> lst_ConsumosMap = consolidarConsumosClick.mthd_getConsumosMap(lst_consumos5);

		Test.stopTest();

	}

}