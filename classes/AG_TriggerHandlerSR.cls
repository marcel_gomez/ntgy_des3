public class AG_TriggerHandlerSR {

    //LOGIC
    private final AG_LogicSR logic;

    //CONSTRUCTOR
    private AG_TriggerHandlerSR() {
        this.logic = new AG_LogicSR();
    }

    //SINGLETON PATTERN
    private static AG_TriggerHandlerSR instance;
    public static AG_TriggerHandlerSR getInstance() {
        if (instance == null) instance = new AG_TriggerHandlerSR();
        return instance;
    }    
    
    //SUMINISTRO HANDLER      
    public void onBeforeInsert(final List<SRs__c> newList) {
        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<SRs__c> newList, final Map<Id, SRs__c> newMap,
                               final List<SRs__c> oldList, final Map<Id, SRs__c> oldMap) {
        //REVIEW: No need to re-calculate owner when the Gestor_External_Key__c has not change.
        this.logic.assignOwnerFromEmpleado(newList, oldMap);
    }

    public void onAfterInsert(final List<SRs__c> newList, final Map<Id, SRs__c> newMap){
       this.logic.setCustomSharing(newList,newMap,null,null);       
    }

    public void onAfterUpdate(final List<SRs__c> newList, final Map<Id, SRs__c> newMap,
                              final List<SRs__c> oldList, final Map<Id, SRs__c> oldMap){
       this.logic.setCustomSharing(newList,newMap,oldList,oldMap);
    }

    public void onAfterDelete(final List<SRs__c> oldList, final Map<Id, SRs__c> oldMap){
    }
}