/**
 * Created by luis.igualada on 04/09/2017.
 */
@isTest
private class GNF_PreciosVistaClasica_ctr_TEST {

	static testMethod void GNF_PreciosVistaClasica(){
		
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		//Cargamos las Oportunidades como ese usuario Administrador
		//Recuperamos todas las Oportunidades cargadas
		list<Opportunity> listaopps = [Select Id from Opportunity];
		Set <Id> idopps = new Set<Id>();

		//Se las vamos pasando a la clase que queremos probar
		for (Opportunity opp :listaopps){
			ApexPages.StandardController sctr = new ApexPages.StandardController(opp);
			GNF_PreciosVistaClasica_ctr ctr = new GNF_PreciosVistaClasica_ctr(sctr);

			//Rellenamos el set<Id> de Oportunidades
			idopps.add(opp.Id);
		}

		//Probamos el método getOpportunitySupplies
		//GNF_PreciosVistaClasica_ctr ctr = new GNF_PreciosVistaClasica_ctr(sctr);
		GNF_PreciosVistaClasica_ctr ctr = new GNF_PreciosVistaClasica_ctr();
		ctr.getOpportunitySupplies(idopps);
	}

}