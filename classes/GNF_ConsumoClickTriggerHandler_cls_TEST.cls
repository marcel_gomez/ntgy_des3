/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_ConsumoClickTriggerHandler_cls.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-22		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_ConsumoClickTriggerHandler_cls_TEST {
	
	static testMethod void scenarioOne() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Test.startTest();
			Opportunity objOpportunity										= new Opportunity();
			objOpportunity													= [SELECT Id,
																					Periodicidad_ToP__c,
																					Tipo_Producto__c
																				FROM Opportunity
																				WHERE Periodicidad_ToP__c = 'Trimestral' 
																				LIMIT 1
																			];
																			
			objOpportunity.Tipo_Producto__c									= 'Q';
			update objOpportunity;

			List<Consumo_Click__c> lstConsumosClick							= new List<Consumo_Click__c>( [
																				SELECT Id
																				FROM Consumo_Click__c
																				WHERE Oportunidad_Punto_de_Suministro__r.Oportunidad__c =: objOpportunity.Id
																				LIMIT 1
																			] );
																			
			for( Consumo_Click__c objConsumoClick : lstConsumosClick ){
				objConsumoClick.GNF_posicionConsumoClick__c					= 2;
			}
			
			update lstConsumosClick;

		Test.stopTest();
	}
}