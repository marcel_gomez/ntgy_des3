public class GNF_contadoresClick_ctr 
{

	public Opportunity												obj_opp				{get; set;}
	/*public list<string> 											lst_idOpp			{get; set;}
	public list<GNF_consolidarConsumosClick_cls.wrp_resultado_2>	lst_consolidado		{get; set;}*/
	public List<wrp_numoperaciones> 								wrappercupscontador {get; set;}
	public GNF_consolidarConsumosClick_cls 							cls_consumos;
	
	
    public GNF_contadoresClick_ctr(ApexPages.StandardController stdController)
    {
    	system.debug('<<GNF_contadoresClick_ctr.GNF_contadoresClick_ctr>> :' + stdController);
    	
		obj_opp = ( Opportunity ) stdController.getRecord();
    }//ends GNF_contadoresClick_ctr
    
    
    public void mthd_getContadoresClick()
    {
    	system.debug('<<GNF_contadoresClick_ctr.mthd_getContadoresClick>> :' + obj_opp.id );

		List<Oportunidad_Punto_de_Suministros__c> puntossum = [Select Contrato__r.CUPS__c, Contrato__r.IF_GNL__c,(Select N_Clicks_Unclicks_ejecutados__c from Consumos_click__r limit 1)
		from Oportunidad_Punto_de_Suministros__c where Oportunidad__r.Id= :obj_opp.id];



		wrappercupscontador = new List<wrp_numoperaciones>();

		for (Oportunidad_Punto_de_Suministros__c opp : puntossum) {
			if (opp.Contrato__r.IF_GNL__c != null && !opp.Consumos_click__r.isEmpty()){
				if (opp.Consumos_click__r[0].N_Clicks_Unclicks_ejecutados__c != null){
					wrappercupscontador.add(new wrp_numoperaciones(opp.Contrato__r.IF_GNL__c,(opp.Consumos_click__r[0].N_Clicks_Unclicks_ejecutados__c != null ? integer.valueOf(opp.Consumos_click__r[0].N_Clicks_Unclicks_ejecutados__c) : 0)));
				}
			}
		}


		/*cls_consumos 		= new GNF_consolidarConsumosClick_cls( new list<string> { obj_opp.id } );
		lst_consolidado 	= cls_consumos.mthd_numeroClicksEjecutados();*/
		    
    }//ends mthd_getConsumosClick

	public class wrp_numoperaciones
	{

		Public String  Cups 				{get; set;}
		Public Integer Clicksejecutados 	{get;set;}

		//Se creará un Wrapper de un consumo click por cada Oportunidad y punto de suministro
		//Para sacar el N_Clicks_Unclicks_Ejecutados_c
		public wrp_numoperaciones(String Suministro, Integer contador){
			Cups = Suministro;
			Clicksejecutados = contador;
		}
	}

   
}//end GNF_contadoresClick_ctr