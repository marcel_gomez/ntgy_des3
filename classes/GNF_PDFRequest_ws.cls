/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase web service que permite conexiones desde Site hacia el generador de documentos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-07-11		Manuel Medina (MM)		Definicion inicial de la clase.
* @version	2.0		2018-02-02		Manuel Medina (MM)		Se incluye el metodo sendPDFRequest() encargado
*															de validar si se ejecutara llamado del WS cuando es
*															procesamiento asincrono; de lo contrario conecta directamente
*															con el flow GNF_AdjuntarDocumentacion.
*********************************************************************************************************/
@RestResource( urlMapping='/pdfRequest/*' )
global class GNF_PDFRequest_ws {
	
	/**
	* @Method: 		sendPDF
	* @param: 		String strMainRecordId
	* @param: 		Boolean blnStartAsync
	* @param: 		Boolean blnIsAsync
	* @Description: Post REST WS que ejecuta flow para procesamiento de documentos via asincrona.
	* @author 		Manuel Medina - 18102017
	*/
	@HTTPPost 
	global static String sendPDF( String strMainRecordId, Boolean blnStartAsync, Boolean blnIsAsync ){
		try{
			runFlow( strMainRecordId, blnStartAsync, blnIsAsync );
			
		}catch( Exception e ){
			String strError														= '\n\n<<<<<<<<< ExceptionType >>>>>>>>> \n@@--> getCause > ' + e.getCause() + '\n@@--> getLineNumber > ' + e.getLineNumber() + '\n@@--> getMessage > '+ e.getMessage() + '\n@@--> getStackTraceString > '+ e.getStackTraceString() + '\n@@--> getTypeName > ' + e.getTypeName();
			System.debug( strError );
			
			attachPDFError( strMainRecordId, strError );
			
			return strError;
		}
		
		return 'OK';
	}
	
	/**
	* @Method: 		runFlow
	* @param: 		String strMainRecordId
	* @param: 		Boolean blnStartAsync
	* @param: 		Boolean blnIsAsync
	* @Description: Ejecuta el flow GNF_AdjuntarDocumentacion para iniciar el generador de ducumentos PDF.
	* @author 		Manuel Medina - 02022018
	*/
	global static void runFlow( String strMainRecordId, Boolean blnStartAsync, Boolean blnIsAsync ){
		Map<String, Object> mapPDFGeneratorFlowParams							= new Map<String, Object>{
																					'strMainRecordId'		=> strMainRecordId,
																					'blnStartAsync'			=> blnStartAsync,
																					'blnIsAsync'			=> blnIsAsync
																				};
		Flow.Interview.GNF_AdjuntarDocumentacion flwAttach						= new Flow.Interview.GNF_AdjuntarDocumentacion( mapPDFGeneratorFlowParams );
		flwAttach.start();
	}
	
	/**
	* @Method: 		sendPDFRequest
	* @param: 		String strMainRecordId
	* @param: 		Boolean blnStartAsync
	* @param: 		Boolean blnIsAsync
	* @Description: Envia peticion REST para generar documento PDF como usuario administrador o integracion.
	* @author 		Manuel Medina - 18102017
	*/
	global static void sendPDFRequest( String strMainRecordId, Boolean blnStartAsync, Boolean blnIsAsync ){
		try{
			if( blnIsAsync ){
				HttpRequest httpREQ												= new HttpRequest();
				httpREQ.setEndpoint( GNF_PDFSettings__c.getInstance().GNF_SalesforceURL__c + 'services/apexrest/pdfRequest' );
				httpREQ.setMethod( 'POST' );
				httpREQ.setHeader( 'Authorization', 'OAuth ' + SFDCLogin() );
				httpREQ.setHeader( 'Content-Type', 'application/json' );
				httpREQ.setBody(
					JSON.serialize(
						new Map<String, String>{
							'strMainRecordId'		=> strMainRecordId,
							'blnStartAsync'			=> String.valueOf( blnStartAsync ),
							'blnIsAsync'			=> String.valueOf( blnIsAsync )
						} 
					) 
				);
				
				Http httpPDF													= new Http();
				HttpResponse httpRES											= httpPDF.send( httpREQ );
				
			}else{
				runFlow( strMainRecordId, blnStartAsync, blnIsAsync );
			}
		
		}catch( AdminException e ){
			String strError														= '\n\n<<<<<<<<< AdminExceptionType >>>>>>>>> \n@@--> getCause > ' + e.getCause() + '\n@@--> getLineNumber > ' + e.getLineNumber() + '\n@@--> getMessage > '+ e.getMessage() + '\n@@--> getStackTraceString > '+ e.getStackTraceString() + '\n@@--> getTypeName > ' + e.getTypeName();
			System.debug( strError );
			
			attachPDFError( strMainRecordId, strError );
			
		}catch( Exception e ){
			String strError														= '\n\n<<<<<<<<< ExceptionType >>>>>>>>> \n@@--> getCause > ' + e.getCause() + '\n@@--> getLineNumber > ' + e.getLineNumber() + '\n@@--> getMessage > '+ e.getMessage() + '\n@@--> getStackTraceString > '+ e.getStackTraceString() + '\n@@--> getTypeName > ' + e.getTypeName();
			System.debug( strError );
			
			attachPDFError( strMainRecordId, strError );
		}
	}
	
	/**
	* @Method: 		SFDCLogin
	* @param: 		String strMainRecordId
	* @param: 		String strError
	* @Description: Adjunta un PDF informando el error presentado durante la ejecucion.
	* @author 		Manuel Medina - 13022018
	*/
	public static void attachPDFError( String strMainRecordId, String strError ){
		if( GNF_PDFSettings__c.getInstance().GNF_AttachErrors__c ){
			Attachment objAttachment											= new Attachment();
			objAttachment.ParentId												= strMainRecordId;
			objAttachment.Name													= 'WS Error.pdf';
			objAttachment.Body													= Blob.toPdf( strError );
			insert objAttachment;
		}
	}
	
	/**
	* @Method: 		SFDCLogin
	* @param: 		String strMainRecordId
	* @param: 		Boolean blnStartAsync
	* @param: 		Boolean blnIsAsync
	* @Description: Realiza la conexion con Salesforce con los parametros de usuario y contraseña definidos en GNF_PDFSettings__c.
	* @author 		Manuel Medina - 02022018
	*/
	global static String SFDCLogin(){
		HttpRequest httpREQToken												= new HttpRequest();
		httpREQToken.setEndpoint( GNF_PDFSettings__c.getInstance().GNF_SalesforceLoginEndpoint__c + 'services/Soap/c/41.0/' );
		httpREQToken.setMethod( 'POST' );
		httpREQToken.setHeader( 'Content-Type', 'text/xml;charset=UTF-8' );
		httpREQToken.setHeader( 'SOAPAction', '""' );
		httpREQToken.setBody(  
			'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:enterprise.soap.sforce.com">' +
				'<soapenv:Header>' +
					'<urn:LoginScopeHeader>' +
						'<urn:organizationId></urn:organizationId>' +
					'</urn:LoginScopeHeader>' +
				'</soapenv:Header>' +
				'<soapenv:Body>' +
					'<urn:login>' +
						'<urn:username>' + GNF_PDFSettings__c.getInstance().GNF_Username__c + '</urn:username>' +
						'<urn:password>' + GNF_PDFSettings__c.getInstance().GNF_Password__c + GNF_PDFSettings__c.getInstance().GNF_Token__c + '</urn:password>' +
					'</urn:login>' +
				'</soapenv:Body>' +
			'</soapenv:Envelope>'
		);
		
		Http httpSFDCLoginREQ													= new Http();
		HttpResponse httpSFDCLoginRES											= httpSFDCLoginREQ.send( httpREQToken );
		
		Dom.Document domBodyDucument											= httpSFDCLoginRES.getBodyDocument();
		Dom.XmlNode domRootElement												= domBodyDucument.getRootElement();
		Dom.XmlNode domBody														= domRootElement.getChildElement( 'Body', 'http://schemas.xmlsoap.org/soap/envelope/' );
		Dom.XmlNode domLoginResponse											= domBody.getChildElement( 'loginResponse', 'urn:enterprise.soap.sforce.com' );
		
		String strSessionId														= '';
		
		if( domLoginResponse != null ){
			Dom.XmlNode domResult												= domLoginResponse.getChildElement( 'result', 'urn:enterprise.soap.sforce.com' );
			strSessionId														= domResult.getChildElement( 'sessionId', 'urn:enterprise.soap.sforce.com' ).getText();
			
		}else{
			Dom.XmlNode domFault												= domBody.getChildElement( 'Fault', 'urn:enterprise.soap.sforce.com' );
			throw new AdminException( domFault.getChildElement( 'faultstring', 'urn:enterprise.soap.sforce.com' ).getText() );
		}
		
		return strSessionId;
	}
	
	/**
	* @Method: 		AdminExceptions
	* @param: 		N/A
	* @Description:	Desencadenar excepciones personalizadas.
	* @author 		Manuel Medina - 02022018
	*/
	public class AdminException extends Exception{}
}