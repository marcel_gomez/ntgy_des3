@isTest
public class AG_TestScheduledBatchContratoCaducado {
    
    public static testMethod void test() {
        // Crear contrato con Caducada__c = true y EstadoBis__c != 'Caducada'
        // El contrato de debe pertenecer a un Sector de suministro
        // 
        String CRON_EXP = '0 0 0 15 3 ? 2022'; // Ejecucion a las 00:00 del 15 de marzo
        
        SSs__c ss = new SSs__c(Name = '203860395');
        insert ss;
        List<SSs__c> listSS = [SELECT id FROM SSs__C WHERE name = '203860395' LIMIT 1];
        System.assert(listSS.size()>0);
        // El contrato debe tener Fecha_de_Fin__c < Today() y Estado_Contrato__c "Vigente" o "Pendiente"
        Date fecha_fin = System.today();
        
        Contrato__c contrato = new Contrato__c (NISS__c = listSS[0].id,Fecha_de_Fin__c  = fecha_fin.addDays(-1), Estado_Contrato__c = 'Vigente', EstadoBis__c = 'Vigente');
        insert contrato;
        List<Contrato__c> contratos = [SELECT id, Caducada__c, NISS__c, Fecha_de_Fin__c, Estado_Contrato__c FROM Contrato__c]; // WHERE Caducada__c = true];
        for (Contrato__c c: contratos){
            System.debug(c.Caducada__c + ' - ' + c.Fecha_de_Fin__c + ' - ' + c.Estado_contrato__C);
        }
        System.assert(contratos.size()> 0);
        Test.startTest();
        // planificacion del batch
        String jobId = System.schedule('TestScheduledBatchContratoCaducado', CRON_EXP,  new AG_ScheduledBatchContratoCaducado());
        // Comprobar que se ha registrado la ejecucion del batch
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verificar que el batch no se ha ejecutado
        System.assertEquals(0, ct.TimesTriggered);
	    // Comprobar que el contrato tiene el valor EstadoBis__c != 'Caducada' y por lo tanto no se ha ejecutado el batch
      	List<Contrato__c> contratos2 = [SELECT Id FROM Contrato__c 
                             		WHERE EstadoBis__c = 'Caducada'];
      	System.assertEquals(contratos2.size(),0);
        
        AG_ScheduledBatchContratoCaducado theBatch = new AG_ScheduledBatchContratoCaducado();
		theBatch.start( (Database.BatchableContext) null );
		theBatch.execute( (Database.BatchableContext) null, contratos );
		theBatch.finish( (Database.BatchableContext) null );
        
      	Test.stopTest();
        // Comprovar ahora que si se ha ejecutado el batch. El contrato debe tener el valor EstadoBis__c = 'Caducada'
        contratos = [SELECT Id FROM Contrato__c 
                             		WHERE EstadoBis__c = 'Caducada'];

      	System.assertEquals(contratos.size(), 1);
        
    }
}