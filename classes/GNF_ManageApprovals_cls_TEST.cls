/**
* VASS
* @author			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba para GNF_ManageApprovals_cls.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-04		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
private class GNF_ManageApprovals_cls_TEST {
	
	public static Opportunity objOpportunity								= new Opportunity();
	
	public static void createData(){
		RecordType objRecordType											= new RecordType();
		objRecordType														= [SELECT Id,
																					DeveloperName
																				FROM RecordType
																				WHERE DeveloperName = 'Click'
																				LIMIT 1
																			];
																			
		GNF_AsignacionAprobadores__c cfgApprovalAsigment					= new GNF_AsignacionAprobadores__c();
		cfgApprovalAsigment.Name											= '1';
		cfgApprovalAsigment.GNF_Objeto__c									= 'Opportunity';
		cfgApprovalAsigment.GNF_Operador__c									= '=';
		cfgApprovalAsigment.GNF_Campo__c									= 'Tipo_de_precio__c';
		cfgApprovalAsigment.GNF_Valor__c									= 'Indicativo';
		cfgApprovalAsigment.GNF_UsuarioAprobador__c							= 'DV_IBERIA_IBERIA';
		cfgApprovalAsigment.GNF_Proceso__c									= 'GNClick';
		insert cfgApprovalAsigment;
		
		GNF_CondicionesAprobacion__c cfgApprovalCondition					= new GNF_CondicionesAprobacion__c();
		cfgApprovalCondition.GNF_Activar__c									= true;
		cfgApprovalCondition.Name											= '1';
		cfgApprovalCondition.GNF_Objeto__c									= 'Opportunity';
		cfgApprovalCondition.GNF_Operador__c								= '=';
		cfgApprovalCondition.GNF_Campo__c									= 'RecordType.DeveloperName';
		cfgApprovalCondition.GNF_Valor__c									= 'Click';
		cfgApprovalCondition.GNF_AreaAprobadora__c							= 'DV_IBERIA_IBERIA';
		cfgApprovalCondition.GNF_Proceso__c									= 'GNClick';
		cfgApprovalCondition.GNF_Mensaje__c									= 'Error test...';
		insert cfgApprovalCondition;
		
		GNF_ValoresPredeterminados__c cfgDefaultSettings					= new GNF_ValoresPredeterminados__c();
		cfgDefaultSettings.GNF_AllowedQ_PS__c								= '';
		cfgDefaultSettings.GNF_ClickEstadoInicial__c						= 'En Elaboración';
		cfgDefaultSettings.ContratoId__c									= '';
		cfgDefaultSettings.GNF_Envio_Delta_Lotes__c							= 1;
		cfgDefaultSettings.GNF_FactorConversionKWhMMBTU__c					= 1000;
		cfgDefaultSettings.GNF_factorConversionNumericoNIITtoSFDC__c		= 100;
		cfgDefaultSettings.GNF_ListaTraders__c								= '';
		cfgDefaultSettings.GNF_LogGuardarFichero__c							= false;
		cfgDefaultSettings.GNF_UnclickEstadoInicial__c						= 'En Elaboración';
		cfgDefaultSettings.GNF_unidadesDecimal__c							= 4;
		//cfgDefaultSettings.Url__c											= '';
		cfgDefaultSettings.UrlBase__c										= '';
		cfgDefaultSettings.GNF_VistaClasicaLimitePS__c						= 10;
		insert cfgDefaultSettings;
		
		objOpportunity.Name													= 'TST';
		objOpportunity.RecordTypeId											= objRecordType.Id;
		objOpportunity.CloseDate											= System.Today().addDays( 3 );
		objOpportunity.Fecha_Inicio__c										= System.Today().addMonths( 1 ).toStartOfMonth();
		objOpportunity.StageName											= 'En Elaboración';
		objOpportunity.Tipo_de_precio__c									= 'Firme';
	}
	
	static testMethod void scenarioOne(){
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName LIKE 'JV%'
																				AND IsActive = true
																				LIMIT 1
																			];
		
		Test.startTest();
		
			System.runAs( objUser ){
				createData();
				
				insert objOpportunity;
			}
			
			objOpportunity.Tipo_de_precio__c								= 'Indicativo';
			update objOpportunity;
			
			GNF_ManageApprovals_cls clsManageApprovals_1					= new GNF_ManageApprovals_cls();
			GNF_ManageApprovals_cls clsManageApprovals						= new GNF_ManageApprovals_cls( 'Opportunity', 'GNClick' );
			clsManageApprovals.validateApprovalConditions( objOpportunity, false );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioTwo(){
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName LIKE 'DT%'
																				AND ManagerId != null
																				AND IsActive = true
																				LIMIT 1
																			];
		
		Test.startTest();
		
			System.runAs( objUser ){
				createData();
				
				insert objOpportunity;
			}
			
			objOpportunity.Tipo_de_precio__c								= 'Indicativo';
			update objOpportunity;
			
			GNF_ManageApprovals_cls clsManageApprovals_1					= new GNF_ManageApprovals_cls();
			GNF_ManageApprovals_cls clsManageApprovals						= new GNF_ManageApprovals_cls( 'Opportunity', 'GNClick' );
			clsManageApprovals.validateApprovalConditions( objOpportunity, false );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioThree(){
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName = 'DV_IBERIA_IBERIA'
																				AND IsActive = true
																				LIMIT 1
																			];
		
		Test.startTest();
		
			System.runAs( objUser ){
				createData();
				
				insert objOpportunity;
			}
			
			objOpportunity.Tipo_de_precio__c								= 'Indicativo';
			update objOpportunity;
			
			GNF_ManageApprovals_cls clsManageApprovals_1					= new GNF_ManageApprovals_cls();
			GNF_ManageApprovals_cls clsManageApprovals						= new GNF_ManageApprovals_cls( 'Opportunity', 'GNClick' );
			clsManageApprovals.validateApprovalConditions( objOpportunity, false );
			
		Test.stopTest();
	}
	
	static testMethod void scenarioFour(){
		Test.startTest();
		
			
			GNF_ManageApprovals_cls clsManageApprovals						= new GNF_ManageApprovals_cls();
			clsManageApprovals.strSObjectType								= 'Opportunity';
			
			SObject sObjOpportunity											= new Opportunity();
			sObjOpportunity.put( 'Fecha_Inicio__c', System.Today() );
			sObjOpportunity.put( 'Tipo_de_precio__c', 'Indicativo' );
			sObjOpportunity.put( 'Prima_comercial__c', 7.888 );
			sObjOpportunity.put( 'Mostrar_F__c', true );
			
			List<String> lstOperators										= new List<String>{
																				'=',
																				'!=',
																				'>',
																				'<',
																				'>=',
																				'<=',
																				'LIKE',
																				'NOTLIKE'
																			};
			
			for( String strOperator : lstOperators ){
				clsManageApprovals.checkRules( sObjOpportunity, 'Fecha_Inicio__c', strOperator, String.valueOf( 0 ) );
				clsManageApprovals.checkRules( sObjOpportunity, 'Tipo_de_precio__c', strOperator, 'Indicativo' );
				clsManageApprovals.checkRules( sObjOpportunity, 'Prima_comercial__c', strOperator, String.valueOf( 7.888 ) );
				clsManageApprovals.checkRules( sObjOpportunity, 'Mostrar_F__c', strOperator, String.valueOf( true ) );
			}
			
			for( String strOperator : lstOperators ){
				clsManageApprovals.checkRules( sObjOpportunity, 'Fecha_Inicio__c', strOperator, String.valueOf( -4 ) );
				clsManageApprovals.checkRules( sObjOpportunity, 'Tipo_de_precio__c', strOperator, 'Firme' );
				clsManageApprovals.checkRules( sObjOpportunity, 'Prima_comercial__c', strOperator, String.valueOf( 8.888 ) );
				clsManageApprovals.checkRules( sObjOpportunity, 'Mostrar_F__c', strOperator, String.valueOf( false ) );
			}
			
		Test.stopTest();
	}
}