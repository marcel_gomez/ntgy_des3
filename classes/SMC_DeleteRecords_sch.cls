/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Agenda el batch SMC_SMC_DeleteRecords_bch.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-11-29		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
global class SMC_DeleteRecords_sch implements Schedulable {
	
	global String strObjectToDeleted;
	global Map<String, SMC_BorradoFicticios__c> mapObjectsToDelete;
	
	global SMC_DeleteRecords_sch(){
	}
	
	/**
	* @Method:		SMC_DeleteRecords_sch
	* @Description:	Scheduler constructor that set next object to deleted.
	* @author		Manuel Medina - 29112017
	*/
	global SMC_DeleteRecords_sch( String strObjectAPIName ){
		this.strObjectToDeleted								= strObjectAPIName;
		this.mapObjectsToDelete								= new Map<String, SMC_BorradoFicticios__c>( SMC_BorradoFicticios__c.getAll() );
	}

	/**
	* @Method: 		execute
	* @param: 		SchedulableContext sc
	* @author		Manuel Medina - 29112017
	*/
	global void execute( SchedulableContext sc ){
		if( String.isBlank( strObjectToDeleted ) ){
			this.mapObjectsToDelete							= new Map<String, SMC_BorradoFicticios__c>( SMC_BorradoFicticios__c.getAll() );
			List<String> lstObjects							= new List<String>( mapObjectsToDelete.keySet() );
			
			this.strObjectToDeleted							= lstObjects.get( 0 );
		}
		
		SMC_DeleteRecords_bch bchDeleteRecords				= new SMC_DeleteRecords_bch();
		bchDeleteRecords.mapObjectsToDelete					= mapObjectsToDelete;
		bchDeleteRecords.strObjectAPIName					= strObjectToDeleted;
		
		Database.executeBatch( bchDeleteRecords, 100 );
	}
}