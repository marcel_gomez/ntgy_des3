/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase que genera el HTML de la plantilla.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-07-31		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
global class GNF_PDFTemplates_cls {
	
	/**
	* @Method: 		getPDFTemplate
	* @param: 		String strTemplateId
	* @Description: Construye el codigo HTML de la plantilla.
	* @author 		Manuel Medina - 31072017
	*/
	webservice static String getPDFTemplate( String strTemplateId ){
		GNF_Plantilla__c objTemplate						= new GNF_Plantilla__c();
		
		try{
			objTemplate										= [SELECT Id,
																	Name
																FROM GNF_Plantilla__c
																WHERE Id =: strTemplateId
																LIMIT 1
															];

			ApexPages.StandardController sCtrTemplate		= new ApexPages.StandardController( objTemplate );
			GNF_PDFTemplates_ctr ctrPDFTemplates			= new GNF_PDFTemplates_ctr( sCtrTemplate );
			ctrPDFTemplates.setSectionRichHTML();
			
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			
			return 'Error: ' + e.getMessage() + '\n\n' + e.getStackTraceString();
		}
		
		return 'OK';
	}
	
	/**
	* @Method: 		clonePDFTemplate
	* @param: 		String strTemplateId
	* @Description: Clona toda la plantilla.
	* @author 		Manuel Medina - 02082017
	*/
	webservice static String clonePDFTemplate( String strTemplateId ){
		SObject sObjTemplate								= new GNF_Plantilla__c();
		SObject sObjTemplateCloned							= new GNF_Plantilla__c();
		Map<Id, SObject> mapSectionById						= new Map<Id, SObject>();
		Map<Id, SObject> mapSectionClonedById				= new Map<Id, SObject>();
		Map<Id, SObject> mapComponentsById					= new Map<Id, SObject>();
		Map<Id, SObject> mapComponentsClonedById			= new Map<Id, SObject>();
		Map<Id, SObject> mapConditionsById					= new Map<Id, SObject>();
		Map<Id, SObject> mapConditionsClonedById			= new Map<Id, SObject>();
		
		Savepoint sp										= Database.setSavepoint();
		
		try{
			sObjTemplate									= Database.query(
																getSOQL(
																	'GNF_Plantilla__c',
																	'WHERE Id = \'' + strTemplateId + '\'\n ' +
																	'LIMIT 1'
																)
															);
																
			sObjTemplateCloned								= sObjTemplate.clone( false, true, false, false );
			sObjTemplateCloned.put( 'GNF_NombreUnico__c', sObjTemplate.get( 'GNF_NombreUnico__c' ) + '_Clonado' + System.now().format( 'ssSSS' ) );
			insert sObjTemplateCloned;
																
			mapSectionById									= new Map<Id, SObject>(
																Database.query(
																	getSOQL(
																		'GNF_Seccion__c',
																		'WHERE GNF_Plantilla__c = \'' + strTemplateId + '\'\n'
																	)
																)
															);
															
			for( SObject sObjRecord : mapSectionById.values() ){
				SObject sObjeRecordCloned					= new GNF_Seccion__c();
				sObjeRecordCloned							= sObjRecord.clone( false, true, false, false );
				sObjeRecordCloned.put( 'GNF_Plantilla__c', sObjTemplateCloned.get( 'Id' ) );
				sObjeRecordCloned.put( 'GNF_IdExterno__c', null );
				sObjeRecordCloned.put( 'GNF_IdExternoPlantilla__c', null );
				
				mapSectionClonedById.put( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ), sObjeRecordCloned );
			}
			
			insert mapSectionClonedById.values();
			
			mapComponentsById								= new Map<Id, SObject>(
																Database.query(
																	getSOQL(
																		'GNF_Componente__c',
																		'WHERE GNF_Seccion__r.GNF_Plantilla__c = \'' + strTemplateId + '\'\n '
																	)
																)
															);
																
			for( SObject sObjRecord : mapComponentsById.values() ){
				SObject sObjeRecordCloned					= new GNF_Componente__c();
				sObjeRecordCloned							= sObjRecord.clone( false, true, false, false );
				sObjeRecordCloned.put( 'GNF_Seccion__c', mapSectionClonedById.get( Id.valueOf( String.valueOf( sObjRecord.get( 'GNF_Seccion__c' ) ) ) ).get( 'Id' ) );
				sObjeRecordCloned.put( 'GNF_IdExterno__c', null );
				sObjeRecordCloned.put( 'GNF_IdExternoSeccion__c', null );
				
				mapComponentsClonedById.put( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ), sObjeRecordCloned );
			}
			
			insert mapComponentsClonedById.values();
															
			mapConditionsById								= new Map<Id, SObject>(
																Database.query(
																	getSOQL(
																		'GNF_Condiciones__c',
																		'WHERE GNF_Seccion__r.GNF_Plantilla__c = \'' + strTemplateId + '\'\n ' +
																		'OR GNF_Componente__r.GNF_Seccion__r.GNF_Plantilla__c = \'' + strTemplateId + '\'\n '
																	)
																)
															);
																
			for( SObject sObjRecord : mapConditionsById.values() ){
				SObject sObjeRecordCloned					= new GNF_Condiciones__c();
				sObjeRecordCloned							= sObjRecord.clone( false, true, false, false );
				sObjeRecordCloned.put( 'GNF_Seccion__c', sObjRecord.get( 'GNF_Seccion__c' ) != null ? mapSectionClonedById.get( Id.valueOf( String.valueOf( sObjRecord.get( 'GNF_Seccion__c' ) ) ) ).get( 'Id' ) : null );
				sObjeRecordCloned.put( 'GNF_Componente__c', sObjRecord.get( 'GNF_Componente__c' ) != null ? mapComponentsClonedById.get( Id.valueOf( String.valueOf( sObjRecord.get( 'GNF_Componente__c' ) ) ) ).get( 'Id' ) : null );
				sObjeRecordCloned.put( 'GNF_IdExterno__c', null );
				sObjeRecordCloned.put( 'GNF_IdExternoSeccion__c', null );
				sObjeRecordCloned.put( 'GNF_IdExternoComponente__c', null );
				
				mapConditionsClonedById.put( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ), sObjeRecordCloned );
			}
			
			insert mapConditionsClonedById.values();
			
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			
			Database.rollback( sp );
			
			return 'Error: ' + e.getMessage() + '\n\n' + e.getStackTraceString();
		}
		
		return String.valueOf( sObjTemplateCloned.get( 'Id' ) );
	}
	
	/**
	* @Method: 		getSOQL
	* @param: 		String strSObject
	* @param: 		String strSOQLWHERE
	* @Description: Obtener SOQL con todos los campos disponibles para clonar.
	* @author 		Manuel Medina - 02082017
	*/
	public static String getSOQL( String strSObject, String strSOQLWHERE ){
		Map<String, Schema.SObjectType> mapSFDCObjects										= Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult sObjResult												= mapSFDCObjects.get( strSObject ).getDescribe();
						 
		Map<String, Schema.SObjectField> mapFields											= sObjResult.fields.getMap();
		
		String strSOQL																		= 'SELECT Id, \n';
		for( String strFieldName : mapFields.keySet() ){
			if( mapFields.get( strFieldName ).getDescribe().isCreateable() ){
				strSOQL																		+= '\t' + strFieldName + ', \n';
			}
		}
		
		strSOQL																				= strSOQL.substringBeforeLast( ',' ) + ' \n';
		strSOQL																				+= 'FROM ' + strSObject + ' \n' + strSOQLWHERE;
		
		return strSOQL;
	}
}