/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcionalidades requeridas por el trigger OportunidadPuntoDeSuministroTrigger.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	2.0		2017-05-11		Manuel Medina (MM)		Se implementa el metodo validateOpportunitySupplies.
*********************************************************************************************************/
public class OpPtoSuministroTriggerHandler {
	
	private static OpPtoSuministroTriggerHandler instance;
    
    public static OpPtoSuministroTriggerHandler getInstance() {
        if (instance == null) instance = new OpPtoSuministroTriggerHandler();
        return instance;
    }
    
    /**
	* @Method: 		validateOpportunitySupplies
	* @param: 		List<Oportunidad_Punto_de_Suministros__c> lstOppSupplies
	* @param: 		Map<Id, Oportunidad_Punto_de_Suministros__c> mapOldOppSuppliesById
	* @Description: Valida los clientes y/o sectores suministro ficticios y genera la eliminacion cuando sea necesario.
	* @author 		Manuel Medina - 10052017
	*/
	public void validateOpportunitySupplies( List<Oportunidad_Punto_de_Suministros__c> lstOppSupplies, Map<Id, Oportunidad_Punto_de_Suministros__c> mapOldOppSuppliesById ){
		Map<Id, Contrato__c> mapContractsById						= new Map<Id, Contrato__c>();
		Map<Id, SSs__c> mapSupplySectorsById						= new Map<Id, SSs__c>();
		
		for( Oportunidad_Punto_de_Suministros__c objOppSuppy : lstOppSupplies ){
			if( String.isNotEmpty( objOppSuppy.Contrato__c ) && String.isNotEmpty( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ) && !objOppSuppy.Contrato__c.equals( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ) ){
				mapContractsById.put( objOppSuppy.Contrato__c, new Contrato__c() );
				mapContractsById.put( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c, new Contrato__c() );
			}
		}
		
		mapContractsById											= new Map<Id, Contrato__c>( [
																		SELECT Id,
																			NISS__r.Ficticio__c,
																			Fecha_de_Inicio__c,
																			Servicio_Contratado__c,
																			Producto__c,
																			Ficticio__c
																		FROM Contrato__c
																		WHERE Id IN: mapContractsById.keySet()
																	] );
																
		for( Oportunidad_Punto_de_Suministros__c objOppSuppy : lstOppSupplies ){
			if( String.isNotEmpty( objOppSuppy.Contrato__c ) && String.isNotEmpty( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ) && !objOppSuppy.Contrato__c.equals( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ) && ( mapContractsById.get( objOppSuppy.Contrato__c ).Ficticio__c || mapContractsById.get( objOppSuppy.Contrato__c ).NISS__r.Ficticio__c || ( mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Fecha_de_Inicio__c != null && mapContractsById.get( objOppSuppy.Contrato__c ).Fecha_de_Inicio__c != null && mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Fecha_de_Inicio__c != mapContractsById.get( objOppSuppy.Contrato__c ).Fecha_de_Inicio__c ) || ( String.isNotBlank( mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Servicio_Contratado__c ) && String.isNotBlank( mapContractsById.get( objOppSuppy.Contrato__c ).Servicio_Contratado__c ) && !mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Servicio_Contratado__c.equals( mapContractsById.get( objOppSuppy.Contrato__c ).Servicio_Contratado__c ) ) || ( String.isNotBlank( mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Producto__c ) && String.isNotBlank( mapContractsById.get( objOppSuppy.Contrato__c ).Producto__c ) && !mapContractsById.get( mapOldOppSuppliesById.get( objOppSuppy.Id ).Contrato__c ).Producto__c.equals( mapContractsById.get( objOppSuppy.Contrato__c ).Producto__c ) ) ) ){
				objOppSuppy.Contrato__c.addError( System.Label.GNF_TgrOppPSMensaje1 );
			}
		}
	}
}