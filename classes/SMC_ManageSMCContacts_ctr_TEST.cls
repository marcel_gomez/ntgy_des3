/**
* VASS
* @author 			Francisco Rojas friancisco.rojas@mad.vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de TEST para la clase controller SMC_ManageSMCContacts_ctr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-11		Francisco Rojas (FR)	Definicion inicial de la clase.
*********************************************************************************************************/

@isTest
private class SMC_ManageSMCContacts_ctr_TEST {
	
	@isTest static void test_SMC_ManageSMCContacts_ctr_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			Opportunity opp = queryOpp.get(0);
        	SMC_ManageSMCContacts_ctr manageSMCContactsCTR = new SMC_ManageSMCContacts_ctr(new ApexPages.StandardController(opp));
        	manageSMCContactsCTR.mapContactById = new Map<Id, Contact>();
		}
		Test.stopTest();
	}
	
	@isTest static void test_addContacts_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			Opportunity opp = queryOpp.get(0);
        	SMC_ManageSMCContacts_ctr manageSMCContactsCTR = new SMC_ManageSMCContacts_ctr(new ApexPages.StandardController(opp));
        	manageSMCContactsCTR.addContacts();
		}
		Test.stopTest();
	}

	@isTest static void test_addContacts2_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity  LIMIT 10];
		List<sObject> lsJunction_SMC_Contactos	= Test.loadData( SMC_ContactosSMC__c.sObjectType, 'SMC_ContactosSMC' );
		if(queryOpp.size() > 0){
			Opportunity opp = queryOpp.get(0);
        	SMC_ManageSMCContacts_ctr manageSMCContactsCTR = new SMC_ManageSMCContacts_ctr(new ApexPages.StandardController(opp));
        	List<Contact> queryContacto = [SELECT Id FROM Contact Where AccountId =: opp.AccountId ];
			List<SMC_ContactosSMC__c> querySMCContacto = [SELECT Id FROM SMC_ContactosSMC__c WHERE SMC_CartaClick__c =: opp.Id ];
			if(queryContacto.size() > 0 && querySMCContacto.size() > 0){
				Contact objContacto = queryContacto.get(0);
				SMC_ContactosSMC__c objSMCContacto = querySMCContacto.get(0);
				ApexPages.currentPage().getParameters().put( 'idSMCContact', objSMCContacto.Id );
	        	ApexPages.currentPage().getParameters().put( 'idContact', objContacto.Id );	        	
	        	manageSMCContactsCTR.deleteSMCContact();
	        }
		}
		Test.stopTest();
	}

	@isTest static void test_objSMCContact_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			Opportunity opp = queryOpp.get(0);
			List<Contact> queryContacto = [SELECT Id FROM Contact Where AccountId =: opp.AccountId ];
			if(queryContacto.size() > 0){
				Contact objContacto = queryContacto.get(0);
        		SMC_ManageSMCContacts_ctr.SMCContact wrapperContactos = new SMC_ManageSMCContacts_ctr.SMCContact(objContacto); 
        	}
		}
		Test.stopTest();
	}

	@isTest static void test_objSMCContact2_mthd() {
		createData();
		Test.startTest();
		List<Opportunity> queryOpp = [SELECT Id, AccountId FROM Opportunity  LIMIT 10];
		if(queryOpp.size() > 0){
			Opportunity opp = queryOpp.get(0);
			List<Contact> queryContacto = [SELECT Id FROM Contact Where AccountId =: opp.AccountId ];
			if(queryContacto.size() > 0){
				Contact objContacto = queryContacto.get(0);
				SMC_ManageSMCContacts_ctr.SMCContact wrapperContactos = new SMC_ManageSMCContacts_ctr.SMCContact(opp.Id, objContacto); 
        	}
		}
		Test.stopTest();
	}

	@isTest
	public static void createData(){
		SMC_TestData_cls.strLoadUntil = 'Contact';
		SMC_TestData_cls.createData();
		List<sObject> lsopportunity	= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
		// SMC_TestData_cls.createSMC();
	}
}