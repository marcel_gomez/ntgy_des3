/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcionalidades requeridas por el trigger GNF_Opportunity_tgr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-08		Manuel Medina (MM)		Definicion inicial de la clase.
* @version	2.0		2017-05-23		Manuel Medina (MM)		Se crea el metodod assignApprovers encargado de conectar con
*															la clase GNF_ManageApprovals_cls para asignar los aprobadores.
*********************************************************************************************************/
public without sharing class GNF_OpportunityTriggerHandler_cls {

	public static Set<String> setRecordTypesByDevName				= new Set<String>{ 'Click', 'Unclick', 'SMC_Servicio_Multiclick' };
	public static Map<String, RecordType> mapRecordTypeIdByDevName	= new Map<String, RecordType>();
	public static Map<String, Id> mapOpportunityIdClonedByOppId		= new Map<String, Id>();
	
	/**
	* @Method: 		setMapRecordTypeIdByDevName
	* @param: 		N/A
	* @Description:	Carga el mapa mapRecordTypeIdByDevName con los tipos de registro requeridos.
	* @author 		Manuel Medina - 08062017
	*/
	public static void setMapRecordTypeIdByDevName(){
		List<RecordType> lstRecordTypes								= new List<RecordType>( [
																		SELECT Id,
																			Name,
																			DeveloperName,
																			SObjectType
																		FROM RecordType
																		WHERE DeveloperName IN: setRecordTypesByDevName
																	] );
																
		for( RecordType objRecordType : lstRecordTypes ){
			mapRecordTypeIdByDevName.put( objRecordType.DeveloperName + '_' + objRecordType.SObjectType, objRecordType );
		}
	}

	/**
	* @Method: 		validateAccount
	* @param: 		List<Opportunity> lstOpportunities
	* @param: 		Map<Id, Opportunity> mapOldOpportunitiesById
	* @Description:	Valida los clientes ficticios y genera la eliminacion cuando sea necesario.
	* @author 		Manuel Medina - 08052017
	*/
	public static void validateAccount( List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpportunitiesById ){
		Map<Id, Account> mapAccountsById							= new Map<Id, Account>();
		
		for( Opportunity objOpportunity : lstOpportunities ){
			if( String.isNotEmpty( objOpportunity.AccountId ) && String.isNotEmpty( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ) && !objOpportunity.AccountId.equals( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ) ){
				mapAccountsById.put( objOpportunity.AccountId, new Account() );
				mapAccountsById.put( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId, new Account() );
			}
		}
		
		mapAccountsById												= new Map<Id, Account>( [
																		SELECT Id,
																			Ficticio__c,
																			NIF_CIF__c
																		FROM Account
																		WHERE Id IN: mapAccountsById.keySet()
																	] );
																
		for( Opportunity objOpportunity : lstOpportunities ){
			if( String.isNotEmpty( objOpportunity.AccountId ) && String.isNotEmpty( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ) && !objOpportunity.AccountId.equals( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ) && ( mapAccountsById.get( objOpportunity.AccountId ).Ficticio__c || ( String.isNotBlank( mapAccountsById.get( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ).NIF_CIF__c ) && String.isNotBlank( mapAccountsById.get( objOpportunity.AccountId ).NIF_CIF__c ) && !mapAccountsById.get( mapOldOpportunitiesById.get( objOpportunity.Id ).AccountId ).NIF_CIF__c.equals( mapAccountsById.get( objOpportunity.AccountId ).NIF_CIF__c ) ) ) ){
				objOpportunity.AccountId.addError( System.Label.GNF_TgrOppMensaje1 );
			}
		}
	}
	
	/**
	* @Method: 		valideOpportunityClosure
	* @param: 		List<Opportunity> lstOpportunities
	* @param: 		Map<Id, Opportunity> mapOldOpportunitiesById
	* @Description:	Valida el cierre de la oportunidad cuando se tiene Cliente, Contrato o Sector Suministro ficticios.
	* @author 		Manuel Medina - 11052017
	*/
	public static void valideOpportunityClosure( List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpportunitiesById ){
		Map<Id, Opportunity> mapOpportunitiesById					= new Map<Id, Opportunity>();
		Map<Id, Boolean> mapAccountById								= new Map<Id, Boolean>();
		Set<Id> setAccountsId										= new Set<Id>();
		
		for( Opportunity objOpportunity : lstOpportunities ){
			if( objOpportunity.IsClosed && !mapOldOpportunitiesById.get( objOpportunity.Id ).IsClosed && objOpportunity.IsWon ){
				mapOpportunitiesById.put( objOpportunity.Id, new Opportunity() );
				setAccountsId.add( objOpportunity.AccountId );
			}
		}

		//22/09/2017 : JSC - Ajuste para validación de ficticios en click multiCiff (cuentas padre-hijas)
		mapAccountById												= validateParentAccount( setAccountsId );

		mapOpportunitiesById										= new Map<Id, Opportunity>( [
																		SELECT Id,
																			AccountId,
																			Account.Ficticio__c,
																			RecordType.Name,
																			(
																				SELECT Id, 
																					Contrato__c,
																					Contrato__r.Name,
																					Contrato__r.Ficticio__c,
																					Contrato__r.NISS__c,
																					Contrato__r.NISS__r.Name,
																					Contrato__r.NISS__r.Ficticio__c,
																					Contrato__r.NISS__r.NIS__c,
																					Contrato__r.NISS__r.NIS__r.Name,
																					Contrato__r.NISS__r.NIS__r.Ficticio__c,
																					Contrato__r.Cliente__c,
																					Contrato__r.Cliente__r.Ficticio__c
																				FROM Oportunidad_Puntos_de_Suministros__r
																			)
																		FROM Opportunity
																		WHERE Id IN: mapOpportunitiesById.keySet()
																	] );
																
		for( Opportunity objOpportunity : lstOpportunities ){
			
			//22/09/2017 : JSC - Ajuste para validación de ficticios en click multiCiff (cuentas padre-hijas)
			if( objOpportunity.IsClosed && !mapOldOpportunitiesById.get( objOpportunity.Id ).IsClosed && objOpportunity.IsWon && String.isNotEmpty( mapOpportunitiesById.get( objOpportunity.Id ).AccountId ) && mapAccountById.containsKey( objOpportunity.AccountId ) && mapAccountById.get( objOpportunity.AccountId ) ){
				objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Cliente' ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
			}/*
			
			if( objOpportunity.IsClosed && !mapOldOpportunitiesById.get( objOpportunity.Id ).IsClosed && objOpportunity.IsWon && objOpportunity.AccountId != null && mapOpportunitiesById.containsKey( objOpportunity.Id ) && mapOpportunitiesById.get( objOpportunity.Id ).Account.Ficticio__c ){
				objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Cliente' ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
				
			}*/else if( objOpportunity.IsClosed && !mapOldOpportunitiesById.get( objOpportunity.Id ).IsClosed && objOpportunity.IsWon ){
				for( Oportunidad_Punto_de_Suministros__c objOppSupplies : mapOpportunitiesById.get( objOpportunity.Id ).Oportunidad_Puntos_de_Suministros__r ){
					
					if( String.isNotEmpty( objOppSupplies.Contrato__c ) && objOppSupplies.Contrato__r.Cliente__c != null && objOppSupplies.Contrato__r.Cliente__r.Ficticio__c ){
						objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Cliente' ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
						
					}else if( String.isNotEmpty( objOppSupplies.Contrato__c ) && objOppSupplies.Contrato__r.Ficticio__c ){
						objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Contrato ' + objOppSupplies.Contrato__r.Name ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
						
					}else if( String.isNotEmpty( objOppSupplies.Contrato__c ) && String.isNotEmpty( objOppSupplies.Contrato__r.NISS__c ) && objOppSupplies.Contrato__r.NISS__r.Ficticio__c ){
						objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Sector Suministro ' + objOppSupplies.Contrato__r.NISS__r.Name ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
						
					}else if( String.isNotEmpty( objOppSupplies.Contrato__c ) && String.isNotEmpty( objOppSupplies.Contrato__r.NISS__c ) && String.isNotEmpty( objOppSupplies.Contrato__r.NISS__r.NIS__c ) && objOppSupplies.Contrato__r.NISS__r.NIS__r.Ficticio__c ){
						objOpportunity.StageName.addError( System.Label.GNF_TgrOppMensaje2.replace( '$FICTICIO$', 'Suministro ' + objOppSupplies.Contrato__r.NISS__r.NIS__r.Name ).replace( '$StageName$', objOpportunity.StageName ).replace( '$OFERTA$', mapOpportunitiesById.get( objOpportunity.Id ).RecordType.Name ) );
					}
				}
			}
		}
	}

	/**
	* @Method: 		validateParentAccount
	* @param: 		Map<Id, Account> mapAccountById
	* @Description:	Validar si las cuentas asociadas son padre.
	* @author 		Manuel Medina - 23062017
	*/
	public static Map<Id, Boolean> validateParentAccount( Set<Id> setAccountsId ){
		Map<Id, Boolean> mapAccountById								= new Map<Id, Boolean>();
		List<Account> lstAccounts									= new List<Account>();
		lstAccounts													= [SELECT Id,
																			Name,
																			Ficticio__c,
																			(
																				SELECT Id,
																					Name,
																					Ficticio__c
																				FROM ChildAccounts
																			)
																		FROM Account
																		WHERE Id IN: setAccountsId
																	];

		for( Account objAccount : lstAccounts ){
			Integer i												= 0;
			Boolean blnFicticio										= false;
			while( !objAccount.ChildAccounts.isEmpty() && i < objAccount.ChildAccounts.size() && !blnFicticio ){
				blnFicticio											= objAccount.ChildAccounts.get( i ).Ficticio__c;
				i++;
			}

			mapAccountById.put( objAccount.Id, blnFicticio ? true : ( !objAccount.ChildAccounts.isEmpty() ? false : objAccount.Ficticio__c ) );
		}

		return mapAccountById;
	}

	/**
	* @Method: 		getSOQL
	* @param: 		String strSObject
	* @param: 		String strSOQLWHERE
	* @Description:	Obtener SOQL con todos los campos disponibles para clonar.
	* @author 		Manuel Medina - 08062017
	*/
	public static String getSOQL( String strSObject, String strSOQLWHERE ){
		Map<String, Schema.SObjectType> mapSFDCObjects										= Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult sObjResult												= mapSFDCObjects.get( strSObject ).getDescribe();
						 
		Map<String, Schema.SObjectField> mapFields											= sObjResult.fields.getMap();
		
		String strSOQL																		= 'SELECT Id, \n';
		for( String strFieldName : mapFields.keySet() ){
			if( mapFields.get( strFieldName ).getDescribe().isCreateable() ){
				strSOQL																		+= '\t' + strFieldName + ', \n';
			}
		}
		
		strSOQL																				= strSOQL.substringBeforeLast( ',' ) + ' \n';
		strSOQL																				+= 'FROM ' + strSObject + ' \n' + strSOQLWHERE;
		
		return strSOQL;
	}
	
	/**
	* @Method: 		assignApprovers
	* @param: 		Map<Id, Opportunity> mapOpportunitiesById
	* @Description:	Asignar aprobadores.
	* @author 		Manuel Medina - 23052017
	*/
	public static void assignApprovers( Map<Id, Opportunity> mapOpportunitiesById, Map<Id, Opportunity> mapOldOpportunitiesById ){
		Map<Id, SObject> mapOpportunitiesToAssign											= new Map<Id, SObject>();
		Map<String, Map<Id, Opportunity>> mapOpportunitiesByProcess							= new Map<String, Map<Id, Opportunity>>(
																								getOpportunitiesByProcess( mapOpportunitiesById )
																							);
		Map<String, Map<Id, Opportunity>> mapOldOpportunitiesByProcess						= new Map<String, Map<Id, Opportunity>>(
																								Trigger.isUpdate ? getOpportunitiesByProcess( mapOldOpportunitiesById ) : new Map<String, Map<Id, Opportunity>>()
																							);
			
		for( String strProcess : mapOpportunitiesByProcess.keySet() ){
			
			if( Trigger.isUpdate ){
				GNF_ManageApprovals_cls clsManageApprovals									= new GNF_ManageApprovals_cls( 'Opportunity' );
				//mapOpportunitiesToAssign													= clsManageApprovals.haveFieldsChanged( mapOpportunitiesById, mapOldOpportunitiesById );
				mapOpportunitiesToAssign													= clsManageApprovals.haveFieldsChanged( mapOpportunitiesByProcess.get( strProcess ), mapOldOpportunitiesByProcess.get( strProcess ) );
			}
			
			if( !System.isQueueable() && ( Trigger.isInsert || ( Trigger.isUpdate && !mapOpportunitiesToAssign.isEmpty() ) ) ){
				//ID jobID 																	= System.enqueueJob( new GNF_ManageApprovals_cls( ( Trigger.isUpdate ? mapOpportunitiesToAssign : mapOpportunitiesById ), 'Opportunity' ) );
				GNF_ManageApprovals_cls clsManageApprovals_Assing							= new GNF_ManageApprovals_cls( ( Trigger.isUpdate ? mapOpportunitiesToAssign : mapOpportunitiesByProcess.get( strProcess ) ), 'Opportunity' );
				clsManageApprovals_Assing.strProcess										= strProcess;
				ID jobID 																	= System.enqueueJob( clsManageApprovals_Assing );
			}
		}
	}

	/**
	* @Method: 		getOpportunitiesByProcess
	* @param: 		Map<Id, Opportunity> mapOpportunitiesById
	* @Description:	Separa las oportunidades por proceso.
	* @author 		Manuel Medina - 03112017
	*/
	public static Map<String, Map<Id, Opportunity>> getOpportunitiesByProcess( Map<Id, Opportunity> mapOpportunitiesById ){
		Map<String, Map<Id, Opportunity>> mapOpportunitiesByProcess							= new Map<String, Map<Id, Opportunity>>();
		Map<Id, Opportunity> mapGNClickById													= new Map<Id, Opportunity>();
		Map<Id, Opportunity> mapSMCById														= new Map<Id, Opportunity>();
		
		for( Opportunity objOpportunity : mapOpportunitiesById.values() ){
			if( objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'Click_Opportunity' ).Id ) || objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ){
				mapGNClickById.put( objOpportunity.Id, objOpportunity );
				
			}else if( objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'SMC_Servicio_Multiclick_Opportunity' ).Id ) ){
				mapSMCById.put( objOpportunity.Id, objOpportunity );
			}
		}
		
		if( !mapGNClickById.isEmpty() ){
			mapOpportunitiesByProcess.put( 'GNClick', mapGNClickById );
		}
		
		if( !mapSMCById.isEmpty() ){
			mapOpportunitiesByProcess.put( 'SMC', mapSMCById );
		}
		
		return mapOpportunitiesByProcess;
	}

	/**
	* @Method: 		mapeoCodigoFormula
	* @param: 		list<Opportunity> list_newOpportunities: Lista con las oportunidades que se han creado o actualizado. Contiene la información actual/nueva de los registros.
	*				list<Opportunity> list_oldOpportunities: Lista con las oportunidades que se han actualizado. Contiene la información que contenia el registro antes de la actualización.
	* @Description:	--
	* @author: 		Juan Cardona - 19/07/2017
	*/
	public static void mapeoCodigoFormula( list<Opportunity> list_newOpportunities, list<Opportunity> list_oldOpportunities )
	{
		Opportunity 				obj_oldOpp;
		
		map<string, string> 		map_formulaXcodigoPaisMaestro;
		map<string, Opportunity> 	map_oldOpportunityXid;
		map<string, Opportunity> 	map_newOpportunityXid;
						
		set<string> 				set_paisFormula;
		set<string> 				set_codigoFormula;
    	
    	if( list_newOpportunities==null || list_newOpportunities.isEmpty() )
    		return;

		map_formulaXcodigoPaisMaestro 	= new map<string, string>();
		map_oldOpportunityXid			= new map<string, Opportunity>(); 
		map_newOpportunityXid			= new map<string, Opportunity>(); 

		set_paisFormula					= new set<string>();
		set_codigoFormula				= new set<string>();

		if( list_oldOpportunities!=null )
			for( Opportunity obj_opp : list_oldOpportunities )
				if( obj_opp.id!=null )
					map_oldOpportunityXid.put( obj_opp.id, obj_opp );	
		
		for( Opportunity obj_opp : list_newOpportunities )
		{
			if( obj_opp.Pais__c!=null )
				set_paisFormula.add( obj_opp.Pais__c );
			
			if( obj_opp.Formula__c!=null )
				set_codigoFormula.add( obj_opp.Formula__c );			
		}	
		
		for( Maestro_Formula__c obj_maestro : [SELECT Id, GNF_paisHomologadoMeastro__c, Codigo_Formula__c, Formula__c, Pais__c, Expresion_Formula_CV__c FROM Maestro_Formula__c WHERE Expresion_Formula_CV__c != NULL AND Pais__c!= NULL AND Formula__c!=NULL AND GNF_paisHomologadoMeastro__c IN : set_paisFormula AND Formula__c IN : set_codigoFormula] )
			map_formulaXcodigoPaisMaestro.put( obj_maestro.Formula__c + '-' + obj_maestro.GNF_paisHomologadoMeastro__c, obj_maestro.Expresion_Formula_CV__c );

    	for( Opportunity obj_newOpp : list_newOpportunities )
    	{
    		if( obj_newOpp.id!=null && map_oldOpportunityXid.containsKey( obj_newOpp.id ) )
    			obj_oldOpp = map_oldOpportunityXid.get( obj_newOpp.id );	
    		else
    			obj_oldOpp = null;
    		
    		if(  obj_newOpp.Pais__c!=null && ( ( obj_oldOpp==null && obj_newOpp.Formula__c!=null ) || ( obj_oldOpp!=null && obj_newOpp.Formula__c!=null && obj_oldOpp.Formula__c!=obj_newOpp.Formula__c ) ) )
    		{		
    			if( map_formulaXcodigoPaisMaestro.containsKey( obj_newOpp.Formula__c + '-' + obj_newOpp.Pais__c ) )
   			 		obj_newOpp.Formula_a_cubrir__c = map_formulaXcodigoPaisMaestro.get( obj_newOpp.Formula__c + '-' + obj_newOpp.Pais__c );
   			 	else
    				obj_newOpp.Formula_a_cubrir__c = null;   			 		
   			 		
    		}else if( obj_oldOpp!=null && obj_newOpp.Formula__c==null && obj_oldOpp.Formula__c!=obj_newOpp.Formula__c  ){
    			obj_newOpp.Formula_a_cubrir__c = null;
    		
    		}
    	}
	}//ends mapeoCodigoFormula	
	
	
	/**
	* @Method: 		setAccountOwnerToOpportunityOwner
	* @param: 		List<Opportunity> lstOpportunities
	* @param: 		Map<Id, Opportunity> mapOldOpportunities
	* @Description:	Asigna el propietario de la cuenta a la oportunidad.
	* @author 		Manuel Medina - 27072017
	*/
	public static void setAccountOwnerToOpportunityOwner( List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpportunities ){
		Map<Id, Opportunity> mapOpportunityById										= new Map<Id, Opportunity>();
		Map<Id, Account> mapAccountById												= new Map<Id, Account>();
		
		for( Opportunity objOpportunity : lstOpportunities ){
			if( Trigger.isUpdate && ( objOpportunity.AccountId != mapOldOpportunities.get( objOpportunity.Id ).AccountId || objOpportunity.OwnerId != mapOldOpportunities.get( objOpportunity.Id ).OwnerId ) ){
				mapOpportunityById.put( objOpportunity.Id, new Opportunity() );
				
			}else if( Trigger.isInsert ){
				mapAccountById.put( objOpportunity.AccountId, new Account() );
			}
		}
		
		mapOpportunityById															= new Map<Id, Opportunity>( [
																						SELECT Id,
																							AccountId,
																							Account.OwnerId,
																							OwnerId
																						FROM Opportunity
																						WHERE Id IN: mapOpportunityById.keySet()
																					] );
																					
		mapAccountById																= new Map<Id, Account>( [
																						SELECT Id,
																							OwnerId
																						FROM Account
																						WHERE Id IN: mapAccountById.keySet()
																					] );
																					
		for( Opportunity objOpportunity : lstOpportunities ){
			objOpportunity.OwnerId													= mapOpportunityById.containsKey( objOpportunity.Id ) ? mapOpportunityById.get( objOpportunity.Id ).Account.OwnerId : ( mapAccountById.containsKey( objOpportunity.AccountId ) ? mapAccountById.get( objOpportunity.AccountId ).OwnerId : objOpportunity.OwnerId ); 
		}
	}
	
	/**
	* @Method: 		validateUnclickSigned
	* @param: 		List<Opportunity> lstOpportunities
	* @Description:	Valida si el click relacionado al unclick ya tiene unclicks firmados.
	* @author 		Manuel Medina - 10082017
	*/
	public static void validateUnclickSigned( List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpportunities ){
		Map<Id, Opportunity> mapOppotunityById										= new Map<Id, Opportunity>();
		Set<Id> setOppotunityId														= new Set<Id>();
		
		for( Opportunity objOpportunity : lstOpportunities ){
			if( ( ( Trigger.isInsert && objOpportunity.StageName.equals( 'Firmada' ) ) || ( Trigger.isUpdate && objOpportunity.StageName.equals( 'Firmada' ) && !objOpportunity.StageName.equals( mapOldOpportunities.get( objOpportunity.Id ).StageName ) ) ) && objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ){
				mapOppotunityById.put( objOpportunity.Click_Asociado__c, new Opportunity() );
				setOppotunityId.add( objOpportunity.Id );
			}
		}
		
		mapOppotunityById															= new Map<Id, Opportunity>( [
																						SELECT Id,
																							(
																								SELECT Id
																								FROM Clicks__r
																								WHERE RecordType.DeveloperName = 'Unclick'
																								AND StageName = 'Firmada'
																								AND Id NOT IN: setOppotunityId
																							)
																						FROM Opportunity
																						WHERE Id IN: mapOppotunityById.keySet()
																					] );
		
		for( Opportunity objOpportunity : lstOpportunities ){
			if( mapOppotunityById.containsKey( objOpportunity.Click_Asociado__c ) && !mapOppotunityById.get( objOpportunity.Click_Asociado__c ).Clicks__r.isEmpty() ){
				objOpportunity.addError( System.Label.GNF_TgrOppMensaje3 );
			}
		}
	}

	/**
	* @Method: 		fetchCountryFromAccount
	* @param: 		List<Opportunity> lstOpportunities
	* @Description:	Valida si el click relacionado al unclick ya tiene unclicks firmados.
	* @author 		Manuel Medina - 10082017
	*/

	public static void fetchCountryFromAccount (List<Opportunity> lstOpportunities){
		Set<Id> setIdAccounts = new Set<Id>();
		Map<Id, String> mapIdxPaisAccount = new Map<Id, String>();
		for(Opportunity opp : lstOpportunities){
			setIdAccounts.add(opp.AccountId); 
		}
		for(Account acc : [SELECT Id, Pais__c FROM Account WHERE Id in: setIdAccounts]){
			mapIdxPaisAccount.put(acc.Id, acc.Pais__c); 
		}	
		for(Opportunity opp : lstOpportunities){
			opp.Pais__c = mapIdxPaisAccount.get(opp.AccountId); 
		}
	}
	
	/**
	* @Method: 		updateConsumosClick
	* @param: 		Map<Id, Opportunity> mapNewOpportunityById
	* @Description:	Actualizar los consumos click relacionados.
	* @author 		Manuel Medina - 26102017
	*/
	/*
	public static void updateConsumosClick( Map<Id, Opportunity> mapNewOpportunityById ){
		List<Consumo_Click__c> lstConsumosClick										= new List<Consumo_Click__c>( [
																							SELECT Id
																							FROM Consumo_Click__c
																							WHERE Oportunidad_Punto_de_Suministro__r.Oportunidad__c IN: mapNewOpportunityById.keySet()
																							OR Cotizacion_de_periodo__r.Opportunity__c IN: mapNewOpportunityById.keySet()
																						] );
		
		if( !Test.isRunningTest() && !System.isFuture() && !System.isBatch() && !lstConsumosClick.isEmpty() ){
			updateConsumosClickAsync( JSON.serialize( lstConsumosClick ) );
		}
	}
	*/
	
	/**
	* @Method: 		updateConsumosClickAsync
	* @param: 		String strLstConsumosClick
	* @Description:	Actualizar los consumos click relacionados.
	* @author 		Manuel Medina - 20112017
	*/
	/*
	@future
	public static void updateConsumosClickAsync( String strLstConsumosClick ){
		List<Consumo_Click__c> lstConsumosClick										= new List<Consumo_Click__c>( ( List<Consumo_Click__c> ) JSON.deserialize( strLstConsumosClick, List<Consumo_Click__c>.class ) );
		
		update lstConsumosClick;
	}
	*/

	/**
	* @Method: 		updateEndDateCB
	* @param: 		
	* @Description:	Actualiza la fecha fin de la condicion de cobertura origen
	* @author 		Francisco Rojas - 20112017
	*/
	public static void updateEndDateCB (List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpp){
		Map<Id,Map<String,Date>> mapSMCDates = new Map<Id,Map<String,Date>>();
		List<Condiciones_de_Cobertura__c> listCB = new List<Condiciones_de_Cobertura__c>();
		Id strIdRecordType = mapRecordTypeIdByDevName.get('SMC_Servicio_Multiclick_Opportunity').Id;
		for(Opportunity opp :lstOpportunities)
			if( Trigger.isUpdate && opp.RecordTypeId == strIdRecordType && ( ( opp.StageName.equals( 'Contratada') && !opp.GNF_FicticiosRelacionados__c) || ( opp.StageName.equals( 'Aceptada') && opp.GNF_FicticiosRelacionados__c) ) && opp.StageName != mapOldOpp.get(opp.Id).StageName && opp.Subtipo__c.equals('Mod') && opp.Click_Asociado__c != null ){
				Map<String, Date> mapDate = new Map<String, Date>();
				mapDate.put('fechaInicio', opp.Fecha_Inicio__c);
				// mapDate.put('fechaFin', opp.Fecha_Fin__c);
				// System.debug('[FRJ] updateEndDateCB -> mapDate :'+ mapDate);
				mapSMCDates.put(opp.Click_Asociado__c, mapDate);
			}

		// System.debug('[FRJ] updateEndDateCB -> mapSMCDates :'+ mapSMCDates);
		for(Condiciones_de_Cobertura__c cb : [SELECT Id, SMC_Oportunidad__c,Fecha_de_fin__c, Fecha_de_inicio__c FROM Condiciones_de_Cobertura__c WHERE SMC_Oportunidad__c != null AND SMC_Oportunidad__c IN:mapSMCDates.keySet() AND Estado__c = 'Activo']) {
			// validate end Date of Condicion de Cobertura
			if (cb.Fecha_de_inicio__c >= mapSMCDates.get(cb.SMC_Oportunidad__c).get('fechaInicio'))
				cb.Estado__c = 'Inactivo';				
			else
				cb.Fecha_de_fin__c = mapSMCDates.get(cb.SMC_Oportunidad__c).get('fechaInicio').addDays(-1);
			
			//add to bulk dml list
			listCB.add(cb);

		}
		// System.debug('[FRJ] updateEndDateCB -> listCB :'+ listCB);
		update listCB;
	}

	/**
	* @Method: 		generateCBClon
	* @param:
	* @Description:	genera un clon de condicion de cobertura
	* @author 		Francisco Rojas  - 20112017
	*/
	public static void generateCBClon (List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpp){
		Set<Id> setSMCId = new Set<Id>();
		List<Condiciones_de_Cobertura__c> listCB = new List<Condiciones_de_Cobertura__c>();
		Id strIdRecordType = mapRecordTypeIdByDevName.get('SMC_Servicio_Multiclick_Opportunity').Id;
		Map<Id, Id> mapSMCCB = new Map<Id, Id>();
		Map<Id, Id> mapSMCCBClon = new Map<Id, Id>();
		Map<Id, List<Oportunidad_Punto_de_Suministros__c>> mapCBOPS = new Map<Id, List<Oportunidad_Punto_de_Suministros__c>>();
		List<GNF_Condiciones_Cobertura_por_Contrato__c> listJuntionObj = new List<GNF_Condiciones_Cobertura_por_Contrato__c>();
		for(Opportunity opp :lstOpportunities)
			if( Trigger.isUpdate && ( ( opp.StageName.equals( 'Contratada') && !opp.GNF_FicticiosRelacionados__c) || ( opp.StageName.equals( 'Aceptada') && opp.GNF_FicticiosRelacionados__c) ) && opp.StageName != mapOldOpp.get(opp.Id).StageName && opp.RecordTypeId == strIdRecordType ){
				setSMCId.add(opp.Id);
			}

		// System.debug('[FRJ] generateCBClon -> setSMCId :'+ setSMCId);
		String soqlQuery = getSOQL('Condiciones_de_Cobertura__c', 'WHERE SMC_ReadOnly__c = false AND Estado__c = \''+ System.Label.SMC_CBEstadoHistorico + '\' AND SMC_Oportunidad__c != null AND SMC_Oportunidad__c IN: setSMCId \n');
		// System.debug('[FRJ] generateCBClon -> soqlQuery :' + soqlQuery);
		for(Condiciones_de_Cobertura__c cb : Database.query(soqlQuery) ){
			Condiciones_de_Cobertura__c clonCB = cb.clone(false, true, false, false);
			// set State of Condiciones_de_Cobertura__c to allow visualization when filter by Estado__c
			cb.Estado__c = 'Activo';
			mapSMCCB.put(cb.SMC_Oportunidad__c, cb.Id);
			// add to list to update
			listCB.add(cb);
			// set Flag of Condiciones_de_Cobertura__c to avoid future modifications
			clonCB.SMC_ReadOnly__c = true;
			// add to list to insert
			listCB.add(clonCB);
		}
		upsert listCB;
		// System.debug('[FRJ] generateCBClon -> mapSMCCB :'+ mapSMCCB);
		for(Condiciones_de_Cobertura__c cb : listCB)
			if(cb.SMC_ReadOnly__c)
				mapSMCCBClon.put(cb.SMC_Oportunidad__c, cb.Id);

		// create map Oportunidad_Punto_de_Suministros__c by Condiciones_de_Cobertura__c
		for(Oportunidad_Punto_de_Suministros__c ops : [SELECT Id, Contrato__c, Oportunidad__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c IN:mapSMCCB.keySet()])
			if(mapCBOPS.containsKey(mapSMCCB.get(ops.Oportunidad__c)))
				mapCBOPS.get(mapSMCCB.get(ops.Oportunidad__c)).add(ops);
			else
				mapCBOPS.put(mapSMCCB.get(ops.Oportunidad__c), new List<Oportunidad_Punto_de_Suministros__c>{ ops });

		// System.debug('[FRJ] generateCBClon -> mapCBOPS :'+ mapCBOPS);
		for(Id idCB : mapCBOPS.keySet())
			for(Oportunidad_Punto_de_Suministros__c ps: mapCBOPS.get(idCB)){
				listJuntionObj.add(new GNF_Condiciones_Cobertura_por_Contrato__c(GNF_Condicion_Cobertura__c = idCB, GNF_Contrato__c = ps.Contrato__c));
				listJuntionObj.add(new GNF_Condiciones_Cobertura_por_Contrato__c(GNF_Condicion_Cobertura__c = mapSMCCBClon.get(ps.Oportunidad__c), GNF_Contrato__c = ps.Contrato__c));
			}

		insert listJuntionObj;
	}

	/**
	* @Method: 		removeFlagFicticio
	* @param:
	* @Description:	quita el flag de fictico de la condicion de cobertura
	* @author 		Francisco Rojas  - 20112017
	*/
	public static void removeFlagFicticio (List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpp){
		Set<Id> setSMCId = new Set<Id>();
		List<Condiciones_de_Cobertura__c> listCB = new List<Condiciones_de_Cobertura__c>();
		Id strIdRecordType = mapRecordTypeIdByDevName.get('SMC_Servicio_Multiclick_Opportunity').Id;
		Map<Id, Id> mapSMCCB = new Map<Id, Id>();
		Map<Id, List<Oportunidad_Punto_de_Suministros__c>> mapCBOPS = new Map<Id, List<Oportunidad_Punto_de_Suministros__c>>();
		List<GNF_Condiciones_Cobertura_por_Contrato__c> listJuntionObj = new List<GNF_Condiciones_Cobertura_por_Contrato__c>();
		for(Opportunity opp :lstOpportunities)
			if( Trigger.isUpdate && ( opp.StageName.equals( 'Contratada') && !opp.GNF_FicticiosRelacionados__c ) && opp.StageName != mapOldOpp.get(opp.Id).StageName && opp.RecordTypeId == strIdRecordType ){
				setSMCId.add(opp.Id);
			}

		System.debug('[FRJ] removeFlagFicticio -> setSMCId :'+ setSMCId);
		for(Condiciones_de_Cobertura__c cb : [SELECT Id, SMC_Oportunidad__c,Fecha_de_fin__c, Fecha_de_inicio__c FROM Condiciones_de_Cobertura__c WHERE SMC_Oportunidad__c != null AND SMC_Oportunidad__c IN:setSMCId AND Estado__c = 'Activo']){
			// set false flag fcticio
			cb.SMC_Ficticio__c = false;
			// add to list to update
			listCB.add(cb);
		}
		System.debug('[FRJ] removeFlagFicticio -> listCB :'+ listCB);
		update listCB;

	}

	/**
	* @Method: 		validatePSWithCBCurrent
	* @param:
	* @Description:	Verifica que los PS no tengan Condiciones de Cobertura activas y vigentes asociadas.
	* @author 		Francisco Rojas - 20112017
	*/
	public static void validatePSWithCBCurrent (List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpp){
		Set<Id> setIdOpp = new Set<Id>();
		Set<Id> setIdPS = new Set<Id>();
		Set<Id> setIdNewCB = new Set<Id>();
		Set<Id> setIdAcc = new Set<Id>();
		Map<Id, Date> mapIdOppInitDate = new Map<Id, Date>();
		Map<Id, List<Id>> mapIdOldCBbyPS = new Map<Id, List<Id>>();
		List<Condiciones_de_Cobertura__c> listCB = new List<Condiciones_de_Cobertura__c>();
		List<GNF_Condiciones_Cobertura_por_Contrato__c> listJunction = new List<GNF_Condiciones_Cobertura_por_Contrato__c>();
		Id strIdRecordType = mapRecordTypeIdByDevName.get('SMC_Servicio_Multiclick_Opportunity').Id;
		for(Opportunity opp :lstOpportunities)
			if( Trigger.isUpdate && opp.StageName.equals( 'Aceptada') && opp.StageName != mapOldOpp.get(opp.Id).StageName && opp.RecordTypeId == strIdRecordType && opp.Subtipo__c.equals('Alt') ) {
				setIdOpp.add(opp.Id);
				setIdAcc.add(opp.AccountId);
				mapIdOppInitDate.put(opp.Id, opp.Fecha_Inicio__c);
			}
		System.debug('[FRJ] validatePSWithCBCurrent --> setIdOpp : ' + setIdOpp);
		for (Oportunidad_Punto_de_Suministros__c ops : [SELECT Id, Contrato__c, Oportunidad__c FROM Oportunidad_Punto_de_Suministros__c WHERE Oportunidad__c IN: setIdOpp])
			setIdPS.add(ops.Contrato__c);

		System.debug('[FRJ] validatePSWithCBCurrent --> setIdPS : ' + setIdPS);
		/*
		for (Condiciones_de_Cobertura__c cb : [SELECT Id, SMC_Oportunidad, Estado__c FROM Condiciones_de_Cobertura__c WHERE SMC_Oportunidad__c IN: setIdOpp AND Estado__c =: System.Label.SMC_CBEstadoHistorico AND SMC_ReadOnly__c != false])
			setIdNewCB.add(cb.Id);

		System.debug('[FRJ] validatePSWithCBCurrent --> setIdNewCB : ' + setIdNewCB);
		*/
		listJunction = [SELECT Id, GNF_Condicion_Cobertura__c, GNF_Condicion_Cobertura__r.Fecha_de_inicio__c, GNF_Condicion_Cobertura__r.Fecha_de_fin__c, GNF_Condicion_Cobertura__r.Estado__c, GNF_Contrato__c FROM GNF_Condiciones_Cobertura_por_Contrato__c WHERE GNF_Contrato__c IN: setIdPS AND GNF_Contrato__r.Cliente__c IN: setIdAcc AND GNF_Condicion_Cobertura__r.Estado__c = 'Activo'];
		/*
		for (GNF_Condiciones_Cobertura_por_Contrato__c junction : listJunction)
			if(mapIdOldCBbyPS.containsKey(junction.GNF_Condicion_Cobertura__c))
				mapIdOldCBbyPS.get(junction.GNF_Condicion_Cobertura__c).add(junction.GNF_Contrato__c);
			else
				mapIdOldCBbyPS.put(junction.GNF_Condicion_Cobertura__c, new List<Id>{ junction.GNF_Contrato__c });

		System.debug('[FRJ] validatePSWithCBCurrent --> listJunction : ' + listJunction);
		*/
		for( Opportunity objOpportunity : lstOpportunities )
			for (GNF_Condiciones_Cobertura_por_Contrato__c junction : listJunction){
				/**/
				System.debug('[FRJ] validatePSWithCBCurrent --> mapIdOppInitDate.get(objOpportunity.Id) : ' + mapIdOppInitDate.get(objOpportunity.Id));
				System.debug('[FRJ] validatePSWithCBCurrent --> junction.GNF_Condicion_Cobertura__r.Fecha_de_inicio__c : ' + junction.GNF_Condicion_Cobertura__r.Fecha_de_inicio__c);
				System.debug('[FRJ] validatePSWithCBCurrent --> junction.GNF_Condicion_Cobertura__r.Fecha_de_fin__c : ' + junction.GNF_Condicion_Cobertura__r.Fecha_de_fin__c);

				if( junction.GNF_Condicion_Cobertura__r.Fecha_de_inicio__c >= mapIdOppInitDate.get(objOpportunity.Id) || ( junction.GNF_Condicion_Cobertura__r.Fecha_de_inicio__c <= mapIdOppInitDate.get(objOpportunity.Id) && junction.GNF_Condicion_Cobertura__r.Fecha_de_fin__c >= mapIdOppInitDate.get(objOpportunity.Id) ) )
					objOpportunity.StageName.addError(System.Label.SMC_ErrorPSWithCBCurrent);
			}
	}

	/**
	* @Method: 		validatePSWithCBCurrent
	* @param:
	* @Description:	Informa la fecha y hora en que se informan los flags de : GNF_PDFEnviarEmail__c y GNF_AnexoAdjuntado__c
	* @author 		Francisco Rojas - 26.03.2018
	*/
	public static void setStampTimeDocuments (List<Opportunity> lstOpportunities, Map<Id, Opportunity> mapOldOpp){
		for( Opportunity objOpportunity : lstOpportunities ){
			if( ( objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'Click_Opportunity' ).Id ) || objOpportunity.RecordTypeId.equals( mapRecordTypeIdByDevName.get( 'Unclick_Opportunity' ).Id ) ) && ( ( !mapOldOpp.get(objOpportunity.Id).GNF_PDFEnviarEmail__c && objOpportunity.GNF_PDFEnviarEmail__c ) || ( !mapOldOpp.get(objOpportunity.Id).GNF_AnexoAdjuntado__c && objOpportunity.GNF_AnexoAdjuntado__c ) ) ){
				
				Datetime dtNow													= System.now();
				
				if( !mapOldOpp.get( objOpportunity.Id ).GNF_PDFEnviarEmail__c && objOpportunity.GNF_PDFEnviarEmail__c ){
					objOpportunity.GNF_DocPreciosCreado__c						= Datetime.newInstance( dtNow.date(), Time.newInstance( dtNow.hour(), dtNow.minute(), 0, 0 ) );
				}
				
				if( !mapOldOpp.get(objOpportunity.Id).GNF_AnexoAdjuntado__c && objOpportunity.GNF_AnexoAdjuntado__c ){
					objOpportunity.GNF_DocAnexosCreado__c						= Datetime.newInstance( dtNow.date(), Time.newInstance( dtNow.hour(), dtNow.minute(), 0, 0 ) );
				}
			}
		}
	}
}