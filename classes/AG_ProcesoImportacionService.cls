global with sharing class AG_ProcesoImportacionService {

	/*
	* Da de alta un proceso de importación y retorna el Id del nuevo objeto para posteriores llamadas.
	* Por defecto, el Estado__c del proceso es 'En curso'.
	*/
	webservice static Id crearProceso(String descripcion) {
		Proceso_Importacion__c proc = new Proceso_Importacion__c(descripcion__c=descripcion);
		insert proc;
		return proc.id;
	}

	/*
	 * Recupera objeto Proceso_Improtación con todos sus campos a partir del Id obtenido en su creación
	*/
	webservice static Proceso_Importacion__c obtenerProcesoImportacion(Id idProceso) {

		List<Proceso_importacion__c> procs = [SELECT Id, descripcion__c, inicio__c, final__c, estado__c, Comentarios__c, Recalcular_Permisos_Cliente__c, Recalcular_Permisos_Objeto__c FROM Proceso_Importacion__c WHERE Id = :idProceso];
		if (!procs.isEmpty()) {
			return procs[0];
		} 
		return null;
	}

	/*
	* Se indica el inicio del proceso de importación.
	*
	* Se actualiza Estado__c y hora de Inicio__c.
	*/
	webservice static void iniciarProceso(Id idProceso) {
		Proceso_importacion__c proc = [SELECT Id, inicio__c, estado__c FROM Proceso_Importacion__c WHERE Id = :idProceso];
		if (proc!=null) {
			proc.estado__c = 'En curso';
			proc.inicio__c = System.Now();
			update proc;
		}
	}

	/*
	* Da por finalizado el proceso de importación.
	*
	* Se actualiza el Estado__c y hora de Final__c.
	* Además, el trigger asociado inicia el recalculo de los permisos, segun la configuración
	* indicada en propio proceso (Recalcular_Permisos_Objeto__c y Recalcular_Permisos_cliente__c).
	*/
	webservice static void finalizarProceso(Id idProceso, Boolean conExito) {
		Proceso_importacion__c proc = [SELECT Id, final__c, estado__c FROM Proceso_Importacion__c WHERE Id = :idProceso];
		if (proc!=null) {
			proc.final__c = System.now();
			if (conExito)
				proc.estado__c = 'Finalizado con éxito';
			else
				proc.estado__c = 'Finalizado con errores';
			update proc;
		}
	}

	/*
	* Concatena el nuevo comentario al final del campo Comentarios__c del Proceso_Importacion__c
	*/
	webservice static String anyadirComentarioProceso(Id idProceso, String nuevoComentario) {
		Proceso_importacion__c proc = [SELECT Id, Comentarios__c FROM Proceso_Importacion__c WHERE Id = :idProceso];
		if (proc!=null) {
			if (nuevoComentario!=null) {
				String comentarioCompleto= nn(proc.Comentarios__c);
				if (!String.isBlank(comentarioCompleto)) comentarioCompleto += '\n';
				comentarioCompleto +=  nuevoComentario;

				proc.Comentarios__c = comentarioCompleto;
				update proc;
			}
			return proc.Comentarios__c;
		}
		return null;
	}

	/* 
	* nn = Not Nulls
	* Avoid null values and replace with empty string '';
	*/
	private static String nn(String value) {
		if (value==null) return '';
		return value;
	}

}