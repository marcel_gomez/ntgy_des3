/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba encargada e crear los datos cargados en recursos estaticos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-09		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
public class SMC_TestData_cls {
	
	public static String strLoadUntil										= '';
	public static Opportunity objOpportunity								= new Opportunity();
	
	public static void createData(){
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName = 'DV_IBERIA_IBERIA'
																				AND IsActive = true
																				LIMIT 1
																			];
																			
		List<sObject> lstBorradoFicticios									= Test.loadData( SMC_BorradoFicticios__c.sobjectType, 'SMC_BorradoFicticios' );
		
		if( strLoadUntil.equals( 'SMC_BorradoFicticios__c' ) ){
			return;
		}

		List<sObject> lsvalorespredeterminados								= Test.loadData( GNF_ValoresPredeterminados__c.sobjectType, 'GNF_ValoresPredeterminados' );
		
		if( strLoadUntil.equals( 'GNF_ValoresPredeterminados__c' ) ){
			return;
		}
		
		List<sObject> lsasignacionaprobadores								= Test.loadData( GNF_AsignacionAprobadores__c.sObjectType, 'GNF_AsignacionAprobadores' );
		
		if( strLoadUntil.equals( 'GNF_AsignacionAprobadores__c' ) ){
			return;
		}
		
		List<sObject> lscondicionesaprobacion								= Test.loadData( GNF_CondicionesAprobacion__c.sObjectType, 'GNF_CondicionesAprobacion' );
		
		if( strLoadUntil.equals( 'GNF_CondicionesAprobacion__c' ) ){
			return;
		}
		
		List<sObject> lsallowedconceptotypes    							= Test.loadData( GNF_AllowedConceptoTypes__c.sobjectType, 'GNF_AllowedConceptoTypes' );
		
		if( strLoadUntil.equals( 'GNF_AllowedConceptoTypes__c' ) ){
			return;
		}
		
		List<sObject> lstAccounts											= Test.loadData( Account.sObjectType, 'GNF_Accounts' );

		for( SObject sObjAccount : lstAccounts ){
			sObjAccount.put( 'OwnerId', objUser.Id );
		}
		
		update lstAccounts;

		if( strLoadUntil.equals( 'Account' ) ){
			return;
		}

		List<sObject> lsContacts 											= Test.loadData(Contact.sObjectType, 'GNF_Contacts');
		
		if( strLoadUntil.equals( 'Contact' ) ){
			return;
		}
		
		List<sObject> lssuministro 											= Test.loadData( Suministros__c.sObjectType, 'GNF_Suministro' );
		
		if( strLoadUntil.equals( 'Suministros__c' ) ){
			return;
		}
		
		List<sObject> lssectorsuministro 									= Test.loadData( SSs__c.sObjectType, 'GNF_SectorSuministro' );
		
		if( strLoadUntil.equals( 'SSs__c' ) ){
			return;
		}
		
		List<sObject> lscondicionescobertura								= Test.loadData( Condiciones_de_Cobertura__c.sObjectType, 'GNF_Condiciones_Cobertura' );
		
		if( strLoadUntil.equals( 'Condiciones_de_Cobertura__c' ) ){
			return;
		}
		
		List<sObject> lstContratos											= Test.loadData( Contrato__c.sObjectType, 'GNF_Contratos' );
		
		if( strLoadUntil.equals( 'Contrato__c' ) ){
			return;
		}
		
		List<sObject> lstConceptos											= Test.loadData( Concepto__c.sObjectType, 'GNF_Conceptos' );
		
		if( strLoadUntil.equals( 'Concepto__c' ) ){
			return;
		}
		
		List<sObject> lsopportunity											= Test.loadData( Opportunity.sObjectType, 'GNF_Opportunity' );
		
		if( strLoadUntil.equals( 'Opportunity' ) ){
			return;
		}
		
		List<sObject> lstCotizacionesPeriodo								= Test.loadData( Cotizacion_de_periodo__c.sObjectType, 'GNF_CotizacionesPeriodo' );
		
		if( strLoadUntil.equals( 'Cotizacion_de_periodo__c' ) ){
			return;
		}
		
		List<sObject> lstOportunidadesPuntoSuministro						= Test.loadData( Oportunidad_Punto_de_Suministros__c.sObjectType, 'GNF_OportunidadPuntodeSuministro' );
		
		if( strLoadUntil.equals( 'Oportunidad_Punto_de_Suministros__c' ) ){
			return;
		}
		
		List<sObject> lstConsumosClick										= Test.loadData( Consumo_Click__c.sObjectType, 'GNF_ConsumoClick' );
		
		if( strLoadUntil.equals( 'Consumo_Click__c' ) ){
			return;
		}
		
		List<sObject> lstCBxContrato										= Test.loadData( GNF_Condiciones_Cobertura_por_Contrato__c.sObjectType, 'GNF_Condiciones_Cobertura_por_Contrato' );
		
		if( strLoadUntil.equals( 'GNF_Condiciones_Cobertura_por_Contrato__c' ) ){
			return;
		}
		
		List<sObject> lstActuacionesContractuales							= Test.loadData( Actuacion_Contractual__c.sObjectType, 'SMC_ActuacionesContractuales' );
	}
	
	public static void createSMC(){
		GNF_OpportunityTriggerHandler_cls.setMapRecordTypeIdByDevName();
		
		objOpportunity														= [SELECT Id
																				FROM Opportunity
																				LIMIT 1
																			];
																				
		objOpportunity.RecordTypeId											= GNF_OpportunityTriggerHandler_cls.mapRecordTypeIdByDevName.get( 'SMC_Servicio_Multiclick_Opportunity' ).Id;
		update objOpportunity;
	}
	
	public static void createStandardCB(){
		List<Condiciones_de_Cobertura__c> lstCB								= new List<Condiciones_de_Cobertura__c>( [
																				SELECT Id,
																					Fecha_de_inicio__c,
																					Fecha_de_fin__c,
																					GNF_IsStandard__c
																				FROM Condiciones_de_Cobertura__c
																			] );
																			
		for( Condiciones_de_Cobertura__c objCB : lstCB ){
			objCB.Fecha_de_inicio__c										= null;
			objCB.Fecha_de_fin__c											= null;
			objCB.GNF_IsStandard__c											= true;
		}
		
		update lstCB;
	}
}