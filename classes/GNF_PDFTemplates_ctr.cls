/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase controladora para la generacion de plantillas.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-07-05		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
public class GNF_PDFTemplates_ctr {
	
	public Map<String, GNF_Plantilla__c> mapTemplateById				{get; set;}
	public Map<String, GNF_Seccion__c> mapSectionById					{get; set;}
	public Map<String, String> mapStylesByName							{get; set;}
	
	public GNF_Plantilla__c objTemplate;
	
	/**
	* @Method: 		GNF_PDFTemplates_ctr
	* @param: 		GNF_Plantilla__c objTemplate
	* @Description: Constructor de la clase controladora.
	* @author 		Manuel Medina - 05072017
	*/
	public GNF_PDFTemplates_ctr( GNF_Plantilla__c objTemplate ){
		this.objTemplate												= new GNF_Plantilla__c();
		this.objTemplate												= objTemplate;
		
		this.mapTemplateById											= new Map<String, GNF_Plantilla__c>();
		this.mapSectionById												= new Map<String, GNF_Seccion__c>();
		this.mapStylesByName											= new Map<String, String>();
	}
	
	/**
	* @Method: 		GNF_PDFTemplates_ctr
	* @param: 		ApexPages.StandardController sCtrTemplate
	* @Description: Constructor de la clase controladora para uso desde visualforce StandardController.
	* @author 		Manuel Medina - 05072017
	*/
	public GNF_PDFTemplates_ctr( ApexPages.StandardController sCtrTemplate ){
		this.objTemplate												= new GNF_Plantilla__c();
		this.objTemplate												= ( GNF_Plantilla__c ) sCtrTemplate.getRecord();
		
		this.mapTemplateById											= new Map<String, GNF_Plantilla__c>();
		this.mapSectionById												= new Map<String, GNF_Seccion__c>();
		this.mapStylesByName											= new Map<String, String>();
		
		getTemplateConfiguration();
		setMapStylesByName();
	}
	
	/**
	* @Method: 		getTemplateConfiguration
	* @param: 		N/A
	* @Description: Obtener la configuracion de la plantilla.
	* @author 		Manuel Medina - 05072017
	*/
	public void getTemplateConfiguration(){
		this.mapTemplateById											= new Map<String, GNF_Plantilla__c>( [
																			SELECT Id,
																				GNF_NombreUnico__c,
																				GNF_ObjetoPrincipal__c,
																				GNF_Imagen__c,
																				GNF_HTMLGenerado__c,
																				GNF_Version__c,
																				GNF_NombrePlantilla__c,
																				GNF_AdjuntarPDF__c,
																				GNF_EsIterable__c,
																				GNF_IterarObjeto__c,
																				GNF_Superior__c,
																				GNF_Inferior__c,
																				GNF_Izquierdo__c,
																				GNF_Derecho__c,
																				GNF_ZonaHoraria__c
																			FROM GNF_Plantilla__c
																			WHERE Id =: objTemplate.Id
																		] );
																		
		this.mapSectionById												= new Map<String, GNF_Seccion__c>( [
																			SELECT Id,
																				Name,
																				GNF_Orden__c,
																				GNF_Plantilla__c,
																				GNF_SeccionGenerada__c,
																				GNF_Titulo__c,
																				GNF_Columnas__c,
																				GNF_NuevaPagina__c,
																				GNF_Borde__c,
																				GNF_VisualizarTitulo__c,
																				GNF_AnchoCampoReferencia__c,
																				GNF_AnchoEtiqueta__c,
																				(
																					SELECT Id,
																						Name,
																						RecordType.DeveloperName,
																						GNF_Orden__c,
																						GNF_Seccion__c,
																						GNF_TextoFijo__c,
																						GNF_ObjetoReferencia__c,
																						GNF_CampoReferencia__c,
																						GNF_Etiqueta__c,
																						GNF_CabecerasTabla__c,
																						GNF_CamposReferencia__c,
																						GNF_CampoRelacionObjetoPrincipal__c,
																						GNF_TipoTabla__c,
																						GNF_CampoAgrupador__c,
																						GNF_ColumnasFijas__c,
																						GNF_Texto__c,
																						GNF_ParticionarTablaPor__c,
																						GNF_OrdenarPor__c,
																						GNF_Ordenar__c
																					FROM GNF_Componentes__r
																					ORDER BY GNF_Orden__c ASC
																				)
																			FROM GNF_Seccion__c
																			WHERE GNF_Plantilla__c =: objTemplate.Id
																			ORDER BY GNF_Orden__c ASC
																		] );
	}
	
	/**
	* @Method: 		setSectionRichHTML
	* @param: 		N/A
	* @Description: Definir el contenido HTML de la seccion.
	* @author 		Manuel Medina - 05072017
	*/
	public void setSectionRichHTML(){
		for( String strSectionId : mapSectionById.keySet() ){
			String strHTML												= '<table ' + ( mapSectionById.get( strSectionId ).GNF_Borde__c ? 'class="tableSection" ' : 'class="tableBlank" ' ) + '>' +
																			( mapSectionById.get( strSectionId ).GNF_VisualizarTitulo__c ? '<tr>' +
																				'<td class="sectionTitle" colspan="' + ( mapSectionById.get( strSectionId ).GNF_Columnas__c * 2 ) + '"><b>' + mapSectionById.get( strSectionId ).GNF_Titulo__c + '</b></td>' +
																			'</tr>' : '' ) + 
																			'<tr class="whiteRow"/>';
																			
			Map<Integer, List<String>> mapReferenceFieldsRows			= new Map<Integer, List<String>>();
			Map<Integer, List<String>> mapSignBlocks					= new Map<Integer, List<String>>();
			Map<Integer, List<String>> mapDescriptiveTexts				= new Map<Integer, List<String>>();
			
			String strPreviousComponent									= '';
			Integer intRefernceFieldSection								= 0;
			Integer intSignBlocks										= 0;
			Integer intDescriptiveTexts									= 0;
			
			for( GNF_Componente__c objComponent : mapSectionById.get( strSectionId ).GNF_Componentes__r ){
				if( objComponent.RecordType.DeveloperName.equals( 'GNF_Parrafo' ) ){
					strHTML												+= 	'<tr id="' + objComponent.Name.normalizeSpace() + '">' +
																				'<td class="textArea" colspan="' + ( mapSectionById.get( strSectionId ).GNF_Columnas__c * 2 ) + '">' + objComponent.GNF_TextoFijo__c.replace( '\n', '<br/>' ) + '</td>' +
																			'</tr>' + 
																			'<tr class="whiteRow"/>';
					
					strPreviousComponent								= 'GNF_Parrafo';
					
				}else if( objComponent.RecordType.DeveloperName.equals( 'GNF_CampoReferencia' ) ){
					if( strPreviousComponent.equals( 'GNF_Firma' ) || strPreviousComponent.equals( 'GNF_TextoDescriptivo' ) ){
						strHTML											+= 	'</tr>';
																			
					}else if( !strPreviousComponent.equals( 'GNF_CampoReferencia' ) ){
						intRefernceFieldSection++;
					}
					
					strHTML												+= 		!strHTML.contains( '$FIELDREF' + intRefernceFieldSection + '$' ) ? '$FIELDREF' + intRefernceFieldSection + '$' : '';
					
					mapReferenceFieldsRows								= setMapComponentBySectionIndex(
																			mapReferenceFieldsRows,
																			intRefernceFieldSection,
																				'<td id="' + objComponent.Name.normalizeSpace() + '" ' + mapStylesByName.get( '.fieldLabel' ).replace( '$WIDTH$', String.valueOf( ( mapSectionById.get( strSectionId ).GNF_AnchoEtiqueta__c / mapSectionById.get( strSectionId ).GNF_Columnas__c ) ) ) + '><b>' + objComponent.GNF_Etiqueta__c.trim() + ':</b></td>' +
																				'<td id="' + objComponent.Name.normalizeSpace() + '" ' + mapStylesByName.get( '.fieldValue' ).replace( '$WIDTH$', String.valueOf( ( mapSectionById.get( strSectionId ).GNF_AnchoCampoReferencia__c / mapSectionById.get( strSectionId ).GNF_Columnas__c ) ) ) + '>{!' + objComponent.GNF_ObjetoReferencia__c.trim() + '.' + objComponent.GNF_CampoReferencia__c.trim() + '}</td>'
																		);
					
					strPreviousComponent								= 'GNF_CampoReferencia';
					
				}else if( objComponent.RecordType.DeveloperName.equals( 'GNF_Tabla' ) ){
					strHTML												+= 	'<tr id="' + objComponent.Name.normalizeSpace() + '">' +
																				'<td colspan="' + ( mapSectionById.get( strSectionId ).GNF_Columnas__c * 2 ) + '">' + 
																					'{!' + objComponent.RecordType.DeveloperName + '.' + objComponent.Name + '}' +
																				'</td>' +
																			'</tr>' + 
																			'<tr class="whiteRow"/>';
																			
					strPreviousComponent								= 'GNF_Tabla';
																			
				}else if( objComponent.RecordType.DeveloperName.equals( 'GNF_Firma' ) ){
					if( strPreviousComponent.equals( 'GNF_CampoReferencia' ) || strPreviousComponent.equals( 'GNF_TextoDescriptivo' ) ){
						strHTML											+= 	'</tr>';
																			
					}else if( !strPreviousComponent.equals( 'GNF_Firma' ) ){
						intSignBlocks++;
					}
					
					strHTML												+= 		!strHTML.contains( '$SIGN' + intSignBlocks + '$' ) ? '$SIGN' + intSignBlocks + '$' : '';
					
					mapSignBlocks										= setMapComponentBySectionIndex(
																			mapSignBlocks,
																			intSignBlocks,
																				'<td id="' + objComponent.Name.normalizeSpace() + '" ' + mapStylesByName.get( '.tdSign' ).replace( '$PAD$', String.valueOf( ( ( ( 198 - ( mapTemplateById.get( objTemplate.Id ).GNF_Derecho__c + mapTemplateById.get( objTemplate.Id ).GNF_Izquierdo__c ) ) / mapSectionById.get( strSectionId ).GNF_Columnas__c ) - 50 ) / 2 ) ) + '><div class="divSign">' + objComponent.GNF_Etiqueta__c + '</div></td>'
																		);
					
					strPreviousComponent								= 'GNF_Firma';
					
				}else if( objComponent.RecordType.DeveloperName.equals( 'GNF_Titulo' ) ){
					strHTML												+= 	'<tr id="' + objComponent.Name.normalizeSpace() + '">' +
																				'<td class="titleCmp" colspan="' + ( mapSectionById.get( strSectionId ).GNF_Columnas__c * 2 ) + '">' + objComponent.GNF_TextoFijo__c + '</td>' +
																			'</tr>' + 
																			'<tr class="whiteRow"/>';
																			
					strPreviousComponent								= 'GNF_Titulo';
					
				}else if( objComponent.RecordType.DeveloperName.equals( 'GNF_TextoDescriptivo' ) ){
					if( strPreviousComponent.equals( 'GNF_CampoReferencia' ) || strPreviousComponent.equals( 'GNF_Firma' ) ){
						strHTML											+= 	'</tr>';
																			
					}else if( !strPreviousComponent.equals( 'GNF_TextoDescriptivo' ) ){
						intDescriptiveTexts++;
					}
					
					strHTML												+= 		!strHTML.contains( '$DESTEXT' + intDescriptiveTexts + '$' ) ? '$DESTEXT' + intDescriptiveTexts + '$' : '';
					
					mapDescriptiveTexts									= setMapComponentBySectionIndex(
																			mapDescriptiveTexts,
																			intDescriptiveTexts,
																				'<td id="' + objComponent.Name.normalizeSpace() + '" ' + mapStylesByName.get( '.descripText' ).replace( '$WIDTH$', String.valueOf( ( mapSectionById.get( strSectionId ).GNF_AnchoEtiqueta__c / mapSectionById.get( strSectionId ).GNF_Columnas__c ) ) ) + '>' + ( String.isNotBlank( objComponent.GNF_Texto__c ) ? objComponent.GNF_Texto__c.replace( '\n', '<br/>' ) : '' ) + '</td>' +
																				'<td id="' + objComponent.Name.normalizeSpace() + '" ' + mapStylesByName.get( '.fieldValue' ).replace( '$WIDTH$', String.valueOf( ( mapSectionById.get( strSectionId ).GNF_AnchoCampoReferencia__c / mapSectionById.get( strSectionId ).GNF_Columnas__c ) ) ) + '>' + ( String.isNotBlank( objComponent.GNF_TextoFijo__c ) ? objComponent.GNF_TextoFijo__c.replace( '\n', '<br/>' ) : '' ) + '</td>'
																		);
					
					strPreviousComponent								= 'GNF_TextoDescriptivo';
				}
			}
			
			strHTML														= replaceComponentsBySectionIndex( mapReferenceFieldsRows, strHTML, 'FIELDREF', strSectionId );
			strHTML														= replaceComponentsBySectionIndex( mapSignBlocks, strHTML, 'SIGN', strSectionId );
			strHTML														= replaceComponentsBySectionIndex( mapDescriptiveTexts, strHTML, 'DESTEXT', strSectionId );
			
			strHTML														+= '</table>';
			
			mapSectionById.get( strSectionId ).GNF_SeccionGenerada__c	= strHTML.replace( '<tr class="whiteRow"/></table>', '</table>' );
		}
		
		update mapSectionById.values();
		
		setTemplateRichHTML();
	}
	
	/**
	* @Method: 		setMapComponentsBySectionIndex
	* @param: 		Map<Integer, List<String>> mapComponentBySectionIndex
	* @param: 		Integer intCurrentIndex
	* @param: 		String strComponentHTML
	* @Description: Almacena los componentes por index de agrupamiento.
	* @author 		Manuel Medina - 18072017
	*/
	public Map<Integer, List<String>> setMapComponentBySectionIndex( Map<Integer, List<String>> mapComponentBySectionIndex, Integer intCurrentIndex, String strComponentHTML ){
		if( mapComponentBySectionIndex.containsKey( intCurrentIndex ) ){
			List<String> lstReferenceFieldsRows							= new List<String>( mapComponentBySectionIndex.get( intCurrentIndex ) );
			lstReferenceFieldsRows.add( strComponentHTML );
			
			mapComponentBySectionIndex.put( intCurrentIndex, lstReferenceFieldsRows );
			
		}else if( !mapComponentBySectionIndex.containsKey( intCurrentIndex ) ){
			mapComponentBySectionIndex.put( intCurrentIndex, new List<String>{ strComponentHTML } );
		}
		
		return mapComponentBySectionIndex;
	}
	
	/**
	* @Method: 		setMapComponentsBySectionIndex
	* @param: 		Map<Integer, List<String>> mapComponentBySectionIndex
	* @param: 		String strInitialHTML
	* @param: 		String strKeyToReplace
	* @param: 		String strSectionId
	* @Description: Reemplaza los componentes por index de ubicacion.
	* @author 		Manuel Medina - 18072017
	*/
	public String replaceComponentsBySectionIndex( Map<Integer, List<String>> mapComponentBySectionIndex, String strInitialHTML, String strKeyToReplace, String strSectionId ){
		for( Integer intSubSection : mapComponentBySectionIndex.keySet() ){
			strInitialHTML												= strInitialHTML.replace( '$' + strKeyToReplace + intSubSection + '$', setComponentsInColumns( strSectionId, mapComponentBySectionIndex.get( intSubSection ) ) );
		}
		
		return strInitialHTML;
	}
	
	/**
	* @Method: 		setComponentsInColumns
	* @param: 		String strSectionId
	* @param: 		List<String> lstTDComponents
	* @Description: Distribuye una lista de componentes basado en la cantidad de columnas de la seccion.
	* @author 		Manuel Medina - 10072017
	*/
	public String setComponentsInColumns( String strSectionId, List<String> lstTDComponents ){
		String strComponentsHTML										= '';
		Integer i														= 1;
		Integer j														= 0;

		while( i <= Integer.valueOf( Decimal.valueOf( lstTDComponents.size() ).divide( mapSectionById.get( strSectionId ).GNF_Columnas__c, 0, System.RoundingMode.CEILING ) ) ){
			strComponentsHTML											+= 	'<tr>';
			
			while( j < lstTDComponents.size() && j < ( i * Integer.valueOf( mapSectionById.get( strSectionId ).GNF_Columnas__c ) ) ){
				strComponentsHTML										+= 		lstTDComponents.get( j );
				
				j++;
			}
			
			strComponentsHTML											+= 	'</tr>' + 
																			'<tr class="whiteRow"/>';
			
			i++;
		}
		
		return strComponentsHTML;
	}
	
	/**
	* @Method: 		setSectionRichHTML
	* @param: 		N/A
	* @Description: Definir el contenido HTML de la seccion.
	* @author 		Manuel Medina - 05072017
	*/
	public void setTemplateRichHTML(){
		String strHTMLSectionsId										= '';
		String strHTML													= '<body>';
		
		strHTML															+=	'<div class="footer">' +
																				'<table style$$="width: ' + ( 210.058 - ( mapTemplateById.get( objTemplate.Id ).GNF_Derecho__c + mapTemplateById.get( objTemplate.Id ).GNF_Izquierdo__c ) ) + 'mm; border-collapse: collapse; font-size: 5pt;">' +
																					'<tr>' +
																						'<td>' + mapTemplateById.get( objTemplate.Id ).GNF_NombrePlantilla__c + ' - @VERSION@</td>' +
																					'</tr>' +
																				'</table>' +
																			'</div>';
																			
		strHTML															+=	String.isNotBlank( mapTemplateById.get( objTemplate.Id ).GNF_Imagen__c ) ? 
																			'<div class="logoSection">' +
																				'<table class="tableLogo">' +
																					'<tr>' +
																						( Test.isRunningTest() ? '' : '<td><img src="/servlet/' + String.valueOf( mapTemplateById.get( objTemplate.Id ).GNF_Imagen__c ).substringBetween( 'src="', '"' ).replace( 'amp;', '' ).substringAfter( 'servlet/' ) + '"/></td>' ) +
																					'</tr>' +
																				'</table>' +
																			'</div>' +
																			'<div class="tablePpal">' :
																			'<div class="tablePpal">';
		
		for( String strSectionId : mapSectionById.keySet() ){
			strHTML														+= 		'<div id="' + mapSectionById.get( strSectionId ).Name + '"' + ( mapSectionById.get( strSectionId ).GNF_NuevaPagina__c ? ' style$$="page-break-before: always;">' : '>' ) +
																					mapSectionById.get( strSectionId ).GNF_SeccionGenerada__c +
																				'</div>';
																				
			strHTMLSectionsId											+= '#' + mapSectionById.get( strSectionId ).Name + ',';
		}
		
		strHTML															+=	'</div>';
		
		
		
		strHTML															+= '</body>';
		
		mapTemplateById.get( objTemplate.Id ).GNF_HTMLGenerado__c		= setHEADTag().replace( '$SECTIONS$', strHTMLSectionsId.substringBeforeLast( ',' ) ) + strHTML.replaceAll( '(<[^>]+) style=".*?"', '$1' ).replace( '$$', '' );
		
		update mapTemplateById.values();
	}
	
	/**
	* @Method: 		setMapStylesByName
	* @param: 		N/A
	* @Description: Cargar mapa de estilos pra la generacion de HTML.
	* @author 		Manuel Medina - 07072017
	*/
	public void setMapStylesByName(){
		Double dbWidth													= 210.058 - ( mapTemplateById.get( objTemplate.Id ).GNF_Derecho__c + mapTemplateById.get( objTemplate.Id ).GNF_Izquierdo__c );
		
		this.mapStylesByName											= new Map<String, String>{
																			'.tablePpal'		=> 'border-collapse: collapse;' +
																									'width: ' + dbWidth + 'mm;' +
																									'',
																									
																			'.tableSection'		=> 'border-color: #000000;' +
																									'border-style: solid;' +
																									'border-width: thin;' +
																									'width: ' + dbWidth + 'mm;' +
																									'margin-bottom: 12px;' +
																									'',
																									
																			'.tableBlank'		=> 'width: ' + dbWidth + 'mm;' +
																									'border-collapse: collapse;' +
																									'margin-bottom: 12px;' +
																									'',
																									
																			'.emptyRow'			=> 'display: block;' +
																									'height: 12px;' +
																									'',
																									
																			'.whiteColumn'		=> 'display: inline-block;' +
																									'width: 5%;' +
																									'',
																									
																			'.tableCmp'			=> 'border-color: #000000;' +
																									'border-style: solid;' +
																									'border-width: thin;' +
																									'font-size: 7.2pt;' +
																									'',
																									
																			'.tableHeader'		=> 'text-align: center;' +
																									'background-color: #E0E0E0;' +
																									'font-weight: bolder;' +
																									'overflow: hidden;' +
																									'text-overflow: visible;' +
																									'',
																									
																			'.tableValue'		=> 'text-align: left;' +
																									'overflow: hidden;' +
																									'text-overflow: visible;' +
																									'',
																										
																			'.fieldLabel'		=> 'style$$="' +
																										'width: $WIDTH$%;' +
																										'text-align: right;' +
																									'"',
																									
																			'.fieldValue'		=> 'style$$="' +
																										'width: $WIDTH$%;' +
																									'"',
																									
																			'.descripText'		=> 'style$$="' +
																										'width: $WIDTH$%;' +
																										'text-align: left;' +
																									'"',
																									
																			'.sectionTitle'		=> 'background-color: #747474;' +
																									'color: #ffffff;' +
																									'',
																									
																			'.whiteRow'			=> 'display: block;' +
																									'height: 12px;' +
																									'',
																									
																			'body'				=> 'font-family: sans-serif;' +
																									/*'font-size: 7pt;' +*/
																									'font-size: 8pt;' +
																									'width: ' + dbWidth + 'mm;' +
																									'margin: 0;' +
																									'',
																									
																			'@page'				=> 'size: 210.058mm 296.926mm;' +
																									'margin: ' + mapTemplateById.get( objTemplate.Id ).GNF_Superior__c + 'mm ' + mapTemplateById.get( objTemplate.Id ).GNF_Derecho__c + 'mm ' + mapTemplateById.get( objTemplate.Id ).GNF_Inferior__c + 'mm ' + mapTemplateById.get( objTemplate.Id ).GNF_Izquierdo__c + 'mm;' +
																									'@bottom-left{' +
																										'content: element( footer );' +
																									'}' +
																									'',
																									
																			'@media$print'		=> 'div $SECTIONS${' +
																										'page-break-inside: avoid;' +
																									'}' +
																									'',
																									
																			'.textArea'			=> 'text-align: justify;' +
																									'text-justify: inter-word;' +
																									'',
																									
																			'.divSign'			=> 'border-style: solid;' +
																									'width: 50mm;' +
																									'border-width: thin;' +
																									'padding-top: 70px;' +
																									'text-align: left;' +
																									'',
																									
																			'.tdSign'			=> 'style$$="' +
																										'padding-left: $PAD$mm;' +
																										'padding-right: $PAD$mm;' +
																									'"',
																									
																			'.titleCmp'			=> 'text-align: left;' +
																									'font-weight: bolder;' +
																									'',
																									
																			'.logoSection'		=> 'margin-bottom: 0%;' +
																									'margin-left: 0%;' +
																									'margin-top: 0%;' +
																									'margin-right: 0%;' +
																									'width: ' + dbWidth + 'mm;' +
																									'',
																									
																			'.tableLogo'		=> 'width: ' + dbWidth + 'mm;' +
																									'text-align: right;' +
																									'',
																									
																			'div.footer'		=> 'display: block;' +
																									'text-align: right;' +
																									'position: running( footer );' +
																									'',
																									
																			'.errorMsg'			=> 'background-color: #fcf8e3;' +
																									'border-color: #faebcc;' +
																									'border-style: solid;' +
																									'width: 196mm;' +
																									'border-width: thin;' +
																									'padding-top: 2mm;' +
																									'padding-bottom: 2mm;' +
																									'padding-left: 2mm;' +
																									'padding-right: 2mm;' +
																									'text-align: center;' +
																									'',
																									
																			'.errorDetail'		=> 'background-color: #f2dede;' +
																									'border-color: #ebccd1;' +
																									'border-style: solid;' +
																									'width: 196mm;' +
																									'border-width: thin;' +
																									'padding-top: 2mm;' +
																									'padding-bottom: 2mm;' +
																									'padding-left: 2mm;' +
																									'padding-right: 2mm;' +
																									'text-align: left;' +
																									''
																		};
	}
	
	/**
	* @Method: 		setHEADTag
	* @param: 		N/A
	* @Description: Define la cabecera de la plantilla incluyendo los estilos como clases.
	* @author 		Manuel Medina - 10072017
	*/
	public String setHEADTag(){
		String strHEAD													= '<head>' +
																			'<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />' +
																			'<style type="text/css">\n';
		
		for(String strStyleClass : mapStylesByName.keySet() ){
			strHEAD														+= 		!strStyleClass.equals( '.fieldLabel' ) && !strStyleClass.equals( '.fieldValue' ) && !strStyleClass.equals( '.descripText' ) && !strStyleClass.equals( '.tdSign' ) ? strStyleClass.replace( '$', ' ' ) + '{' +
																					mapStylesByName.get( strStyleClass ).replace( 'style=', '' ).replace( '"', '' ) +
																				'}' : '';
		}
		
		strHEAD															+= 	'\n</style>';
		strHEAD															+= '</head>';
		
		return strHEAD;
	}
}