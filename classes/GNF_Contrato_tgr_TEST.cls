/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_Contrato_tgr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-07		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
private class GNF_Contrato_tgr_TEST {
	
	static testMethod void scenarioOne(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Contrato__c objContrato												= new Contrato__c();
		objContrato															= [SELECT Id,
																					Cliente__c,
																					NISS__c
																				FROM Contrato__c
																				LIMIT 1
																			];
																			
		Account objAccount													= new Account();
		objAccount															= [SELECT Id
																				FROM Account
																				LIMIT 1
																			];
																			
		objAccount.Ficticio__c												= false;
		update objAccount;
		
		objContrato.Cliente__c												= objAccount.Id;
		
		Test.startTest();
		
			update objContrato;
		
		Test.stopTest();
	}
	
	static testMethod void scenarioTwo(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Contrato__c objContrato												= new Contrato__c();
		objContrato															= [SELECT Id,
																					Cliente__c,
																					NISS__c
																				FROM Contrato__c
																				LIMIT 1
																			];
																			
		SSs__c objSS														= new SSs__c();
		objSS																= [SELECT Id
																				FROM SSs__c
																				LIMIT 1
																			];
																			
		objSS.Ficticio__c													= false;
		update objSS;
		
		objContrato.NISS__c													= objSS.Id;
		update objContrato;
		
		SSs__c objSS2														= new SSs__c();
		objSS2																= [SELECT Id
																				FROM SSs__c
																				WHERE Id !=: objSS.Id 
																				LIMIT 1
																			];
																			
		objContrato.NISS__c													= objSS2.Id;
		
		Test.startTest();
		
			update objContrato;
		
		Test.stopTest();
	}
	
	static testMethod void scenarioThree(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		List<Account> lstAccountsToDelete									= new List<Account>();
		List<SSs__c> lstSupplySectorsToDelete								= new List<SSs__c>();
		
		Test.startTest();
		
			String strAccountsToDelete										= System.JSON.serialize( lstAccountsToDelete );
			String strSupplySectorsToDelete									= System.JSON.serialize( lstSupplySectorsToDelete );
			
			GNF_ContratoTriggerHandler_cls.deleteOldRecords( strAccountsToDelete, strSupplySectorsToDelete );
		
		Test.stopTest();
	}
}