/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase controladora generica para las visualforce que usan el componente GNF_ShareRecords_cmp.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-07-11		Manuel Medina (MM)		Definicion inicial de la clase.
* @version	2.0		2018-02-21		Manuel Medina (MM)		Se incluye logica para colaborar los registros de los objetos
*															hijo relacionados al objeto padre basado en la configuracion
*															persoanlizada de metadatos GNF_ShareObjects__mdt.
*******************************************************************************************************************************/
public without sharing class GNF_ShareRecords_ctr implements Queueable {
	
	public List<SObject> lstRecords												{get; set;}
	public List<Schema.FieldSetMember> lstFieldSetMembers						{get; set;}
	public List<String> lstSelected2Add											{get; set;}
	public List<String> lstSelected2Quit										{get; set;}
	public List<SelectOption> lstUserOrGroup									{get; set;}
	public List<SelectOption> lstUserOrGroupShared								{get; set;}
	public List<SelectOption> lstTypeList										{get{
																						lstTypeList		= new List<SelectOption>();
																						lstTypeList.add( new SelectOption( '--Seleccionar--', '--Seleccionar--') );
																						lstTypeList.add( new SelectOption( 'Usuarios', 'Usuarios') );
																						lstTypeList.add( new SelectOption( 'Grupos', 'Grupos') );
																						
																						return lstTypeList;
																					}
																					set;
																				}
																				
	public Map<String, SObject> mapRecordById									{get; set;}
	public Map<String, String> mapFieldLabelByFieldName							{get; set;}
	
	public Object objRecordList													{get; set;}
	
	public String strSObjectType												{get; set;}
	public String strSelectedType												{get; set;}
	
	public Boolean blnLoadFinish												{get; set;}
	public Boolean blnIsFirstExecution											{get; set;}
	
	/**
	* @Method: 		GNF_ShareRecords_ctr
	* @param: 		N/A
	* @Description:	Constructor de la clase controladora.
	* @author 		Manuel Medina - 01092017
	*/
	public GNF_ShareRecords_ctr(){
		this.blnLoadFinish														= true;
		this.lstRecords															= new List<SObject>();
		this.mapRecordById														= new Map<String, SObject>();
		
		this.lstUserOrGroup														= new List<SelectOption>();
		this.lstUserOrGroup.add( new SelectOption( '--Ninguno--', '--Ninguno--') );
		
		this.lstUserOrGroupShared												= new List<SelectOption>();
		this.lstUserOrGroupShared.add( new SelectOption( '--Ninguno--', '--Ninguno--') );
	}
	
	/**
	* @Method: 		GNF_ShareRecords_ctr
	* @param: 		N/A
	* @Description:	Constructor de la clase controladora para iniciar funcionalidad Queueable.
	* @author 		Manuel Medina - 21022018
	*/
	public GNF_ShareRecords_ctr( List<SObject> lstRecords ){
		this.lstRecords															= new List<SObject>();
		this.lstRecords															= lstRecords;
	}
	
	/**
	* @Method: 		executed
	* @param: 		N/A
	* @Description:	Ejecuta el procesamiento de una porcion de registros.
	* @author 		Manuel Medina - 21022018
	*/
	public void execute( QueueableContext context ){
		insert lstRecords;
	}
	
	/**
	* @Method: 		GNF_ShareRecords_ctr
	* @param: 		N/A
	* @Description:	Constructor de la clase controladora.
	* @author 		Manuel Medina - 01092017
	*/
	public void load(){
		try{
			this.lstRecords														= ( List<SObject> ) objRecordList;
			
			setFieldSet();
			getRecordsDetail();
			
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
		}
	}
	
	/**
	* @Method: 		GNF_ShareRecords_ctr
	* @param: 		N/A
	* @Description:	Constructor de la clase controladora.
	* @author 		Manuel Medina - 31082017
	*/
	public GNF_ShareRecords_ctr( ApexPages.StandardSetController sCtrRecords ){
		this.blnLoadFinish														= false;
		this.lstRecords															= new List<SObject>( !sCtrRecords.getSelected().isEmpty() ? sCtrRecords.getSelected() : sCtrRecords.getRecords() );
		this.mapRecordById														= new Map<String, SObject>();
		this.strSObjectType														= String.valueOf( lstRecords.get( 0 ).getSObjectType() );
	}
	
	/**
	* @Method: 		GNF_ShareRecords_ctr
	* @param: 		N/A
	* @Description:	Constructor de la clase controladora.
	* @author 		Manuel Medina - 31082017
	*/
	public PageReference loadComponent(){
		this.blnLoadFinish														= true;
		
		return null;
	}
	
	/**
	* @Method: 		changeType
	* @param: 		N/A
	* @Description:	Carga la lista de usuarios o grupos disponibles.
	* @author 		Manuel Medina - 01092017
	*/
	public PageReference changeType() {

		this.blnIsFirstExecution												= false;
		this.lstUserOrGroup														= new List<SelectOption>();
		List<String> lstValues													= new List<String>();
		
		if( strSelectedType.equals( 'Usuarios' ) ){
			for( User objUser : [SELECT Id, FirstName, LastName, UserRole.Name FROM User WHERE IsActive = true] ){
				//this.lstUserOrGroup.add( new SelectOption( objUser.Id, ( String.isBlank( objUser.FirstName ) ? objUser.LastName : objUser.FirstName + ' ' + objUser.LastName ) ) );
				
				lstValues.add( String.isBlank( objUser.FirstName ) ? objUser.LastName : objUser.FirstName + ' ' + objUser.LastName + '$$' + objUser.Id );
			}
			
		}else if( strSelectedType.equals( 'Grupos' ) ){
			for( Group objGroup : [SELECT Id, Name FROM Group WHERE Type = 'Regular'] ){
				//this.lstUserOrGroup.add( new SelectOption( objGroup.Id, objGroup.Name ) );
				
				lstValues.add( objGroup.Name + '$$' + objGroup.Id );
			}
		}
		
		lstValues.sort();
		
		for( String strValue : lstValues ){
			this.lstUserOrGroup.add( new SelectOption( strValue.substringAfterLast( '$$' ), strValue.substringBeforeLast( '$$' ) ) );
		}
		
		return null;
	}
	
	/**
	* @Method: 		add
	* @param: 		N/A
	* @Description:	Añande los valores seleccionados.
	* @author 		Manuel Medina - 01092017
	*/
	public PageReference add(){
		this.blnIsFirstExecution												= false;
		
		if( lstUserOrGroupShared != null && lstUserOrGroupShared.get( 0 ).getValue().contains( 'Ninguno' ) ){
			this.lstUserOrGroupShared											= new List<SelectOption>();
		}
		
		SelectOption option;
		
		if( lstSelected2Add != null ){
			for( String selectedOption : lstSelected2Add ){
				for( Integer i = 0; i < lstUserOrGroup.size(); i++ ){
					option														= lstUserOrGroup.get( i );
					
					if( selectedOption.equals( option.getValue() ) ){
						this.lstUserOrGroupShared.add( option );
						this.lstUserOrGroup.remove( i );
						
						break;
					}
				}
			}
		}
		
		if( lstUserOrGroup.isEmpty() ){
			this.lstUserOrGroup													= new List<SelectOption>();
			this.lstUserOrGroup.add( new SelectOption( '--Ninguno--', '--Ninguno--') );
		}
	
		return null;
	}
	
	/**
	* @Method: 		remove
	* @param: 		N/A
	* @Description:	Remueve los valores seleccionados.
	* @author 		Manuel Medina - 01092017
	*/
	public PageReference remove() {
		this.blnIsFirstExecution												= false;
		
		SelectOption option;
		
		if( lstSelected2Quit != null ){
			for( String selectedOption : lstSelected2Quit ){
				for( Integer i = 0; i < lstUserOrGroupShared.size(); i++ ){
					option														= lstUserOrGroupShared.get( i );
					
					if( selectedOption.equals( option.getValue() ) ){
						this.lstUserOrGroup.add( option );
						this.lstUserOrGroupShared.remove( i );
						
						break;
					}
				}
			}
		}
		
		if( lstUserOrGroupShared.isEmpty() ){
			this.lstUserOrGroupShared											= new List<SelectOption>();
			this.lstUserOrGroupShared.add( new SelectOption( '--Ninguno--', '--Ninguno--') );
		}
	
		return null;
	}
	
	/**
	* @Method: 		shareRecords
	* @param: 		N/A
	* @Description:	Colabora los registros con los usuarios o grupos seleccionados.
	* @author 		Manuel Medina - 31082017
	*/
	public void shareRecords(){
		try{
			Map<String, Integer> mapRecordsQuantityBySObjectName				= new Map<String, Integer>();
			Map<String, List<SObject>> mapRecordsToShareBySObjectName			= new Map<String, List<SObject>>();
			Map<String, List<SObject>> mapRecordsBySObjectName					= new Map<String, List<SObject>>( getChildRecords( mapRecordById.keySet() ) );
			mapRecordsBySObjectName.put( strSObjectType, lstRecords );
			
			for( String strSObjectName : mapRecordsBySObjectName.keySet() ){
				List<SObject> lstRecordsToShare									= new List<SObject>();
				
				for( SObject sObjRecord : mapRecordsBySObjectName.get( strSObjectName ) ){
					for( SelectOption soUser : lstUserOrGroupShared ){
						SObject sObjMainObject									= Schema.getGlobalDescribe().get( ( strSObjectName.right( 3 ).equals( '__c' ) ? strSObjectName.substringBefore( '__c' ) + '__Share' : strSObjectName + 'Share' ) ).newSObject();
						sObjMainObject.put( ( strSObjectName.right( 3 ).equals( '__c' ) ? 'AccessLevel' : strSObjectName + 'AccessLevel' ), 'Edit' );
						sObjMainObject.put( ( strSObjectName.right( 3 ).equals( '__c' ) ? 'ParentId' : strSObjectName + 'Id' ), sObjRecord.get( 'Id' ) );
						sObjMainObject.put( 'UserOrGroupId', soUser.getValue() );
						
						if( strSObjectName.equals( 'Account' ) ){
							sObjMainObject.put( 'OpportunityAccessLevel', 'Edit' );
						}
						
						lstRecordsToShare.add( sObjMainObject );
					}
				}
				
				mapRecordsBySObjectName.put( strSObjectName, lstRecordsToShare );
				mapRecordsQuantityBySObjectName.put( strSObjectName, lstRecordsToShare.size() );
			}
			
			processRecordsToShare( mapRecordsBySObjectName, mapRecordsQuantityBySObjectName );
			
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, 'Se han colaborado los registros seleccionados.' ) );
			
		}catch( Exception e ){
			System.debug( '\n\n\n\t<<<<<<<<< ExceptionType >>>>>>>>> \n\t\t@@--> getCause > ' + e.getCause() + '\n\t\t@@--> getLineNumber > ' + e.getLineNumber() + '\n\t\t@@--> getMessage > '+ e.getMessage() + '\n\t\t@@--> getStackTraceString > '+ e.getStackTraceString() + '\n\t\t@@--> getTypeName > ' + e.getTypeName() + '\n\n' );
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
		}
	}
	
	/**
	* @Method: 		processRecordsToShare
	* @param: 		Map<String, SObject> mapRecordsBySObjectName
	* @param: 		Map<String, Integer> mapRecordsQuantityBySObjectName
	* @Description: Obtiene todos los registros de los objetos hijo relacionados al objeto padre.
	* @author		Manuel Medina - 21022018
	*/
	public void processRecordsToShare( Map<String, List<SObject>> mapRecordsBySObjectName, Map<String, Integer> mapRecordsQuantityBySObjectName ){
		List<SObject> lstRecordsToShare											= new List<SObject>();
		Integer intRecordsSize													= 0;
		Integer intDMLLimit														= 10000;
		
		for( Integer intListSize : mapRecordsQuantityBySObjectName.values() ){
			intRecordsSize														+= intListSize;
		}
		
		for( String strSObjectName : mapRecordsBySObjectName.keySet() ){
			lstRecordsToShare.addAll( mapRecordsBySObjectName.get( strSObjectName ) );
		}
		
		if( intRecordsSize <= intDMLLimit ){
			insert lstRecordsToShare;
			
		}else if( intRecordsSize > intDMLLimit ){
			Integer k															= 0;
			
			for( Integer i=0; i<lstRecordsToShare.size(); i+=intDMLLimit ){
				List<SObject> lstRecordsToShareTMP								= new List<SObject>();
				
				for( Integer j=k; j<i+intDMLLimit; j++ ){
					lstRecordsToShareTMP.add( lstRecordsToShare.get( j ) );
					
					k = j + 1;
				}
				
				Id idJob														= System.enqueueJob( new GNF_ShareRecords_ctr( lstRecordsToShareTMP ) );
			}
		}
	}
	
	/**
	* @Method: 		getChildRecords
	* @param: 		Set<String> setParentRecordIds
	* @Description: Obtiene todos los registros de los objetos hijo relacionados al objeto padre.
	* @author		Manuel Medina - 21022018
	*/
	public Map<String, List<SObject>> getChildRecords( Set<String> setParentRecordIds ){
		Map<String, List<SObject>> mapRecordsBySObjectName						= new Map<String, List<SObject>>();
		GNF_ShareObjects__mdt mdtShareObject									= new GNF_ShareObjects__mdt();
		mdtShareObject															= [SELECT Id,
																						GNF_ParentObject__c,
																						GNF_ChildObjects__c
																					FROM GNF_ShareObjects__mdt
																					WHERE GNF_ParentObject__c =: strSObjectType
																					LIMIT 1
																				];
		
		if( String.isNotEmpty( mdtShareObject.GNF_ChildObjects__c ) ){
			for( String strChildObject : mdtShareObject.GNF_ChildObjects__c.split( ',' ) ){
				String strParentReferenceField										= getReferenceField( strChildObject );
				
				if( String.isNotBlank( strParentReferenceField ) ){
					mapRecordsBySObjectName.put( strChildObject, Database.query( 'SELECT Id FROM ' + strChildObject + ' WHERE ' + strParentReferenceField + ' =: setParentRecordIds' ) );
				}
			}
		}
		
		return mapRecordsBySObjectName;
	}
	
	/**
	* @Method: 		getReferenceField
	* @param: 		String strChildObject
	* @Description: Obtener el campo referencia basado en el objeto padre.
	* @author		Manuel Medina - 23052017
	*/
	public String getReferenceField( String strChildObject ){
		Map<String, Schema.SObjectType> mapSFDCObjects							= Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult sObjResult									= mapSFDCObjects.get( strChildObject ).getDescribe();
						 
		Map<String, Schema.SObjectField> mapFields								= sObjResult.fields.getMap();
		
		for( Schema.SObjectField field : mapFields.values() ){
			Schema.DescribeFieldResult fieldResult								= field.getDescribe();
			List<Schema.sObjectType> lstSOjectRelated							= fieldResult.getReferenceTo();
			
			if( !fieldResult.isNamePointing() && !lstSOjectRelated.isEmpty() && String.valueOf( lstSOjectRelated.get( 0 ) ).equals( strSObjectType ) ){
				return String.valueOf( fieldResult.getName() );
			}
		}
		
		return null;
	}
	
	/**
	* @Method: 		getRecordsDetail
	* @param: 		N/A
	* @Description:	Obtiene informacion adicional de los registros seleccionados.
	* @author 		Manuel Medina - 31082017
	*/
	public void getRecordsDetail(){
		Set<String> setRecordIds												= new Set<String>();
		
		for( SObject sObjRecord : lstRecords ){
			this.mapRecordById.put( String.valueOf( sObjRecord.get( 'Id' ) ), Schema.getGlobalDescribe().get( strSObjectType ).newSObject() );
			setRecordIds.add( String.valueOf( sObjRecord.get( 'Id' ) ) );
		}
		
		this.mapRecordById														= new Map<String, SObject>( 
																					Database.query(
																						getSOQL(
																							strSObjectType,
																							'WHERE Id IN: setRecordIds \n'
																						)
																					)
																				);
	}
	
	/**
	* @Method: 		setFieldSet
	* @param: 		N/A
	* @Description:	Obtener el conjunto de campos GNF_CamposVFCompartir.
	* @author 		Manuel Medina - 31082017
	*/
	public void setFieldSet(){
		this.mapFieldLabelByFieldName											= new Map<String, String>();
		Map<String, Schema.SObjectType> mapSFDCObjects							= Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult sObjResult									= mapSFDCObjects.get( strSObjectType ).getDescribe();
		this.lstFieldSetMembers													= sObjResult.fieldSets.getMap().get( 'GNF_CamposVFCompartir' ).getFields();
		
		for( FieldSetMember objFieldSetMember : lstFieldSetMembers ){
			this.mapFieldLabelByFieldName.put( objFieldSetMember.getFieldPath(), objFieldSetMember.getLabel() );
		}
	}
	
	/**
	* @Method: 		getFieldSet
	* @param: 		N/A
	* @Description:	Obtener el conjunto de campos GNF_CamposVFCompartir.
	* @author 		Manuel Medina - 31082017
	*/
	public List<Schema.FieldSetMember> getFieldSet(){
		return lstFieldSetMembers;
	}
	
	/**
	* @Method: 		getSOQL
	* @param: 		String strSObject
	* @param: 		String strSOQLWHERE
	* @Description:	Obtener SOQL con todos los campos disponibles en el field set GNF_CamposVFCompartir
	*				defindo para el objeto indicado en strSObjectType.
	* @author 		Manuel Medina - 08062017
	*/
	public String getSOQL( String strSObject, String strSOQLWHERE ){
		String strSOQL																		= 'SELECT \n';
		for( String strFieldName : mapFieldLabelByFieldName.keySet() ){
			strSOQL																			+= '\t' + strFieldName + ', \n';
		}
		
		strSOQL																				= strSOQL.substringBeforeLast( ',' ) + ' \n';
		strSOQL																				+= 'FROM ' + strSObject + ' \n' + strSOQLWHERE;

		return strSOQL;
	}
}