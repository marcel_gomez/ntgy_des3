global with sharing class AG_Batch_RecalculateAccountShare implements Database.Batchable<sObject>, Database.Stateful {

	public  static final List<String> BUSINESS_OBJECTS = 
		new List<String> {'Suministros__c', 'Actividad_Comercial__c','Ofertas__c', 'SRs__c','SSs__c'};
    private final Integer tableIndex;
	
	//Lista de elemento viene ordenados por Account y Owner del objeto.
	//De esta forma, si vienen varios iguales (Account y owner) solo hace falta tratar el primero.
	//String query ='SELECT Id, (Select OwnerId FROM Suministros__r ORDER BY OwnerId) FROM Account' ;
	public final String QUERY_TEMPLATE = 
		' SELECT Id, OwnerId, Cliente__c, Gestor_Cliente__c, Cliente__r.recalcular_permisos__c ' + 
		' FROM #TABLE# #RECALCULATE_ALL# ORDER BY Cliente__c, OwnerId';

	private final String RECALCULATE_ALL_CONDITION ='WHERE Cliente__r.recalcular_permisos__c = true';

	private String query = null;

	//Dejamos calculado previamente, para cada usuario, a que grupos se tendría que dar permisos.
	private final Map<Id, Id[]> grpOrUsrIdToShare;
	
	//Para cada Account.Id, guardamos los owner a los que se ha dado permisos, para no repetir.
	private Map<Id,Set<Id>> processedAccounts = new Map<Id, Set<Id>>();

	//Chaining batch pattern to process all tables BUSINESS_OBJECTS
	private final Boolean chainProcess;
	//Process all Account's or just those with Account.recalculat_permisos__c = true
	private final Boolean recalculateAllAccounts;	

	/*
	* Constructor can parametros.
	* @parametros tableIndex In dice de la tabla a tratar
	* @processedAccounts Es un Map<Id,Set<Id>> indexado por Account.Id y con los Id de owners tratados.
	*/
	@testVisible
	private AG_Batch_RecalculateAccountShare (Integer tableIndex,  Map<Id,Set<Id>> processedAccounts, 
											Boolean recalculateAllAccounts, Boolean chainProcess) 
	{
		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('**** Constructor: start');
		System.debug('**** Parametros: tableIndex=' + tableIndex + 
					 ' recalculateAllAccounts=' + recalculateAllAccounts + 
					 ' chainProcess=' +chainProcess + 
					 ' #processedAccounts=' + processedAccounts.size());*/

		if (tableIndex>=BUSINESS_OBJECTS.size())
			throw new BatchProcessException('Table index out of bounds');

		grpOrUsrIdToShare = precalculateUSerOrGroupsToShare();

		this.processedAccounts = processedAccounts;
		System.debug('**** #Processed accounts:' + this.processedAccounts.size());

		this.chainProcess = chainProcess;
		this.recalculateAllAccounts = recalculateAllAccounts;
		this.tableIndex = tableIndex;

		query = QUERY_TEMPLATE.replace('#TABLE#', BUSINESS_OBJECTS.get(tableIndex));
		if (recalculateAllAccounts)
			query = query.replace('#RECALCULATE_ALL#', ' ');		//eliminamos filtro
		else
			query = query.replace('#RECALCULATE_ALL#', RECALCULATE_ALL_CONDITION);	//ponemos filtro: Account.recalcular_permisos__c =true

		System.debug('**** QUERY: ' + query);

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Constructor: end');
	}
	
	/*
	* Calculamos todos los grupos/role ascendentes a cada usuario, de forma que lo obtenemos rápido.
	*/	
	private Map<Id, Id[]> precalculateUserOrGroupsToShare() {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** precalculateUserOrGroupsToShare: start');

	    final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
	    final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
	    final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

		Map<Id, Id[]> mapMembersToShare = new Map<Id, Id[]>();

		//Calculamos los grupos/usuarios ascendentes en la jerarquia para todos los usuarios.
		for (Id  userId : allUsers.keySet()) {
 			Id[] membersToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(userId, allUsers, allRoles, allGroups);
			mapMembersToShare.put(userId, membersToShare);
		}

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** precalculateUserOrGroupsToShare: ' + mapMembersToShare.size());
		//System.debug('**** precalculateUserOrGroupsToShare: end');
		return mapMembersToShare;

	}

	/*
	 * Inicio del batch, ejecutamos query del objeto.
	*/	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Batch start: open query ');

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> Batch execute: start ');

		//List of AccountShare to upsert at the end of the process.
		List<AccountShare> accountSharesGlobal = new List<AccountShare>();

		//Recordamos Account/Owner que estamos tratando, para no dar permisos otra vez si ya lo hemos tratado (ORDER BY Cliente__c, OwnerId)
		Id currentAccount, currentOwner;

   		for (sObject obj : scope) {

   			Id accountId = (Id)obj.get('Cliente__c');
   			Id ownerId = (Id)obj.get('OwnerId');
   			Id ownerAccountId = (Id)obj.get('Gestor_Cliente__c');

   			/*if (accountId==null) {
				//System.debug('**** Object has no Account set (Cliente__c): Object Id=' + obj.Id);
   			} else if (ownerId==ownerAccountId) {
				//System.debug('**** Account and Suministros has the same Owner:' + ownerId);
			} else {*/
			if(accountId != null && ownerId != ownerAccountId){
	   			if (accountId!=currentAccount || ownerID!=currentOwner) {

	   				currentAccount = accountId;
	   				//Se comenta debug(Victor Velandia (VV))
					//System.debug('**** Current account:' + currentAccount);
	   				currentOwner = ownerId;
	   				//Se comenta debug(Victor Velandia (VV))
					//System.debug('**** Current owner:' + currentOwner);

	   				if (!processedAccounts.containsKey(accountId) || !processedAccounts.get(accountId).contains(ownerId)) {
	   					//Obtenemor lista precalculada de a quien hay que dar permisos (ascendentes) para ese ownerId
	   					List<Id> membersToShare = grpOrUsrIdToShare.get(OwnerId);
	   					//Crea lista de permisos
	   					List<AccountShare> accountShares = AG_OrganizationModelUtil.createAccountSharedRecord(accountId,membersToShare);
						//Se comenta debug(Victor Velandia (VV))
						//System.debug('**** Shares otorgados:' + accountShares);

						accountSharesGlobal.addAll(accountShares);

						//Actualizamops lista de owners tratados para el Account y así no repetirnos.
						updateProcessedAccounts(accountId, ownerId);
	   				/*} else {
	   					System.debug('**** Account/Owner ya tratado previamente (map)');*/
	   				}
	   			/*} else {
	   				System.debug('**** Mismo Account/Owner que elemento anterior tratado (order by)');*/
	   			}
	   		}
   		}
		
		upsert accountSharesGlobal;
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** BatchProcess.finish: Number of share upsert:' + accountSharesGlobal.size());
	}

	
	global void finish(Database.BatchableContext BC) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> AG_Batch_RecalculateAccountShare.finish');

		if (chainProcess) {
			Integer nextTableIndex = tableIndex + 1;	//Siguiente tabla
		
			if (nextTableIndex<BUSINESS_OBJECTS.size()) {
				//Se comenta debug(Victor Velandia (VV))
				//System.debug ('**** Siguiente tabla: NextTableIndex=' + nextTableIndex + ' - table Name=' + BUSINESS_OBJECTS.get(nextTableIndex));

				AG_Batch_RecalculateAccountShare batchProc = new AG_Batch_RecalculateAccountShare(nextTableIndex, processedAccounts, recalculateAllAccounts, chainProcess);
				Id batchId = Database.executeBatch(batchProc);
			} else {
				//Una vez finalizado el calculo de todos los procesos, se eliminan las marcas en Account
				if (!this.recalculateAllAccounts && this.chainProcess) {
					cleanRecalculateFieldOnAcount();
				}
			}
		}

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- AG_Batch_RecalculateAccountShare.finish');
	}
	
	public static Id executeFullProcess(Boolean recalculateAllAccounts) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Start batch process to recalculate Account Shares');

		AG_Batch_RecalculateAccountShare batchProcess = new AG_Batch_RecalculateAccountShare(0, new  Map<Id, Set<Id>>(), recalculateAllAccounts, true);
		Id batchId = Database.executeBatch(batchProcess);

		System.debug ('*** BatchProcess.finish: ends. Id=' + batchId);

		return batchId;
	}

	public static Id executeProcessOneTable(Integer tableIndex, Boolean recalculateAllAccounts) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Start batch process to recalculare Account Shares');

		AG_Batch_RecalculateAccountShare batchProcess = new AG_Batch_RecalculateAccountShare(tableIndex, new  Map<Id, Set<Id>>(), recalculateAllAccounts, false);
		Id batchId = Database.executeBatch(batchProcess);
		//Se comenta debug(Victor Velandia (VV))
		//System.debug ('*** Batch process id:' + batchId);
		
		return batchId;
	}

/////////////////////// PRIVATE METHODS ///////////////////////////

	/*
	* ACtualzamos el mapa processedAccounts con un nuevo (account,ownerId) tratado
	*/
	private void updateProcessedAccounts(Id accountId, Id ownerId) {

		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('**** updateProcessedAccounts: start');

		System.debug('**** updateProcessedAccounts: Account ->' + accountId);
		System.debug('**** updateProcessedAccounts: OwnerId ->' + ownerId);*/

		Set<Id> procOwners=processedAccounts.get(accountId);
		if (procOwners==null) 	//not found
			procOwners= new Set<Id>();

		procOwners.add(ownerId);
		processedAccounts.put(accountId,procOwners);

		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('**** updateProcessedAccounts: Owners ->' + procOwners);
		System.debug('**** updateProcessedAccounts: end');*/

	}

   //-- CUSTOM EXCEPTION
    public class BatchProcessException extends Exception {}

    /*
    * Se elimina la marca de Recalcular_Permisos__c de objetos Account.
    */
    private void cleanRecalculateFieldOnAcount() {
		//Vass 31052018 AppGestor 2018 - Implementación de esta clase batch y comentado del método original para setear el flag de las cuentasa true
		Id batchJobId = Database.executeBatch(new ApG_Batch_AccountUpdateShareFlag('Recalcular_Permisos__c', null, false), 200);
    	/*for (List<Account> bucket : [SELECT Id, Recalcular_Permisos__c FROM Account WHERE Recalcular_Permisos__c = true]) {
    		for (Account account : bucket) {
    			account.Recalcular_Permisos__c= false;
    		}
    		update bucket;
    	}*/
    }

}