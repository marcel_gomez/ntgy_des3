public class AG_TriggerHandlerSuministro {

    //LOGIC
    private final AG_LogicSuministro logic;

    //CONSTRUCTOR
    private AG_TriggerHandlerSuministro() {
        this.logic = new AG_LogicSuministro();
    }

    //SINGLETON PATTERN
    private static AG_TriggerHandlerSuministro instance;
    public static AG_TriggerHandlerSuministro getInstance() {
        if (instance == null) instance = new AG_TriggerHandlerSuministro();
        return instance;
    }    
    
    //SUMINISTRO HANDLER      
    public void onBeforeInsert(final List<Suministros__c> newList) {
        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<Suministros__c> newList, final Map<Id, Suministros__c> newMap,
                               final List<Suministros__c> oldList, final Map<Id, Suministros__c> oldMap) {
        //REVIEW: No need to re-calculate owner when the Gestor_External_Key__c has not change.
        this.logic.assignOwnerFromEmpleado(newList, oldMap);
    }

    public void onAfterInsert(final List<Suministros__c> newList, final Map<Id, Suministros__c> newMap){
       this.logic.setCustomSharing(newList,newMap,null,null);
       //this.logic.addAboveUsersToClientPlaceHolder(newList,newMap,null,null);
    }

    public void onAfterUpdate(final List<Suministros__c> newList, final Map<Id, Suministros__c> newMap,
                              final List<Suministros__c> oldList, final Map<Id, Suministros__c> oldMap){
       this.logic.setCustomSharing(newList,newMap,oldList,oldMap);
       //this.logic.addAboveUsersToClientPlaceHolder(newList,newMap,oldList,oldMap);
    }

    public void onAfterDelete(final List<Suministros__c> oldList, final Map<Id, Suministros__c> oldMap){
    }
}