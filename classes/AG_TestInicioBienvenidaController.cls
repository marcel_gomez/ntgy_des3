@isTest
public class AG_TestInicioBienvenidaController {
    public static testMethod void testController() {
        PageReference pageRef = Page.AG_InicioBienvenida;
        Test.setCurrentPage(pageRef);

        // Obtener un cliente cualquiera
        Account myAccount = new Account(Name='test'); 
        insert myAccount;
        Test.startTest();
        // Crear una instancia del controlador
        ApexPages.StandardController sc = new ApexPages.StandardController(myAccount);
        AG_InicioBienvenidaController controller = new AG_InicioBienvenidaController(sc);
        PageReference redirect = controller.redirect();
        Test.stopTest();
        System.assert(redirect.getUrl()=='/home/home.jsp');

    }
}