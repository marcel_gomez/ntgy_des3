/**
* VASS
* @author			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades requeridas para los consumos click.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-13		Manuel Medina (MM)		Definicion inicial del trigger.
*********************************************************************************************************/
public class GNF_ConsumoClickTriggerHandler_cls {

	/**
	* @Method:		createQuarterRecords
	* @param:		Map<Id, Consumo_Click__c> mapNewConsumosClickById
	* @Description:	Crea un registro de consumo trimestral por cada tres registros mensualizados de consumo click
					si la oportunidad es de tipo trimestral.
	* @author		Manuel Medina - 13092017
	*/
	public static void createQuarterRecords( Map<Id, Consumo_Click__c> mapNewConsumosClickById ){
		Map<Id, Consumo_Click__c> mapConsumosClickById						= new Map<Id, Consumo_Click__c>();
		List<GNF_ConsumoTrimestral__c> lstConsumosTrimestrales				= new List<GNF_ConsumoTrimestral__c>();

		for( Consumo_Click__c objConsumoClick : mapNewConsumosClickById.values() ){
			mapConsumosClickById.put( objConsumoClick.Id, new Consumo_Click__c() );
		}
		
		mapConsumosClickById												= new Map<Id, Consumo_Click__c>( [
																				SELECT Id,
																					GNF_ConsumoTotalizadoPeriodo_kWh__c,
																					GNF_Consumo_Totalizado_Periodo_Informado__c,
																					PFAnexo__c,
																					Q_comprometida_kWh__c,
																					Descuento__c,
																					GNF_posicionConsumoClick__c,
																					TV_Anexo__c,
																					Oportunidad_Punto_de_Suministro__r.GNF_VolumenTotalPS__c,
																					Oportunidad_Punto_de_Suministro__r.GNF_PuntoSuministro__c,
																					Oportunidad_Punto_de_Suministro__r.Oportunidad__c,
																					Oportunidad_Punto_de_Suministro__r.Oportunidad__r.ID_Opportunity__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.Tarifa_Acceso__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c,
																					Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Anual_Estimado__c,
																					Oportunidad_Punto_de_Suministro__r.Concepto__r.Maestro_Peaje__r.Name,
																					Oportunidad_Punto_de_Suministro__r.Concepto__r.Valor_de_F__c,
																					Oportunidad_Punto_de_Suministro__r.Concepto__r.GNF_ValorK__c,
																					Cotizacion_de_periodo__r.Name,
																					Cotizacion_de_periodo__r.Year__c,
																					Cotizacion_de_periodo__r.Periodo__c,
																					Cotizacion_de_periodo__r.Periodo_Formula__c,
																					Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c,
																					Cotizacion_de_periodo__r.Brent_de_referencia__c,
																					Cotizacion_de_periodo__r.GNF_TVAnexo__c,
																					Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c
																				FROM Consumo_Click__c
																				WHERE Id IN: mapConsumosClickById.keySet()
																				AND (
																					(
																						Oportunidad_Punto_de_Suministro__r.Oportunidad__r.Tipo_Producto__c IN ( 'S', 'C' )
																						AND GNF_posicionConsumoClick__c = 1
																					) OR (
																						Oportunidad_Punto_de_Suministro__r.Oportunidad__r.Tipo_Producto__c = 'Q'
																						AND Oportunidad_Punto_de_Suministro__r.Oportunidad__r.Periodicidad_ToP__c IN ( 'Trimestral', 'Mensual' )
																						AND GNF_posicionConsumoClick__c = 2
																					)
																				)
																			] );

		for( Consumo_Click__c objConsumoClick : mapConsumosClickById.values() ){
			GNF_ConsumoTrimestral__c objConsumoTrimestral					= new GNF_ConsumoTrimestral__c();
			objConsumoTrimestral.GNF_IdExterno__c							= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Oportunidad__r.ID_Opportunity__c + objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c + objConsumoClick.Cotizacion_de_periodo__r.Year__c + objConsumoClick.Cotizacion_de_periodo__r.Periodo_Formula__c;
			objConsumoTrimestral.GNF_Posicion__c							= Integer.valueOf( String.valueOf( objConsumoClick.Cotizacion_de_periodo__r.Periodo__c ) + String.valueOf( objConsumoClick.GNF_posicionConsumoClick__c ) );
			objConsumoTrimestral.GNF_ConsumoTotalizadoPeriodokWh__c			= objConsumoClick.GNF_Consumo_Totalizado_Periodo_Informado__c;
			objConsumoTrimestral.GNF_PFAnexo__c								= objConsumoClick.PFAnexo__c;
			objConsumoTrimestral.GNF_QComprometidakWh__c					= objConsumoClick.Q_comprometida_kWh__c;
			objConsumoTrimestral.GNF_Descuento__c							= objConsumoClick.Descuento__c;
			objConsumoTrimestral.GNF_TVAnexo__c								= objConsumoClick.TV_Anexo__c;
			objConsumoTrimestral.GNF_VolumenTotalPS__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.GNF_VolumenTotalPS__c;
			objConsumoTrimestral.GNF_PuntoSuministro__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.GNF_PuntoSuministro__c;
			objConsumoTrimestral.GNF_Opportunity__c							= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Oportunidad__c;
			objConsumoTrimestral.GNF_Denominacion__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.NISS__r.NIS__r.Denominacion__c;
			objConsumoTrimestral.GNF_CodigoContrato__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Codigo_Contrato__c;
			objConsumoTrimestral.GNF_IFGNL__c								= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.IF_GNL__c;
			objConsumoTrimestral.GNF_TarifaAcceso__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Tarifa_Acceso__c;
			objConsumoTrimestral.GNF_CUPS__c								= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.CUPS__c;
			objConsumoTrimestral.GNF_ConsumoAnualEstimadokWh__c				= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Contrato__r.Consumo_Anual_Estimado__c;
			objConsumoTrimestral.GNF_MaestroPeaje__c						= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Concepto__r.Maestro_Peaje__r.Name;
			objConsumoTrimestral.GNF_ValorF__c								= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Concepto__r.Valor_de_F__c;
			objConsumoTrimestral.GNF_ValorK__c								= objConsumoClick.Oportunidad_Punto_de_Suministro__r.Concepto__r.GNF_ValorK__c;
			objConsumoTrimestral.GNF_Year__c								= objConsumoClick.Cotizacion_de_periodo__r.Year__c;
			objConsumoTrimestral.GNF_Periodo__c								= objConsumoClick.Cotizacion_de_periodo__r.Name;
			objConsumoTrimestral.GNF_ParidadUSDEUR__c						= objConsumoClick.Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c;
			objConsumoTrimestral.GNF_BrentReferenciaBbl__c					= objConsumoClick.Cotizacion_de_periodo__r.Brent_de_referencia__c;
			objConsumoTrimestral.GNF_CVClientePrimaClick__c					= objConsumoClick.Cotizacion_de_periodo__r.GNF_CVClientePrima_Click__c;

			lstConsumosTrimestrales.add( objConsumoTrimestral );
		}

		upsert lstConsumosTrimestrales GNF_IdExterno__c;
	}
}