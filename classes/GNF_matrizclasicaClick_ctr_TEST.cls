/**
 * Created by luis.igualada on 04/09/2017.
 */
@istest
private class GNF_matrizclasicaClick_ctr_TEST {
	static testMethod void GNF_matrizclasicaClick(){
		
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		list<Opportunity> listaopps = [Select Id from Opportunity];
		
		for (Opportunity opp :listaopps){
			ApexPages.StandardController sctr = new ApexPages.StandardController(opp);
			GNF_matrizclasicaClick_ctr ctr = new GNF_matrizclasicaClick_ctr(sctr);
			ctr.getMatriz2();
		}
	}
}