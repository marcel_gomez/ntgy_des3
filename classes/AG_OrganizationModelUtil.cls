public with sharing class AG_OrganizationModelUtil {

    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';


  	private Map<String,User> allUSers;
  	private Map<String, Empleado__c> allEmpleados;

    private static final Map<String, Schema.SObjectType> TYPES_MAP = Schema.getGlobalDescribe();


   /**
    * Read all empleados in a map indexed by field 'ID_Empleado__c'
    */
    public static Map<String, Empleado__c> getAllEmpleados() {

        // Populate Auxiliar collections
        Map<String, Empleado__c> empleadosMap = new Map<String, Empleado__c>();
        for(Empleado__c theEmpleado : [SELECT Id, ID_Empleado__c, ID_Manager__c FROM Empleado__c]) {
          empleadosMap.put(theEmpleado.ID_Empleado__c,theEmpleado);
        }

        System.debug('@@@@ getAllEmpleados: Load Empleados in a Map<ID_Empleado__c,Empleado__c>: ' + empleadosMap.size());

        return empleadosMap;
    }

    /**
    * Using the map with all Empleados__c, creates a map with all the managers of each empleado.
    */
    public static Map<String,String> getManagersEmpleados(Map<String,Empleado__c> allEmpleados) {

      System.debug('@@@@ getManagersEmpleados: Start');

      Map<String,String> managersMap = new Map<String,String>();
      for (Empleado__c empleado: allEmpleados.values()) {

        String managers = getManagers(allEmpleados, empleado.ID_Empleado__c);
        System.debug('@@@@ Managers del empleado ' + empleado.ID_Empleado__c + ' =' + managers);

        managersMap.put(empleado.ID_Empleado__c, managers);
      }

      System.debug('@@@@ getManagersEmpleados: End');

      return managersMap;
    }

    /**
    * Funcion recursiva para construir String separado por comas con los ID_Empleado__c de sus managers.
    */
    public static String getManagers(Map<String,Empleado__c> allEmpleados, String idEmpleado) {

        Empleado__c manager = allEmpleados.get(idEmpleado);
        if (manager!=null && manager.ID_Manager__c!=null)
          return idEmpleado + ',' + getManagers(allEmpleados, manager.ID_Manager__c);
        else {
          return idEmpleado;
        }
    }

    /**
    * Read all users in a map indexed by field 'ID_Empleado'
    */
    public static Map<String, User> getAllUsers() {

        Map<String,User> userMap = new Map<String,User>();

        for(User theUser : [SELECT Id, ID_Empleado__c, isActive, name FROM User]) {
          User prevUser = userMap.put(theUser.ID_Empleado__c,theUser);
          system.debug('usuario que se va a poner en el mapa     '+ theUser.ID_Empleado__c +'   -->    '+ theUser);
          //TODO: What happend if some users has the same Empleado. 
          if (prevUser<>null) {
            System.debug('@@@@ Warning: There are some users with the same Empleado: User1: ' + prevUser.name + ' - User2:' + theUser.name + ' - Emplado:' + theUser.ID_Empleado__c);
          }
        }

        System.debug('@@@@ getAllUsers: Load User in a Map<ID_Empleado__c,User> : ' + userMap.size());

        return userMap;
    }

    /**
    * Read all users in a Map indexed by Id.
    */
    public static Map<Id, User> getAllUsersById() {

        Map<Id,User> userMap = new Map<Id,User> ([SELECT Id, FirstName,ID_Empleado__c,IsActive,LastName,Name,UserRoleId,UserType FROM User]);

        System.debug('@@@@ getAllUsersById: Load User in a Map<Id,User> : ' + userMap.size());

        return userMap;
    }

    /**
    * Read all roles in a Map indexed by Id.
    */
    public static Map<Id,UserRole> getAllRolesById () {

        Map<Id,UserRole> userRoles = new Map<Id,UserRole>([SELECT Id, DeveloperName,Name,ParentRoleId FROM UserRole]);

        System.debug('@@@ getUserRolesFromId:' + userRoles.size());

        return userRoles;
    }

 	  /**
     * Busca por la jerarquia de empleados hasta encontrar el que tiene un usuario activo
     */
    public static Id recursiveUserFind(final String gestorId, 
                                 final Map<String,Empleado__c> empleadoTable, 
                                 final Map<String,User> userTable) {

      System.debug('recursiveUserFind:' + gestorId + '- USer:' + userTable.containsKey(gestorId) );

      // Paso 1 - Si existe un user activo para ese gestor, success!
      if(userTable.containsKey(gestorId) && userTable.get(gestorId).isActive)
        return userTable.get(gestorId).Id;

      // Paso 2 - Como ese gestor no tiene usuario o su usuario no está activo, buscamos su Manager
      if(empleadoTable.containsKey(gestorId) && empleadoTable.get(gestorId).ID_Manager__c <> null) {

        //PROBLEM: If the Empleado hierachy has a cycle, it will be in a infinite loop and throw a Stack Overflow Exception
        //SOLUTION: Just remove from the Empleados list the items already visited.
        Map<String,Empleado__c> copyEmpleadoTable = empleadoTable.clone();
        Empleado__c empleadoGestor = copyEmpleadoTable.remove(gestorId);
        return recursiveUserFind(empleadoGestor.ID_Manager__c, empleadoTable, userTable);
      }
      // Paso 3 - Hemos iterado recursivamente hasta encontrar un gestor sin manager ni usuario activo, 
      // es decir cima del árbol sin cumplir condiciones
      return null;
    }
 
    /**
     * Obtenemos UserRole de un usuario.
     */
    public static UserRole getUserRole (Id theUserId) {

        List<UserRole> userRoles = [Select Id, name, PArentRoleId FROM UserRole where Id IN (SELECT UserRoleId FROM User WHERE Id = :theUserId)];
        UserRole ur=null;
        if (!userRoles.isEmpty())
          ur = userRoles.get(0);

        return ur;
    }

    /**
     * Obtenemos UserRole a partir de su Id.
     */
    public static UserRole getUserRoleFromId (Id userRoleId) {
        UserRole userRole = [Select Id, name, parentRoleId FROM UserRole where Id= :userRoleId];
        return userRole;
    }


    /**
     * Obtenemos grupo asociado a un role
     */
    public static Group getGroupFromUserRoleId(Id userRoleId) {

      //Get Common Group Id with UserRole Id
      Group  grp = [Select g.Id, name, developerName, type, relatedId From Group g where g.Type='Role' and g.RelatedId= :userRoleId];
      return grp;
    }
    
    /**
     * Obtenemos los roles ascendentes en la jerarquia de roles al pasado por parametro 
     * El propio role pasado por parametro, no se incluye en la lista de roles del resultado
     */  
    public static List<UserRole> getAncestorRoles(UserRole userRole) {
        List<userRole> ancestorRoles = new List<Userrole>();
        UserRole currentRole = userRole;
        while (currentRole!=null && currentRole.parentRoleId!=null) {
          currentRole = getUserRoleFromId(currentRole.parentRoleId);
          ancestorRoles.add(currentRole);
        }
        return ancestorRoles;
    }

    /**
     * Obtenemos los roles ascendentes en la jerarquia de roles al pasado por parametro 
     * El propio role pasado por parametro, no se incluye en la lista de roles del resultado
     */  
    public static List<UserRole> getAncestorRoles2(UserRole userRole, Map<Id,UserRole> rolesMap) {

      System.debug('@@@@ getAncestorRoles2:start');

        List<userRole> ancestorRoles = new List<Userrole>();
        UserRole currentRole = userRole;
        while (currentRole!=null && currentRole.parentRoleId!=null) {
          currentRole = rolesMap.get(currentRole.parentRoleId);
          if (currentRole!=null)
            ancestorRoles.add(currentRole);
        }

      System.debug('@@@@ getAncestorRoles2:end: ' + ancestorRoles);

        return ancestorRoles;
    }

    /**
     * Se obtiene Mapa con los grupos públicos asociados a cada tole, indexado por Role.Id.
     */
    public static Map<Id, Group> getRoleGroupsByRoleId() {

      Map<Id, Group> roleGroups = new Map<Id, Group>();
      for (Group g : [SELECT Id, name, developerName, Type, RelatedId FROM Group Where Type ='Role']) {
          roleGroups.put(g.relatedId,g);
      }

      System.debug('@@@@ getRoleGroupsByRoleId: #Public Groups that belongs to a Role: ' + roleGroups.size());

      return roleGroups;
    }

    /**
     * Obtenemso grupos asociados a lista de roles.
     */
    public static List<Group> getPublicGroupsOfRoleList(List<UserRole> userRoles) {

        Set<Id> userRoleIds = new Set<Id>();
        List<Group> groups = new List<Group>();
        for (UserRole ur : userRoles) {
          groups.add(getGroupFromUserRoleId(ur.Id));
        }
        return groups;
    }

    /**
     * Obtenemso grupos asociados a lista de roles, cuando disponemos de Map con grupos puclicos asociados a role, 
     * indexado por Role.id (obtenido por #getRoleGroupsByRoleId)
     */
    public static List<Group> getPublicGroupsOfRoleListEx(List<UserRole> userRoles, final Map<Id,Group> groupsByRoleId) {

        Set<Id> userRoleIds = new Set<Id>();
        List<Group> groups = new List<Group>();
        for (UserRole ur : userRoles) {
          groups.add(groupsByRoleId.get(ur.Id));
        }
        return groups;
    }

    /**
    * Calculate the id of the public groups linked with a role that are above the role of a
    * user specified by parameter.
    */
    public static List<Id> calculateMembersIdsToShare(Id baseUser) {

       //get the role of the user assigned to the record account
      System.debug('@@@@ Base user:' + baseUser);

      UserRole userRole = getUserRole(baseUser);
      System.debug('@@@@ User role :' + userRole);

      List<userRole> rolesAbove = getAncestorRoles(userRole);
      System.debug('@@@@ Above roles:' + rolesAbove);                

      List<Group> groupsAbove = getPublicGroupsOfRoleList(rolesAbove);
      System.debug('@@@@ Above groups :' + groupsAbove);   
      
      List<ID> ids = new List<ID>();
      ids.add(baseUser);    //add the base user id to the list
      for (Group gr : groupsAbove) 
        ids.add(gr.id);     //add the public groups related to roles above base user role

      return ids;
    }

    /**
     * Calculate the id of the public groups linked with a role that are above the role of a user specified by parameter.
     */
    public static List<Id> calculateMembersIdsToSharesEx(Id baseUserId, final Map<Id,User> users, final Map<Id,USerRole> roles, final Map<Id, Group> roleGroups) {

//      System.debug('@@@@calculateMembersIdsToSharesEx:start');

       //get the role of the user assigned to the record account
//      System.debug('@@@@ Base user:' + baseUserId);

      if (baseUserId==null) return new List<ID>();

      //add base user in result list
      List<ID> ids = new List<ID>();
      ids.add(baseUserId);    //add the base user id to the list

      //Gt user reference
      User baseUser = users.get(baseUserId);

      //get role of the user
      if (baseUser!=null && baseUSer.UserRoleId!=null) {
        UserRole userRole = roles.get(baseUser.UserRoleId);
//        System.debug('@@@@ User role :' + userRole);

        //get role above user's role
        List<userRole> rolesAbove = getAncestorRoles2(userRole, roles);
//        System.debug('@@@@ Above roles:' + rolesAbove);                

        //get role's groups
        List<Group> groupsAbove = getPublicGroupsOfRoleListEx(rolesAbove, roleGroups);
//        System.debug('@@@@ Above groups :' + groupsAbove);   
      
        //add the groups id's
        for (Group gr : groupsAbove) 
          ids.add(gr.id);     //add the public groups related to roles above base user role
      }

//      System.debug('@@@@calculateMembersIdsToSharesEx:ends');

      return ids;
    }

    /**
     * Crea  Account__Share para objeto y usuario/grupo especioficados.
     */
    public static AccountShare createAccountSharedRecord(Id accountId, Id userOrGroupId) {

          AccountShare accountSharing = new AccountShare(
	        AccountAccessLevel = SHARE_MODE_READ,
          	OpportunityAccessLevel = SHARE_MODE_READ,
          	AccountId = accountId,
          	UserOrGroupId = userOrGroupId
            );

          return accountSharing;
    }

    public static List<AccountShare> createAccountSharedRecord(Id accountId, Id[] userOrGroupId) {
      List<AccountShare> shareList = new List<AccountShare>();

      for (Id usrOrGrpId : userOrGroupId) {
          shareList.add(createAccountSharedRecord(AccountId,usrOrGrpId));
      }
      return shareList;
    }

    /**
     *  Borra todos los shared records asociados a un account.
     */
    public static void removeAccountSharedRecords(Id accountId) {
      //Standard objects can not define RowCause in Share records. It is always Manual.
      List<AccountShare> sharedRecords = [SELECT Id FROM AccountShare WHERE AccountId = :AccountId AND RowCause='Manual'];
      System.debug('**** removeAccountSharedRecords:' + sharedRecords.size());
      delete sharedRecords;
    }

    /**
     *  Borra todos los shared records asociados a un account y a un user or group determinado
     */
    public static void removeAccountSharedRecords(Id accountId, Id userOrGroup) {
      //Standard objects can not define RowCause in Share records. It is always Manual.
      List<AccountShare> sharedRecords = [SELECT Id FROM AccountShare WHERE AccountId = :AccountId AND RowCause='Manual' AND UserOrGroupId =:userOrGroup];
            System.debug('**** removeAccountSharedRecords:' + sharedRecords.size());
      delete sharedRecords;
    }

    /**
     * Elimina los shares de un objeto determinado. 
     * El objeto dejara de ser accesible para su Account.Owner.
     */
    public static Integer removeShares(Id objectId) {

        Schema.SObjectType token = objectId.getSObjectType();

        String RowCause = 'Apex_Sharing__c';  //Si el objeto es standard, tendría que se 'Manual'

        // Using the token, do a describe
        // and construct a query dynamically.
        Schema.DescribeSObjectResult dr = token.getDescribe();
        String className = dr.getName().toLowerCase();
        String shareClassName = className.replace('__c', '__share');

        List<SObject> sharesToDelete = Database.Query('SELECT Id FROM ' + shareClassName + ' WHERE RowCause= :RowCause AND ParentId = :objectId');
        Integer count = sharesToDelete.size();
        delete sharesToDelete;

        return count;
    }

    /**
     * Crea  Account__Share para objeto y usuario/grupo especioficados.
     */
    public static SObject createSharedRecord(Id objectId, Id userOrGroupId) {

      //get object type name from Id
      String className = getClassName(objectId);
      String shareClassName = className.replace('__c', '__share');

      SobjectType shareObjectType = TYPES_MAP.get(shareClassName);
      SObject shareObject =null;
      if (shareObjectType!=null) {
        shareObject = shareObjectType.newSObject();
        shareObject.put('ParentId',objectId);
        shareObject.put('UserOrGroupId', userOrGroupId);
        shareObject.put('AccessLevel','Read');
        shareObject.put('RowCause','Apex_Sharing__c');  //Schema.Suministros__Share.RowCause.Apex_Sharing__c);
      }
      return shareObject;
    }

    /**
     * Crea  Account__Share para objeto y usuario/grupo especioficados.
     */
    public static List<sObject> createSharedRecords(Id objectId, Id[] userOrGroupIds) {

      List<sObject> shareObjectList = new List<sObject>();
      for (Id memberId : userOrGroupIds) {
        SObject shareObject = createSharedRecord(objectId, memberId);
        shareObjectList.add(shareObject);
      }

      return shareObjectList;      
    }


    /**
     * Get the SobjectType name from the Id the object using Schema api.
     */
    @testVisible
    private static String getClassName(Id objectId) {

      //get object type name from Id
      Schema.SObjectType objectType = objectId.getSObjectType();
      Schema.DescribeSObjectResult dr = objectType.getDescribe();
      String className = dr.getName();
      return className;
    }

    /**
    * Marcamos el flag de recalcular perisos para los ID de accounts especificados por parametro.
    */
    public static void updateAccountsToRecalculate(List<Id> accountIds) {
      system.debug('lista de Ids recibidos  ' + accountIds);
      Id batchJobId = Database.executeBatch(new ApG_Batch_AccountUpdateShareFlag('Recalcular_permisos__c', accountIds, true), 200);
      /*for (List<Account> accounts : [SELECT Id, Recalcular_permisos__c FROM Account WHERE Id IN :accountIds]) {

        for(Account acc : accounts) 
          acc.recalcular_permisos__c = true;
        
        update accounts;
      }
*/
    }


}