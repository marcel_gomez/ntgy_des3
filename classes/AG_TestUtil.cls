@isTest
public class AG_TestUtil {
	
    public static List<Suministros__Share> getSuministrosShare(Id supplyId) {

        List<Suministros__Share> supplyShares = 
            [SELECT AccessLevel,Id,IsDeleted,ParentId,RowCause,UserOrGroupId FROM Suministros__Share WHERE parentId = :supplyId AND RowCause='Apex_Sharing__c'];
        return supplyShares;
    }

    public static Map<String,UserRole> getMockUserRoles() {

        //get mock roles
        List<UserRole> mockRoles= [select Id, name,ParentRoleId FROM UserRole WHERE name like 'Role_'];
        System.debug('MockRoles:' + mockRoles);
        Map<String,UserRole> mapMockRoles = new Map<String,USerRole>();
        for (UserRole r : mockRoles) 
            mapMockRoles.put(r.name,r);
        System.debug('Map MockRoles:' + mapMockRoles);

        return mapMockRoles;
    }

    public static Map<String,User> getMockUsers() {

        List<User> mockUsers = 
            [SELECT Id, username, firstName, lastName, profileid, UserRoleId,ID_Empleado__c FROM User WHERE username like 'test.user_@edelta.com'];
        System.debug('Mock users:' + mockUsers);
        Map<String,User> mapMockUSers = new Map<String,User>();
        for(User u: mockUSers) 
            mapMockUSers.put(u.lastName,u);
        System.debug('Map Mock users:' + mapMockUsers);

        return mapmockUSers;
    }

    public static Map<String,Group> getMockGroups() {
                //get mock groups
        List<Group> mockGroups = [select id, developerName FROM GROUP WHERE Type='Role' AND developerName like 'Role_%'];
        System.debug('Mock groups:' + mockGroups);
        Map<String,Group> mapMockGRoups = new Map<String,GRoup>();
        for (GRoup r : mockGRoups) 
            mapMockGroups.put(r.developerName,r);
        System.debug('Map Mock groups:' + mapMockGroups);

        return mapmockGroups;
    }

    public static Map<Id,Group> getMockGroupsById() {

        Map<Id,Group> mockGroups = new Map<Id,Group>([select id, developerName FROM GROUP WHERE Type='Role' AND developerName like 'Role_%']);
        return mockGroups;
    }


    public static Map<String,EMpleado__c> getMockEmpleados() {

        List<Empleado__c> mockEmpleados = [SELECT Id, name, ID_Empleado__c, ID_Manager__c, Activo__c FROM Empleado__c WHERE name like 'Empleado_%'];
        System.debug('Mock empleados:' + mockEmpleados);
        Map<String,Empleado__c> mapMockEmpleados = new Map<String,Empleado__c>();
        for(Empleado__c e : mockEmpleados) 
            mapMockEmpleados.put(e.name,e);
        System.debug('Map Mock empleados:' + mapMockEmpleados);

        return mapmockEmpleados;    
    }

    public static Map<String,Account> getMockAccounts() {

        List<Account> mockAccounts = [SELECT Id, description, name, Gestor__c, recalcular_permisos__c, recalcular_permisos_objetos__c FROM Account WHERE name LIKE 'Account_%'];
        System.debug('Mock accounts:' + mockAccounts);
        Map<String,Account> mapMockAccounts = new Map<String,Account>();
        for (Account a: mockAccounts) 
            mapMockAccounts.put(a.name,a);
        System.debug('Map Mock Accounts:' + mapMockAccounts);           

        return mapmockAccounts;

    }

    /*
     * Lista de AccountShare asociado a un Account.id
     */
    public static List<AccountShare> getAccountShares(Id accountId) {

        List<AccountShare> shares = [SELECT Id, AccountId, RowCause, UserOrGroupId
            FROM AccountShare WHERE accountId = :accountId AND RowCause = 'Manual'];

        return shares;
    }

    /*
     * Lista de Shares asociado a un Account.id indexado por Id de usuario.
     */
    public static Map<Id,AccountShare> getMapAccountShares(Id accountId) {

        Map<Id,AccountShare> mapShares = new Map<Id,AccountShare>();
        List<AccountShare> shares = getAccountShares(accountId);
        for (AccountShare share : shares) {
            mapShares.put(share.UserOrGroupId, share);
        }

        return mapShares;
    }

	
}