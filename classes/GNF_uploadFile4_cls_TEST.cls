@isTest
private class GNF_uploadFile4_cls_TEST {
	
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	
	static testMethod void GeneraCSV(){
		
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */

		test.startTest();
        
        List<Consumo_Click__c> ls 	= [Select Cotizacion_de_periodo__r.Opportunity__r.Id, 
                                       Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c,
                                       Cotizacion_de_periodo__r.Opportunity__r.StageName,
                                       Cotizacion_de_periodo__r.Opportunity__r.QA_Contratada__c,
                                       Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName,
                                       Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_precio__c,
                                       Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c,
                                       Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c,
                                       Oportunidad_Punto_de_Suministro__r.Concepto__r.Tipo_Tarifa__c
                                       FROM Consumo_Click__c WHERE
										Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c = false and
										Cotizacion_de_periodo__r.Opportunity__r.StageName ='En Elaboración' and
										Cotizacion_de_periodo__r.Opportunity__r.QA_Contratada__c > 0 and
										Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName='Click' and
										Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_precio__c not in ('Indicativo') and
										Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c not in ( 'SWAP Fórmula','SWAP Hub' ) and
										Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c not in ('Interna') and
										Oportunidad_Punto_de_Suministro__r.Concepto__r.Tipo_Tarifa__c != 'Cantidades comprometidas (POR TRAMOS)'
                                    ];
        String str = '';
        for(Consumo_Click__c obj : ls){
            str		+= obj.Cotizacion_de_periodo__r.Opportunity__r.Id + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.GNF_Enviado_Edelta__c + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.StageName + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.QA_Contratada__c + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.RecordType.DeveloperName + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_precio__c + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c + ' ' +
            			obj.Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_cobertura__c + ' ' +
            			obj.Oportunidad_Punto_de_Suministro__r.Concepto__r.Tipo_Tarifa__c + ' ' + '\n\n'
            		;
        }
        
        System.debug( '\n\n\n@@--> str > \n' + str + '\n\n');
		
		Test.setMock( HttpCalloutMock.class, new GNF_MockServiciosWeb_mws() );
		
		GNF_uploadFile4_sch m = new GNF_uploadFile4_sch();

		String strJobId	= System.schedule( 'GNF_uploadFile4_sch', CRON_EXP, m );
		
		Test.stopTest();
	}
}