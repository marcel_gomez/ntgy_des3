public without sharing class AG_FileViewerController {
    public List<ContentDocument> documents {get;set;}
    public AG_FileViewerController () {
        documents = [SELECT CreatedDate,FileExtension,FileType,Id,Title, Owner.Name, OwnerId FROM ContentDocument Where OwnerId = :UserInfo.getUserId() ORDER BY CreatedDate DESC];
    }

}