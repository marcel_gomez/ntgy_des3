/**
* VASS
* @author 			Juan Cardona juan.cardona@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase --
*					
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-19		Juan Cardona (JSC)		Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_calculoPeriodicidad_cls 
{
	 
	public static map<string, map<string, string>> map_periodos = new map<string, map<string, string>>{ 	'M'=>new map<string, string>{'01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12'}, 
																											'Q'=>new map<string, string>{'01'=>'03','04'=>'06','07'=>'09','10'=>'12'},
																											'S'=>new map<string, string>{'10'=>'03','04'=>'09'},
																											'C'=>new map<string, string>{'01'=>'12'}};	
	
	/**
	* @Method: 		mthd_generarPerdiodicidad
	* @param: 		--
	* @Description: Obtener --
	* @author 		Juan Cardona -
	*/	
	public static list<Cotizacion_de_periodo__c> mthd_generarPerdiodicidad( String str_idCobertura, list<String> lst_idConcepto, String str_idClick )
	{
		map<string, list<Long>>				map_inicioConpetosXcontrato 		= new map<string, list<Long>>();					
		map<string, list<Long>>				map_finalConpetosXcontrato			= new map<string, list<Long>>();
		
		list<Concepto__c> 					lst_concepto 						= new list<Concepto__c>(); 		
		list<Condiciones_de_Cobertura__c> 	lst_ccobertura 						= new list<Condiciones_de_Cobertura__c>();
		list<Opportunity> 					lst_opprtny							= new list<Opportunity>();
		list<Cotizacion_de_periodo__c>		lst_periodo							= new list<Cotizacion_de_periodo__c>();		

		list<Long>							lst_fechasInicio					= new list<Long>();                                             
		list<Long>							lst_fechasfinal						= new list<Long>();

		list<Long>							lst_fechasInicioConceptos			= new list<Long>();
		list<Long>							lst_fechasFinalConceptos			= new list<Long>();

		Cotizacion_de_periodo__c			obj_periodo;
		
		Decimal								dcm_valorK;
		
		Date								dt_fechaIncicial;
		Date								dt_fechaFinal;
		Date								dt_fecha;
		
		String								str_year;
		
		Integer								int_posicion = 0;
		Integer								int_periodo = 0;		 	
			 	
	 	system.debug('<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> Inicio str_idCobertura: ' + str_idCobertura + ' lst_idConcepto: ' + lst_idConcepto + ' str_idClick: ' + str_idClick);
				 	
	 	if(str_idCobertura==null || lst_idConcepto==null || str_idClick==null)
	 		return null;
	 	
	 	lst_concepto	= [	SELECT Id, GNF_ValorK__c, Fecha_Inicio__c, Fecha_Fin__c,Contrato__r.Id,Contrato__r.Fecha_de_Inicio__c,Contrato__r.Fecha_de_Fin__c  
	 						FROM Concepto__c 
	 						WHERE Id IN : lst_idConcepto 
	 								AND Fecha_Inicio__c!=NULL 
	 								AND Fecha_Fin__c!=NULL 
	 								AND Contrato__c!=NULL
	 								AND Contrato__r.Fecha_de_Inicio__c!=NULL 
	 								AND Contrato__r.Fecha_de_Fin__c!=NULL];
	 								
	 	lst_ccobertura 	= [	SELECT Id, Fecha_de_inicio__c, Fecha_de_fin__c, Antelacion_click_dias__c 
	 						FROM Condiciones_de_Cobertura__c 
	 						WHERE Id=:str_idCobertura
	 						AND Fecha_de_inicio__c!=NULL
	 						AND Fecha_de_fin__c!=NULL];
	 						
	 	lst_opprtny     = [	SELECT Id, Fecha_Inicio__c,Tipo_Producto__c 
	 						FROM Opportunity 
	 						WHERE Id=:str_idClick 
	 								AND Fecha_Inicio__c!=NULL
	 								AND Tipo_Producto__c!=NULL];	
	
		if(lst_concepto==null && lst_ccobertura==null && lst_opprtny==null)
			return null;
			
 		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> lst_concepto: ' + lst_concepto + ' lst_ccobertura: ' + lst_ccobertura + ' lst_opprtny: ' + lst_opprtny );			
		
		lst_fechasInicio.add( Datetime.newInstance( lst_opprtny[0].Fecha_Inicio__c.year(), lst_opprtny[0].Fecha_Inicio__c.month(), lst_opprtny[0].Fecha_Inicio__c.day()).getTime() );
		lst_fechasfinal.add( Datetime.newInstance( lst_opprtny[0].Fecha_Inicio__c.year(), lst_opprtny[0].Fecha_Inicio__c.month(), lst_opprtny[0].Fecha_Inicio__c.day()).addYears(2).getTime() );
			
		for(Concepto__c cnp : lst_concepto)
		{
 			system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> concepto: ' + cnp );					
			
			if( lst_concepto!=null && lst_concepto.size()>0 && dcm_valorK==null )
				dcm_valorK = lst_concepto[0].GNF_ValorK__c!=null ? lst_concepto[0].GNF_ValorK__c : null;	
			
			//lst_fechasInicioConceptos.add( Datetime.newInstance( cnp.Fecha_Inicio__c.year(), cnp.Fecha_Inicio__c.month(), cnp.Fecha_Inicio__c.day()).getTime()  );			
			//lst_fechasFinalConceptos.add( Datetime.newInstance( cnp.Fecha_Fin__c.year(), cnp.Fecha_Fin__c.month(), cnp.Fecha_Fin__c.day()).getTime() );
			
			if(  map_inicioConpetosXcontrato.containsKey( cnp.Contrato__c ) && map_finalConpetosXcontrato.containsKey( cnp.Contrato__c) )
			{
				map_inicioConpetosXcontrato.get( cnp.Contrato__c ).add( Datetime.newInstance( cnp.Fecha_Inicio__c.year(), cnp.Fecha_Inicio__c.month(), cnp.Fecha_Inicio__c.day()).getTime() );
				map_finalConpetosXcontrato.get( cnp.Contrato__c ).add( Datetime.newInstance( cnp.Fecha_Fin__c.year(), cnp.Fecha_Fin__c.month(), cnp.Fecha_Fin__c.day()).getTime() );
				
			}else{

				map_inicioConpetosXcontrato.put( cnp.Contrato__c, new list<Long>{ Datetime.newInstance( cnp.Fecha_Inicio__c.year(), cnp.Fecha_Inicio__c.month(), cnp.Fecha_Inicio__c.day()).getTime() } );
				map_finalConpetosXcontrato.put( cnp.Contrato__c, new list<Long>{ Datetime.newInstance( cnp.Fecha_Fin__c.year(), cnp.Fecha_Fin__c.month(), cnp.Fecha_Fin__c.day()).getTime() } );			
			}
			
			lst_fechasInicio.add( Datetime.newInstance( cnp.Contrato__r.Fecha_de_Inicio__c.year(), cnp.Contrato__r.Fecha_de_Inicio__c.month(), cnp.Contrato__r.Fecha_de_Inicio__c.day()).getTime() );		
			//lst_fechasInicio.add( Datetime.newInstance( cnp.Fecha_Inicio__c.year(), cnp.Fecha_Inicio__c.month(), cnp.Fecha_Inicio__c.day()).getTime() );			

			lst_fechasfinal.add( Datetime.newInstance( cnp.Contrato__r.Fecha_de_Fin__c.year(), cnp.Contrato__r.Fecha_de_Fin__c.month(), cnp.Contrato__r.Fecha_de_Fin__c.day()).getTime() );
			//lst_fechasfinal.add( Datetime.newInstance( cnp.Fecha_Fin__c.year(), cnp.Fecha_Fin__c.month(), cnp.Fecha_Fin__c.day()).getTime() );
		}
		
		//lst_fechasInicioConceptos.sort();
		//lst_fechasFinalConceptos.sort();
		
		for( String str_idContrato : map_inicioConpetosXcontrato.keySet() )
		{
			lst_fechasInicioConceptos.clear();
			lst_fechasFinalConceptos.clear();			

			lst_fechasInicioConceptos.addAll( map_inicioConpetosXcontrato.get( str_idContrato ) );
			lst_fechasFinalConceptos.addAll( map_finalConpetosXcontrato.get( str_idContrato ) );
			
			lst_fechasInicioConceptos.sort();
			lst_fechasFinalConceptos.sort();

			lst_fechasInicio.add( lst_fechasInicioConceptos.get(0) );
			lst_fechasfinal.add( lst_fechasFinalConceptos.get(lst_fechasFinalConceptos.size()-1) );	
		}
		
		//lst_fechasInicio.add( lst_fechasInicioConceptos.get(0) );
		//lst_fechasfinal.add( lst_fechasFinalConceptos.get(lst_fechasFinalConceptos.size()-1) );			

		for( Condiciones_de_Cobertura__c ccb : lst_ccobertura )
		{
 			system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> Condición Cobertura: ' + ccb );					
			
			lst_fechasInicio.add( Datetime.newInstance( ccb.Fecha_de_inicio__c.year(), ccb.Fecha_de_inicio__c.month(), ccb.Fecha_de_inicio__c.day()).getTime() );
			lst_fechasfinal.add( Datetime.newInstance( ccb.Fecha_de_fin__c.year(), ccb.Fecha_de_fin__c.month(), ccb.Fecha_de_fin__c.day()).getTime() );
		}
				
		lst_fechasInicio.sort();
		lst_fechasfinal.sort();
		
		dt_fechaIncicial 	= Datetime.newInstance( lst_fechasInicio.get(lst_fechasInicio.size()-1) ).date();
		dt_fechaFinal		= Datetime.newInstance( lst_fechasfinal.get(0) ).date();

		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fechaIncicial: ' + dt_fechaIncicial );	
		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fechaFinal: ' + dt_fechaFinal );		
		
		if( dt_fechaIncicial.monthsBetween( dt_fechaIncicial.addDays(1) ) == 1 )
			dt_fechaIncicial = dt_fechaIncicial.addDays(1);

		if( dt_fechaFinal.monthsBetween( dt_fechaFinal.addDays(-1) ) == -1 )
			dt_fechaFinal = dt_fechaFinal.addDays(-1);

			
		dt_fecha = dt_fechaIncicial;	
		
		if( lst_opprtny[0].Id.equals( '0063E0000048DG6' ) )
			dt_fechaFinal = dt_fechaFinal.addMonths( 12 );		
					
		
		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fechaIncicial 2: ' + dt_fechaIncicial );	
		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fechaFinal 2: ' + dt_fechaFinal );	

		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> map_periodos: ' + GNF_consolidarConsumosClick_cls.map_periodos.get(lst_opprtny[0].Tipo_Producto__c) );
		system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> monthsBetween: ' + dt_fechaIncicial.monthsBetween( dt_fechaFinal ) );	
		
		if( map_periodos.containsKey( lst_opprtny[0].Tipo_Producto__c ) &&
			GNF_consolidarConsumosClick_cls.map_periodos.containsKey( lst_opprtny[0].Tipo_Producto__c ) &&
			//( map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).containsKey( '0' + string.valueOf( dt_fechaIncicial.addDays(1).month() ) )  || map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).containsKey( string.valueOf( dt_fechaIncicial.addDays(1).month() ) ) ) &&
			//map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).containsKey( string.valueOf( dt_fechaFinal.addDays(-1).month() ) ) &&
			dt_fechaIncicial.monthsBetween( dt_fechaFinal ) >= ( GNF_consolidarConsumosClick_cls.map_periodos.get(lst_opprtny[0].Tipo_Producto__c) - 1 ) )
			
			while( dt_fecha < dt_fechaFinal && dt_fecha.monthsBetween( dt_fechaFinal.addDays(1) ) >= GNF_consolidarConsumosClick_cls.map_periodos.get(lst_opprtny[0].Tipo_Producto__c) )
			{
				system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fecha: ' + dt_fecha );					
				
				if( map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).containsKey( '0' + string.valueOf( dt_fecha.addDays(1).month() ) ) || map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).containsKey( string.valueOf( dt_fecha.addDays(1).month() ) ))
				{	
					int_periodo++;
					int_posicion = 0;
					
					for( string s : map_periodos.get( lst_opprtny[0].Tipo_Producto__c ).keyset() ){
						int_posicion++;	

						system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>>  string s: ' +  s );						
						system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fecha.addDays(1).month(): ' + dt_fecha.month() );						
						
						if( dt_fecha.month() ==  Integer.valueOf(s) )
							break;							 
					}						


					obj_periodo 							= new Cotizacion_de_periodo__c();
								
					obj_periodo.GNF_FechaInicioValidez__c	= dt_fecha;				
			
					dt_fecha								= dt_fecha.addMonths( GNF_consolidarConsumosClick_cls.map_periodos.get(lst_opprtny[0].Tipo_Producto__c) );
					str_year								= string.valueOf( dt_fecha.addDays(-1).year() );
				
					obj_periodo.periodo__c                  = int_periodo;
					obj_periodo.Opportunity__c				= lst_opprtny[0].Id;
					obj_periodo.Posicion__c					= int_posicion;

					obj_periodo.Year__c						= str_year;					
					obj_periodo.Fecha_limite_de_validez__c	= dt_fecha.addDays(-1);
					
					obj_periodo.GNF_ValorK__c 				= dcm_valorK;		
			
					if(  lst_opprtny[0].Tipo_Producto__c == 'M'  && GNF_consolidarConsumosClick_cls.map_mesesNombre.containsKey(int_posicion) )
						obj_periodo.Name	= '' + GNF_consolidarConsumosClick_cls.map_mesesNombre.get(int_posicion) + '-' + str_year.right(2);	
					else
						obj_periodo.Name	= '' + lst_opprtny[0].Tipo_Producto__c + int_posicion + '-' + str_year.right(2);
											
					system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> dt_fecha Nueva: ' + dt_fecha );							
 					system.debug( '<<GNF_calculoPeriodicidad_cls.mthd_generarPerdiodicidad>> obj_periodo: ' + obj_periodo );						
					
					lst_periodo.add(obj_periodo);
				}
				else
					dt_fecha = dt_fecha.addMonths( 1 );			
			}
				
		if( lst_periodo.size()>0 )
			insert lst_periodo;
			
		return lst_periodo;
	}//ends mthd_generarPerdiodicidad	
}//ends GNF_calculoPeriodicidad_cls