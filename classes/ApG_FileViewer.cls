/**
* VASS Latam
* @author            Victor Velandia victor.velandia@vass.es
* Project:           Gas Natural Fenosa - AppGestor
* Description:       Clase invocable para VisualForce ApG_FileViewer.page a partir de una ficha "Mis Archivos"
*
*            No.     Date            Author                  Description
*            -----   ----------      --------------------    ---------------
* @version   1.0     2018-05-29      Victor Velandia (VV)    Clase que muestras los registros asociados a cada usuario  
**************************************************************************************************************************************/

   global with sharing class ApG_FileViewer {

    //public ApG_FileViewer() { } // empty constructor
    
    @RemoteAction
    global static list<map<String, String>> getContentDocument() {

        list<ContentDocument> listContDoc = [SELECT Id, CreatedDate, FileExtension,  FileType, Title, OwnerId, Owner.Name, Description 
                                                FROM ContentDocument 
                                                where OwnerId =: UserInfo.getUserId() ORDER BY Title DESC];

        system.debug('Document for user' + listContDoc);
        
        String type='ContentDocument';

        Map<String, Schema.SObjectType>     schemaMap           =   Schema.getGlobalDescribe();
        Schema.SObjectType                  leadSchema          =   schemaMap.get(type);
        map<String, Schema.SObjectField>    mapfields           =   leadSchema.getDescribe().fields.getMap();
        map<String, String>                 mapFieldlabel       =   new map<string, string>();
        
        //Etiqueta Label para las columnas
        mapFieldlabel.put('Title', system.label.ApG_NombreArchivo);
        mapFieldlabel.put('CreatedDate', mapfields.get('CreatedDate').getDescribe().getLabel());
        mapFieldlabel.put('FileExtension', mapfields.get('FileExtension').getDescribe().getLabel());
        
        
        
        
        //fieldMap.get(fieldName).getDescribe().getLabel();//It provides to get the object fields label.
        
        list<map<String, String>> listMapRet           =   new list<map<String, String>>();
        listMapRet.add(mapFieldlabel); 

        for(ContentDocument objContentDocument :listContDoc){

            map<String, String> mapField            =   new map<string, string>();
            date fechaCreatedDate                   =   Date.valueOf(objContentDocument.CreatedDate);
            Time myTime                             =   Time.newInstance(0, 0, 0, 0);
            Datetime datetimes                      =   DateTime.newInstance(fechaCreatedDate, myTime); //DateTime.newInstance(fechaCreatedDate, objContentDocument.CreatedDate.time());
            String fechaCreated                     =   datetimes.format('dd/MM/yyyy'); //datetimes.format('dd/MM/yyyy HH:mm');

            //se llena mapa a mostrar los registros
            mapField.put('Title',objContentDocument.Title);
            mapField.put('CreatedDate',fechaCreated);
            string strFileExtension = '-';
            if(!string.isBlank(objContentDocument.FileExtension)){

                strFileExtension=objContentDocument.FileExtension;
            }
            mapField.put('FileExtension',strFileExtension);

            /*string strDescription = '-';
            if(!string.isBlank(objContentDocument.Description)){

                strDescription=objContentDocument.Description;
            }
            mapField.put('Description',strDescription);
           */
            
            mapField.put('Id',objContentDocument.id);
            /*mapField.put('createdDate',fechaCreated);
            mapField.put('createdDate',fechaCreated);*/
            listMapRet.add(mapField);
        }

        return listMapRet;

    }
}



    //public String idConten { get; set; }
    //public static ContentDocument contDoc { get; set; }
    //public List<ContentDocument> listContDoc {get; set;}
    //public static map<String, String> mapField      {get; set;}

    //public  List<ContentDocument> listContDoc {get; set;}
    
    //map<String, String> mapField = new map<String, String>();

    //@RemoteAction
    //global static ContentDocument  getContentDocPage(String mapField ){
    /*global static ApG_FileViewer(){

         listContDoc = [SELECT Id, CreatedDate, FileExtension,  FileType, Title, OwnerId, Owner.Name 
                            FROM ContentDocument 
                            where id =: idConten and OwnerId =: UserInfo.getUserId() ORDER BY CreatedDate];

        system.debug('Document for user' + listContDoc);

         //map<String, String> mapField = new Map<String, String>();
         mapField = new Map<String, String>();

        for(ContentDocument objContentDocument :listContDoc){
           
            date fechaCreatedDate           =   Date.valueOf(objContentDocument.CreatedDate);
            Time myTime                     =   Time.newInstance(0, 0, 0, 0);
            Datetime datetimes              =   DateTime.newInstance(fechaCreatedDate, myTime);
            String fechaCreated             =   datetimes.format('dd/MM/yyyy');

            
            mapField.put(objContentDocument.id,fechaCreated) ;
           
        }


        //return mapField;
    }*/

    /*public Map<String, String> getmapField() {

        return mapField;

    }*/
//}