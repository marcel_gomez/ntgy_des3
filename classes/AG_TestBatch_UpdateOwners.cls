@isTest
private class AG_TestBatch_UpdateOwners {
    
    @testSetup
    private static void setup() {
        
        //Jerarquía de Roles:
        UserRole topRole = new UserRole(name='RoleT');
        insert topRole;
        UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
        insert userRole2;
        
        UserRole userRole3 = new UserRole(name='Role3', parentRoleId= userRole2.Id);
        insert userRole3;
        
        System.debug('AG_TestUpdateOwner. Setup: get Custom Standar Gestores ID');
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Custom Standar Gestores' LIMIT 1];
        User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
        System.runAs(adminUser) {
            System.debug('AG_TestUpdateOwner. Setup: creación del usuario top de la jerarquía');
            // Usuario top en la jerarquía de empleados. Actual owner de los clientes, ofertas, etc.
            User theUser1 = new User(
                LastName = 'User1',                 FirstName='Test',   
                Alias = 'TU1',                      Email = 'test.user1@unit4.test.com',
                Username = 'test.user1@unit4.test.com',   ProfileId = profileId.id,
                TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
                UserRoleId= topRole.Id,             ID_Empleado__c= '999999',
                isActive=true);
            insert theUser1;
            
            // El usuario 2 no se crea en el setup, sino el método de test, ya que es el que provocará que se ejecute el batch
            // El usuario 2 corresponde a un empleado situando entre el empleado del usuario 1 y el empleado del usuario 3 en la jerarquía
            // Usuario intermedio en la jerarquía, owner de algunos objetos cliente, ofertas, etc
            System.debug('AG_TestUpdateOwner. Setup: creación del usuario 3, mando intermedio, con un empleado por debajo en la jerarquía');
            User theUser3 = new User(
                LastName = 'User3',                 FirstName='Test',   
                Alias = 'TU1',                      Email = 'test.user3@unit4.test.com',
                Username = 'test.user3@unit4.test.com',   ProfileId = profileId.id,
                TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
                UserRoleId= userRole3.Id,           ID_Empleado__c= '999996',
                isActive=true);
            insert theUser3;

            System.debug('AG_TestUpdateOwner. Setup: creación de 5 empleados en 5 niveles de jerarquía');
            // Creación de una jerarquía de empleados, algunos con user y otros no
            List<Empleado__c> employees = new List<Empleado__c>();
            // Empleado 1: con usuario 1, top de la jerarquía
            Empleado__c employee1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='999999', ID_Manager__c = ''); //No tiene manage
            employees.add(employee1);
            System.debug('AG_TestUpdateOwner. Setup: empleado 1, Id Empleado 999999');
            // Empleado 2: sin usuario, depende del Empleado 1. corresponde al usuario que se va a crear en test method
            Empleado__c employee2 = new Empleado__c(name='Empleado2', OwnerId=theUSer1.id, ID_Empleado__c='999998', ID_Manager__c = '999999');
            employees.add(employee2);
            System.debug('AG_TestUpdateOwner. Setup: empleado 2, Id Empleado 999998, depende del empleado 1');
            // Empleado 3: sin usuario, depende del Empleado 2
            Empleado__c employee3 = new Empleado__c(name='Empleado3', OwnerId=theUSer1.id, ID_Empleado__c='999997', ID_Manager__c = '999998');
            employees.add(employee3);
            System.debug('AG_TestUpdateOwner. Setup: empleado 3, Id Empleado 999997, depende del empleado 2');
            // Empleado 4: con usuario 3, depende del Empleado 3
            Empleado__c employee4 = new Empleado__c(name='Empleado4', OwnerId=theUSer3.id, ID_Empleado__c='999996', ID_Manager__c = '999997');
            employees.add(employee4);
            System.debug('AG_TestUpdateOwner. Setup: empleado 4, Id Empleado 999996, depende del empleado 3');
            // Empleado 5: sin usuario 3, depende del Empleado 4
            Empleado__c employee5 = new Empleado__c(name='Empleado5', OwnerId=theUSer3.id, ID_Empleado__c='999995', ID_Manager__c = '999996');
            employees.add(employee5);
            System.debug('AG_TestUpdateOwner. Setup: empleado 5, Id Empleado 999995, depende del empleado 4');
            insert employees;
            
            
            // Create accounts
            List<Account> accounts = new List<Account>();
            for (integer i=0; i<10; i++) { 
                for (integer j=0; j<employees.size(); j++) { // el empleado 1 no tiene cuentas
                    accounts.add(new Account(name='Account' + j + '-' + i, Gestor__c=employees.get(j).Id, OwnerId=employees.get(j).OwnerId));  
                }
                
            }
            insert accounts;
            
            // Para el resto de objetos no hace falta asignarlos a todos los empleados
            
            // Create "Actividades comerciales", "ofertas", "SS", "Srs", "Suministros"
            List<Actividad_Comercial__c> actividades = new List<Actividad_Comercial__c>();
            List<Ofertas__c> ofertas = new List<Ofertas__c>();
            List<SSs__c> SSs = new List<SSs__c>();
            List<SRs__c > SRs = new List<SRs__c >();
            List<Suministros__c> Suministros = new List<Suministros__c>();
            integer idx = 0;
            for (Account tmpAccount: accounts) { 
                for (integer j=1; j<employees.size(); j++) { 
                    if (tmpAccount.Name.startsWith('Account' + j)) {
                        Actividad_Comercial__c act = new Actividad_Comercial__c(Name = 'AC' + j + '-' + idx, OwnerId = employees.get(j).OwnerId, 
                                                                                Cliente__c = tmpAccount.Id, 
                                                                                ID_Codigo_Actividad__c = 'Act' + j + '-' + idx, 
                                                                                Gestor__c = employees.get(j).Id);
                        actividades.add(act);
                        
                        Ofertas__c offer = new Ofertas__c(Name = 'OF' + j + '-' + idx, 
                                                          OwnerId = employees.get(j).OwnerId, 
                                                          Cliente__c = tmpAccount.Id, 
                                                          ID_Oferta__c = 'Ofe000' + j + '-' + idx, 
                                                          Gestor__c = employees.get(j).Id);
                        ofertas.add(offer);
                        
                        SSs__c ss = new SSs__c(Name = 'SS' + j + '-' + idx, 
                                               OwnerId = employees.get(j).OwnerId, 
                                               Cliente__c = tmpAccount.Id, 
                                               ID_SS__c = 'SSs000' + j + '-' + idx,
                                               Gestor__c = employees.get(j).Id);
                        SSs.add(ss);
                        
                        SRs__c sr = new SRs__c(Name = 'SR' + j + '-' + idx, 
                                               OwnerId = employees.get(j).OwnerId, 
                                               Cliente__c = tmpAccount.Id, 
                                               ID_SR__c = 'SR000' + j + '-' + idx, 
                                               Gestor__c = employees.get(j).Id);
                        SRs.add(sr);
                        
                        Suministros__c sum = new Suministros__c(Name = 'Sum' + j + '-' + idx, 
                                                                OwnerId = employees.get(j).OwnerId, 
                                                                Cliente__c = tmpAccount.Id, 
                                                                ID_Supply__c = 'Sum000' + j + '-' + idx, 
                                                                Gestor__c = employees.get(j).Id);
                        Suministros.add(sum);
                        idx++;                                                                            
                    }
                }
            }
            insert actividades;
            insert ofertas;
            insert SSs;
            insert SRs;
            insert Suministros;
        }
    }
    
    // Test new user
    @isTest static void test_create_user() {
        System.debug('AG_TestUpdateOwner.CreateUser: Datos de prueba antes de ejecutar el proceso batch');
        /*
for (Empleado__c employee : [SELECT ID, Name, ID_Empleado__c FROM Empleado__c Order by ID_Empleado__c DESC]) {
Integer nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee.Id];
System.debug('AG_TestUpdateOwner. Numero de Clientes asignados al gestor ' + employee.Name + ' (' + employee.ID_Empleado__c + '). Esperado = 0, obtenido = ' + nAccounts);

}
*/
        
        Empleado__c employee = [SELECT ID, ID_Empleado__c FROM Empleado__c WHERE ID_Empleado__c = '999999' LIMIT 1];
        Integer nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes asignados al gestor Empleado 1 (999999). Esperado = 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);
        
        User topUser = [SELECT ID FROM User Where ID_Empleado__c = '999999' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE OwnerId = :topUser.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes propiedad del usuario 1. Esperado = 30, obtenido = ' + nAccounts);
        System.assert(nAccounts == 30);
        
        Integer nAC = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Actividades comerciales asignadas al gestor Empleado 1 (999999). Esperado = 0, obtenido = ' + nAC);
        System.assert(nAC == 0);
        
        Integer nOfers = [Select count() FROM Ofertas__c WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Ofertas asignadas al gestor Empleado 1 (999999). Esperado = 0, obtenido = ' + nOfers);
        System.assert(nOfers == 0);
        
        Integer nSSs = [Select count() FROM SSs__c WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SSs asignadas al gestor Empleado 1 (999999). Esperado = 0, obtenido = ' + nSSs);
        System.assert(nSSs == 0);        
        
        Integer nSRs = [Select count() FROM SRs__c WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SRs asignadas al gestor Empleado 1 (999999). Esperado = 0, obtenido = ' + nSRs);
        System.assert(nSRs == 0);        
        
        Integer nSums = [Select count() FROM Suministros__c WHERE Gestor__c = :employee.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Suministros asignadas al gestor Empleado 1 (999999). Esperado = 0, obtenido = ' + nSums);
        System.assert(nSums == 0);        
        
        //Empleado 2
        Empleado__c employee2 = [SELECT ID, ID_Empleado__c FROM Empleado__c WHERE ID_Empleado__c = '999998' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes asignados al gestor Empleado 2 (999998). Esperado = 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);
        
        nAC = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Actividades comerciales asignadas al gestor Empleado 2 (999998). Esperado = 10, obtenido = ' + nAC);
        System.assert(nAC == 10);
        
        nOfers = [Select count() FROM Ofertas__c WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Ofertas asignadas al gestor Empleado 2 (999998). Esperado = 10, obtenido = ' + nOfers);
        System.assert(nOfers == 10);
        
        nSSs = [Select count() FROM SSs__c WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SSs asignadas al gestor Empleado 2 (999998). Esperado = 10, obtenido = ' + nSSs);
        System.assert(nSSs == 10);        
        
        nSRs = [Select count() FROM SRs__c WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SRs asignadas al gestor Empleado 2 (999998). Esperado = 10, obtenido = ' + nSRs);
        System.assert(nSRs == 10);        
        
        nSums = [Select count() FROM Suministros__c WHERE Gestor__c = :employee2.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Suministros asignadas al gestor Empleado 2 (999998). Esperado = 100, obtenido = ' + nSums);
        System.assert(nSums == 10);        
        
        // Empleado 3
        Empleado__c employee3 = [SELECT ID, ID_Empleado__c FROM Empleado__c WHERE ID_Empleado__c = '999997' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes asignados al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);
        
        nAC = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Actividades comerciales asignadas al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nAC);
        System.assert(nAC == 10);
        
        nOfers = [Select count() FROM Ofertas__c WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Ofertas asignadas al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nOfers);
        System.assert(nOfers == 10);
        
        nSSs = [Select count() FROM SSs__c WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SSs asignadas al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nSSs);
        System.assert(nSSs == 10);        
        
        nSRs = [Select count() FROM SRs__c WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SRs asignadas al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nSRs);
        System.assert(nSRs == 10);        
        
        nSums = [Select count() FROM Suministros__c WHERE Gestor__c = :employee3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Suministros asignadas al gestor Empleado 3 (999997). Esperado = 10, obtenido = ' + nSums);
        System.assert(nSums == 10);                
        
        // Empleado 4
        Empleado__c employee4 = [SELECT ID, ID_Empleado__c FROM Empleado__c WHERE ID_Empleado__c = '999996' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes asignados al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);
        
        nAC = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Actividades comerciales asignadas al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nAC);
        System.assert(nAC == 10);
        
        nOfers = [Select count() FROM Ofertas__c WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Ofertas asignadas al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nOfers);
        System.assert(nOfers == 10);
        
        nSSs = [Select count() FROM SSs__c WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SSs asignadas al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nSSs);
        System.assert(nSSs == 10);        
        
        nSRs = [Select count() FROM SRs__c WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SRs asignadas al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nSRs);
        System.assert(nSRs == 10);        
        
        nSums = [Select count() FROM Suministros__c WHERE Gestor__c = :employee4.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Suministros asignadas al gestor Empleado 4 (999996). Esperado = 10, obtenido = ' + nSums);
        System.assert(nSums == 10);                
        
        // Empleado 5
        Empleado__c employee5 = [SELECT ID, ID_Empleado__c FROM Empleado__c WHERE ID_Empleado__c = '999995' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Clientes asignados al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);
        
        nAC = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Actividades comerciales asignadas al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nAC);
        System.assert(nAC == 10);
        
        nOfers = [Select count() FROM Ofertas__c WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Ofertas asignadas al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nOfers);
        System.assert(nOfers == 10);
        
        nSSs = [Select count() FROM SSs__c WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SSs asignadas al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nSSs);
        System.assert(nSSs == 10);        
        
        nSRs = [Select count() FROM SRs__c WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de SRs asignadas al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nSRs);
        System.assert(nSRs == 10);        
        
        nSums = [Select count() FROM Suministros__c WHERE Gestor__c = :employee5.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de Suministros asignadas al gestor Empleado 5 (999995). Esperado = 10, obtenido = ' + nSums);
        System.assert(nSums == 10);                
        
        User user3 = [SELECT ID FROM User Where ID_Empleado__c = '999996' LIMIT 1];
        nAccounts = [Select count() FROM Account WHERE OwnerId = :user3.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Numero de clientes propiedad del usuario 3. Esperado = 20, obtenido = ' + nAccounts);
        System.assert(nAccounts == 20);        
        
        
        // Implement test code
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Custom Standar Gestores' LIMIT 1];
        UserRole userRole2 = [Select ID FRom UserRole WHERE Name = 'Role2' LIMIT 1];
        User newUser = new User(
            LastName = 'User',                  FirstName='New',   
            Alias = 'NU',                       Email = 'test.user2@unit4.test.com',
            Username = 'test.user2@unit4.test.com',   ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole2.Id,              ID_Empleado__c= '999998',
            isActive=true);
        Test.startTest();
        System.debug('AG_TestUpdateOwner.CreateUser: Se crea el usuario');
        insert newUser; // This action executes a process builder that invokes a method which executes the batch process
        Test.stopTest();
        
        //nAccounts = [Select count() FROM Account WHERE OwnerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.CreateUser: Después de la ejecución del proceso batch. Núm. de clientes con el ownerid del nuevo usuario. Esperado 20, obtenido = ' + nAccounts);
        //System.assert(nAccounts == 20);
        
        /*nAccounts = [Select count() FROM Account WHERE OwnerId = :topUser.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Después de la ejecución del proceso batch. Núm. de clientes con el ownerid del usuario top en la jerarquía. Esperado 10, obtenido = ' + nAccounts);
        System.assert(nAccounts == 10);

        
        //Actividades comerciales
        Integer nActCom = [Select count() FROM Actividad_Comercial__c WHERE ownerId = :newUser.Id];
        System.debug('AG_TestUpdateOwner.CreateUser: Actividades comerciales con el owner id del nuevo usuario. Esperado 20, obtenido = ' + nActCom);
        System.assert(nActCom == 20);
*/
        /*nAccounts = [Select count() FROM Actividad_Comercial__c WHERE Gestor__c = :employee.Id];
System.debug('AG_TestUpdateOwner. Numbers of actividades comerciales with Gestor__C == new employee = ' + nActCom);
System.assert(nActCom == 10);
*/
        
        //Ofertas
        //nOfers = [Select count() FROM Ofertas__c WHERE ownerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.CreateUser: Ofertas con el owner id del nuevo usuario. Esperado 20, obtenido = ' + nOfers);
        //System.assert(nOfers == 20);
        /*nOfers = [Select count() FROM Account WHERE Gestor__c = :employee.Id];
System.debug('AG_TestUpdateOwner. Numbers of ofertas with Gestor__C == new employee = ' + nOfers);
System.assert(nOfers == 10);
*/
        //SS
        //Integer nSS = [Select count() FROM SSs__c WHERE ownerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.CreateUser: SS con el owner id del nuevo usuario. Esperado 20, obtenido = ' + nSS);
        //System.assert(nSS == 20);
        /*nOfers = [Select count() FROM SSs__c WHERE Gestor__c = :employee.Id];
System.debug('AG_TestUpdateOwner. Numbers of SS with Gestor__C == new employee = ' + nSS);
System.assert(nSS == 10);
*/
        //SRs
        //Integer nSR = [Select count() FROM Srs__c WHERE ownerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.CreateUser: SRs con el owner id del nuevo usuario. Esperado 20, obtenido = ' + nSR);
        //System.assert(nSR == 20);
        
        
        //Suministros 
        //Integer nSum = [Select count() FROM Suministros__c WHERE ownerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.CreateUser: Suministros con el owner id del nuevo usuario. Esperado 20, obtenido = = ' + nSum);
        //System.assert(nSum == 20);
    }
    
    // Test new user
    @isTest static void test_update_user() {
        // Update user3: se cambia el id empleado del usuario 3 y se le informa el del empleado 999995
        User user3 = [SELECT Id_Empleado__c FROM User Where Id_Empleado__c = '999996'];
        Integer nAccounts = [Select count() FROM Account WHERE OwnerId = :user3.Id];
        System.debug('AG_TestUpdateOwner.UpdateUser: Numero de Clientes propiedad del usuario 3. Esperado = 20, obtenido = ' + nAccounts);
        System.assert(nAccounts == 20);     
        System.debug('AG_TestUpdateOwner.UpdateUser: Se cambia el ID Empleado del usuario 3, se informa el ID del Empleado 3, que está por encima en la jerarquía');
        user3.ID_Empleado__c = '999997';
        Test.startTest();
        update user3;
        Test.stopTest();
        System.debug('AG_TestUpdateOwner.UpdateUser: usuario actualizado. Ahora pasa a ser el propietario de los clientes de los empleados 3, 4 y 5');
        //nAccounts = [Select count() FROM Account WHERE OwnerId = :user3.Id];
        //System.debug('AG_TestUpdateOwner.UpdateUser: Numero de Clientes propiedad del usuario 3. Esperado = 30, obtenido = ' + nAccounts);
        //System.assert(nAccounts == 30);     
        
    }
    
    // Test when a user is deactivated
    @isTest static void test_deactivate_user() {
        // Deactivate User3
        User user3 = [SELECT Id_Empleado__c FROM User Where Id_Empleado__c = '999996'];
        Integer nAccounts = [Select count() FROM Account WHERE OwnerId = :user3.Id];
        System.debug('AG_TestUpdateOwner.DeactivateUser: Numero de Clientes propiedad del usuario 3. Esperado = 20, obtenido = ' + nAccounts);
        System.assert(nAccounts == 20);     
        System.debug('AG_TestUpdateOwner.DeactivateUser: Se desactiva el usuario 3, por lo que deja ser propietario de sus registros, que pasan a ser propiedad del empleado que está por encima en la jerarquía y que tiene usuario');
        user3.isActive = false;
        
        // Create a User 
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Custom Standar Gestores' LIMIT 1];
        UserRole userRole2 = [Select ID FRom UserRole WHERE Name = 'Role2' LIMIT 1];
        User newUser = new User(
            LastName = 'User',                  FirstName='New',   
            Alias = 'NU',                       Email = 'test.user2@unit4.test.com',
            Username = 'test.user2@unit4.test.com',   ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole2.Id,              ID_Empleado__c= '999998',
            isActive=true);
        
        Test.startTest();
        insert newUser; // This action executes a process builder that invokes a method which executes the batch process
        update user3;
        Test.stopTest();
        //nAccounts = [Select count() FROM Account WHERE OwnerId = :user3.Id];
        //System.debug('AG_TestUpdateOwner.DeactivateUser: Numero de Clientes propiedad del usuario 3. Esperado = 0, obtenido = ' + nAccounts);
        //System.assert(nAccounts == 0);     
        
        //nAccounts = [Select count() FROM Account WHERE OwnerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.DeactivateUser: Los clientes del usuario 3 pasan a ser propiedad del usuario 2, que está por encima en la jeraqrquía');
        //System.debug('AG_TestUpdateOwner.DeactivateUser: Numero de Clientes propiedad del usuario 2. Esperado = 40 (20 que ya tenía + 20 del user desactivado), obtenido = ' + nAccounts);
        //System.assert(nAccounts == 40);     
        
    }
    
    // Test when a user is activated
    @isTest static void test_reactivate_user() {
        // Deactivate User3
        
        
         // Create a User 
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Custom Standar Gestores' LIMIT 1];
        UserRole userRole2 = [Select ID FRom UserRole WHERE Name = 'Role2' LIMIT 1];
        User newUser = new User(
            LastName = 'User',                  FirstName='New',   
            Alias = 'NU',                       Email = 'test.user2@unit4.test.com',
            Username = 'test.user2@unit4.test.com',   ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole2.Id,              ID_Empleado__c= '999998',
            isActive=false);
        
        Test.startTest();
        insert newUser;
        Integer nAccounts = [Select count() FROM Account WHERE OwnerId = :newUser.Id];
        System.debug('AG_TestUpdateOwner.ReactivateUser: Numero de Clientes propiedad del usuario. Esperado = 0, obtenido = ' + nAccounts);
        System.assert(nAccounts == 0);   
        
        newUser.isActive = true;
        update newUser;
        Test.stopTest();
        //nAccounts = [Select count() FROM Account WHERE OwnerId = :newUser.Id];
        //System.debug('AG_TestUpdateOwner.ReactivateUser: Numero de Clientes propiedad del usuario despues de reactivarlo. Esperado = 20 (10 propios y 10 de un subordinado sin usuario, obtenido = ' + nAccounts);
        //System.assert(nAccounts == 20);     
        
        
        
    }
    
}