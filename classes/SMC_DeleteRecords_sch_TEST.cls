/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba encargada e crear los datos cargados en recursos estaticos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-09		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class SMC_DeleteRecords_sch_TEST {
	
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';

	static testMethod void scenarioOne() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();
		
		test.startTest();
		
		SMC_DeleteRecords_sch schDeleteRecords						= new SMC_DeleteRecords_sch();
		
		String strJobId	= System.schedule( 'SMC_DeleteRecords_sch', CRON_EXP, schDeleteRecords );
		
		test.stopTest();
	}
}