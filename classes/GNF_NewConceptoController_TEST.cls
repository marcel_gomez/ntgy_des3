/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_NewConceptoController.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-08		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class GNF_NewConceptoController_TEST {
	
	static testMethod void scenarioOne() {
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		List<Contrato__c> lstConceptos										= new List<Contrato__c>();
		lstConceptos														= [SELECT Id,
																					Name
																				FROM Contrato__c
																			];
		
		Test.startTest();
		
			ApexPages.StandardSetController sCtrConcepto					= new ApexPages.StandardSetController( lstConceptos );
			System.currentPageReference().getParameters().put( 'id', lstConceptos.get( 0 ).Id );
			System.currentPageReference().getParameters().put( 'Name', lstConceptos.get( 0 ).Name );
			
			GNF_NewConceptoController ctrNewConceptoController				= new GNF_NewConceptoController( sCtrConcepto );
			ctrNewConceptoController.returnPage();
			ctrNewConceptoController.getRecordTypeId();
		
		Test.stopTest();
	}
}