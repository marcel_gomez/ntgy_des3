/**
* VASS
* @author           Juan Cardona juan.cardona@vass.es
* Project:          Gas Natural Fenosa
* Description:      Clase con logica
*
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-06-26      Juan Cardona (JSC)      Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_cotizador_cls {
    private string                          str_idClick;
    private string                          str_fechaPresentacion;
    private string                          str_tipo_Operacion;

    private string                          str_idConsumoClick;

    public double                           preciocvfrcfinalmedioponderado=0;
    public double                           preciocvclientemedioponderado=0;
    public double                           preciocvf2medioponderado=0;
    public double                           spread=0;
    public double                           spreadofertado=0;
    public double                           fanexo=0;
    public double                           MediaPonderadaBrent = 0;
    public double                           MediaPonderadaParidad = 0;
    public double                           MediaPonderadaCVPrimaClienteClick = 0;

    public double                           preciocvfrcinicialmedioponderado=0;
    public double                           diferencialdecierrefrc; //Fórmula
    public double                           diferencialdecierref2; //Fórmula
    public double                           diferencialdecierrefrcmedio=0;
    public double                           preciocvf2cerradomedioponderado=0;
    public double                           diferencialdecierref2medio=0;
    public double                           QClickTotalPeriodo=0;
    public double                           QClickTotalPeriodoMMbtu=0;

    public double                           preciocvfrcfinalmedioponderadommbtu = 0;
    public double                           preciocvfrcinicialmedioponderadommbtu = 0;
    public double                           diferencialdecierrefrcmediommbtu = 0;
    public Map<String,double>               tvanexoperiodo;

    //18072017: JSC - Ajustes para calculo margen negociación med. Pond. operación (c€/kWh)
    public double                           db_margenNegociacionMediaPondOperacion = 0;

    private static List<Opportunity>        list_opportunity;
    private static List<Consumo_Click__c>   list_consumoClick;
    private Map<Id,Double>                  Mapa_Q_click_total_oper;  //<IdOportunidad,TotalQComprometida>
    private Map<Id,Double>                  Mapa_Q_click_total_oper_Mmbtu;
    private Double                          Q_click_total_oper=0;
    Private  Map<Id,Double>                 Mapa_Q_click_total_periodo; //<IDOportunidad,<NombrePeriodo,TotalQComprometida>
    private  Map<Id,Double>                 Mapa_Q_click_total_periodo_MMbtu;

    /* BEGIN - Manuel Medina - 04072017 */
    public List<Cotizacion_de_periodo__c> list_cotizacionPeriodo;
    /* END - Manuel Medina - 04072017 */

    //Marcel - 24/04/2018 - GNME-18
    public Double preciocvclientemedio   = 0;
    public Double preciocvf2mediocerrado = 0;

    /*
    */
    public GNF_cotizador_cls() {

    }

    /*
    */
    public GNF_cotizador_cls(List<string> list_idOpp) {
        if( list_idOpp==NULL || list_idOpp.isempty())
            return;
    }

    //Luis Igualada
    public void CalculoFormulasSite(Id Oportunidad) {
        //Inicialización de Mapas
        Map<Id,Double> consumosxperiodo         = new Map<Id,Double>();
        Map<Id,Double> diferencialdecierrefrc   = new Map<Id,Double>();
        Map<Id,Double> diferencialdecierref2    = new Map<Id,Double>();

        //Inicializamos el mapa de IdPeriodo tvanexo
        tvanexoperiodo = new Map<String,double>();

        List<Cotizacion_de_periodo__c> camposperiodos = new List<Cotizacion_de_periodo__c>();
        List<Opportunity> camposoportunidad = new List<Opportunity>();

        //Obtenemos los campos de la Oportunidad que nos interesan
        camposoportunidad = getOpps(Oportunidad);

        //Nos traemos los campos que necesitamos de Cotizacion Periodo pasándole una Oportunidad
        camposperiodos = CamposCotizacionPeriodo(Oportunidad);


        Double CVFRCFinal                       = 0;
        Double CVCliente                        = 0;
        Double CVF2                             = 0;
        Double CVFRC                            = 0;
        Double Brent                            = 0;
        Double Paridad                          = 0;
        Double CVPrimaClienteClick              = 0;
        Double CVFRCInicialMmbtu                = 0;
        Double CVFRCFinalMmbtu                  = 0;
        Double tvanexo                          = 0;

        Double CVFormulaDestinoFinal            = 0;
        Double QClickTotalOper                  = 0;
        Double QClickTotalOperMmbtu             = 0;
        Double SumatorioConCVFRCFinal           = 0;
        Double SumatorioConCVClientePonderado   = 0;
        Double SumatorioConCVCliente            = 0;
        Double SumatorioConCVF2                 = 0;
        Double SumatorioconCVFRC                = 0;
        Double SumatorioConFormulaDestinoFinal  = 0;
        Double SumatorioConFormulaDestinoFinalPonderado = 0;
        Double SumatorioconBrent                = 0;
        Double SumatorioconParidad              = 0;
        Double SumatorioconCVPrimaClienteClick  = 0;
        Double SumatorioconCVFRCIncialMmbtu     = 0;
        Double SumatorioconCVFRCFinalMmbtu      = 0;

        //18072017: JSC - Ajustes para calculo margen negociación med. Pond. operación (c€/kWh)     
        Double db_sumConsumosPorDifCV   = 0;

        //obtenemos la suma de consumos por periodo
        getQClickTotalPeriodo(Oportunidad);

        //Rellenamos los Mapas Globales del Sumatorios de QClick por Oportunidad
        getQClickTotalOper(Oportunidad);

        //Obtenemos el Q_CLICK_TOTAl_OPER de la fórmula
        QClickTotalOper      = Mapa_Q_click_total_oper.get(Oportunidad);
        QClickTotalOperMmbtu = Mapa_Q_click_total_oper_Mmbtu.get(Oportunidad);


        //Recorremos los periodos de la oportunidad
        for(Opportunity opp : camposoportunidad) {

            //Recorremos las COTIZACIONES DE PERIODO de esa Oportunidad
            for(Cotizacion_de_periodo__c CotizPeriodo : camposperiodos) {
                //Obtenemos el CVFRCFinal de ese Periodo
                if(CotizPeriodo.CV_FRC_Final_c_KWh__c != NULL)
                    CVFRCFinal = CotizPeriodo.CV_FRC_Final_c_KWh__c;

                //Obtenemos el CVCliente de ese Periodo
                if(CotizPeriodo.CV_Cliente__c != NULL)
                    CVCliente = CotizPeriodo.CV_Cliente__c;

                //Obtenemos el CVF2 de ese Periodo
                if(CotizPeriodo.GNF_FormulaDestinoInicial__c != NULL)
                    CVF2 = Double.valueOf(CotizPeriodo.GNF_FormulaDestinoInicial__c);

                //Obtenemos el el CVFRC Inicial de ese Periodo
                if(CotizPeriodo.CV_FRC_c_kWh__c != NULL)
                    CVFRC = Double.valueOf(CotizPeriodo.CV_FRC_c_kWh__c);

                //Obtenemos el CV Formula Destino Final de ese periodo
                if(CotizPeriodo.GNF_FormulaDestinoFinal__c != NULL)
                    CVFormulaDestinoFinal = Double.valueOf(CotizPeriodo.GNF_FormulaDestinoFinal__c);

                //Obtenemos el brent de ese Periodo
                if(CotizPeriodo.Brent_de_referencia__c != NULL)
                    Brent = Double.valueOf(CotizPeriodo.Brent_de_referencia__c);

                //Obtenemos la paridad de ese Periodo
                if(CotizPeriodo.Paridad_de_referencia_USD_EUR__c != NULL)
                    Paridad = Double.valueOf(CotizPeriodo.Paridad_de_referencia_USD_EUR__c);

                //Obtenemos el CV PrimaClienteClick de la Cotización de Periodo
                if(CotizPeriodo.GNF_CVClientePrima_Click__c != NULL)
                    CVPrimaClienteClick = Double.valueOf(CotizPeriodo.GNF_CVClientePrima_Click__c);

                //Obtenemos el CVFRC final Mmbtu
                if(CotizPeriodo.CV_FRC_Final__c != NULL)
                    CVFRCFinalMmbtu = Double.valueOf(CotizPeriodo.CV_FRC_Final__c);

                //Obtenemos el CVFRC Inicial Mmbtu
                if(CotizPeriodo.CV_FRC_Inicial__c != NULL)
                    CVFRCInicialMmbtu = Double.valueOf(CotizPeriodo.CV_FRC_Inicial__c);


                //Obtenemos el sumatorio de QcComprometida de ese Periodo
                QClickTotalPeriodo = Mapa_Q_click_total_periodo.get(CotizPeriodo.Id);

                //Obtenemos el sumatorio de QcComprometida MMbtu de ese Periodo
                QClickTotalPeriodoMMbtu = Mapa_Q_click_total_periodo_MMbtu.get(CotizPeriodo.Id);

                //Marcel - 24/04/2018 - GNME-18
                SumatorioConCVCliente += CVCliente;
                SumatorioConFormulaDestinoFinal += CVFormulaDestinoFinal;

                //Sumatorio de la QComprometida de cada periodo
                if(QClickTotalPeriodo != NULL) {

                    //Sumatorio Con CVFRCFinal
                    SumatorioConCVFRCFinal += CVFRCFinal * QClickTotalPeriodo;

                    //Sumatorio Con CVCliente
                    SumatorioConCVClientePonderado += CVCliente * QClickTotalPeriodo;

                    //Sumatorio con CV Formula Destino Final
                    SumatorioConFormulaDestinoFinalPonderado += CVFormulaDestinoFinal * QClickTotalPeriodo;

                    //Sumatorio con CVF2
                    SumatorioConCVF2 += CVF2 * QClickTotalPeriodo;

                    //Sumatorio con CV_FRC_c_kWh__c
                    SumatorioconCVFRC += CVFRC * QClickTotalPeriodo;

                    //Sumatorio con Brent
                    SumatorioconBrent += Brent * QClickTotalPeriodo;

                    //Sumatorio con CVPrimaClienteClick
                    SumatorioconCVPrimaClienteClick += CVPrimaClienteClick * QClickTotalPeriodo;

                    //Sumatorio con Paridad
                    SumatorioconParidad += Paridad * QClickTotalPeriodo;
                                        
                    //18072017: JSC - Ajustes para calculo margen negociación med. Pond. operación (c€/kWh) 
                    db_sumConsumosPorDifCV += ( CVFRCFinal - CVFRC) * QClickTotalPeriodo;
                }


                if(QClickTotalPeriodoMMbtu != NULL) {
                    //Sumatorio con CVFRC Inicial Mmbtu
                    SumatorioconCVFRCIncialMmbtu += CVFRCInicialMmbtu * QClickTotalPeriodoMMbtu;

                    //Sumatorio con CVFRC Final Mmbtu
                    SumatorioconCVFRCFinalMmbtu += CVFRCFinalMmbtu * QClickTotalPeriodoMMbtu;
                }

                //Cálculos por periodo
                diferencialdecierrefrc.put(CotizPeriodo.Id,(CVFRCFinal - CVFRC));
                diferencialdecierref2.put(CotizPeriodo.Id,Double.valueOf(CVFormulaDestinoFinal) - Double.valueOf(CVF2));

                if(opp.Pais__c == 'Portugal') {
                        //TVAnexo
                        if(CotizPeriodo.GNF_ValorK__c != NULL || CotizPeriodo.GNF_ValorK__c != 0) {
                            //Recorremos las CV Prima Cliente Click de cada Cotización de periodo
                            tvanexo = CVPrimaClienteClick + (CotizPeriodo.GNF_ValorK__c != NULL ? (Double)CotizPeriodo.GNF_ValorK__c : 0);
                            tvanexoperiodo.put(CotizPeriodo.Id,tvanexo);
                        }
                }
                else if(opp.Pais__c == 'España' && !CotizPeriodo.Consumos_click__r.isempty()) {
                    //Es España
                    //TVAnexo
                    System.debug('CotizPeriodo'+CotizPeriodo.Id);
                    tvanexo=CVPrimaClienteClick+CotizPeriodo.Consumos_click__r[0].GNF_ValorF__c;
                    System.debug('Recuperar TV Anexo'+tvanexo);
                    tvanexoperiodo.put(CotizPeriodo.Id,tvanexo);
                }
            }

            //Una vez recorridos todos los periodos y hecho el sumatorio lo dividimos entre la QClickTotalOper
            //Y rellenamos la variable Global
            if(QClickTotalOper != 0) {
                preciocvfrcfinalmedioponderado  = SumatorioConCVFRCFinal / QClickTotalOper;
                preciocvf2medioponderado        = SumatorioConCVF2 / QClickTotalOper;
                preciocvfrcinicialmedioponderado= SumatorioconCVFRC / QClickTotalOper;
                preciocvclientemedioponderado   = SumatorioConCVClientePonderado / QClickTotalOper;
                preciocvf2cerradomedioponderado = SumatorioConFormulaDestinoFinalPonderado / QClickTotalOper;

                //Cálculo Media Ponderada Brent
                MediaPonderadaBrent             = SumatorioconBrent / QClickTotalOper;
                
                //Cálculo Media Ponderada Paridad
                MediaPonderadaParidad           = SumatorioconParidad / QClickTotalOper;

                //Cálculo Media Ponderada CVClientePrimaClick
                MediaPonderadaCVPrimaClienteClick = SumatorioconCVPrimaClienteClick / QClickTotalOper;

                //18072017: JSC - Ajustes para calculo margen negociación med. Pond. operación (c€/kWh)
                db_margenNegociacionMediaPondOperacion = db_sumConsumosPorDifCV / QClickTotalOper;
            }

            //Marcel - 24/04/2018 - GNME-18
            if(camposperiodos.size() > 0) {
                preciocvclientemedio    = SumatorioConCVCliente / camposperiodos.size();
                preciocvf2mediocerrado  = SumatorioConFormulaDestinoFinal / camposperiodos.size();
            }

            if(QClickTotalOperMmbtu != 0) {
                //Cálculo preciocv frcfinal medioponderado mmbtu
                preciocvfrcfinalmedioponderadommbtu = SumatorioconCVFRCFinalMmbtu / QClickTotalOperMmbtu;
                //Cálculo preciocv frcinicial medioponderado mmbtu
                preciocvfrcinicialmedioponderadommbtu = SumatorioconCVFRCIncialMmbtu / QClickTotalOperMmbtu;
            }

            //Marcel - 24/04/2018 - GNME-18 - commented
            //spread = preciocvclientemedioponderado - preciocvf2medioponderado;
            //A nivel de Oportunidad
            //spreadofertado = spread + (opp.Prima_de_gestion__c != NULL ? opp.Prima_de_gestion__c : 0) +
            //                (opp.Prima_de_riesgo__c != NULL ? opp.Prima_de_riesgo__c : 0) +
            //                (opp.Prima_comercial__c != NULL ? opp.Prima_comercial__c : 0);

            //Marcel - 24/04/2018 - GNME-18
            spread = preciocvclientemedio - preciocvf2mediocerrado;
            spreadofertado = preciocvclientemedio - preciocvf2medioponderado + 
                                (opp.Prima_de_gestion__c != NULL ? opp.Prima_de_gestion__c : 0) +
                                (opp.Prima_de_riesgo__c  != NULL ? opp.Prima_de_riesgo__c : 0) +
                                (opp.Prima_comercial__c  != NULL ? opp.Prima_comercial__c : 0);

            //Cálculo 1 - Cálculo 14
            //Marcel - 24 / 04 / 2018 - GNME-18
            diferencialdecierrefrcmedio = preciocvfrcfinalmedioponderado - preciocvfrcinicialmedioponderado;

            //Cálculo 3 - Cálculo 18
            diferencialdecierref2medio = preciocvf2medioponderado - preciocvf2cerradomedioponderado;

            //Cálculo 1 - Cálculo 14 en Mmbtu
            diferencialdecierrefrcmediommbtu= preciocvfrcfinalmedioponderadommbtu - preciocvfrcinicialmedioponderadommbtu;

            //Recorremos las OPORTUNIDAD PUNTO DE SUMINISTRO de esa Oportunidad
            for( Oportunidad_Punto_de_Suministros__c objPS : opp.Oportunidad_Puntos_de_Suministros__r) {
                if(opp.Pais__c == 'Portugal') {
                    if(objPS.Concepto__r.GNF_ValorK__c != NULL || objPS.Concepto__r.GNF_ValorK__c != 0) {
                        //Calculo F ANEXO para Portugal
                        if(opp.Tipo_de_servicio__c=='SWAP Fórmula' || opp.Tipo_de_servicio__c=='SWAP Hub') {
                            fanexo = spreadofertado + (objPS.Concepto__r.GNF_ValorK__c != NULL ? (Double)objPS.Concepto__r.GNF_ValorK__c : 0);
                        }
                    }
                }
                else if(opp.Pais__c == 'España') {
                    //Calculo F ANEXO para España
                    if(opp.Tipo_de_servicio__c=='SWAP Fórmula' || opp.Tipo_de_servicio__c=='SWAP Hub') {
                        fanexo = spreadofertado + (objPS.Concepto__r.Valor_de_F__c != NULL ? (Double)objPS.Concepto__r.Valor_de_F__c : 0);
                    }
                }
            }
        }
    }

    //Luis Igualada
    public void getQClickTotalOper(Id Oportunidad) {
        AggregateResult[] groupedresults = [SELECT 
                                                SUM(Q_comprometida_kWh__c),
                                                SUM(Q_comprometida_MMbtu__c),
                                                Cotizacion_de_periodo__r.Opportunity__r.Id
                                            FROM
                                                Consumo_Click__c
                                            WHERE
                                                Cotizacion_de_periodo__r.Opportunity__r.Id =: Oportunidad
                                            GROUP BY
                                                Cotizacion_de_periodo__r.Opportunity__r.Id];

        Mapa_Q_click_total_oper = New Map<Id,Double>();
        Mapa_Q_click_total_oper_Mmbtu = New Map<Id, Double>();

        for(AggregateResult ar: groupedresults) {
            Mapa_Q_click_total_oper.put((Id)ar.get('Id'), (Double)ar.get('expr0'));
            Mapa_Q_click_total_oper_Mmbtu.put((Id)ar.get('Id'), (Double)ar.get('expr1'));
        }
    }

    //Luis Igualada
    public void getQClickTotalPeriodo(Id Oportunidad) {
        AggregateResult[] groupedresults= [SELECT 
                                                SUM(Q_comprometida_kWh__c),
                                                SUM(Q_comprometida_MMbtu__c),

                                                Cotizacion_de_periodo__r.Id cotiz,
                                                Cotizacion_de_periodo__r.Opportunity__r.Id
                                            FROM
                                                Consumo_Click__c
                                            WHERE
                                                Cotizacion_de_periodo__r.Opportunity__r.Id =: Oportunidad
                                            GROUP BY
                                                Cotizacion_de_periodo__r.Id,Cotizacion_de_periodo__r.Opportunity__r.Id];

        Mapa_Q_click_total_periodo = new Map<Id,Double>();
        Mapa_Q_click_total_periodo_MMbtu = new Map<Id,Double>();

        for(AggregateResult ar: groupedresults) {
            Mapa_Q_click_total_periodo.put((Id)ar.get('Cotiz'), (Double)ar.get('expr0'));
            Mapa_Q_click_total_periodo_MMbtu.put((Id)ar.get('Cotiz'), (Double)ar.get('expr1'));
        }
    }

    //Luis Igualada
    //Método en el que nos traemos los campos que necesitamos de Cotización de Periodo para hacer los cálculos
    public List<Cotizacion_de_periodo__c> CamposCotizacionPeriodo(Id Oportunidad) {
        return [SELECT 
                    Id,

                    CV_Cliente__c,
                    CV_FRC_Final__c,
                    CV_FRC_Inicial__c,
                    CV_FRC_Final_c_KWh__c,
                    CV_FRC_c_kWh__c,

                    GNF_FormulaDestinoInicial__c,
                    GNF_FormulaDestinoFinal__c,
                    GNF_CVClientePrima_Click__c,
                    GNF_ValorK__c,

                    Brent_de_referencia__c,
                    Paridad_de_referencia_USD_EUR__c,
                    (
                        SELECT
                            GNF_ValorF__c
                        FROM
                            Consumos_click__r
                        WHERE
                            GNF_ValorF__c != NULL
                        LIMIT 1
                    )
                FROM
                    Cotizacion_de_periodo__c
                WHERE
                    Opportunity__r.Id =: Oportunidad];
    }

    //Luis Igualada
    public List<Opportunity> getOpps(Id Oportunidad) {
        return [SELECT 
                    Id,
                    RecordType.DeveloperName,
                    Prima_comercial__c,
                    Prima_de_gestion__c,
                    Prima_de_riesgo__c,
                    Pais__c,
                    Tipo_de_servicio__c,
                    StageName,
                    (
                        SELECT
                            Concepto__r.GNF_ValorK__c,
                            Concepto__r.Valor_de_F__c
                        FROM
                            Oportunidad_Puntos_de_Suministros__r
                    )
                FROM
                    Opportunity
                WHERE
                    Id =: Oportunidad];
    }

    /*
    */
    public static List<Opportunity> getObjOpp(List<string> list_idOpp) {
        return [SELECT 
                    Id, GNF_FechaLimiteValidez__c, AccountId, Tipo_de_precio__c, Tipo_de_servicio__c, Tipo_Producto__c, Tipo_Fijacion__c, Metodo_aplicacion__c,
                    Periodicidad_del_precio__c, Formula__c, Formula_Destino__c,Formula_destino_Descripcion__c, Prima_de_riesgo__c, recordTypeId, recordType.Name,
                    Fecha_Cotizacion__c,GNF_Trader__c, Fecha_limite_de_validez__c, Fecha_Fin_Orden_de_Cierre__c, Precio_final_objetivo__c, GNF_cvFinalObjetivoKwh__c,
                    Target_Price__c, StageName, Tipo_Producto_Mail__c, Formula_a_cubrir__c, Account.Name, ID_Opportunity__c, SMC_CantidadPS__c
                FROM
                    Opportunity
                WHERE
                    Id IN: list_idOpp];
    }

    /*
    */
    public static List<Consumo_Click__c> getObjConsumosClick( List<string> list_idOpp) {
        return  [SELECT 
                    Id, Periodo__c, Trade_Group__c, Mes_de_aplicacion__c, Q_comprometida_kWh__c, Q_comprometida_MMbtu__c, Cotizacion_de_periodo__c, Cotizacion_de_periodo__r.CV_Cliente__c,
                    Cotizacion_de_periodo__r.CV_FRC_c_kWh__c, Cotizacion_de_periodo__r.CV_FRC_Inicial__c, Cotizacion_de_periodo__r.Brent_de_referencia__c, Cotizacion_de_periodo__r.Total_de_consumos_comprometidos_MMBtu__c,
                    Cotizacion_de_periodo__r.Paridad_de_referencia_USD_EUR__c, Cotizacion_de_periodo__r.CV_FRC_Final_c_KWh__c, Cotizacion_de_periodo__r.CV_FRC_Final__c, Cotizacion_de_periodo__r.Total_de_consumos_comprometidos__c,
                    Cotizacion_de_periodo__r.GNF_FormulaDestinoInicial__c, Cotizacion_de_periodo__r.GNF_FormulaDestinoFinal__c, Cotizacion_de_periodo__r.GNF_FormulaID_Contrato_Espejo_Venta_SWAP__c,
                    Cotizacion_de_periodo__r.GNF_FormulID_Contrato_Espejo_Compra_SWAP__c, Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Venta_SWAP__c, Cotizacion_de_periodo__r.GNF_Formula_Trade_Group_Compra_SWAP__c,
                    Cotizacion_de_periodo__r.Periodo_Formula__c,Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_precio__c, Cotizacion_de_periodo__r.Opportunity__r.Tipo_Producto__c,
                    Cotizacion_de_periodo__r.Opportunity__r.Tipo_de_servicio__c, Cotizacion_de_periodo__r.Opportunity__r.Tipo_Fijacion__c, Cotizacion_de_periodo__r.Opportunity__r.Metodo_aplicacion__c,
                    Cotizacion_de_periodo__r.Opportunity__r.Periodicidad_del_precio__c, Cotizacion_de_periodo__r.Opportunity__r.Formula__c, Cotizacion_de_periodo__r.Opportunity__r.Formula_Destino__c,
                    Cotizacion_de_periodo__r.Opportunity__r.Prima_de_riesgo__c, Cotizacion_de_periodo__r.Opportunity__r.RecordTypeId, Cotizacion_de_periodo__r.Opportunity__r.recordType.Name,
                    Cotizacion_de_periodo__r.Opportunity__r.Fecha_Cotizacion__c,Cotizacion_de_periodo__r.Opportunity__r.GNF_Trader__c, Cotizacion_de_periodo__r.Opportunity__r.Fecha_limite_de_validez__c,
                    Cotizacion_de_periodo__r.Opportunity__r.Fecha_Fin_Orden_de_Cierre__c, Cotizacion_de_periodo__r.Opportunity__r.Precio_final_objetivo__c, Cotizacion_de_periodo__r.Opportunity__r.Target_Price__c,
                    Cotizacion_de_periodo__r.Opportunity__r.StageName, Cotizacion_de_periodo__r.Opportunity__r.AccountId, Cotizacion_de_periodo__r.Opportunity__r.GNF_cvFinalObjetivoKwh__c 
                FROM
                    Consumo_Click__c
                WHERE
                    Cotizacion_de_periodo__r.Opportunity__r.Id IN: list_idOpp
                ORDER BY
                    GNF_AnioAplicacion__c ASC, Mes_de_aplicacion__c ASC];
    }
}