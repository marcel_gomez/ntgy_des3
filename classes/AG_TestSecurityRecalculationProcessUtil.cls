@isTest
private class AG_TestSecurityRecalculationProcessUtil {


    @isTest static void executeBatchToRecalculateObjetShares_Test() {

	    Id batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateObjetShares(false, false);
	    System.assert(batchId!=null);

	    batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateObjetShares(false, true);
	    System.assert(batchId!=null);
    }
	
    @isTest static void executeBatchToRecalculateAccountShares_Test() {

		Id batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateAccountShares(false, false);
	    System.assert(batchId!=null);

		batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateAccountShares(false, true);
	    System.assert(batchId!=null);
    }


    @isTest static void executeBatchToRecalculateShares_Test() { 

		Id[] batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateShares(false,false);
		System.assert(batchId!=null);
		System.assertEquals(batchId.size(),2);

		batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateShares(false,true);
		System.assert(batchId!=null);
		System.assertEquals(batchId.size(),2);

    }


    @isTest static void executeBatchToRecalculateShares_Test_noparam() {

		Id[] batchId = AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateShares();
		System.assert(batchId!=null);
		System.assertEquals(batchId.size(),2);

    }
}