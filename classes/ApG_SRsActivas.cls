public without sharing class ApG_SRsActivas implements ApG_PosicionGlobalReportInterface {
    public ApG_SRsActivas(){
    	system.debug('Ejecutando ApG_SRsActivas');
    }
    
    public list<map<string, string>> getsObjectInformation(string itemToGetFrom, string pageType, string forceUser){
    	boolean isHierarchy = !pageType.equalsIgnoreCase('status');
    	/*Recoger la custom metadata type, sacar sus campos, poner los campos en el mapa según el nivel especificado y si es jerarquía, primero la jerarquía, si es estado, primero el estado.*/
		ApG_GlobalPositionItem__mdt gpi = [SELECT Id, lvl0__c, lvl1__c, lvl2__c, lvl3__c, lvl4__c, objectName__c FROM ApG_GlobalPositionItem__mdt WHERE DeveloperName =: 'SrsActivasEstado'];
		system.debug('globalPositionItem recuperado	'+gpi);
		map<string, string> fieldMap = fillFieldList(gpi);
    	list<map<string, string>> retListMap = new list<map<string, string>>();
    	map<string, map<string, string>> hierarchyMap = ApG_PosicionGlobalController.getHierarchyMap(forceUser);
    	set<string> gestorSet = hierarchyMap.keySet();

    	string theQuery = 'SELECT '+string.join(fieldMap.values(), ', ')+ ', GestorId__c, Id, Name FROM '+gpi.objectName__c + ' WHERE Name >: itemToGetFrom AND GestorId__c IN: gestorSet ORDER BY Name ASC LIMIT 7000'; 
    	system.debug('laQuery	'+  theQuery);
    	list<sObject> srList = database.query(theQuery);//[SELECT Id, Estado__c, CreatedDate, Name, Gestor__c, GestorId__c FROM SRs__c WHERE Name >: itemToGetFrom AND GestorId__c IN: hierarchyMap.keySet() ORDER BY Name ASC LIMIT 7000];
		system.debug('lista de Srs    ' + srList);

    	for(sObject i : srList){
    		map<string, string> instMap = new map<string, string>();
    		if(isHierarchy){
	    		instMap.putAll(hierarchyMap.get(string.valueOf(i.get('GestorId__c'))));
	    		integer levelNumber = instMap.keySet().size();
	    		for(string j : fieldMap.keySet()){
	    			string theLevel = 'lvl' +levelnumber+'__c';
	    			instMap.put(theLevel, string.valueOf(i.get(fieldMap.get(j))));
	    			levelnumber++;
	    		}
    		}else{
    			integer levelNumber = 0;
    			for(string j : fieldMap.keySet()){
    				string theLevel = 'lvl' +levelNumber+'__c';
	    			instMap.put(theLevel, string.valueOf(i.get(fieldMap.get(j))));
	    			levelnumber++;
    			}
    			for(string j : hierarchyMap.get(string.valueOf(i.get('GestorId__c'))).keySet()){
    				string theLevel = 'lvl' +levelNumber+'__c';
    				instMap.put(theLevel, hierarchyMap.get(string.valueOf(i.get('GestorId__c'))).get(j));
    				levelNumber++;
    			}
    		}
	    	instMap.put('fieldToGetFrom', string.valueOf(i.get('Name')));
	    	instMap.put('id', string.valueOf(i.get('id')));
	    	retListMap.add(instMap);
    	}
    	system.debug('lista de mapas que va de vuelta	'+retListMap);
    	return retListMap;
    }



    private map<string, string> fillFieldList(ApG_GlobalPositionItem__mdt gpi){
    	map<string, string> retMap = new map<string, string>();
    	for(integer i = 0; i<5; i++){
    		string fieldName = 'lvl'+i+'__c';
    		if(gpi.get(fieldName)!= null){
    			retMap.put(fieldName, string.valueOf(gpi.get(fieldName)));
    		}
    	}
    	return retMap;
    }

}