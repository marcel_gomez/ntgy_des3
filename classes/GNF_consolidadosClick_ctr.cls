/**
* VASS
* @author 			Juan Cardona juan.cardona@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase --
*
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-24		Juan Cardona (JSC)		Definicion inicial de la clase.
*********************************************************************************************************/

public class GNF_consolidadosClick_ctr
{

	public Opportunity											obj_opp			{get; set;}
	public list<string> 										lst_idOpp		{get; set;}
	public list<GNF_consolidarConsumosClick_cls.wrp_resultado>	lst_consolidado	{get; set;}

	public GNF_consolidarConsumosClick_cls 						cls_consumos;


	public GNF_consolidadosClick_ctr(ApexPages.StandardController stdController)
		{
			system.debug('<<GNF_consolidadosClick_ctr.GNF_consolidadosClick_ctr>> :' + stdController);

			if( !Test.isRunningTest() ){
				stdController.addFields( new list<string> {'RecordType.DeveloperName'} );

				obj_opp = ( Opportunity ) stdController.getRecord();
			}else{
				obj_opp = [SELECT Id,
				 				RecordType.DeveloperName
				 			FROM Opportunity
				 			WHERE Id =: String.valueOf( stdController.getRecord().get( 'Id' ) )
				 			LIMIT 1
							];
			}



		}//ends GNF_consolidadosClick_ctr


	public void mthd_getConsumosClick()
		{
			system.debug('<<GNF_consolidadosClick_ctr.mthd_getConsumosClick>> :' + obj_opp.id );

			cls_consumos 		= new GNF_consolidarConsumosClick_cls( new list<string> { obj_opp.id }, new set<string>(), true  );
			lst_consolidado 	= cls_consumos.mthd_consumoCubiertosMes();

			cls_consumos.mthd_ConsumoCubiertototalPS( lst_consolidado );

			cls_consumos.mthd_ConsumoCubiertoAnterior( lst_consolidado );

			cls_consumos.mthd_consumoDisponiblePeriodo( lst_consolidado );

			if( obj_opp.RecordType.DeveloperName.equalsIgnoreCase('Click') )
				cls_consumos.mthd_consumoDisponiblePeriodo( lst_consolidado );
			else
				cls_consumos.mthd_consumoDisponiblePeriodoUnclick( lst_consolidado );

			if (lst_consolidado != null && !lst_consolidado.isEmpty()){
				for( GNF_consolidarConsumosClick_cls.wrp_resultado obj_wrp : lst_consolidado )
					{
						obj_wrp.str_consumoCubiertoMes				=	obj_wrp.str_consumoCubiertoMes!=null ? '' + Decimal.valueOf( obj_wrp.str_consumoCubiertoMes ).format() : '0';
						obj_wrp.str_consumoEstimado					=	obj_wrp.str_consumoEstimado!=null ? '' + Decimal.valueOf( obj_wrp.str_consumoEstimado ).format() : '0';
						obj_wrp.str_consumoCubiertototalPS			=	obj_wrp.str_consumoCubiertototalPS!=null ? '' + Decimal.valueOf( obj_wrp.str_consumoCubiertototalPS ).format(): '0';
						obj_wrp.str_consumoCubiertoTotalPeriodo		=	obj_wrp.str_consumoCubiertoTotalPeriodo !=null ? '' + Decimal.valueOf( obj_wrp.str_consumoCubiertoTotalPeriodo ).format() : '0';
						obj_wrp.str_consumoCubiertoAnterior			=	obj_wrp.str_consumoCubiertoAnterior !=null ? '' + Decimal.valueOf( obj_wrp.str_consumoCubiertoAnterior ).format() : '0';
						obj_wrp.str_consumoDisponiblePeriodo		=	obj_wrp.str_consumoDisponiblePeriodo !=null ? '' + Decimal.valueOf( obj_wrp.str_consumoDisponiblePeriodo ).format(): '0';
					}
			}


		}//ends mthd_getConsumosClick

}//ends GNF_consolidadosClick_ctr