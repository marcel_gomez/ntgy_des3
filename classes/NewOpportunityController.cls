/** 
 * =====================================================================================
 * @className        NewOpportunityController
 * @description      Class to create a new opportunity
 * @author           Lorena Sancho
 * @version          V1.0
 * @date created     2017/05/31
 * @last change      2017/05/31
 * =====================================================================================
 **/
public with sharing class NewOpportunityController {    
    
    private ApexPages.StandardSetController standardController;
    public String accId {get; set; }
    public String recordTypeId {get; set; }
    public List<SelectOption> typesList {
        get {
            List<RecordType> recordTypes = [SELECT Id, Name, SObjectType FROM RecordType WHERE SObjectType = 'Opportunity'];
            typesList = new List<SelectOption>();
            //typesList.add(new SelectOption('', '--None--'));
            /*for (RecordType rt: recordTypes){
                typesList.add(new SelectOption(rt.Id, rt.Name));
            }*/
            
            for(RecordTypeInfo info: Opportunity.SObjectType.getDescribe().getRecordTypeInfos()) {
                if(info.isAvailable() & info.getName() <> 'Master' & info.getName() <> 'Principal') {
                typesList.add(new SelectOption(info.getRecordTypeId(), info.getName()));
            }
            }
            return typesList;
        }
        set;
    }
    public Contact contact {get; set;}
    public Account account {get; set;}
    private String accountName {get; set;}
    
    public NewOpportunityController (ApexPages.StandardSetController controller) {   
        this.standardController = controller;
        //Account id --> New offer created from an account record
        if (String.IsNotBlank(ApexPages.currentPage().getParameters().get('id'))) {
            accId = ApexPages.currentPage().getParameters().get('id');
            System.debug('accountId: ' + accId);
            List<Contact> contacts = [SELECT Id, Name,  Account.Name FROM Contact Where AccountId = :accId];
            if (contacts  != null && contacts.size() > 0) {
                contact = contacts[0];
                accountName = contact.Account.Name;
            } else {
                List<Account> accounts = [SELECT Id, Name FROM Account Where Id = :accId];
                if (accounts != null && accounts.size() > 0) {
                    account = accounts[0];
                    accountName = account.Name;
                }
            }
            
        }
    }
    
    public PageReference returnPage() {
        
        //This method only applies in Classic Mode
        PageReference np = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Opportunity&retURL=%2F006%2Fo' + (String.IsNotBlank(accountName) ? '&opp4=' + accountName : '') + 
                                            (String.IsNotBlank(accId) ? '&opp4_lkid=' + accId : '') + (String.IsNotBlank(accountName) ? '&opp3=' + accountName : '') + 
                                            '&opp11=' + Label.GNF_In_Process + '&opp9=' + System.Today().format() +
                                            '&save_new_url=%2F006%2Fe%3FretURL%3D%252F006%252Fo');
        
        
        np.setRedirect(true);
        return np;
        
    }
    
    public PageReference getRecordTypeId() { return null; }

}