public class AG_LogicAccount {

    //-- CONSTANTS
    private static final String ERROR_MSG_TOOMANYFAV = label.ApG_tooManyFavouriteAccounts;  

    private static final List<String> BUSINESS_OBJECTS = AG_SecurityRecalculationProcessUtil.BUSINESS_OBJECTS;

    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicAccount() {}

    //-- PUBLIC METHODS
 
   /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<Account__c>}
     */
    public void assignOwnerFromEmpleado(final List<Account> newList, final Map<Id, Account> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: start' + newList.size());

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);        
        Map<String,User> userTable =  AG_OrganizationModelUtil.getAllUsers();

        // for (String k : userTable.keySet()) 
        //   System.debug('Map users:' + userTable.get(k));
        // for (String ke : empleadoTable.keySet())
        //   System.debug('Map Empleados:' + empleadoTable.get(ke));


        if (oldMap==null) oldMap = new Map<Id, Account>(); //just to avoid null checks
        
        // Iterate over accounts and find and assign owner based on its empleado
        for(Account theAccount : newList) {

          if(theAccount.Gestor_External_Key__c != null) {
            
            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theAccount.Id), theAccount)) {    

              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theAccount.managers__c = managersEmpleados.get(theAccount.Gestor_External_Key__c);
                              
                String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theAccount.Gestor_External_Key__c,empleadoTable,userTable);
                if(assignedOwner != null) {
                  if (theAccount.OwnerId!=assignedOwner)  //el owner del Account cambia, lo marcamos para recalcular
                    theAccount.recalcular_permisos_objetos__c =true;
                  theAccount.OwnerId = assignedOwner;
                } else {
                  //TODO: What todo in if not find a valid user?
                  final String errMsg = 'No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_customer=' + theAccount.ID_Customer__c + ' - gestor_external_key='+ theAccount.Gestor_External_Key__c;
                  //Se comenta debug(Victor Velandia (VV))
                  //System.debug(loggingLevel.Error, errMsg);
                  //throw new AccountLogicException(errMsg);
                }
            }
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '@@@@ No esta informada el gestor para el cliente :' + theAccount.ID_Customer__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new AccountLogicException('No esta informada el gestor para el cliente :' + theAccount.ID_Customer__c);
          }
        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }



    /**
     *  Parece que lo que hace esta función es controlar que un gestor no tenga asignada más de 10
     *  cuentas con marca 'Favorito__c = true'
     *  Posiblemente con SELECT y GROUP BY se calcularía más facilmente
     *  Esta mal planteado: No se valida que el update efectivamente cambie el campo Favorito,
     *  además, la función no se llama en before insert, por lo que pueden entrar más de 10 por allí.
     * 
     * @param newList {@code List<Account>}
     */
    public void checkOnly10FavByGestor(final List<Account> newList) {

        // Auxiliar collections
        Set<Id> gIds = new Set<Id>();
        Map<Id,Id> CuentaGestor = new Map<Id,Id>();
        Map<Id,Integer> gestores = new Map<Id,Integer>();
        // GEt gestores Id
        for(Account theAccount : newList) {
            if (theAccount.Gestor__c != null) {
                gIds.add(theAccount.Gestor__c);
                CuentaGestor.put(theAccount.id,theAccount.Gestor__c);
            }
        }

        // Retrieve Favs
        for(Account acc :[SELECT Id, Gestor__c FROM Account WHERE Favorito__c = true AND Gestor__c IN :gIds]) {

            if (gestores.get(acc.Gestor__c) == null) {
                gestores.put(acc.Gestor__c,1);
            } else {
                Integer num = gestores.get(acc.Gestor__c);
                num = num + 1;
                gestores.put(acc.Gestor__c,num);
            }
         }
         // Add error if fav number above 10
         for( Account a : newList ) {
            if (gestores.get(a.gestor__c) > 10) {
                a.addError(ERROR_MSG_TOOMANYFAV);
            }
        }
    }

    //-- PRIVATE METHODS
    /*
     * @return true if the field value "Gestor__c" has changed or oldAccount is null.
    */
    private boolean hasChangedGestor(Account oldAccount, Account newAccount) {

      if (oldAccount==null) return true;   //new object
      return (oldAccount.Gestor__c <> newAccount.Gestor__c);
    }      

    /*
    * @return true if the owner has changed or oldAccount is null.
    */
    private boolean hasChangedOwner(Account oldAccount, Account newAccount) {

      if (oldAccount==null) return true;   //new object
      return (oldAccount.ownerId <> newAccount.OwnerId);
    }      


    //-- CUSTOM EXCEPTION
    public class AccountLogicException extends Exception {}


//---------------------------------------

  /*
   * Busca en las tablas de objetos de negocio si hay algun registro asociado al Account y con Owner especificados en parametros.
  *  Si el método se llama AfterUpdate, no tendria que encontrar nada para poder borrar. 
  *  Si se llama Before Update, encontrara el del valor que cambia....que todavía no lo ha hecho
  */
  public static Boolean canRemoveAccountSharedRecords(Id accountId, Id ownerId) {

    String soqlTemplate = 'SELECT count() FROM %TABLE% where Cliente__c = :accountId and OwnerId = :OwnerId';


    for (String tableName : BUSINESS_OBJECTS) {
      String soql = soqlTemplate.replace('%TABLE%', tableName);
      Integer numRows = Database.countQuery(soql);
      if (numROws>0)
        return false;   //Si encuentra algo, ya no hace falta borrarlo.
    }
    return true;
  }

  /*
  * Convierte una lista de Id en un string en forma: 'xxxxid1','xxxxid2','xxxid3',....
  */
  private static String idList2String(Id[] idList) {

    String out ='';
    for (Id i : idList) {
      if (out.length()>0) out = out + ',';
      out = out + '\'' + i +'\'';
    }
    return out;

  }

}