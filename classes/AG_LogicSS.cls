public class AG_LogicSS {


    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicSS() {}

    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<SSs__c>}
     */
    public void assignOwnerFromEmpleado(final List<SSs__c> newList, final Map<Id, SSs__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: start' + newList.size());

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();

        if (oldMap==null) oldMap = new Map<Id, SSs__c>(); //just to avoid null checks

        // Iterate over sss and find and assign owner based on its empleado
        for(SSs__c theSS : newList) {

          if(theSS.Gestor_External_Key__c <> null) {

            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theSS.Id), theSS)) {

              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theSS.managers__c = managersEmpleados.get(theSS.Gestor_External_Key__c);
              
              String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theSS.Gestor_External_Key__c,empleadoTable,userTable);
              if(assignedOwner <> null) {
                theSS.OwnerId = assignedOwner;
              } else {
                //TODO: What todo in if not find a valid user?
                final String errMsg = '#ERROR# No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_ss=' + theSS.ID_SS__c + ' - gestor_external_key='+ theSS.Gestor_External_Key__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug(loggingLevel.Error, errMsg);
                //throw new SSLogicException(errMsg);
              }
            }   
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '#ERROR# No esta informada el gestor para el ss :' + theSS.ID_SS__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new SSLogicException('No esta informada el gestor para el ss :' + theSS.ID_SS__c);
          }
        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }
    
    /**
     * Add sharing roles to SSs__c and Account to allow visibility to role hierachy.
     */
    public void setCustomSharing(final List<SSs__c> newList, final Map<Id, SSs__c> newMap,
                                 final List<SSs__c> oldList, final Map<Id, SSs__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ SetCustomSharing: start');

        //List of ss sharing records to be inserted
        List<SSs__Share> ssSharingList = new List<SSs__Share>();

        //List of account sharing records to be inserted
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, SSs__c>(); //just to avoid null checks

         //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();

        //Iterate over collection
        for(SSs__c theSS : newList) {

          Id ssId = theSS.Id;
          SSs__c oldSS = oldMap.get(ssId);
          SSs__c newSS = theSS;

          //Se comenta debug(Victor Velandia (VV))
          /*System.debug('@@@@ ID ss:' + ssId);
          System.debug('@@@@ Old ss:' + oldSS);
          System.debug('@@@@ New ss:' + newSS);*/

          //Get only new records or records whose cliente has changed or owner has changed
          if (hasChangedCliente(oldSS, newSS)) {

            if (oldSS!=null)  //si es insert no hay previos que borrar
              Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(oldSS.Id);

              //get the role of the user assigned to the record account
            Id ownerSS= theSS.ownerId;
            Id ownerRelatedAccount = theSS.Gestor_Cliente__c;
            //Se comenta debug(Victor Velandia (VV))
            //System.debug('@@@@ SS gestion_cliente__C:' + ownerSS);

            if (ownerSS!=ownerRelatedAccount) {
              List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount,allUsers,allRoles,allGroups);
              for (Id grpOrUsrId : gruposRoleSuperiores) {
                SSs__Share clienteAbobeRoleSharing = createSSSharedRecord(theSS, grpOrUsrId);
                ssSharingList.add(clienteAbobeRoleSharing);
              }
            }
          }

          //Si es un nuevo SS, calculamos los permisops para el Account en el trigger,
          //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.

          //El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
          if (oldSS!=null && hasChangedOwner(oldSS, newSS)) {

            accountToRecalculate.add(theSS.Cliente__c);

          //Se trata de un alta de suministro --> calculamos permisos a dar al Account
          } else if (oldSS==null) { 

            if(!String.isBlank(theSS.Cliente__c)) { //No hay nada que hacer, no hay cliente

              Id accountOwnerId = theSS.Gestor_Cliente__c;
              Id ssOwnerId = theSS.OwnerId;

              //Si owner del cliente es el mismo que el del objeto, no es necesario dar más permisos
              if (accountOwnerId!=ssOwnerId) {

                //Se comenta debug(Victor Velandia (VV))
                /*System.debug('@@@Account Propietrario ss:' + ssOwnerId);
                System.debug('@@@Account Propietrario cuenta:' + accountOwnerId);
                System.debug('@@@Account Cliente:' + theSS.cliente__r.name);*/

                List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ssOwnerId,allUsers,allRoles,allGroups);
                for (Id grpOrUsrId : grpOrUsrIdToShare) {
                  AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(theSS.Cliente__c, grpOrUsrId);
                  accountSharingList.add(accountSharing);
                }

                //Se comenta debug(Victor Velandia (VV))
                //System.debug('###Account Account Share:' + accountSharingList);
              }
            }
          }
        }

        if (!accountToRecalculate.isEmpty()) {
          AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!ssSharingList.isEmpty())
          upsert ssSharingList;


        if(!accountSharingList.isEmpty())
          upsert accountSharingList;

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS

   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldSS is null.
    */
    @testVisible
    private boolean hasChangedGestor(SSs__c oldSS, SSs__c newSS) {

      if (oldSS==null) return true;   //new object
      return (oldSS.Gestor__c <> newSS.Gestor__c);
    }      

    /*
     * @return true if the field value "Cliente__c" has changed or oldSS is null.
    */
    @testVisible
    private boolean hasChangedCliente(SSs__c oldSS, SSs__c newSS) {

      if (oldSS==null) return true;   //new object
      return (oldSS.cliente__c <> newSS.Cliente__c);
    }      

    /*
    * @return true if the owner has changed or oldSS is null.
    */
    @testVisible
    private boolean hasChangedOwner(SSs__c oldSS, SSs__c newSS) {

      if (oldSS==null) return true;   //new object
      return (oldSS.ownerId <> newSS.OwnerId);
    }      

    /**
     * Crea  SS__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible
    private SSs__Share createSSSharedRecord(final SSs__c ss, Id userOrGroup) {
        return new SSs__Share(
                  ParentId = ss.Id,
                  UserOrGroupId = userOrGroup, //ss.Gestor_Cliente__c,
                  AccessLevel = SHARE_MODE_READ,
                  RowCause = Schema.SSs__Share.RowCause.Apex_Sharing__c);
    }


    /*
    * Remove all the sharing added from Apex.
    */
    @testVisible
    private Integer deleteSSShares(Id ssId) {

      List<SSs__Share> shareList = [SELECT Id FROM SSs__Share WHERE parentId = :ssId AND RowCause = :Schema.SSs__Share.RowCause.Apex_Sharing__c];
      Integer count = shareList.size();
      if (count>0)
        delete shareList;

      return count;
    }
   

    //-- CUSTOM EXCEPTION
    public class SSLogicException extends Exception {}

}