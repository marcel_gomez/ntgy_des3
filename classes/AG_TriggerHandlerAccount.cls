public class AG_TriggerHandlerAccount {

    //LOGIC 
    private final AG_LogicAccount logic;

    //SINGLETON PATTERN
    
    //CONSTRUCTOR
    private AG_TriggerHandlerAccount() {
        this.logic = new AG_LogicAccount();
    }

    private static AG_TriggerHandlerAccount instance;

    public static AG_TriggerHandlerAccount getInstance() {

        if (instance == null) instance = new AG_TriggerHandlerAccount();
        return instance;
    }    
    
    //ACCOUNT HANDLER      
    public void onBeforeInsert(final List<Account> newList) {

        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<Account> newList, final Map<Id, Account> newMap,
                               final List<Account> oldList, final Map<Id, Account> oldMap) {

        this.logic.assignOwnerFromEmpleado(newList, oldMap);
        this.logic.checkOnly10FavByGestor(newList);
    }

    /**
    * description: Se comenta metodos (onAfterInsert, onAfterUpdate, onAfterDelete, onBeforeDelete, onAfterUnDelete) 
    * debido a que no tienen ninguna logica que se cumpla para su llamado
    * @param:   Req_RD_1_4 Metodos sin logica
    * @author:  victor.velandia@vass.es
    * @date:    20/06/2018
    * Begin 
    */
    /*public void onAfterInsert(final List<Account> newList, final Map<Id, Account> newMap){
       
    }*/

    /*public void onAfterUpdate(final List<Account> newList, final Map<Id, Account> newMap,
                               final List<Account> oldList, final Map<Id, Account> oldMap){
       
    }*/

    /*public void onAfterDelete(final List<Account> oldList, final Map<Id, Account> oldMap){
    }*/

    /*public void onBeforeDelete(final List<Account> oldList, final Map<Id, Account> oldMap){
    }*/

    /*public void onAfterUnDelete(final List<Account> oldList){
    }*/

    //End

}