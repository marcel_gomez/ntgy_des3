/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Batch encargado de realizar el merge de tablas para plantillas iterables.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-10-20		Manuel Medina (MM)		Definicion inicial de la clase.
* @version	2.0		2018-02-01		Manuel Medina (MM)		Se realiza encapsulamiento de logica para ser usada de
*															forma sincrona o asincrona.
*********************************************************************************************************/
global class GNF_PDFGenerator_bch implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful{
	
	global String strQuery												{get; set;}
	global String strTemplateUniqueName									{get; set;}
	global String strMainRecordId										{get; set;}
	global String strSObjectType										{get; set;}
	
	global GNF_PDFGenerator_ctr ctrPDFGenerator							{get; set;}
	
	global GNF_PDFGenerator_bch( String strTemplateUniqueName, String strMainRecordId, String strSObjectType ){
		GNF_PDFAsincronos__c objAsyncPDF								= new GNF_PDFAsincronos__c();
		objAsyncPDF														= [SELECT Id
																			FROM GNF_PDFAsincronos__c
																			WHERE GNF_Identificador__c =: strTemplateUniqueName + strMainRecordId.left( 15 ) + strSObjectType
																			LIMIT 1
																		];
		
		this.strQuery													= 'SELECT Id, ' +
																			'Name, ' +
																			'ParentId, ' +
																			'Body ' +
																		'FROM Attachment ' +
																		'WHERE ParentId = \'' + objAsyncPDF.Id + '\' ' +
																		'ORDER BY Name ASC';
																		
		this.strTemplateUniqueName										= strTemplateUniqueName;
		this.strMainRecordId											= strMainRecordId;
		this.strSObjectType												= strSObjectType;
		
		this.ctrPDFGenerator											= new GNF_PDFGenerator_ctr();
	}
	
	/**
	* @Method: 		start
	* @param: 		Database.BatchableContext BC
	* @Description:	Retorna los registros a procesar.
	* @author 		Manuel Medina - 20102017
	*/
	global Database.QueryLocator start( Database.BatchableContext BC ) {
		
		return Database.getQueryLocator( strQuery );
	}
	
	/**
	* @Method: 		execute
	* @param: 		Database.BatchableContext BC
	* @param: 		List<SObject> scope
	* @Description:	Se encarga de procesar registros para realizar el merge de las tablas contenidas.
	* @author 		Manuel Medina - 20102017
	*/
	global void execute( Database.BatchableContext BC, List<SObject> scope ){
		scope															= executeLogic( scope );
	}
	
	/**
	* @Method: 		executeLogic
	* @param: 		List<SObject> scope
	* @Description:	Implementa la logica del metodo execute del batch para ser accesible fuera de contexto batch.
	* @author 		Manuel Medina - 28112017
	*/
	global List<SObject> executeLogic( List<SObject> scope ){
		List<Attachment> lstAttachments									= new List<Attachment>();
		
		ctrPDFGenerator.strPreviousGrouperValue							= '';
		ctrPDFGenerator.strCurrentGroupuerValue							= '';
		
		for( SObject sObjAttachment : scope ){
			Attachment objAttachment									= new Attachment();
			objAttachment												= ( Attachment ) sObjAttachment;
			
			Integer intFileToMerge										= Integer.valueOf( objAttachment.Name.substringAfterLast( '_' ) );
			ctrPDFGenerator.strHTML										= objAttachment.Body.toString();
			ctrPDFGenerator.mergeHTMLTables( intFileToMerge );
			
			objAttachment.Body											= Blob.valueOf( ctrPDFGenerator.strHTML );
			
			lstAttachments.add( objAttachment );
		}
		
		update lstAttachments;
		
		return scope;
	}
	
	/**
	* @Method: 		finish
	* @param: 		Database.BatchableContext BC
	* @Description:	Actualiza el registro de pdf asincrono relacionado a los ficheros procesados.
	* @author 		Manuel Medina - 20102017
	*/
	global void finish( Database.BatchableContext BC ){
		GNF_PDFAsincronos__c objAsyncPDF								= new GNF_PDFAsincronos__c();
		objAsyncPDF														= finishLogic();
		
		System.enqueueJob( new GNF_PDFAsincronosTriggerHandler_cls( System.JSON.serialize( new List<GNF_PDFAsincronos__c>{ objAsyncPDF } ) ) );
	}
	
	/**
	* @Method: 		finishLogic
	* @param: 		Database.BatchableContext BC
	* @Description:	Implementa la logica del metodo finish del batch para ser accesible fuera de contexto batch.
	* @author 		Manuel Medina - 28112017
	*/
	global GNF_PDFAsincronos__c finishLogic(){
		GNF_PDFGenerator_ctr ctrPDFGenerator							= new GNF_PDFGenerator_ctr( strTemplateUniqueName, strMainRecordId, strSObjectType );
		String strHTML													= '';
		GNF_PDFAsincronos__c objAsyncPDF								= new GNF_PDFAsincronos__c();
		
		objAsyncPDF														= [SELECT Id,
																				GNF_Identificador__c,
																				GNF_Objeto__c,
																				GNF_IdRegistro__c,
																				GNF_Plantilla__c
																			FROM GNF_PDFAsincronos__c
																			WHERE GNF_Identificador__c =: strTemplateUniqueName + strMainRecordId.left( 15 ) + strSObjectType
																			LIMIT 1
																		];
		
		List<Attachment> lstAttachments									= new List<Attachment>( [
																			SELECT Id,
																				Name,
																				ParentId,
																				Body
																			FROM Attachment
																			WHERE ParentId =: objAsyncPDF.Id
																		] );
		
		for( Attachment objAttachment : lstAttachments ){
			strHTML														+= objAttachment.Body.toString();
		}
		
		strHTML															= ctrPDFGenerator.ctrPDFTemplates.mapTemplateById.get( ctrPDFGenerator.ctrPDFTemplates.objTemplate.Id ).GNF_HTMLGenerado__c.normalizeSpace().replaceAll( '(<div class="logoSection">.*<\\/div>)<div class="tablePpal">', '<div class="tablePpal">' ).replaceAll( '(<div class="tablePpal">.*<\\/div>)', strHTML );
		strHTML															= strHTML.replace( '@VERSION@', ctrPDFGenerator.ctrPDFTemplates.mapTemplateById.get( ctrPDFGenerator.ctrPDFTemplates.objTemplate.Id ).GNF_Version__c );
		
		delete lstAttachments;
		Database.emptyRecycleBin( lstAttachments );
		
		Attachment objAttachment										= new Attachment();
		objAttachment.Name												= strTemplateUniqueName + strMainRecordId.left( 15 ) + strSObjectType;
		objAttachment.ParentId											= objAsyncPDF.Id;
		objAttachment.Body												= Blob.valueOf( strHTML );
		insert objAttachment;
		
		return objAsyncPDF;
	}
}