public class AG_LogicOferta {


    //-- CONSTANTS
    private final static String SHARE_MODE_EDIT = 'Edit';
    private final static String SHARE_MODE_READ = 'Read';
    private final static String SHARE_MODE_NONE = 'None';
    
    //-- ATTRIBUTES 

    //-- CONSTRUCTOR
    public AG_LogicOferta() {}

    //-- PUBLIC METHODS

    /**
     * Change the record owner depending the Gestor.
     * @param newList {@code List<Ofertas__c>}
     */
    public void assignOwnerFromEmpleado(final List<Ofertas__c> newList, final Map<Id, Ofertas__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: start' + newList.size());

        // Auxiliar collections
        Map<String,Empleado__c> empleadoTable = AG_OrganizationModelUtil.getAllEmpleados();
        Map<String,String> managersEmpleados = AG_OrganizationModelUtil.getManagersEmpleados(empleadoTable);
        Map<String,User> userTable = AG_OrganizationModelUtil.getAllUsers();

        if (oldMap==null) oldMap = new Map<Id, Ofertas__c>(); //just to avoid null checks

        // Iterate over ofertas and find and assign owner based on its empleado
        for(Ofertas__c theOferta : newList) {

          if(theOferta.Gestor_External_Key__c <> null) {

            //if gestor has not change, it is not necessary to recalculate owner.
            if (hasChangedGestor(oldMap.get(theOferta.Id), theOferta)) {

              //set a list with the managers of the Gestor__c in the Empleado__c hierachy
              theOferta.managers__c = managersEmpleados.get(theOferta.Gestor_External_Key__c);

              String assignedOwner = AG_OrganizationModelUtil.recursiveUserFind(theOferta.Gestor_External_Key__c,empleadoTable,userTable);
              if(assignedOwner <> null) {
                theOferta.OwnerId = assignedOwner;
              } else {
                //TODO: What todo in if not find a valid user?
                final String errMsg = '#ERROR# No se ha podido encontrar un usuario activo en la jerarquia de empleados: id_oferta=' + theOferta.ID_Oferta__c + ' - gestor_external_key='+ theOferta.Gestor_External_Key__c;
                //Se comenta debug(Victor Velandia (VV))
                //System.debug(loggingLevel.Error, errMsg);
                //throw new OfertaLogicException(errMsg);
              }
            }   
          } else {
              //TODO: What to do if Gestor_External_key__c it is not set?
              final String errMsg = '@@@@ No esta informada el gestor para la oferta :' + theOferta.ID_Oferta__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug(loggingLevel.Error, errMsg);
              //throw new OfertaLogicException('No esta informada el gestor para la oferta :' + theOferta.ID_Oferta__c);
          }
        }

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ assignOwnerFromEmpleado: end');
    }
    
    /**
     * Add sharing roles to Ofertas__c and Account to allow visibility to role hierachy.
     */
    public void setCustomSharing(final List<Ofertas__c> newList, final Map<Id, Ofertas__c> newMap,
                                 final List<Ofertas__c> oldList, final Map<Id, Ofertas__c> oldMap) {

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ SetCustomSharing: start');

        //List of oferta sharing records to be inserted
        List<Ofertas__Share> ofertaSharingList = new List<Ofertas__Share>();

        //List of account sharing records to be inserted
        List<AccountShare> accountSharingList = new List<AccountShare>();

        if (oldMap==null) oldmap = new Map<Id, Ofertas__c>(); //just to avoid null checks

        //Load in memory organization model informacion to avoid too much SOQL sentences
        final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
        final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
        final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

        //Account.id que se han modificado y hay que recalcular permisos en proceso batch.
        Set<Id> accountToRecalculate = new Set<Id>();

        //Iterate over collection
        for(Ofertas__c theOferta : newList) {

            Id ofertaId = theOferta.Id;
            Ofertas__c oldOferta = oldMap.get(ofertaId);
            Ofertas__c newOferta = theOferta;

            //Se comenta debug(Victor Velandia (VV))
            /*System.debug('@@@@ ID oferta:' + ofertaId);
            System.debug('@@@@ Old oferta:' + oldOferta);
            System.debug('@@@@ New oferta:' + newOferta);*/

            //Get only new records or records whose cliente has changed or owner has changed
            if (hasChangedCliente(oldOferta, newOferta)) {

              if (oldOferta!=null)  //si es insert no hay previos que borrar
                Integer sharesRemoved = AG_OrganizationModelUtil.removeShares(oldOferta.Id);

              //get the role of the user assigned to the record account
              Id ownerOferta = theOferta.ownerId;
              Id ownerRelatedAccount = theOferta.Gestor_Cliente__c;
              //Se comenta debug(Victor Velandia (VV))
              //System.debug('@@@@ Oferta gestion_cliente__C:' + ownerRelatedAccount);

              if (ownerOferta!=ownerRelatedAccount) {
                List<Id> gruposRoleSuperiores = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ownerRelatedAccount,allUSers, allRoles, allGroups);
                for (Id grpOrUsrId : gruposRoleSuperiores) {
                  Ofertas__Share clienteAbobeRoleSharing = createOfertaSharedRecord(theOferta, grpOrUsrId);
                  ofertaSharingList.add(clienteAbobeRoleSharing);
                }
              }
            }

            //Si es un nuevo Suministro, calculamos los permisops para el Account en el trigger,
            //pero si ha cambiado gestor/owner, se recalcula todo al final en un proceso batch.

            //El owner ha cambiado y no se trata de un alta --> lo dejamos para el batch
            if (oldOferta!=null && hasChangedOwner(oldOferta, newOferta)) {

              accountToRecalculate.add(theOferta.Cliente__c);

            //Se trata de un alta de suministro --> calculamos permisos a dar al Account
            } else if (oldOferta==null) { 

                if(!String.isBlank(theOferta.Cliente__c)) { //No hay nada que hacer, no hay cliente

                  Id accountOwnerId = theOferta.Gestor_Cliente__c;
                  Id ofertaOwnerId = theOferta.OwnerId;

                  //Si owner del cliente es el mismo que el del objeto, no es necesario dar más permisos
                  if (accountOwnerId!=ofertaOwnerId) {

                    //Se comenta debug(Victor Velandia (VV))
                    /*System.debug('@@@Account Propietrario oferta:' + ofertaOwnerId);
                    System.debug('@@@Account Propietrario cuenta:' + accountOwnerId);
                    System.debug('@@@Account Cliente:' + theOferta.cliente__r.name);*/

                    List<Id> grpOrUsrIdToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(ofertaOwnerId,allUSers,allRoles,allGroups);
                    for (Id grpOrUsrId : grpOrUsrIdToShare) {
                      AccountShare accountSharing = AG_OrganizationModelUtil.createAccountSharedRecord(theOferta.Cliente__c, grpOrUsrId);
                      accountSharingList.add(accountSharing);
                    }

                    //Se comenta debug(Victor Velandia (VV))
                    //System.debug('###Account Account Share:' + accountSharingList);
                  }
                }
            }
        }

        if (!accountToRecalculate.isEmpty()) {
          AG_OrganizationModelUtil.updateAccountsToRecalculate(new List<Id>(accountToRecalculate));
        }

        if(!ofertaSharingList.isEmpty())
          upsert ofertaSharingList;


        if(!accountSharingList.isEmpty())
          upsert accountSharingList;

        //Se comenta debug(Victor Velandia (VV))
        //System.debug('@@@@ setCustomSharing: end');
    }


    //-- PRIVATE METHODS

   
  
    /*
     * @return true if the field value "Gestos__c" has changed or oldOferta is null.
    */
    @testVisible
    private boolean hasChangedGestor(Ofertas__c oldOferta, Ofertas__c newOferta) {

      if (oldOferta==null) return true;   //new object
      return (oldOferta.Gestor__c <> newOferta.Gestor__c);
    }      

    /*
     * @return true if the field value "Cliente__c" has changed or oldOferta is null.
    */
    @testVisible
    private boolean hasChangedCliente(Ofertas__c oldOferta, Ofertas__c newOferta) {

      if (oldOferta==null) return true;   //new object
      return (oldOferta.cliente__c <> newOferta.Cliente__c);
    }      

    /*
    * @return true if the owner has changed or oldOferta is null.
    */
    @testVisible
    private boolean hasChangedOwner(Ofertas__c oldOferta, Ofertas__c newOferta) {

      if (oldOferta==null) return true;   //new object
      return (oldOferta.ownerId <> newOferta.OwnerId);
    }      

    /**
     * Crea  Oferta__Share para objeto y usuario/grupo especioficados.
     */
    @testVisible
    private Ofertas__Share createOfertaSharedRecord(final Ofertas__c oferta, Id userOrGroup) {
        return new Ofertas__Share(
                  ParentId = oferta.Id,
                  UserOrGroupId = userOrGroup, //oferta.Gestor_Cliente__c,
                  AccessLevel = SHARE_MODE_READ,
                  RowCause = Schema.Ofertas__Share.RowCause.Apex_Sharing__c);
    }


    /*
    * Remove all the sharing added from Apex.
    */
    @testVisible
    private Integer deleteOfertaShares(Id ofertaId) {

      List<Ofertas__Share> shareList = [SELECT Id FROM Ofertas__Share WHERE parentId = :ofertaId AND RowCause = :Schema.Ofertas__Share.RowCause.Apex_Sharing__c];
      Integer count = shareList.size();
      if (count>0)
        delete shareList;

      return count;
    }
   

    //-- CUSTOM EXCEPTION
    public class OfertaLogicException extends Exception {}

}