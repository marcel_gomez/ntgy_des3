@isTest
private class AG_TestLogicSS {

    static AG_LogicSS logic = new AG_LogicSS();


    @testSetup
    private static void setup() {
        System.debug('Test setup');

        /*  Se crean 7 roles en dos ramas con raiz comun (testRole)
            --testRole
            |   |_testRole2
            |       |_testRole3
            |           |_testRole4     --> User1
            |___testRoleA
                    |_testRoleB
                        |_testRoleC --> User2
        */

        UserRole topRole = new UserRole(name='RoleT');
        insert topRole;
        UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
        insert userRole2;
        UserRole userRole3 = new UserRole(name='Role3', parentRoleId = userRole2.Id);
        insert userRole3;
        UserRole userRole4 = new UserRole(name='Role4', parentRoleId = userRole3.id);
        insert userRole4;
        System.debug('userRole:' + userRole.id + '- userRrole2:' + userRole2.id + '- userRrole3:' + userRole3.id + '- userRole4:' + userRole4.id);

        UserRole userRoleA = new UserRole(name='RoleA', parentRoleId = topRole.id);
        insert userRoleA;
        UserRole userRoleB = new UserRole(name='RoleB', parentRoleId = userRoleA.id);
        insert userRoleB;
        UserRole userRoleC = new UserRole(name='RoleC', parentRoleId = userRoleB.id);
        insert userRoleC;

        /* Se crean 2 usuarios
            User1 y User2) asignados a los roles Role4 y RoleC y a los
            empleados__c 'Empleado1' (k=111) y 'Empleado2' (k=222) respectivamente.
        */

        System.debug('Look for Standard USer profile ');
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
  
        User theUser1 = new User(
            LastName = 'User1',                 FirstName='Test',   
            Alias = 'tu1',                      Email = 'test.user1@edelta.com',
            Username = 'test.user1@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole4.Id,           ID_Empleado__c= '111',isActive=true);

 
        insert theUser1;
        System.debug('theUser1:' + theUser1);
        System.debug('theUser1 active:' + theUser1.isActive);

        User theUser2 = new User(
            LastName = 'User2',                 FirstName='Test',
            Alias = 'tu',                       Email = 'test.user2@edelta.com',
            Username = 'test.user2@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US', 
            UserRoleId= userRoleC.Id,           ID_Empleado__c='222', isActive=true);

        insert theUser2;
        System.debug('theUser2:' + theUser2);
        System.debug('theUser2 active:' + theUser2.isActive);

        User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
        System.runAs(adminUser) {

            Empleado__c empleado1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='111');
            insert empleado1;
            Empleado__c empleado2 = new Empleado__c(name='Empleado2', OwnerId=theUSer2.id, ID_Empleado__c='222');
            insert empleado2;

            Account cliente1 = new Account(name='Account1', Gestor__c=empleado1.Id, OwnerId=theUser1.id);
            insert cliente1;
            System.debug('@#@# Account1:' + cliente1);

            Account cliente2 = new Account(name='Account2', Gestor__c=empleado2.Id, OwnerId=theUser2.id);
            insert cliente2;
            System.debug('@#@# Account2:' + cliente2);

            List<Account> xx = [SELECT Id, name, OwnerId FROM Account WHERE name like 'Account_'];
            System.debug('@#@# List accounts:' + xx);
        }   
    }
    
    @isTest 
    static void setCustomSharing_test() {

        //get mock roles
        Map<String, UserRole> mapMockRoles= getMockUserRoles();
        List<UserRole> mockRoles= mapMockRoles.values();

        //get mock users
        Map<String, User> mapMockUsers = getMockUsers();
        List<User> mockUsers =  mapMockUsers.values();
        
        //get mock groups
        Map<String, Group> mapMockGroups =getMockGroups();
        List<Group> mockGroups = mapMockGroups.values();

        //get empleados
        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        List<Empleado__c> mockEmpleados = mapMockEmpleados.values();

        //get Account 
        Map<String, Account> mapMockAccounts = getMockAccounts();
        List<Account> mockAccounts = mapMockAccounts.values();

        //Diferent owner in SSs and account
        //Cliente__c ='Test Cliente2' and owner is 'Test User2' who belongs to 'testRoleC' role group (upper roles are 'testRoleA', 'testRoleB', 'testRole')
        //Gestor__c = 'Test User1', the same his owner.
        //So need add shares for the Cliente__c hierachy: 'Test cliente2', 'testRoleA', 'testRoleB', 'testRole'

        //Call
        SSs__c ss2 = new SSs__c(
            name='Test SS2', 
            Cliente__C=mapMockAccounts.get('Account2').Id,
            gestor__c=mapMockEmpleados.get('Empleado1').Id);
        insert ss2;

        //build a set with the object Id that must be share to easily check
        Set<Id> ids = new Set<Id> { 
            mapMockUSers.get('User2').Id, 
            mapMockGroups.get('RoleA').Id, 
            mapMockGroups.get('RoleB').Id , 
            mapMockGroups.get('RoleT').Id };

        List<SSs__Share> ssShares = getSSsShare(ss2.Id);
        for (SSs__Share ss : ssShares) {
            System.assert(ids.contains(ss.UserOrGRoupId));
        }
    }

    @isTest 
    static void setCustomSharing_test2() {

        //get empleados
        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        List<Empleado__c> mockEmpleados = mapMockEmpleados.values();

        //get Account 
        Map<String, Account> mapMockAccounts = getMockAccounts();
        List<Account> mockAccounts = mapMockAccounts.values();

        //add new suply, so insert triggers are axecuted.
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));

        //check: Owner of the SSs and account are the same. No need to add shared.
        // Cliente__c='Test Cliente1' and his owner='Test User1' who belongs to 'testRole4' role group (upper roles are 'testRole3', 'testRole2', 'testRole') 
        // Gestor__c of SS__c is also 'Test User1', the same his owner.
        List<SSs__Share> ssShares = getSSsShare(ss1.Id);

        System.assert(ssShares.size()==0);

    }

    @isTest
    static void hasChangedGestor_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();

        //New suply with gestor__c =  Empleado1
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));

        //New suply with gestor__c =  Empleado2
        SSs__c ss2 = createSS('Test SS2', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado2'));

        Boolean haschange = logic.hasChangedGestor(ss1, ss2);
        System.assertEquals(haschange, true);

        haschange = logic.hasChangedGestor(ss1, ss1);
        System.assertEquals(haschange, false);

    }

    @isTest
    static void hasChangedCliente_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();

        //New suply with gestor__c =  Empleado1
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));        

        //New suply with gestor__c =  Empleado2
        SSs__c ss2 = createSS('Test SS2', mapMockAccounts.get('Account2'), mapMockEmpleados.get('Empleado1'));        

        Boolean haschange = logic.hasChangedCliente(ss1, ss2);
        System.assertEquals(haschange, true);

        haschange = logic.hasChangedCliente(ss1, ss1);
        System.assertEquals(haschange, false);

    }

    @isTest
    static void hasChangedOwner_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();

        //New suply with gestor__c =  Empleado1 who has associated User1
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));

        //New suply with gestor__c =  Empleado2 who has associated User2
        SSs__c ss2 = createSS('Test SS2', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado2'));        

        Boolean haschange = logic.hasChangedOwner(ss1, ss2);
        System.assertEquals(haschange, true);

        haschange = logic.hasChangedOwner(ss1, ss1);
        System.assertEquals(haschange, false);

    }
    
    @isTest
    static void deleteSSshares_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();

        //New suply with gestor__c =  Empleado1
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado2'));        

        List<SSs__share> sharesBefore = [SELECT id FROM SSs__share WHERE parentId = :ss1.id AND RowCause= :Schema.SSs__Share.RowCause.Apex_Sharing__c];
        System.assert(sharesBefore.size()>0);

        Integer numShareRemoved = logic.deleteSSshares(ss1.id);

        List<SSs__share> sharesAfter = [SELECT id FROM SSs__share WHERE parentId = :ss1.id AND RowCause= :Schema.SSs__Share.RowCause.Apex_Sharing__c];
        System.assertEquals(sharesAfter.size(), 0);
    }

    @isTest 
    static void createSSsharedRecord_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();

        //New suply with gestor__c =  Empleado1
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));

        SSs__Share share = logic.createSSsharedRecord(ss1, UserInfo.getUserId());

        System.assertEquals(share.userOrGroupId,UserInfo.getUserId());
        System.assertEquals(share.parentId,ss1.id);
    }


    @isTest static void onUpdateTrigger_Test() {

        Map<String, Empleado__c> mapMockEmpleados = getMockEmpleados();
        Map<String, Account> mapMockAccounts = getMockAccounts();
        SSs__c ss1 = createSS('Test SS1', mapMockAccounts.get('Account1'), mapMockEmpleados.get('Empleado1'));        
        ss1.name = ss1.name + '_';
        update ss1;

        System.assert(true);

        delete ss1;

        System.assert(true);
    }


////////////// PRIVATE METHODS /////////////

    private static List<SSs__Share> getSSsShare(Id ssId) {

        List<SSs__Share> ssShares = 
            [SELECT AccessLevel,Id,IsDeleted,ParentId,RowCause,UserOrGroupId FROM SSs__Share WHERE parentId = :ssId AND RowCause='Apex_Sharing__c'];
        return ssShares;
    }

    private static Map<String,UserRole> getMockUserRoles() {

        //get mock roles
        List<UserRole> mockRoles= [select Id, name FROM UserRole WHERE name like 'Role_'];
        System.debug('MockRoles:' + mockRoles);
        Map<String,UserRole> mapMockRoles = new Map<String,USerRole>();
        for (UserRole r : mockRoles) 
            mapMockRoles.put(r.name,r);
        System.debug('Map MockRoles:' + mapMockRoles);

        return mapMockRoles;
    }

    private static Map<String,User> getMockUsers() {

        List<User> mockUsers = 
            [SELECT Id, username, firstName, lastName, profileid, UserRoleId FROM User WHERE username like 'test.user_@edelta.com'];
        System.debug('Mock users:' + mockUsers);
        Map<String,User> mapMockUSers = new Map<String,User>();
        for(User u: mockUSers) 
            mapMockUSers.put(u.lastName,u);
        System.debug('Map Mock users:' + mapMockUsers);

        return mapmockUSers;
    }

    private static Map<String,Group> getMockGroups() {
                //get mock groups
        List<Group> mockGroups = [select id, developerName FROM GROUP WHERE Type='Role' AND developerName like 'Role_'];
        System.debug('Mock groups:' + mockGroups);
        Map<String,Group> mapMockGRoups = new Map<String,GRoup>();
        for (GRoup r : mockGRoups) 
            mapMockGroups.put(r.developerName,r);
        System.debug('Map Mock groups:' + mapMockGroups);

        return mapmockGroups;
    }

    private static Map<String,EMpleado__c> getMockEmpleados() {

        List<Empleado__c> mockEmpleados = [SELECT Id, name FROM Empleado__c WHERE name like 'Empleado_'];
        System.debug('Mock empleados:' + mockEmpleados);
        Map<String,Empleado__c> mapMockEmpleados = new Map<String,Empleado__c>();
        for(Empleado__c e : mockEmpleados) 
            mapMockEmpleados.put(e.name,e);
        System.debug('Map Mock empleados:' + mapMockEmpleados);

        return mapmockEmpleados;    
    }

    private static Map<String,Account> getMockAccounts() {

        List<Account> mockAccounts = [SELECT Id, name FROM ACcount WHERE name LIKE 'Account_'];
        System.debug('Mock accounts:' + mockAccounts);
        Map<String,Account> mapMockAccounts = new Map<String,Account>();
        for (Account a: mockAccounts) 
            mapMockAccounts.put(a.name,a);
        System.debug('Map Mock Accounts:' + mapMockAccounts);           

        return mapmockAccounts;
    }

    private static SSs__c createSS(String ssName, Account cliente, Empleado__c gestor) {

        SSs__c ss1 = new SSs__c(
            name= ssName, 
            Cliente__C= cliente.id,
            gestor__c=gestor.id);
        insert ss1;

        return [SELECT Id, name, Cliente__c, Gestor__c, OwnerId, Gestor_cliente__c, Gestor_external_Key__c FROM SSs__c WHERE Id = :ss1.id];

        return ss1;
    }
}