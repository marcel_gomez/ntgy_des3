@isTest
private class AG_TestBatch_MoverConceptosAntiguos {

@testSetup
private static void setup() {

	SSs__c ss= new SSs__c();
	ss.name = 'Sector Suministro';

	insert ss;

	Contrato__c contrato  = new Contrato__c();
	contrato.name = 'Contrto1';
	contrato.Anno__c = 2016;
	contrato.NISS__c = ss.id;


	insert contrato;

	Concepto__c c1 = new Concepto__c();

	c1.Name				= 'Concepto1';
	c1.Fecha_Fin__c 	= Date.Today();
	c1.Fecha_Inicio__c 	=  Date.Today();
	c1.ID_Concepto__c	= '111';
	c1.Contrato__c 		= contrato.id;
	c1.Descripcion__c	= 'Nuevo concepto';
	c1.Precio__c  		= 105.55;
	c1.Tipo_Tarifa__c 	= 'Tarifa 123';

	Concepto__c c2 = c1.clone();
	c2.Name = 'Concepto2'; c2.ID_Concepto__c	= '222';
	c2.Fecha_Inicio__c = Date.Today().addYears(-2).addMonths(-2);	//dos años y dos meses antes
	c2.Fecha_Fin__c = Date.Today().addYears(-2).addMonths(-1);		//dos años y un mes antes
	System.debug('Fecha_Inicio:' + c2.Fecha_Inicio__c );
	System.debug('Fecha_Fin:' + c2.Fecha_Fin__c );

	Concepto__c c3 = c1.clone();
	c3.Name = 'Concepto3';	c3.ID_Concepto__c	= '333';
	c3.Fecha_Inicio__c = Date.Today().addYears(-3).addMonths(-1);	//3 años y un mes antes
	c3.Fecha_Fin__c = Date.Today().addYears(-3);					//3 años antes
	System.debug('Fecha_Inicio:' + c3.Fecha_Inicio__c );
	System.debug('Fecha_Fin:' + c3.Fecha_Fin__c );

	Concepto__c c4 = c1.clone();
	c4.Name = 'Concepto4';		c4.ID_Concepto__c	= '444';
	c4.Fecha_Inicio__c = Date.Today().addMonths(-2);	//dos meses antes
	c4.Fecha_Fin__c = Date.Today().addMonths(-1);		//un mes antes
	System.debug('C4 Fecha_Inicio:' + c4.Fecha_Inicio__c );
	System.debug('C4 Fecha_Fin:' + c4.Fecha_Fin__c );

	Concepto__c c5 = c1.clone();
	c5.Name = 'Concepto5';		c5.ID_Concepto__c	= '555';
	c5.Fecha_Inicio__c = Date.Today().addYears(-2).addMonths(-1);	//dos años y un mes antes
	c5.Fecha_Fin__c = Date.Today().addYears(-2);					//justo 2 años
	System.debug('C5 Fecha_Inicio:' + c5.Fecha_Inicio__c );
	System.debug('C5 Fecha_Fin:' + c5.Fecha_Fin__c );

	insert new List<Concepto__c> {c1, c2, c3, c4, c5};

	List<Concepto__c> conceptos = [SELECT	ID_Concepto__c, Name, Descripcion__c, Fecha_Fin__c, Fecha_Inicio__c, Contrato__c, Tipo_Tarifa__c, Precio__c FROM Concepto__c];
	System.debug('Conceptos:' + conceptos);

}

	@isTest static void test_method_one() {

		Test.startTest();

		AG_Batch_MoverConceptosAntiguos batchProcess = new AG_Batch_MoverConceptosAntiguos();
		Id batchId = Database.executeBatch(batchProcess);
		System.debug('Batch id:' + batchId);


		Test.stopTest();

		List<Concepto__c> conceptos = [SELECT	ID_Concepto__c, Name, Descripcion__c, Fecha_Fin__c, Fecha_Inicio__c, Contrato__c, Tipo_Tarifa__c, Precio__c FROM Concepto__c];
		List<Concepto_Historico__c> conceptosHistoricos = [SELECT	ID_Concepto__c, Name, Descripcion__c, Fecha_Fin__c, Fecha_Inicio__c, Contrato__c, Tipo_Tarifa__c, Precio__c FROM Concepto_Historico__c];		
		System.debug('Conceptos:' + conceptos);
		System.debug('Conceptos Historicos:' + conceptosHistoricos);

		for (Concepto__c concepto : conceptos) {
			System.assert(concepto.fecha_Fin__c> Date.today().addYears(-2));

		}

		for (Concepto_Historico__c conceptoHist : conceptosHistoricos) {
			System.assert(conceptoHist.fecha_Fin__c<= Date.today().addYears(-2));
			System.assert(conceptoHist.name.length()>0);
			System.assert(conceptoHist.ID_Concepto__c.length()>0);
			System.assert(conceptoHist.Contrato__c!=null);
			System.assert(conceptoHist.Precio__c!=0);
			System.assert(conceptoHist.Descripcion__c.length()>0);
				
		}
	}

}