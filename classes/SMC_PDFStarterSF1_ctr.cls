/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase controladora para la generacion de pdf desde SMC.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-17		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
public without sharing class SMC_PDFStarterSF1_ctr {
	
	/**
	* @Method: 		SMC_PDFStarterSF1_ctr
	* @param: 		N/A
	* @Description: Constructor de la clase encargada de enlazar con el generador de documentos.
	* @author 		Manuel Medina - 17012018
	*/
	public SMC_PDFStarterSF1_ctr( ApexPages.StandardController sCtrOpportunity ){
		
	}
	
	/**
	* @Method: 		SMC_PDFStarterSF1_ctr
	* @param: 		N/A
	* @Description: Envia los parametros requeridos para la generacion del PDF.
	* @author 		Manuel Medina - 17012018
	*/
	@RemoteAction 
	public static void startPDFGenerator( String strTemplateUniqueName, String strMainRecordId ){
		Opportunity objOpportunity												= new Opportunity();
		objOpportunity															= [SELECT Id,
																						SMC_CantidadPS__c
																					FROM Opportunity
																					WHERE Id =: strMainRecordId
																					LIMIT 1
																				];
		
		if( objOpportunity.SMC_CantidadPS__c >= Integer.valueOf( GNF_ValoresPredeterminados__c.getInstance().SMC_LimitePSSincronos__c ) ){
			GNF_PDFGenerator_ctr ctrPDFGeneratorAsync							= new GNF_PDFGenerator_ctr( strTemplateUniqueName, strMainRecordId, 'Opportunity', true );
			ctrPDFGeneratorAsync.blnIsAsync										= true;
			ctrPDFGeneratorAsync.startAsync();
			
		}else if( objOpportunity.SMC_CantidadPS__c < Integer.valueOf( GNF_ValoresPredeterminados__c.getInstance().SMC_LimitePSSincronos__c ) ){
			GNF_PDFGenerator_ctr ctrPDFGeneratorSync							= new GNF_PDFGenerator_ctr( strTemplateUniqueName, strMainRecordId, 'Opportunity' );
			ctrPDFGeneratorSync.blnIsAsync										= false;
			ctrPDFGeneratorSync.attachPDF();
		}
	}
}