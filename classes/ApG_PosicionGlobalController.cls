/*
Provider-----Date--------Change Description
VASS         23052018    Initial implementation

Global description - This class is the controller of the visualforce page ApG_PosicionGlobal.
Contains all the methods needed by that page. ApG_PosicionGlobal displays records summarized by fields of those records and by the hierarchy of the related "Empleado" in a tree structure.
It also allows to get more detail for the records displaying them in a table. This table can also be exported as csv format.
ApG_PosicionGlobal is configured by three custom metadata types: 
    ApG_GlobalPositionGroup__mdt  --> Clasifies the reports displayed in GlobalPosition
    ApG_GlobalPositionItem__mdt --> Contains the configuration of the reports
    ApG_GlobapPositionReportField__mdt --> Contains the configuration of the table which shows the detail of the records
Related Test class: ApG_PosicionGlobalController_TEST
*/
global without sharing class ApG_PosicionGlobalController {
    public string userId{get; set;}
    public string profileName{get; set;}
    public ApG_PosicionGlobalController(){
        userId=userinfo.getUserId();
        profileName = [SELECT Id, Profile.Name FROM User WHERE Id =: userId].Profile.Name;
    }

    /*
    VASS 23052018
    Gets the records of ApG_GlobalPositionGroup__mdt and its dependant ApG_GlobalPositionItem__mdt, needed for the first load of 
    the page.
    */
    @RemoteAction
    global static list<ApG_GlobalPositionGroup__mdt> getGlobalPositionPage(string tipo){
        list<ApG_GlobalPositionGroup__mdt> retList = [SELECT Id, developername, Type__c, GlobalPositionOrder__c, color__c, toLabel(GroupLabel__c), (SELECT Id, developername, ApexClassName__c, SummaryUnits__c, GlobalPositionOrder__c, toLabel(titleLabel__c), GlobalPositionGroup__c FROM GlobalPositionItems__r WHERE Active__c =: true ORDER BY GlobalPositionOrder__c) FROM ApG_GlobalPositionGroup__mdt WHERE Type__c =: tipo ORDER By GlobalPositionOrder__c];
        system.debug('lista de grupos e hijos que va a ser devuelta ' + retList);
        return retList;
    }


    /*
    VASS 23052018
    Returns a list of maps where each map corresponds to a record.
    Each item in the map is a pair "field - value" but, in the key, instead of storing the name of the related field, 
    its stored a keyword "keyX__c" the X is an integer which points to the level that the field should be placed in the tree.
    */    
    @RemoteAction
    global static list<map<string, string>> getTreeContent(string itemId, string itemToGetFrom, string forceUser){
        
        system.debug('clase apex que debe realizar la busqueda  ' + itemId);
        list<map<string, string>> retList = new list<map<string,string>>();
        if(itemId != null){
            retList = getsObjectInformation(itemId, itemToGetFrom, forceUser);
            system.debug('informacion devuelta por la clase ' + retList);
        }
        return retList;
    }

    /*
    VASS 27062018
    Builds the list of records for the report of the item, only the clasification for the tree: Hierarchy and status fields.
    Retrieves the query which will get the records from the custom metadata type which id is in itemId.
    Returns a list of 7000 records sorted by their name and parting from the parameter itemToGetFrom.
    The list returned is always filtered by the field "GestorId__c", all the records must have its GestorId__c within a set retrieved
    by getHierarchyMap. The ids in that set are the ones of the logged user and all the "Empleados" below him in its hierarchy branch. 
    The paramater forceUser allows to override the GestorId of the current user and retrieve records from a custom GestorId. This capability is 
    only implemented for System Administrators
    */
    private static list<map<string, string>> getsObjectInformation(string itemId, string itemToGetFrom, string forceUser){
        ApG_GlobalPositionItem__mdt gpi = [SELECT Id, lvl0__c, ApexClassName__c, lvl1__c, lvl2__c, lvl3__c, lvl4__c, objectName__c, fieldToSum__c, Query_Filter__c, GlobalPositionGroup__r.Type__c FROM ApG_GlobalPositionItem__mdt WHERE Id =: itemId];
        if(string.isBlank(forceUser)){
            User thisUser = [SELECT Id, ID_Gestor__c FROM User WHERE Id =: Userinfo.getUserId()];
            forceUser = thisUser.ID_Gestor__c;
        }
        list<map<string, string>> retListMap = new list<map<string, string>>();
        if(gpi.ApexClassName__c == null){
            retListMap = getSobjectInformationGeneric(gpi, itemToGetFrom, forceUser);
        }else{
            boolean isHierarchy = gpi.GlobalPositionGroup__r.Type__c =='hierarchy';
            Type apexClassType = Type.forName(gpi.ApexClassName__c);
            ApG_PosicionGlobalReportInterface recordRetriever = (ApG_PosicionGlobalReportInterface)apexClassType.newInstance();
            retListMap = recordRetriever.getsObjectInformation(itemToGetFrom, forceUser, isHierarchy);
            system.debug('informacion devuelta por la clase ' + retListMap);
        }
        return retListMap;

    }

    private static list<map<string, string>> getSobjectInformationGeneric(ApG_GlobalPositionItem__mdt gpi, string itemToGetFrom, string forceUser){
        boolean isHierarchy = gpi.GlobalPositionGroup__r.Type__c =='hierarchy';
        system.debug('globalPositionItem recuperado '+gpi);
        map<string, string> fieldMap = fillFieldList(gpi);
        string fieldToSum = '';
        if(gpi.fieldToSum__c != null){
            fieldToSum = gpi.fieldToSum__c+', ';
        }
        list<map<string, string>> retListMap = new list<map<string, string>>();
        map<string, map<string, string>> hierarchyMap = ApG_PosicionGlobalController.getHierarchyMap(forceUser);
        set<string> gestorSet = new set<string>();
        gestorSet.addAll(hierarchyMap.keySet());
        //gestorSet.add(forceUser);
        system.debug('set de gestores por los que se van a recoger registros    ' + gestorSet);
        string theQuery = 'SELECT '+string.join(fieldMap.values(), ', ')+ ', GestorId__c,'+ fieldToSum+' Id, Name FROM '+gpi.objectName__c + ' WHERE Name >: itemToGetFrom AND GestorId__c IN: gestorSet [FILTER] ORDER BY Name ASC LIMIT 7000'; 
        string theFilter ='';
        if(!string.isBlank(gpi.Query_Filter__c)){
            theFilter = ' AND '+gpi.Query_Filter__c;
        }
        theQuery = theQuery.replace('[FILTER]', theFilter);
        system.debug('laQuery   '+  theQuery);
        list<sObject> srList = database.query(theQuery);
        system.debug('lista de Srs    ' + srList);

        for(sObject i : srList){
            map<string, string> instMap = new map<string, string>();
            if(isHierarchy){
                instMap.putAll(hierarchyMap.get(string.valueOf(i.get('GestorId__c'))));
                integer levelNumber = instMap.keySet().size();
                for(string j : fieldMap.keySet()){
                    string theLevel = 'lvl' +levelnumber;
                    instMap.put(theLevel, string.valueOf(i.get(fieldMap.get(j))));
                    levelnumber++;
                }
            }else{
                integer levelNumber = 0;
                for(string j : fieldMap.keySet()){
                    string theLevel = 'lvl' +levelNumber;
                    instMap.put(theLevel, string.valueOf(i.get(fieldMap.get(j))));
                    levelnumber++;
                }
                for(string j : hierarchyMap.get(string.valueOf(i.get('GestorId__c'))).keySet()){
                    string theLevel = 'lvl' +levelNumber;
                    instMap.put(theLevel, hierarchyMap.get(string.valueOf(i.get('GestorId__c'))).get(j));
                    levelNumber++;
                }
            }
            string valueToSum = '1';
            if(gpi.fieldToSum__c != null){
                valueToSum = string.valueOf(i.get(gpi.fieldToSum__c));
            }
            instMap.put('fieldToSum', valueToSum);
            instMap.put('fieldToGetFrom', string.valueOf(i.get('Name')));
            instMap.put('id', string.valueOf(i.get('id')));
            retListMap.add(instMap);
        }
        system.debug('lista de mapas que va de vuelta   '+retListMap);
        return retListMap; 
    }


    /*
    VASS 27062018
    The custom metadata type retrieved from ApG_GlobalPositionItem__mdt contains 5 fields (lvl0__c, lvl1__c...) where there are stored api names of
    fields of objects. Those fields can be filled or not. This method iterates over those fields to get only those ones that are filled
    */    
    private static map<string, string> fillFieldList(ApG_GlobalPositionItem__mdt gpi){
        map<string, string> retMap = new map<string, string>();
        for(integer i = 0; i<5; i++){
            string fieldName = 'lvl'+i+'__c';
            if(gpi.get(fieldName)!= null){
                retMap.put(fieldName, string.valueOf(gpi.get(fieldName)));
            }
        }
        return retMap;
    }


    /*
    VASS 27062018
    From the tree displayed on Global Position, the user can generate a table which display detailed information of those records.
    This method receives a list of ids of the record and the id of an ApG_GlobalPositionItem__mdt. With this last id retrieves all its related
    ApG_GlobalPositionReportField__mdt which contains the fields that should be displyed as columns of the records in idList.
    */  
    @RemoteAction
    global static list<map<string, string>> getReport(list<id> idList, string itemId){
        list<map<string, string>> retList = new list<map<string, string>>();
        list<ApG_GlobalPositionReportField__mdt> fieldMetadataList = new list<ApG_GlobalPositionReportField__mdt>([SELECT id, APInameOfTheLookup__c, toLabel(ColumnLabel__c), FieldAPIname__c, GlobalPositionItem__c, GlobalPositionItem__r.ObjectName__c, ColumnNumber__c FROM ApG_GlobalPositionReportField__mdt WHERE GlobalPositionItem__c =: itemId ORDER BY ColumnNumber__c ASC]);
        system.debug('lista de campos encontrada por la custom metadata ' + fieldMetadataList);
        map<string, string> innerMap = new map<string, string>();
        map<string, string> fieldAPIMap = new map<string, string>();
        integer counter = 0;
        string objectName;
        for(ApG_GlobalPositionReportField__mdt i : fieldMetadataList){
            objectName = i.GlobalPositionItem__r.ObjectName__c;
            string theField = 'lvl'+i.ColumnNumber__c;
            innerMap.put(theField, i.ColumnLabel__c);
            fieldAPIMap.put(theField, i.fieldAPIName__c);
            if(i.APInameOfTheLookup__c != null){
                string theFieldLookup = 'lvlId'+i.ColumnNumber__c;
                innerMap.put(theFieldLookup, i.APInameOfTheLookup__c);
                fieldAPIMap.put(theFieldLookup, i.APInameOfTheLookup__c);
            }
        }
        
        integer innerMapSize = innerMap.keySet().size();
        set<string> fieldsWithoutDuplicates = new set<string>();
        fieldsWithoutDuplicates.addAll(fieldAPIMap.values());
        list<string> fieldList = new list<string>(fieldsWithoutDuplicates);
        string fieldQuery = string.join(fieldList, ', ');
        string query = 'SELECT '+fieldQuery+' FROM '+objectName+' WHERE Id IN: idList';
        list<sObject> objList = database.query(query);
        retList.add(innerMap);
        for(sObject i : objList){
            innerMap = new map<string, string>();
            for(string j : fieldAPIMap.keySet()){
                string theValue = '-';
                if(!string.isBlank(string.valueOf(i.get(fieldAPIMap.get(j))))){
                    theValue = string.valueOf(i.get(fieldAPIMap.get(j)));
                }
                innerMap.put(j, theValue);
            }
            retList.add(innerMap);
        }

        return retList;
    }


    /*
    VASS 27062018
    Reports of Posicion Global clasify records by their status and the hierarchy chain below the id received in forceUser.
    Then, there is needed the information of this hierarchy. This data is returned by this method as a map where each key is the Id o 
    a "Gestor" (Its ID_Gestor__c from the Object Empleado) and the value for each key is another map with the hierarchy below this user as follows:
    lvl0 --> first Empleado Id below the empleado in the key
    lvl1 --> second Empleado Id below the empleado in the key
    ....
    */    
    public static map<string, map<string, string>> getHierarchyMap(string thisGestorId){
        map<id, Empleado__c> allEmployees = new map<id, Empleado__c>([SELECT Id, Name, ID_Empleado__c, ID_Manager__c FROM Empleado__c]);
        map<string, string> employeeAndManager = new map<string, string>();
        map<string, string> employeeName = new map<string, string>();
        for(Empleado__c i : allEmployees.values()){
            employeeAndManager.put(i.ID_Empleado__c, i.ID_Manager__c);
            employeeName.put(i.ID_Empleado__c, i.Name);
        }
        /*string thisGestorId =forceUser;
        if(string.isBlank(forceUser)){
            User thisUser = [SELECT Id, ID_Gestor__c FROM User WHERE Id =: Userinfo.getUserId()];
            thisGestorId = thisUser.ID_Gestor__c;
        }*/

        system.debug('empleado y su manager    '+employeeAndManager);
        map<string, map<string, string>> retMap = new map<string, map<string, string>>();
        for(id i : allEmployees.keyset()){
            map<integer, string> innerMap = new map<integer, string>{0 => allEmployees.get(i).ID_Empleado__c};
            string thisEmployee = allEmployees.get(i).ID_Empleado__c;
            string itsBoss = employeeAndManager.get(thisEmployee);
            set<string> thisUserBranch = new set<string>{
                thisEmployee
            };
            while(!string.isBlank(itsBoss)){
                thisUserBranch.add(itsBoss);
                integer keysetMapSize = innerMap.keySet().size();
                innerMap.put(keysetMapSize, itsBoss);
                itsBoss = employeeAndManager.get(itsBoss);
            }
            if(thisUserBranch.contains(thisGestorId)){
                map<string, string> sortedMap = new map<string, string>();
                integer theCounter = 0;
                boolean fromThisUser = false;
                for(integer j = innerMap.keySet().size()-1; j>=0; j--){
                    if(fromThisUser){
                        string theLevel = 'lvl'+theCounter;
                        sortedMap.put(theLevel, employeeName.get(innerMap.get(j)));
                        theCounter++;
                    }
                    if(!fromThisUser){
                        fromThisUser = innerMap.get(j) == thisGestorId;
                    }
                }
                retMap.put(allEmployees.get(i).ID_Empleado__c, sortedMap);
            }
        }
        system.debug('mapa de empleado con mapa de su escala de jefes   ' + retMap);
        return retMap;
    }

    /**
    * @Method:      getdashboard
    * @param:       String strDeveloperName
    * @param:       
    * @Description: Metodo para consultar los dashboard.
    * @author       Victor Velandia - (victor.velandia@vass.es)
    */
    @RemoteAction
    global static String getdashboard(String strDeveloperName) {

        string strRetornDashboard;

        list<Dashboard> listDashboard = [SELECT Id, DeveloperName FROM Dashboard WHERE DeveloperName =: strDeveloperName];

        for(Dashboard objDashboard : listDashboard){

            strRetornDashboard = objDashboard.id;
        }
        return strRetornDashboard;
    }

        /*
    VASS 27062018
    Users can export the records displayed in the table as a *.csv file. is generated in the client side. The file is splited into
    chuncks of 130000 charactes and sent to the server. In the server, those chuncks are stored as record of the object "PosicionGLobalTempFile__c"
    identified by an unique id which is the id of the logged user and the system miliseconds in the right time where the user launched the
    export process.
    When the last chunck is received, this method calls to createContentVersion
    */ 
    @RemoteAction
    global static string createTableChuncks(string theChunck, string tableIdentifier, boolean lastChunck, string tableName){
        system.debug('lastChunck    ' + lastChunck);
        string retString = 'NOT YET';
        try{
            if(!string.isBlank(theChunck)){
                PosicionGlobalTempFile__c newChunck = new PosicionGlobalTempFile__c(
                TableChunck__c=theChunck,
                TableId__c = tableIdentifier
                );
                insert newChunck;
            }
        }catch(exception e){
            retString = 'Error: '+ e.getDMLmessage(0) + '\n\r'+label.ApG_CSVexportFailMessage;
        }
        if(lastChunck){
            retString = createContentVersion(tableIdentifier, tableName);
        }
        return retString;
    }

    /*
    VASS 27062018
    When all the *.csv chuncks have been received, this method retrieves all of them from the object "PosicionGlobalTempFile__c", 
    concatenates the chuncks creating the *.csv and saves it as a contentVersion.
    This method has a limitation: table chuncks are strings and a content version needs the data to be saved as a blob. This means 
    that the data must be converted from string to blob. In this right moment, the data volume will be duplicated, then if the
    table chucnks concatenated contains more than 6M characters, it will fail for head size limit
    */ 
    private static string createContentVersion(string tableIdentifier, string tableName){
        list<PosicionGlobalTempFile__c> chunckList = [SELECT Id, TableChunck__c, TableId__c FROM PosicionGlobalTempFile__c WHERE TableId__c =: tableIdentifier ORDER BY CreatedDate ASC];
        //system.debug('Lista de trozos de tabla encontrada   ' + chunckList);
        string tableString = '';
        for(PosicionGlobalTempFile__c i : chunckList){
            tableString+=i.TableChunck__c;
        }
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.VersionData = Blob.valueOf(tableString);
        cv.Title = tableName+'.csv';
        cv.PathOnClient = tableName+'.csv';
        string retString = '';
        try{
            insert cv;
            retString = cv.Id;
        }catch(exception e){
            retString = 'Error: '+ e.getDMLmessage(0) + '\n\r'+label.ApG_CSVexportFailMessage;
        }
        deleteChuncks(tableIdentifier);
        return retString;

    }

    /*
    VASS 27062018
    Once the chuncks of table have been joined back again and the table has been exported or the process has failed (in which case, this table will not
    be able to be exported), the PosicionGlobalTempFiles will be useless, then this method deletes them. 
    This method is called either from the server if everything has be done right or by the client if the process has failed.
    */ 
    @RemoteAction
    global static void deleteChuncks(string tableIdentifier){
        list<PosicionGlobalTempFile__c> chunckList = new list<PosicionGlobalTempFile__c>([SELECT Id FROM PosicionGlobalTempFile__c WHERE TableId__c =: tableIdentifier]);
        delete chunckList;
    }

}