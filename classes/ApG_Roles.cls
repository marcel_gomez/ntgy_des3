public with sharing class ApG_Roles implements ApG_PosicionGlobalReportInterface {
    public ApG_Roles(){
    	system.debug('Ejecutando ApG_Roles');
    }
    
    public list<map<string, string>> getsObjectInformation(string itemToGetFrom, string pageType){
    	system.debug('se van a recoger objetos para posicion global');
    	map<id, UserRole> roleMap = new map<id, UserRole>([SELECT Id, DeveloperName, Name, parentRoleId FROM UserRole WHERE DeveloperName >: itemToGetFrom ORDER BY DeveloperName]);
    	system.debug('lista de roles recogida	'+roleMap);
    	map<id, list<id>> roleAndParentList = new map<id, list<id>>();
    	for(UserRole i : roleMap.values()){
    		id parentOfThisRole = roleMap.get(i.id).parentRoleId;
    		list<id> parentRoleList = new list<id>{parentOfThisRole};
    		while(parentOfThisRole != null){
    			parentOfThisRole = roleMap.get(parentOfThisRole).parentRoleId;
    			parentRoleList.add(parentOfThisRole);
    		}
    		roleAndParentList.put(i.id, parentRoleList);
    	}
    	system.debug('mapa de roles y lista de superiores	' + roleAndParentList);
    	list<map<string, string>> retMap = new list<map<string, string>>();
    	map<string, list<string>> roleAndParentDevName = new map<string, list<string>>();
    	for(id i : roleAndParentList.keySet()){
    		list<string> bossList = new list<string>();
    		integer k = 0;
    		map<string, string> innerMap = new map<string, string>();
    		for(integer j=roleAndParentList.get(i).size()-1; j>=0; j--){
    			if(roleAndParentList.get(i)[j]!=null){
    				innerMap.put('lvl'+k, roleMap.get(roleAndParentList.get(i)[j]).Name);
    				innerMap.put('IdRow'+k, roleMap.get(roleAndParentList.get(i)[j]).Id);
    				bossList.add(roleMap.get(roleAndParentList.get(i)[j]).Name);
    				k++;
    			}
    		}
    		innerMap.put('lvl'+k, roleMap.get(i).Name);
    		innerMap.put('IdRow'+k, roleMap.get(i).Id);
    		innerMap.put('DeveloperName', roleMap.get(i).DeveloperName);
    		innerMap.put('Id', i);
    		innerMap.put('fieldToGetFrom', roleMap.get(i).DeveloperName);
    		innerMap.put('yOtroMas', i);
    		innerMap.put('yOtroCampoMas', i);
    		retMap.add(innerMap);
    		roleAndParentDevName.put(roleMap.get(i).Name, bossList);
    	}
    	system.debug('mapa de roles y lista de superiores por developerName	' + roleAndParentDevName);
    	return retMap;
    }
    
    public map<string, string> getLabelInfo(list<string> levels){
    	map<string, string> retMap = new map<string, string>();
    	for(string i : levels){
    		retMap.put(i, 'RoleName');
    	}
    	return retMap;
    }

    public list<map<string, string>> getReport(list<id> idList){
        return null;
    }
}