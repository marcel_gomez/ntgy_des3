public class AG_TriggerHandlerOferta {

    //LOGIC
    private final AG_LogicOferta logic;

    //CONSTRUCTOR
    private AG_TriggerHandlerOferta() {
        this.logic = new AG_LogicOferta();
    }

    //SINGLETON PATTERN
    private static AG_TriggerHandlerOferta instance;
    public static AG_TriggerHandlerOferta getInstance() {
        if (instance == null) instance = new AG_TriggerHandlerOferta();
        return instance;
    }    
    
    //SUMINISTRO HANDLER      
    public void onBeforeInsert(final List<Ofertas__c> newList) {
        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<Ofertas__c> newList, final Map<Id, Ofertas__c> newMap,
                               final List<Ofertas__c> oldList, final Map<Id, Ofertas__c> oldMap) {
        //REVIEW: No need to re-calculate owner when the Gestor_External_Key__c has not change.
        this.logic.assignOwnerFromEmpleado(newList, oldMap);
    }

    public void onAfterInsert(final List<Ofertas__c> newList, final Map<Id, Ofertas__c> newMap){
       this.logic.setCustomSharing(newList,newMap,null,null);
       //this.logic.addAboveUsersToClientPlaceHolder(newList,newMap,null,null);
    }

    public void onAfterUpdate(final List<Ofertas__c> newList, final Map<Id, Ofertas__c> newMap,
                              final List<Ofertas__c> oldList, final Map<Id, Ofertas__c> oldMap){
       this.logic.setCustomSharing(newList,newMap,oldList,oldMap);
       //this.logic.addAboveUsersToClientPlaceHolder(newList,newMap,oldList,oldMap);
    }

    /**
    * description: Se comenta metodos (onAfterDelete) 
    * debido a que no tienen ninguna logica que se cumpla para su llamado
    * @param:   Req_RD_1_4 Metodos sin logica
    * @author:  victor.velandia@vass.es
    * @date:    20/06/2018
    * Begin 
    */
    /*public void onAfterDelete(final List<Ofertas__c> oldList, final Map<Id, Ofertas__c> oldMap){
    }*/
    //End
}