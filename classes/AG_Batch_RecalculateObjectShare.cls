global with sharing class AG_Batch_RecalculateObjectShare implements Database.Batchable<sObject> {

	public  static final List<String> BUSINESS_OBJECTS = AG_SecurityRecalculationProcessUtil.BUSINESS_OBJECTS;

    private final Integer tableIndex;

	//Lista de elemento viene ordenados por Account y Owner del objeto.
	//De esta forma, si vienen varios iguales (Account y owner) solo hace falta tratar el primero.
	//String query ='SELECT Id, (Select OwnerId FROM Suministros__r ORDER BY OwnerId) FROM Account' ;
	public final String QUERY_TEMPLATE = 
		' SELECT Id, OwnerId, Gestor_Cliente__c ' +
		' FROM #TABLE# #RECALCULATE_ALL_CONDITION# ORDER BY Cliente__c, OwnerId';

	private final String RECALCULATE_ALL_CONDITION ='WHERE Cliente__r.recalcular_permisos__c = true';

	private String query = null;

	//Dejamos calculado previamente, para cada usuario, a que grupos se tendría que dar permisos.
	private final Map<Id, Id[]> grpOrUsrIdToShare;

	//Chaining batch pattern to process all BUSINESS_OBJECTS tables
	private final Boolean chainProcess;
	//Process all Account's or just those with Account.recalculat_permisos__c = true
	private final Boolean recalculateAllAccounts;	

	/*
	*
	*/	
	@testVisible
	private AG_Batch_RecalculateObjectShare(Integer tableIndex, Boolean recalculateAllAccounts, Boolean chainProcess)  {

		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('--> AG_Batch_RecalculateObjectShare.Constructor');
		System.debug('**** Parametros: tableIndex=' + tableIndex + ' recalculateAllAccounts=' + recalculateAllAccounts + 'chainProcess=' +chainProcess);*/

		if (tableIndex>=BUSINESS_OBJECTS.size())
			throw new BatchChangeAccountOwnerException('Table index out of bounds');

		grpOrUsrIdToShare = precalculateUSerOrGroupsToShare();

		this.chainProcess = chainProcess;
		this.recalculateAllAccounts = recalculateAllAccounts;
		this.tableIndex = tableIndex;

		query = QUERY_TEMPLATE.replace('#TABLE#', BUSINESS_OBJECTS.get(tableIndex));
		if (recalculateAllAccounts)
			query = query.replace('#RECALCULATE_ALL_CONDITION#', ' ');		//eliminamos filtro
		else
			query = query.replace('#RECALCULATE_ALL_CONDITION#', RECALCULATE_ALL_CONDITION);	//ponemos filtro: Account.recalcular_permisos__c =true


		System.debug('**** QUERY: ' + query);
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- AG_Batch_RecalculateObjectShare.Constructor');
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Batch start: open query ');

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> AG_Batch_RecalculateObjectShare.execute.');

		//List of AccountShare to upsert at the end of the process.
		List<sObject> objectSharesGlobal = new List<SObject>();
		Integer numSameOwners=0;
   		for (sObject obj : scope) {

   			Id objectId = obj.Id;
   			Id ownerId = (Id)obj.get('OwnerId');
   			Id ownerAccountId = (Id)obj.get('Gestor_Cliente__c');

   			if (ownerId==ownerAccountId) {
				//System.debug('**** Account and object has the same Owner:' + ownerId);
				numSameOwners++;
			} else {

				//Obtenemor lista precalculada de a quien hay que dar permisos (ascendentes en jerarquía) para ese ownerId
				List<Id> membersToShare = grpOrUsrIdToShare.get(OwnerId);
				//Crea lista de permisos
				List<sObject> objectShares = AG_OrganizationModelUtil.createSharedRecords(objectId, membersToShare);

				objectSharesGlobal.addAll(objectShares);
	   		}
   		}
		
		Database.SaveResult[] resultList = Database.insert(objectSharesGlobal, false);
		Integer numFails =0, numSucess=0;
		String failError=null;
		for (Database.SaveResult result : resultList) {
			if (!result.isSuccess()) {
				numFails++;
				if (failError==null) {
					failError = 'Id=' + result.getId();
					for (Database.Error err : result.getErrors() )
						failError = failError + '\n' + err.getMessage() + ' - ' + err.getStatusCode();
				}
			 } else {
				numSucess++;
			}
		}
		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('**** Database inserts: Success: ' + numSucess + ' - Fails:' + numFails);
		System.debug('**** Database fails: ' + failError);
		System.debug('**** Num objets with same owner than Account:' + numSameOwners);

		System.debug('--> AG_Batch_RecalculateObjectShare.execute:  Number of share upsert:' + objectSharesGlobal.size());*/
	}


	
	global void finish(Database.BatchableContext BC) {
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> AG_Batch_RecalculateObjectShare.finish');

		if (this.chainProcess) {
			Integer nextTableIndex = tableIndex + 1;	//Siguiente tabla
		
			if (nextTableIndex<BUSINESS_OBJECTS.size()) {
				System.debug ('**** Siguiente tabla: NextTableIndex=' + nextTableIndex + ' - table Name=' + BUSINESS_OBJECTS.get(nextTableIndex));

				AG_Batch_RecalculateObjectShare batchProc = new AG_Batch_RecalculateObjectShare(nextTableIndex, recalculateAllAccounts, chainProcess);
				Id batchId = Database.executeBatch(batchProc);
			} else {
				//Una vez finalizado el calculo de todos los permisos, se eliminan las marcas en Account
				if (!this.recalculateAllAccounts && this.chainProcess) {
					cleanRecalculateFieldOnAcount();
				}
			}
		}

		System.debug('<-- AG_Batch_RecalculateObjectShare.finish');

	}

	/*
	* Lanza proceso batch de recalculo de Share sobre todos los objetos.
	* @cleanAll Borrar todos los permisos (share) otorgados, y no únicamnente la de los completamente
	*/
	public static Id executeFullProcess(Boolean recalculateAll) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** Start batch process to recalculate Object Shares');

		AG_Batch_RecalculateObjectShare batchProcess = new AG_Batch_RecalculateObjectShare(0, recalculateAll, true);
		Id batchId = Database.executeBatch(batchProcess);

		//Se comenta debug(Victor Velandia (VV))
		//System.debug ('*** BatchProcess.finish: ends. Id=' + batchId);

		return batchId;
	}

	/*
	* Lanza proceso batch de borrado de Sahre para un objeto (TableIndex) en particular
	*/
	public static Id executeProcessOneTable(Integer tableIndex, Boolean recalculateAll) {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('--> executeProcessOneTableStart: cleanAll=' + recalculateAll);

		AG_Batch_RecalculateObjectShare batchProcess = new AG_Batch_RecalculateObjectShare(tableIndex, recalculateAll, false);
		Id batchId = Database.executeBatch(batchProcess);
		
		//Se comenta debug(Victor Velandia (VV))
		//System.debug('<-- executeProcessOneTableStart: Batch.Id:' + batchId);
		
		return batchId;
	}


/////////////////// PRIVATE METHODS ///////////////////////

	/*
	* Calculamos todos los grupos/role ascendentes a cada usuario, de forma que lo obtenemos rápido.
	*/	
	private Map<Id, Id[]> precalculateUserOrGroupsToShare() {

		//Se comenta debug(Victor Velandia (VV))
		//System.debug('**** precalculateUserOrGroupsToShare: start');

	    final Map<Id, User> allUsers = AG_OrganizationModelUtil.getAllUsersById();
	    final Map<Id,USerRole> allRoles =  AG_OrganizationModelUtil.getAllRolesById();
	    final Map<Id,Group> allGroups =  AG_OrganizationModelUtil.getRoleGroupsByRoleId();

		Map<Id, Id[]> mapMembersToShare = new Map<Id, Id[]>();

		//Calculamos los grupos/usuarios ascendentes en la jerarquia para todos los usuarios.
		for (Id  userId : allUsers.keySet()) {
 			Id[] membersToShare = AG_OrganizationModelUtil.calculateMembersIdsToSharesEx(userId, allUsers, allRoles, allGroups);
			mapMembersToShare.put(userId, membersToShare);
		}

		//Se comenta debug(Victor Velandia (VV))
		/*System.debug('**** precalculateUserOrGroupsToShare: ' + mapMembersToShare.size());
		System.debug('**** precalculateUserOrGroupsToShare: end');*/
		return mapMembersToShare;

	}

	  //-- CUSTOM EXCEPTION
    public class BatchChangeAccountOwnerException extends Exception {}

   /*
    * Se elimina la marca de Recalcular_Permisos__c de objetos Account.
    */
    private void cleanRecalculateFieldOnAcount() {
		
		//Vass 31052018 AppGestor 2018 - Implementación de esta clase batch y comentado del método original para setear el flag de las cuentasa true
		Id batchJobId = Database.executeBatch(new ApG_Batch_AccountUpdateShareFlag('Recalcular_Permisos_Objetos__c', null, false), 200);
    	/*for (List<Account> bucket : [SELECT Id, Recalcular_Permisos__c FROM Account WHERE Recalcular_Permisos_objetos__c = true]) {
    		for (Account account : bucket) {
    			account.Recalcular_Permisos_Objetos__c= false;
    		}
    		update bucket;
    	}*/
    }

	
}