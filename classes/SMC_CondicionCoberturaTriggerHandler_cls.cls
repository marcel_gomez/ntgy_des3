/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades ejecutadas por el trigger SMC_CondicionCobertura_tgr.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-11-07		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
public class SMC_CondicionCoberturaTriggerHandler_cls {
	
	public static GNF_ManageApprovals_cls clsManageApprovals;
	
	/**
	* @Method: 		opportunitySync
	* @param: 		Map<Id, SObject> mapNewCoverageConditions 
	* @Description:	Sincroniza campos de la condicion de cobertura con la oportunidad de tipo SMC relacionada.
	* @author 		Manuel Medina - 10112017
	*/
	public static void opportunitySync( Map<Id, SObject> mapNewCoverageConditionById ){
		List<SObject> lstOpportunities											= new List<SObject>();
		Map<String, String> mapOppFieldByCCField								= new Map<String, String>( getMapOppFieldByCCField() );
		
		for( SObject sObjCoverageCondition : mapNewCoverageConditionById.values() ){
			if( sObjCoverageCondition.get( 'SMC_Oportunidad__c' ) != null && sObjCoverageCondition.get( 'Estado__c' ) != null && String.valueOf( sObjCoverageCondition.get( 'Estado__c' ) ).equals( System.Label.SMC_CBEstadoHistorico ) && !Boolean.valueOf( sObjCoverageCondition.get( 'GNF_IsStandard__c' ) )  && !Boolean.valueOf( sObjCoverageCondition.get( 'SMC_ReadOnly__c' ) ) ){
				SObject sObjOpportunity											= Schema.getGlobalDescribe().get( 'Opportunity' ).newSObject();
				sObjOpportunity.put( 'Id', sObjCoverageCondition.get( 'SMC_Oportunidad__c' ) );
				
				for( String strCCField : mapOppFieldByCCField.keySet() ){
					sObjOpportunity.put( mapOppFieldByCCField.get( strCCField ), sObjCoverageCondition.get( strCCField ) );
				}
				
				lstOpportunities.add( sObjOpportunity );
			}
		}
		
		if( !lstOpportunities.isEmpty() ){
			update lstOpportunities;
		}
	}
	
	/**
	* @Method: 		getMapOppFieldByCCField
	* @param: 		N/A
	* @Description:	Obtener los campos a sincronizar.
	* @author 		Manuel Medina - 10112017
	*/
	public static Map<String, String> getMapOppFieldByCCField(){
		Map<String, String> mapOppFieldByCCField								= new Map<String, String>();
		List<SMC_SincronizacionCBSMC__mdt> lstSyncFields						= new List<SMC_SincronizacionCBSMC__mdt>( [
																					SELECT Id,
																						SMC_CampoOrigenCB__c,
																						SMC_CampoDestinoSMC__c
																					FROM SMC_SincronizacionCBSMC__mdt
																				] );
																				
		for( SMC_SincronizacionCBSMC__mdt mtdSyncFields : lstSyncFields ){
			mapOppFieldByCCField.put( mtdSyncFields.SMC_CampoOrigenCB__c, mtdSyncFields.SMC_CampoDestinoSMC__c );
		}
		
		return mapOppFieldByCCField;
	}
	
	/**
	* @Method: 		syncCfgApprovalConditions
	* @param: 		Map<Id, Condiciones_de_Cobertura__c> mapNewCoverageConditions 
	* @param: 		Map<Id, Condiciones_de_Cobertura__c> mapOldCoverageConditionById
	* @Description:	Sincroniza las condiciones de aprobacion definidas para SMC en la cofiguracion personalizada
	*				GNF_CondicionesAprobacion__c cada vez que un campo de los definidos en la configuracion cambia
	*				en una o mas condiciones de cobertura estandar.
	* @author 		Manuel Medina - 29112017
	*/
	public static void syncCfgApprovalConditions( Map<Id, Condiciones_de_Cobertura__c> mapNewCoverageConditionById, Map<Id, Condiciones_de_Cobertura__c> mapOldCoverageConditionById ){
		List<GNF_CondicionesAprobacion__c> lstConditionsToUpdate					= new List<GNF_CondicionesAprobacion__c>();
		Map<Id, List<String>> mapConditionsNameByRecordId							= new Map<Id, List<String>>();
		Map<String, Object> mapFieldChangeObjectByType								= new Map<String, Object>();
		Map<Id, SObject> mapCoverageConditionByIdToProcess							= new Map<Id, SObject>();
		
		clsManageApprovals															= new GNF_ManageApprovals_cls( 'Condiciones_de_Cobertura__c', 'SMC' );
		clsManageApprovals.loadApprovalConditionsMap();
		
		mapFieldChangeObjectByType													= haveFieldsChanged( mapNewCoverageConditionById, mapOldCoverageConditionById );
		
		mapCoverageConditionByIdToProcess											= ( Map<Id, SObject> ) mapFieldChangeObjectByType.get( 'RECORDS' );
		mapConditionsNameByRecordId													= ( Map<Id, List<String>> ) mapFieldChangeObjectByType.get( 'CONDITIONS' );
		
		for( Id idRecord : mapConditionsNameByRecordId.keySet() ){
			for( String strConditionName : mapConditionsNameByRecordId.get( idRecord ) ){
				GNF_CondicionesAprobacion__c cfgCondition							= new GNF_CondicionesAprobacion__c();
				cfgCondition.Id														= clsManageApprovals.mapApprovalConditions.get( strConditionName ).Id;
				cfgCondition.GNF_Valor__c											= String.valueOf( mapCoverageConditionByIdToProcess.get( idRecord ).get( clsManageApprovals.mapApprovalConditions.get( strConditionName ).GNF_Campo__c ) );
				
				lstConditionsToUpdate.add( cfgCondition );
			}
		}
		
		update lstConditionsToUpdate;
	}
	
	/**
	* @Method: 		haveFieldsChanged
	* @param: 		Map<Id, SObject> mapNewRecords
	* @param: 		Map<Id, SObject> mapOldRecords
	* @Description: Compara Trigger.newMap con Trigger.oldMap para determinar si el registro debe
					ser validado para asignar aprobadores.
	* @author 		Manuel Medina - 07062017
	*/
	public static Map<String, Object> haveFieldsChanged( Map<Id, SObject> mapNewRecords, Map<Id, SObject> mapOldRecords ){
		Map<Id, SObject> mapRecordsToProcess										= new Map<Id, SObject>();
		Map<Id, List<String>> mapConditionsNameByRecordId							= new Map<Id, List<String>>();
		Map<String, GNF_CondicionesAprobacion__c> mapApprovalConditions				= new Map<String, GNF_CondicionesAprobacion__c>();
		
		for( SObject sObjRecord : mapNewRecords.values() ){
			for( GNF_CondicionesAprobacion__c cfgCondition : clsManageApprovals.mapApprovalConditions.values() ){
				if( Boolean.valueOf( sObjRecord.get( 'GNF_IsStandard__c' ) ) && String.isNotBlank( cfgCondition.GNF_SubProceso__c ) && cfgCondition.GNF_SubProceso__c.equals( String.valueOf( sObjRecord.get( 'Pais__c' ) ) ) ){
					List<String> lstFields											= new List<String>( cfgCondition.GNF_Campo__c.replace( '.', ',' ).split( ',' ) );
					String strField													= lstFields.size() > 1 && lstFields.get( 0 ).contains( '__r' ) ? lstFields.get( 0 ).replace( '__r', '__c' ) : ( lstFields.size() > 1 && !lstFields.get( 0 ).contains( '__r' ) ? lstFields.get( 0 ) + 'Id' : lstFields.get( 0 ) );
					
					if( sObjRecord.get( strField ) != mapOldRecords.get( String.valueOf( sObjRecord.get( 'Id' ) ) ).get( strField ) ){
						mapRecordsToProcess.put( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ), sObjRecord );
						
						if( mapConditionsNameByRecordId.containsKey( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ) ) ){
							mapConditionsNameByRecordId.get( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ) ).add( cfgCondition.Name );
							
						}else if( !mapConditionsNameByRecordId.containsKey( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ) ) ){
							mapConditionsNameByRecordId.put( Id.valueOf( String.valueOf( sObjRecord.get( 'Id' ) ) ), new List<String>{ cfgCondition.Name } );
						}
					}
				}
			}
		}
		
		return new Map<String, Object>{
			'RECORDS'		=> mapRecordsToProcess,
			'CONDITIONS'	=> mapConditionsNameByRecordId
		};
	}
}