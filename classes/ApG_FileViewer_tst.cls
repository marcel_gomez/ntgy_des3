/**
* VASS Latam
* @author            Victor Velandia victor.velandia@vass.es
* Project:           Gas Natural Fenosa - AppGestor
* Description:       Clase test
*
*            No.     Date            Author                  Description
*            -----   ----------      --------------------    ---------------
* @version   1.0     2018-06-13     Victor Velandia (VV)    Clase test para ApG_FileViewer.cls
**************************************************************************************************************************************/
@isTest
private class ApG_FileViewer_tst {

	private static testMethod void test_Scenario_One() {
    	
    	ContentVersion contentVersion_1 = new ContentVersion(
      	Title 				= 	'Gas',
      	PathOnClient 		= 	'Gas.jpg',
      	VersionData 		= 	Blob.valueOf('Test Content')
      	
    	);
    	
    	insert contentVersion_1;
    
	    ContentVersion contentVersion_2 = 	[SELECT Id, Title, ContentDocumentId 
	    										FROM ContentVersion 
	    										WHERE Id = :contentVersion_1.Id 
	    										LIMIT 1
	    									];

	    List<ContentDocument> documents = 	[SELECT Id, Title, LatestPublishedVersionId 
	    										FROM ContentDocument];

	    System.assertEquals(documents.size(), 1);
	    System.assertEquals(documents[0].Id, contentVersion_2.ContentDocumentId);
	    System.assertEquals(documents[0].LatestPublishedVersionId, contentVersion_2.Id);
	    System.assertEquals(documents[0].Title, contentVersion_2.Title);

	    ApG_FileViewer.getContentDocument();
	    
  }
	
	
}