/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_CloneClick_cls.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-06		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
private class GNF_CloneClick_cls_TEST {
	
	static testMethod void scenarioOne(){
		SMC_TestData_cls.createData();
		
		User objUser														= new User();
		objUser																= [SELECT Id
																				FROM User
																				WHERE UserRole.DeveloperName = 'DV_IBERIA_IBERIA'
																				AND IsActive = true
																				LIMIT 1
																			];
		
		Test.startTest();
		
			
		
		System.runAs( objUser ){	
			Opportunity objOpportunity										= [SELECT Id
																				FROM Opportunity
																				WHERE RecordType.DeveloperName = 'Click'
																				LIMIT 1
																			];															
			GNF_CloneClick_cls.cloneClick( objOpportunity.Id, true );
		}
			
		Test.stopTest();
	}
	
	static testMethod void scenarioTwo(){
		SMC_TestData_cls.createData();
		
		Opportunity objOpportunity											= [SELECT Id,
																					AccountId,
																					(
																						SELECT Id, 
																							Contrato__c,
																							Contrato__r.NISS__c,
																							Contrato__r.NISS__r.NIS__c
																						FROM Oportunidad_Puntos_de_Suministros__r
																					)
																				FROM Opportunity
																				WHERE RecordType.DeveloperName = 'Click'
																				AND Name = 'SCAMARA_TARGET_PRICE'
																				LIMIT 1
																			];
																			
		Test.startTest();
		
			GNF_CloneClick_cls.cloneClick( objOpportunity.Id, false );
			
		Test.stopTest();
	}
}