public class AG_TriggerHandlerActividad_Comercial {

    //LOGIC
    private final AG_LogicActividad_Comercial logic;

    //CONSTRUCTOR
    private AG_TriggerHandlerActividad_Comercial() {
        this.logic = new AG_LogicActividad_Comercial();
    }

    //SINGLETON PATTERN
    private static AG_TriggerHandlerActividad_Comercial instance;
    public static AG_TriggerHandlerActividad_Comercial getInstance() {
        if (instance == null) instance = new AG_TriggerHandlerActividad_Comercial();
        return instance;
    }    
    
    //SUMINISTRO HANDLER      
    public void onBeforeInsert(final List<Actividad_Comercial__c> newList) {
        this.logic.assignOwnerFromEmpleado(newList, null);
    }

    public void onBeforeUpdate(final List<Actividad_Comercial__c> newList, final Map<Id, Actividad_Comercial__c> newMap,
                               final List<Actividad_Comercial__c> oldList, final Map<Id, Actividad_Comercial__c> oldMap) {
        //REVIEW: No need to re-calculate owner when the Gestor_External_Key__c has not change.
        this.logic.assignOwnerFromEmpleado(newList, oldMap);
    }

    public void onAfterInsert(final List<Actividad_Comercial__c> newList, final Map<Id, Actividad_Comercial__c> newMap){
       this.logic.setCustomSharing(newList,newMap,null,null);       
    }

    public void onAfterUpdate(final List<Actividad_Comercial__c> newList, final Map<Id, Actividad_Comercial__c> newMap,
                              final List<Actividad_Comercial__c> oldList, final Map<Id, Actividad_Comercial__c> oldMap){
       this.logic.setCustomSharing(newList,newMap,oldList,oldMap);
    }

    public void onAfterDelete(final List<Actividad_Comercial__c> oldList, final Map<Id, Actividad_Comercial__c> oldMap){
    }
}