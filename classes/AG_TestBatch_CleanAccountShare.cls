@isTest
private class AG_TestBatch_CleanAccountShare {

	static Map<String,Account> mockAccounts = AG_TestUtil.getMockAccounts();
	static Map<String,Empleado__c> mockEmpleados = AG_TestUtil.getMockEmpleados();

    @testSetup
    private static void setup() {

    	//Jerarquía de Roles: RoleT <- Role2 <- Role3 <- Role4
        UserRole topRole = new UserRole(name='RoleT');
        insert topRole;
        UserRole userRole2 = new UserRole(name='Role2', parentRoleId= topRole.Id);
        insert userRole2;
        UserRole userRole3 = new UserRole(name='Role3', parentRoleId = userRole2.Id);
        insert userRole3;
        UserRole userRole4 = new UserRole(name='Role4', parentRoleId = userRole3.id);
        insert userRole4;

        //Recuperamos profile: Standard User
        System.debug('Look for Standard USer profile ');
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

        //Dos usuarios: User1 (Role4 y Empleado=111) y User2(Role3 y Empleado=222)
        User theUser1 = new User(
            LastName = 'User1',                 FirstName='Test',   
            Alias = 'tu1',                      Email = 'test.user1@edelta.com',
            Username = 'test.user1@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US',
            UserRoleId= userRole4.Id,           ID_Empleado__c= '111',isActive=true);
        insert theUser1;

        User theUser2 = new User(
            LastName = 'User2',                 FirstName='Test',
            Alias = 'tu',                       Email = 'test.user2@edelta.com',
            Username = 'test.user2@edelta.com', ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',             LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',         LocaleSidKey = 'en_US', 
            UserRoleId= userRole3.Id,           ID_Empleado__c='222', isActive=true);
        insert theUser2;

        User adminUSer = [SELECT Id FROM User WHERE isActive=true AND ProfileId IN (SELECT Id FROM Profile WHERE name='System Administrator') LIMIT 1];
        System.runAs(adminUser) {


            Empleado__c empleado1 = new Empleado__c(name='Empleado1', OwnerId=theUSer1.id, ID_Empleado__c='111');
            insert empleado1;
            Empleado__c empleado2 = new Empleado__c(name='Empleado2', OwnerId=theUSer2.id, ID_Empleado__c='222');
            insert empleado2;

            //Dos Accounts: Account1 (Gestor=Empleado1, Owner=User1), Account2 (Gestor=Empleado2, Owner=User2)
            Account account1 = new Account(name='Account1', Gestor__c=empleado1.Id, OwnerId=theUser1.id);
            insert account1;
            System.debug('@#@# Account1:' + account1);

            Account account2 = new Account(name='Account2', Gestor__c=empleado2.Id, OwnerId=theUser2.id);
            insert account2;
            System.debug('@#@# Account2:' + account1);

            List<Account> xx = [SELECT Id, name, OwnerId FROM Account WHERE name like 'Account_'];
            System.debug('@#@# List accounts:' + xx);
        }
    }

    private static void initData() {

    	//Recuperamos las accounts creadas
		Account account1 = mockAccounts.get('Account1');
		Map<Id, AccountShare> account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		Account account2 = mockAccounts.get('Account2');
		Map<Id, AccountShare> account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		//Actualizamos flag como si se hibieses tocado
		account1.recalcular_permisos__c =true;
		update account1;
		account2.recalcular_permisos__c =true;
		update account2;

		System.debug('TTTT Account1 shares:' + account1Shares);
		System.debug('TTTT Account2 shares:' + account2Shares);

		Empleado__c empleado1 = mockEmpleados.get('Empleado1');
		Empleado__c empleado2 = mockEmpleados.get('Empleado2');

		//Creamos suministro, por lo que se generan shares.
		//Se crean con el Account y Empleado cruzados (Account1 con Empleado2 y Account2 con Empleado1) 
		//ya que si Account y Suministro tienen mismo owner, no se crean Share
		createSuministro('Supply1',account1, empleado2);
		account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		System.debug('TTTT Account1 shares:' + account1Shares);
		System.assert(account1shares.size()>0);

		createSuministro('Supply2',account2, empleado1);
		account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		System.debug('TTTT Account2 shares:' + account2Shares);
		System.assert(account2shares.size()>0);
    }

	@isTest static void cleanAccountShareModified_Test() {

		initData();

		//Recuperamos las Accounts
		Account account1 = mockAccounts.get('Account1');
		Account account2 = mockAccounts.get('Account2');

		//Preaparamos ejecución
		Test.startTest();

		//Ejecutamos batch: clanAll=false, recalculateAfter=false
		Id batchId = AG_Batch_CleanAccountShare.executeProcess(false, false);
		System.debug('Batch Id:' + batchId);

		Test.stopTest();

		//Despues del proceso de borrado, no tendrían que haber shares
		Map<Id, AccountShare> account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		System.debug('TTTT Account1 shares:' + account1Shares);
		System.assertEquals(account1Shares.size(),0);

		Map<Id, AccountShare> account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		System.debug('TTTT Account2 shares:' + account2Shares);
		System.assertEquals(account2Shares.size(),0);

		//Verificamos que la marca recalcular_permisos__c se mantiene ya que no se ha recalculado
		mockAccounts = AG_TestUtil.getMockAccounts();	//Recargamos Acocunts de la BD.
		account1 = mockAccounts.get('Account1');
		account2 = mockAccounts.get('Account2');

		System.assertEquals(account1.recalcular_permisos__c,true);
		System.assertEquals(account2.recalcular_permisos__c,true);

	}


	@isTest static void cleanAllAccountShareModified_Test() {

		initData();

		//Recuperamos las Accounts
		Account account1 = mockAccounts.get('Account1');
		Account account2 = mockAccounts.get('Account2');

		//Vamos a quitar la marca recalcular_permisos, para verificar que se ejecutan para todos independientemente de la marca
		account1.recalcular_permisos__c =false;
		account2.recalcular_permisos__c =false;
		update new List<Account> {account1, account2 };

		//Preaparamos ejecución
		Test.startTest();

		//Ejecutamos batch: clanAll=false, recalculateAfter=false
		Id batchId = AG_Batch_CleanAccountShare.executeProcess(true, false);
		System.debug('Batch Id:' + batchId);

		Test.stopTest();

		//Despues del proceso de borrado, no tendrían que haber shares
		Map<Id, AccountShare> account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		System.debug('TTTT Account1 shares:' + account1Shares);
		System.assertEquals(account1Shares.size(),0);

		Map<Id, AccountShare> account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		System.debug('TTTT Account2 shares:' + account2Shares);
		System.assertEquals(account2Shares.size(),0);

		//Verificamos que la marca recalcular_permisos__c se mantiene ya que no se ha recalculado
		mockAccounts = AG_TestUtil.getMockAccounts();	//Recargamos Acocunts de la BD.
		account1 = mockAccounts.get('Account1');
		account2 = mockAccounts.get('Account2');

		System.assertEquals(account1.recalcular_permisos__c,false);
		System.assertEquals(account2.recalcular_permisos__c,false);

	}


	@isTest static void cleanAndRecalculateAccountSharesModifies() {

		initData();

		Account account1 = mockAccounts.get('Account1');
		Account account2 = mockAccounts.get('Account2');

		//Empezamos test
		Test.startTest();

		//Esta vez, ejecuta proceso de borrado y despues recalcula permisos.
		Id batchId = AG_Batch_CleanAccountShare.executeFullProcess(false);
		System.debug('Batch Id:' + batchId);

		Test.stopTest();

		Map<Id, AccountShare> account1Shares = AG_TestUtil.getMapAccountShares(account1.id);
		System.debug('TTTT Account1 shares:' + account1Shares);
		System.assert(account1Shares.size()>0);	//Como ha recalculado, ha de haber shares

		Map<Id, AccountShare> account2Shares = AG_TestUtil.getMapAccountShares(account2.id);
		System.debug('TTTT Account2 shares:' + account2Shares);
		System.assert(account2Shares.size()>0);	//Como ha recalculado, ha de haber shares

		//Verificamos que la marca recalcular_permisos__c se ha borrado. 
		mockAccounts = AG_TestUtil.getMockAccounts();	//Recargamos Acocunts de la BD.
		account1 = mockAccounts.get('Account1');
		account2 = mockAccounts.get('Account2');

		System.assertEquals(account1.recalcular_permisos__c,false);
		System.assertEquals(account2.recalcular_permisos__c,false);
	}

///////////////////////////


    private static Suministros__c createSuministro(String supplyName, Account cliente, Empleado__c gestor) {

        Suministros__C supply1 = new Suministros__C(
            name= supplyName, 
            Cliente__C= cliente.id,
            gestor__c=gestor.id);
        insert supply1;

        return [SELECT Id, name, Cliente__c, Gestor__c, OwnerId, Gestor_cliente__c, Gestor_external_Key__c FROM Suministros__c WHERE Id = :supply1.id];

        return supply1;
    }

}