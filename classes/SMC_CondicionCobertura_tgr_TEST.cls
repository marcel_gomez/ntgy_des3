/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba encargada e crear los datos cargados en recursos estaticos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2018-01-09		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest
private class SMC_CondicionCobertura_tgr_TEST {
	
	static testMethod void scenarioOne() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createSMC();
		//SMC_TestData_cls.createStandardCB();
		
		test.startTest();
		
		List<Condiciones_de_Cobertura__c> lstCB								= new List<Condiciones_de_Cobertura__c>( [
																				SELECT Id,
																					Unclicks_permitidos__c,
																					GNF_IsStandard__c,
																					Estado__c,
																					SMC_ReadOnly__c,
																					SMC_Oportunidad__c,
																					Tipo_ToP_Clicks__c
																				FROM Condiciones_de_Cobertura__c
																				LIMIT 1
																			] );
																			
		for( Condiciones_de_Cobertura__c objCB : lstCB ){
			objCB.Unclicks_permitidos__c									= false;
			objCB.SMC_ReadOnly__c											= false;
			objCB.Estado__c													= System.Label.SMC_CBEstadoHistorico;
			objCB.SMC_Oportunidad__c										= SMC_TestData_cls.objOpportunity.Id;
			objCB.Tipo_ToP_Clicks__c										= null;
		}
		
		update lstCB;
		
		test.stopTest();
	}
	
	static testMethod void scenarioTwo() {
		SMC_TestData_cls.createData();
		SMC_TestData_cls.createStandardCB();
		
		test.startTest();
		
		List<Condiciones_de_Cobertura__c> lstCB								= new List<Condiciones_de_Cobertura__c>( [
																				SELECT Id,
																					Unclicks_permitidos__c,
																					GNF_IsStandard__c,
																					Estado__c,
																					SMC_ReadOnly__c,
																					SMC_Oportunidad__c,
																					Tipo_ToP_Clicks__c
																				FROM Condiciones_de_Cobertura__c
																				LIMIT 1
																			] );
																			
		for( Condiciones_de_Cobertura__c objCB : lstCB ){
			objCB.Unclicks_permitidos__c									= false;
			objCB.SMC_ReadOnly__c											= false;
		}
		
		update lstCB;
		
		test.stopTest();
	}
}