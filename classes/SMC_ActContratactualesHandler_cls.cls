/**
* VASS
* @author 			Francisco Rojas francisco.rojas@mad.vass.es
* Project:			Gas Natural Fenosa
* Description:		Handler del Trigger SMC_ActuacionesContratactuales_tgr de actuaciones contractuales.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-12-11		Francisco Rojas (FR)		Definicion inicial del handler.
*********************************************************************************************************/
public without sharing class SMC_ActContratactualesHandler_cls {
	
	/**
	* @Method: 		validateUpdateCoverageCondition
	* @param: 		Map<Id, Actuacion_Contractual__c> mapNewContractualActionById
	* @Description:	Actualiza la fecha fin de las condiciones de cobertura asociadas a un contrato basado en la informacion
					entrante de una nueva actuacion contractual
	* @author 		Manuel Medina - 13122017
	*/
	public static void validateUpdateCoverageCondition( Map<Id, Actuacion_Contractual__c> mapNewContractualActionById ){
		Map<Id, Id> mapContractedServiceIdByContractualId								= new Map<Id, Id>();
		Map<Id, Id> mapContractualIdByContractedServiceId								= new Map<Id, Id>();
		Map<Id, List<Condiciones_de_Cobertura__c>> mapCoverageConditionsByContractId	= new Map<Id, List<Condiciones_de_Cobertura__c>>();
		Map<Id, GNF_Condiciones_Cobertura_por_Contrato__c> mapContractConditionsById	= new Map<Id, GNF_Condiciones_Cobertura_por_Contrato__c>();
		Map<Id, Condiciones_de_Cobertura__c> mapCoverageConditionByContractId			= new Map<Id, Condiciones_de_Cobertura__c>();
		List<Condiciones_de_Cobertura__c> lstCoverageConditions							= new List<Condiciones_de_Cobertura__c>();
		
		for( Actuacion_Contractual__c objContactualAction : mapNewContractualActionById.values() ){
			if( String.isNotBlank( objContactualAction.Name_action_subtype__c ) && ( objContactualAction.Name_action_subtype__c.equals( System.Label.SMC_ACEstado1 ) || objContactualAction.Name_action_subtype__c.equals( System.Label.SMC_ACEstado2 ) || objContactualAction.Name_action_subtype__c.equals( System.Label.SMC_ACEstado3 ) ) ){
				mapContractedServiceIdByContractualId.put( objContactualAction.Id, objContactualAction.Id_contracted_service__c );
				mapContractualIdByContractedServiceId.put( objContactualAction.Id_contracted_service__c, objContactualAction.Id );
			}
		}
		
		mapContractConditionsById														= new Map<Id, GNF_Condiciones_Cobertura_por_Contrato__c>( [
																							SELECT Id,
																								GNF_Contrato__c,
																								GNF_Condicion_Cobertura__c,
																								GNF_Condicion_Cobertura__r.Fecha_de_fin__c,
																								GNF_Condicion_Cobertura__r.SMC_Oportunidad__r.RecordType.DeveloperName
																							FROM GNF_Condiciones_Cobertura_por_Contrato__c
																							WHERE GNF_Condicion_Cobertura__r.Estado__c =: System.Label.SMC_ACEstadoCB1 
																							AND GNF_Condicion_Cobertura__r.SMC_Oportunidad__r.RecordType.DeveloperName = 'SMC_Servicio_Multiclick'
																							AND GNF_Contrato__c IN: mapContractedServiceIdByContractualId.values()
																						] );
																						
		for( GNF_Condiciones_Cobertura_por_Contrato__c objContractCondition : mapContractConditionsById.values() ){
			if( mapCoverageConditionsByContractId.containsKey( objContractCondition.GNF_Contrato__c ) ){
				mapCoverageConditionsByContractId.get( objContractCondition.GNF_Contrato__c ).add( new Condiciones_de_Cobertura__c( Id = objContractCondition.GNF_Condicion_Cobertura__c, Fecha_de_fin__c = objContractCondition.GNF_Condicion_Cobertura__r.Fecha_de_fin__c ) );
				
			}else if( !mapCoverageConditionsByContractId.containsKey( objContractCondition.GNF_Contrato__c ) ){
				mapCoverageConditionsByContractId.put( objContractCondition.GNF_Contrato__c, new List<Condiciones_de_Cobertura__c>{ new Condiciones_de_Cobertura__c( Id = objContractCondition.GNF_Condicion_Cobertura__c, Fecha_de_fin__c = objContractCondition.GNF_Condicion_Cobertura__r.Fecha_de_fin__c ) } );
			}
		}
		
		mapCoverageConditionByContractId												= getFutureConditionCoverage( mapCoverageConditionsByContractId );
		
		for( Id idContract : mapCoverageConditionByContractId.keySet() ){
			Condiciones_de_Cobertura__c objCoverageCondition							= new Condiciones_de_Cobertura__c();
			objCoverageCondition														= mapCoverageConditionByContractId.get( idContract );
				
			if( objCoverageCondition.Fecha_de_fin__c != null && objCoverageCondition.Fecha_de_fin__c < mapNewContractualActionById.get( mapContractualIdByContractedServiceId.get( idContract ) ).New_end_date__c ){
				objCoverageCondition.Fecha_de_fin__c									= mapNewContractualActionById.get( mapContractualIdByContractedServiceId.get( idContract ) ).New_end_date__c;
				
				lstCoverageConditions.add( objCoverageCondition );
			}
		}
		
		if( !lstCoverageConditions.isEmpty() ){
			upsert lstCoverageConditions;
		}
	}
	
	/**
	* @Method: 		getFutureConditionCoverage
	* @param: 		Map<Id, List<Condiciones_de_Cobertura__c>> mapCoverageConditionsByContractId
	* @Description:	Devuelve la condicion de covertura mas futura por contrato.
	* @author 		Manuel Medina - 14122017
	*/
	public static Map<Id, Condiciones_de_Cobertura__c> getFutureConditionCoverage( Map<Id, List<Condiciones_de_Cobertura__c>> mapCoverageConditionsByContractId ){
		Map<Id, Condiciones_de_Cobertura__c> mapCoverageConditionByContractId			= new Map<Id, Condiciones_de_Cobertura__c>();
		
		for( Id idContract : mapCoverageConditionsByContractId.keySet() ){
			Map<Long, Condiciones_de_Cobertura__c> mapContidionIdByLongDate				= new Map<Long, Condiciones_de_Cobertura__c>();
			
			for( Condiciones_de_Cobertura__c objConditionCoverage : mapCoverageConditionsByContractId.get( idContract ) ){
				Datetime dttConditionEndDate											= Datetime.newInstance( objConditionCoverage.Fecha_de_fin__c, Time.newInstance( 0, 0, 0, 0 ) );
				mapContidionIdByLongDate.put( dttConditionEndDate.getTime(), objConditionCoverage );
			}
			
			List<Long> lstLongDates														= new List<Long>( mapContidionIdByLongDate.keySet() );
			lstLongDates.sort();
			
			mapCoverageConditionByContractId.put( idContract, mapContidionIdByLongDate.get( lstLongDates.get( lstLongDates.size() - 1 ) ) );
		}
		
		return mapCoverageConditionByContractId;
	}
	
	/**
	* @Method: 		linkCoverageConditionToNewContract
	* @param: 		Map<Id, Actuacion_Contractual__c> mapNewContractualActionById
	* @Description:	Crea un nuevo enlace para una condicion de cobertura con el nuevo contrato referenciado en la actuacion contractual.
	* @author 		Manuel Medina - 14122017
	*/
	public static void linkCoverageConditionToNewContract( Map<Id, Actuacion_Contractual__c> mapNewContractualActionById ){
		Map<Id, Contrato__c> mapOldContractById											= new Map<Id, Contrato__c>();
		Map<Id, Contrato__c> mapNewContractById											= new Map<Id, Contrato__c>();
		Map<Id, Id> mapNewContractIdByOldContractId										= new Map<Id, Id>();
		List<GNF_Condiciones_Cobertura_por_Contrato__c> lstContractConditions			= new List<GNF_Condiciones_Cobertura_por_Contrato__c>();
		
		for( Actuacion_Contractual__c objContactualAction : mapNewContractualActionById.values() ){
			if( String.isNotBlank( objContactualAction.Name_action_subtype__c ) && ( objContactualAction.Name_action_subtype__c.equals( System.Label.SMC_ACEstado4 ) || objContactualAction.Name_action_subtype__c.equals( System.Label.SMC_ACEstado5 ) ) && String.isNotBlank( objContactualAction.Id_contracted_service_old__c ) && !objContactualAction.Id_contracted_service__c.equals( objContactualAction.Id_contracted_service_old__c ) ){
				mapOldContractById.put( objContactualAction.Id_contracted_service_old__c, new Contrato__c() );
				mapNewContractById.put( objContactualAction.Id_contracted_service__c, new Contrato__c() );
				mapNewContractIdByOldContractId.put( objContactualAction.Id_contracted_service_old__c, objContactualAction.Id_contracted_service__c );
			}
		}
		
		mapOldContractById																= new Map<Id, Contrato__c>( [
																							SELECT Id,
																								Estado_Contrato__c,
																								(
																									SELECT Id,
																										GNF_Condicion_Cobertura__c
																									FROM Condiciones_Cobertura_por_Contrato__r
																									WHERE GNF_Condicion_Cobertura__r.Estado__c =: System.Label.SMC_ACEstadoCB1
																									AND GNF_Condicion_Cobertura__r.SMC_Oportunidad__r.RecordType.DeveloperName = 'SMC_Servicio_Multiclick'
																								)
																							FROM Contrato__c
																							WHERE Id IN: mapOldContractById.keySet()
																						] );
																						
		mapNewContractById																= new Map<Id, Contrato__c>( [
																							SELECT Id
																							FROM Contrato__c
																							WHERE Id IN: mapNewContractById.keySet()
																							AND Estado_Contrato__c IN: new List<String>{ System.Label.SMC_ACEstadoContrato1, System.Label.SMC_ACEstadoContrato2, System.Label.SMC_ACEstadoContrato3 }
																						] );
																						
		for( Contrato__c objContract : MapOldContractById.values() ){
			if( mapNewContractIdByOldContractId.containsKey( objContract.Id ) && mapNewContractById.containsKey( mapNewContractIdByOldContractId.get( objContract.Id ) ) ){
				for( GNF_Condiciones_Cobertura_por_Contrato__c objContractCondition : objContract.Condiciones_Cobertura_por_Contrato__r ){
					GNF_Condiciones_Cobertura_por_Contrato__c objNewContractCondition	= new GNF_Condiciones_Cobertura_por_Contrato__c();
					objNewContractCondition.GNF_Condicion_Cobertura__c					= objContractCondition.GNF_Condicion_Cobertura__c;
					objNewContractCondition.GNF_Contrato__c								= mapNewContractIdByOldContractId.get( objContract.Id );
					
					lstContractConditions.add( objNewContractCondition ); 
				}
			}
		}
		
		if( !lstContractConditions.isEmpty() ){
			insert lstContractConditions;
		}
	}
}