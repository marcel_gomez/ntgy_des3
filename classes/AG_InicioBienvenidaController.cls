public class AG_InicioBienvenidaController {
 
    public User u{get;set;}
    public DateTime lastLogin {get; set;}
    
    public AG_InicioBienvenidaController(ApexPages.StandardController controller) {
        u = [SELECT Id, LastLoginDate FROM User WHERE Id = :UserInfo.getUserId()];
        DateTime lastLoginHist = [SELECT loginTime 
        					      FROM LoginHistory 
            					  WHERE userId = :UserInfo.getUserId() 
            					  AND loginTime < :u.LastLoginDate
            					  ORDER BY LoginTime Desc 
            					  LIMIT 1].loginTime;
        lastLogin = lastLoginHist <> null ? lastLoginHist : u.LastLoginDate;
    }    

    public pagereference redirect(){
        //Pagereference p = new PageReference('/apex/AG_PosicionGlobal'); 
        Pagereference p = new PageReference('/home/home.jsp'); 
        return p;
    }

}