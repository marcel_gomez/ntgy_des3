/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Clase de prueba de GNF_StatusOpportunity_CTR.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-08		Manuel Medina (MM)		Definicion inicial de la clase.
*********************************************************************************************************/
@isTest 
private class GNF_StatusOpportunity_CTR_TEST {
	
	static testMethod void scenarioOne(){
		/* BEGIN - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		SMC_TestData_cls.createData();
		/* END - Manuel Medina - Implementacion clase de creacion de datos - 08012018 */
		
		Opportunity objOppotunity											= [SELECT Id,
																					StageName
																				FROM Opportunity
																				LIMIT 1
																			];
		
		Test.startTest();
		
			ApexPages.StandardController sCtrOpportunity					= new ApexPages.StandardController( objOppotunity );
			GNF_StatusOpportunity_CTR ctrStatusOpportunity					= new GNF_StatusOpportunity_CTR( sCtrOpportunity );
			ctrStatusOpportunity.getStageListValues();
			
		Test.stopTest();
	}
}