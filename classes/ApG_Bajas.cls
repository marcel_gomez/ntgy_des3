
public with sharing class ApG_Bajas implements ApG_PosicionGlobalReportInterface{
	public ApG_Bajas() {
		system.debug('iniciando recuperacion de bajas');
	}

	public list<map<string, string>> getsObjectInformation(string itemToGetFrom, string forceUser, boolean isHierarchy){
		list<map<string, string>> retListMap = new list<map<string, string>>();
		set<string> allowedProductSet = new set<string>{
			'Luz',
			'Gas Canalizado',
			'GNL',
			'Gas natural'
		};
		list<Contrato__c> contratoList = new list<Contrato__c>([
			SELECT Id, Name, NISS__c, Fecha_De_Baja__c, Estado_Contrato__c, Producto__c, NISS__r.Gestor__c 
			FROM Contrato__c 
			WHERE Fecha_De_Baja__c <= TODAY 
				AND Fecha_De_Baja__c = THIS_YEAR 
				AND Producto__c IN: allowedProductSet 
				AND Estado_Contrato__c =: 'Baja' 
				AND NISS__r.Cartera_Asignada__c =: True 
				AND Tipo__c != null
				AND Producto__c != null
				AND NISS__r.Name >: itemToGetFrom LIMIT 7000]);

		set<id> sSsIdSet = new set<id>();
		set<id> gestorSet = new set<id>();
		for(Contrato__c i : contratoList){
			sSsIdSet.add(i.NISS__c);
			gestorSet.add(i.NISS__r.Gestor__c);
		}
		map<string, map<string, string>> hierarchyMap =ApG_PosicionGlobalController.getHierarchyMap(forceUser);
		list<SSs__c> ssList = new list<SSs__c>([SELECT Id, Gestor__c, Energia_Contratada__c, (SELECT Id, Estado_Contrato__c, Tipo__c, Producto__c, Fecha_De_Baja__c FROM Contratos__r WHERE Producto__c IN: allowedProductSet AND NISS__r.Cartera_Asignada__c =: True) FROM SSs__c WHERE Id IN: sSsIdSet]);
		list<string> fieldList = new list<string>{
			'Tipo__c',
			'Producto__c'
		};
		for(SSs__c i : ssList){
			boolean allowThisSS = true;
			id theContractToTake;
			date highestUnregistrationDate = null;
			map<id, Contrato__c> thisSScontractMap = new map<id, Contrato__c>();
			for(Contrato__c j : i.Contratos__r){
				thisSScontractMap.put(j.id, j);
				if(j.Fecha_de_Baja__c != null && (highestUnregistrationDate ==null || (highestUnregistrationDate != null && highestUnregistrationDate < j.Fecha_De_Baja__c))){
					highestUnregistrationDate = j.Fecha_De_Baja__c;
					theContractToTake=j.id;
				}
				if(j.Estado_Contrato__c != 'Baja' && allowThisSS){
					allowThisSS = false;
				}
			}
			if(allowThisSS && !string.isBlank(theContractToTake)){
				map<string, string> instMap = new map<string, string>();
	            if(isHierarchy){
	                instMap.putAll(hierarchyMap.get(i.Gestor__c));
	                integer levelNumber = instMap.keySet().size();
	                for(string k : fieldList){
	                    string theLevel = 'lvl' +levelnumber;
	                    instMap.put(theLevel, string.valueOf(thisSScontractMap.get(theContractToTake).get(k)));
	                    levelnumber++;
                	}
	            }else{
	                integer levelNumber = 0;
	                for(string k : fieldList){
	                    string theLevel = 'lvl' +levelNumber;
	                    instMap.put(theLevel, string.valueOf(thisSScontractMap.get(theContractToTake).get(k)));
	                    levelnumber++;
	                }
	                for(string k : hierarchyMap.get(i.Gestor__c).keySet()){
	                    string theLevel = 'lvl' +levelNumber;
	                    instMap.put(theLevel, hierarchyMap.get(i.Gestor__c).get(k));
	                    levelNumber++;
	                }
	            }
	            instMap.put('fieldToSum', string.valueOf(i.Energia_Contratada__c));
	            instMap.put('fieldToGetFrom', i.Name);
	            instMap.put('id', i.id);
	            retListMap.add(instMap);
			}
		}
		return retListMap;
	}
}