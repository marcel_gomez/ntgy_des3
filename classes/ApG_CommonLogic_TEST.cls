@isTest
private class ApG_CommonLogic_TEST {
	
	@testSetup
	private static void testSetup(){
		map<string, id> profileMap = ApG_TestUtil.getProfileMap(new set<string>{'Custom Standar Gestores'}); 
		map<string, id> roleMap = ApG_TestUtil.getRoleMap(new set<string>{'MAYORISTAS', 'DV_IBERIA_IBERIA'});
		User usr0, usr1, usr2;
		system.runAs(new User(Id = userInfo.getUserId())){
			usr0 = ApG_TestUtil.getUser('fName0', 'lName0', profileMap.get('Custom Standar Gestores'), roleMap.get('MAYORISTAS'));
			usr1 = ApG_TestUtil.getUser('fName1', 'lName1', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
			usr2 = ApG_TestUtil.getUser('fName2', 'lName2', profileMap.get('Custom Standar Gestores'), roleMap.get('DV_IBERIA_IBERIA'));
			usr0.ID_Gestor__c = 'Desesperado0';
			usr1.ID_Gestor__c = 'Desesperado1';
			usr2.ID_Gestor__c = 'Desesperado2';			
			usr0.ID_Empleado__c = 'Desesperado0';
			usr1.ID_Empleado__c = 'Desesperado1';
			usr2.ID_Empleado__c = 'Desesperado2';
			list<User> userList = new list<User>{usr0, usr1, usr2};
			insert userList;
		}
		Empleado__c emp0 = ApG_TestUtil.getEmpleado('Desesperado0','Desesperado0','','theHigh','España');
		Empleado__c emp1 = ApG_TestUtil.getEmpleado('Desesperado1','Desesperado1','Desesperado0','theMiddle','España');
		Empleado__c emp2 = ApG_TestUtil.getEmpleado('Desesperado2','Desesperado2','Desesperado1','theLow','España');
		list<Empleado__c> empList = new list<Empleado__c>{emp0, emp1, emp2};
		insert empList;

		system.runAs(usr0){
			Account acc0;
			acc0 = ApG_TestUtil.getAccount('acc0', 'España', 'acc0');
			acc0.Gestor__c = emp0.id;
			insert acc0;
		}
		system.runAs(usr1){
			Account acc1;
			acc1 = ApG_TestUtil.getAccount('acc1', 'España', 'acc1');
			acc1.Gestor__c = emp1.id;
			insert acc1;
		}

	}
	

	@isTest 
	private static void testForAssignOwnerAndShare(){
		Account acc0 = [SELECT Id FROM Account WHERE Name =: 'acc0'];
		Empleado__c emp2 = [SELECT Id FROM Empleado__c WHERE Name =: 'theLow theLow'];
		User usr2 = [SELECT Id FROM User WHERE FirstName =: 'fName2'];
		AccountShare shareForusr2 = new AccountShare(
			AccountId = acc0.id, 
			AccountAccessLevel = 'Read',
			OpportunityAccessLevel = 'Read', 
			UserOrGroupId = usr2.id);
		insert shareForUsr2;
		SRs__c srs0 = ApG_TestUtil.getSRs(emp2.id, acc0.id);
		srs0.name = '00';
		srs0.Estado__c = 'Cerrada';
		list<SRs__c> SRsList = new list<SRs__c>{srs0};
		insert SRsList;
		
		Account acc1 = [SELECT Id FROM Account WHERE Name =: 'acc1'];
		srs0.Cliente__c = acc1.id;
		update srs0;

		Empleado__c emp1 = [SELECT Id FROM Empleado__c WHERE Name =: 'theMiddle theMiddle'];
		srs0.Gestor__c = emp1.id; 
		update srs0;
	}

	
}