({
	// Función que ejecuta el método getReportsForSearch de
	// la clase AG_Reports_Controller y obtiene un array
	// List<report> con Id y Name de los informes.
    getReportsForSearch : function(component) {       
        var action = component.get("c.getReportsForSearch");
        var self = this;
        action.setCallback(this, function(a){
			var reportList = a.getReturnValue();
            component.set("v.reportList", reportList);
            console.log('reportList: ' + reportList);
            // Muestra toast message indicando el estado de la carga.
            var toastEvent = $A.get("e.force:showToast");
            if(action.getState() ==='SUCCESS'){
                toastEvent.setParams({"title": "Preparado!!!", "duration": 500 });
            }else{
                toastEvent.setParams({"title": "Error!!!"});
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    },    

	// Función que ejecuta el método getReportResponse de
	// la clase AG_Reports_Controller y obtiene un array
	// de json con todos los datos del informe, pasando como
	// parámetro el id del informe.    
    getReportResponse : function(component, event, helper) {
        var action = component.get("c.getReportResponse");
		var report = event.getParam("report");
        action.setParams({"reportId": report.Id});
        action.setCallback(this, function(a){            
			var reportResponseObj = JSON.parse(a.getReturnValue());        
            component.set("v.tabResp", reportResponseObj.tabResp);
            component.set("v.sumResp", reportResponseObj.sumResp);
            component.set("v.reportResponse", reportResponseObj);

			console.log('reportResponseObj');
            console.log(reportResponseObj); 
            
            // Muestra toast message indicando el estado de la carga.
            var toastEvent = $A.get("e.force:showToast");
            if(action.getState() ==='SUCCESS'){
                toastEvent.setParams({
                    "title": "Informe cargado!!!",
                    "duration": 500
                });
            }else{
                toastEvent.setParams({"title": "Error!!!"});
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    },

})