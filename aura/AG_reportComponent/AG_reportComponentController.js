({
    doInit : function(component, event, helper) {
        // Devuelve un listado con todos los informes.
        helper.getReportsForSearch(component);
    },
    
    loadReport : function(component, event, helper) {
        // Devuelve el listado del informe seleccionado.
        helper.getReportResponse(component, event, helper);   
    },
    
})