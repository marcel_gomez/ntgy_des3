<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_PDFAsignarIdExternoPlantilla</fullName>
        <field>GNF_IdExternoPlantilla__c</field>
        <formula>GNF_Plantilla__r.GNF_NombreUnico__c</formula>
        <name>GNF PDF Asignar Id Externo de Plantilla</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_PDFGenerarIdExternoSeccion</fullName>
        <field>GNF_IdExterno__c</field>
        <formula>SUBSTITUTE( GNF_Plantilla__r.GNF_NombreUnico__c, &quot;_&quot;, &quot;&quot; ) + SUBSTITUTE( Name, &quot;-&quot;, &quot;&quot; )</formula>
        <name>GNF PDF Generar Id Externo - Sección</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF_PDFAsignarIdExternoPlantilla</fullName>
        <actions>
            <name>GNF_PDFAsignarIdExternoPlantilla</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 	ISCHANGED( GNF_Plantilla__c ), 	ISBLANK( GNF_IdExternoPlantilla__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GNF_PDFGenerarIdExterno_Seccion</fullName>
        <actions>
            <name>GNF_PDFGenerarIdExternoSeccion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GNF_Seccion__c.GNF_IdExterno__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
