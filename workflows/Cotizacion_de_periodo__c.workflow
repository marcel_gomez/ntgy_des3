<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_UpdateCVClienteClickPrima</fullName>
        <field>GNF_CVClientePrima_Click__c</field>
        <formula>IF( 
AND( 
Opportunity__r.RecordType.DeveloperName = &apos;Click&apos;, 
TEXT(Opportunity__r.Account.Pais__c) = &apos;España&apos;, 
TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP PF&apos; 
), 
CV_Cliente__c + IF( ISNULL( Opportunity__r.Prima_de_riesgo__c ), 0, Opportunity__r.Prima_de_riesgo__c ) + IF( ISNULL( Opportunity__r.Prima_de_gestion__c ), 0, Opportunity__r.Prima_de_gestion__c ) + IF( ISNULL( Opportunity__r.Prima_comercial__c ), 0, Opportunity__r.Prima_comercial__c ), 
IF( 
AND( 
Opportunity__r.RecordType.DeveloperName = &apos;Unclick&apos;, 
TEXT(Opportunity__r.Account.Pais__c) = &apos;España&apos;, 
TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP PF&apos; 
), 
CV_Cliente__c - IF( ISNULL( Opportunity__r.Prima_de_gestion__c ), 0, Opportunity__r.Prima_de_gestion__c ) - IF( ISNULL( Opportunity__r.Prima_comercial__c ), 0, Opportunity__r.Prima_comercial__c ), 
IF( 
AND( 
Opportunity__r.RecordType.DeveloperName = &apos;Click&apos;, 
TEXT(Opportunity__r.Account.Pais__c) = &apos;Portugal&apos;, 
TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP PF&apos; 
), 
CV_Cliente__c + IF( ISNULL( Opportunity__r.Prima_de_riesgo__c ), 0, Opportunity__r.Prima_de_riesgo__c ) + IF( ISNULL( Opportunity__r.Prima_de_gestion__c ), 0, Opportunity__r.Prima_de_gestion__c ) + IF( ISNULL( Opportunity__r.Prima_comercial__c ), 0, Opportunity__r.Prima_comercial__c ), 
IF( 
AND( 
Opportunity__r.RecordType.DeveloperName = &apos;Unclick&apos;, 
TEXT(Opportunity__r.Account.Pais__c) = &apos;Portugal&apos;, 
TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP PF&apos; 
), 
CV_Cliente__c - IF( ISNULL( Opportunity__r.Prima_de_gestion__c ), 0, Opportunity__r.Prima_de_gestion__c ) - IF( ISNULL( Opportunity__r.Prima_comercial__c ), 0, Opportunity__r.Prima_comercial__c ), 
IF( 
AND( 
Opportunity__r.RecordType.DeveloperName = &apos;Click&apos;,
OR(TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP Fórmula&apos;, TEXT(Opportunity__r.Tipo_de_servicio__c) = &apos;SWAP Hub&apos; ) 
), 
Opportunity__r.GNF_Spread_Cliente__c, 
0 
) 
) 
) 
) 
)</formula>
        <name>Update CV&apos; Cliente Click</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF_CalculoCVClientePrima</fullName>
        <actions>
            <name>GNF_UpdateCVClienteClickPrima</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Opportunity__r.StageName, &apos;Cotizada&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
