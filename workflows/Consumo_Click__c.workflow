<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_SubirValor_F</fullName>
        <field>GNF_PDF_ValorF_Consumo__c</field>
        <formula>GNF_ValorF__c</formula>
        <name>GNF_SubirValor_F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Oportunidad_Punto_de_Suministro__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_SubirValor_K</fullName>
        <field>GNF_PDF_ValorK_Consumo__c</field>
        <formula>GNF_ValorK__c</formula>
        <name>GNF_SubirValor_K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Oportunidad_Punto_de_Suministro__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>GNF_SubirValores_F_K</fullName>
        <actions>
            <name>GNF_SubirValor_F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GNF_SubirValor_K</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sube los valores de F y K a la cotización para calcular el PF Anexo</description>
        <formula>IF(  	ISNEW(), 	true, 	OR(  		ISCHANGED( GNF_ValorF__c ),  		ISCHANGED( GNF_ValorK__c )  	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
