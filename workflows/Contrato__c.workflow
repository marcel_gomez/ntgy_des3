<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_ActualizarCUPSContrato</fullName>
        <field>GNF_CUPSText__c</field>
        <formula>NISS__r.CUPS__c</formula>
        <name>GNF Actualizar CUPS Contrato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF Actualizar CUPS Contratos</fullName>
        <actions>
            <name>GNF_ActualizarCUPSContrato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
