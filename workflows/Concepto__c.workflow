<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Actualizar_Valor_de_IHC</fullName>
        <field>IHC__c</field>
        <formula>Impuesto_Hidrocarburos__r.Valor__c</formula>
        <name>Actualizar - Valor de IHC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_ActualizarValorF</fullName>
        <field>Valor_de_F__c</field>
        <formula>IF( 
NOT( ISBLANK( Maestro_Peaje__c ) ) &amp;&amp; 
ISPICKVAL( Maestro_Peaje__r.Peaje__c , &quot;GNL&quot; ), 
ATR__c + Formula_contratada_tramo_Maestro__r.Termino_independiente__c + IHC__c,
ATR__c + Formula_contratada_tramo_Maestro__r.Termino_independiente__c 
)</formula>
        <name>Actualizar - Valor de F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_Actualizar_Peaje_Contrato</fullName>
        <field>Tarifa_Acceso_Concepto_TV__c</field>
        <formula>Maestro_Peaje__r.Name</formula>
        <name>GNF Actualizar Peaje Contrato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contrato__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_actualizarValorK</fullName>
        <description>Actualizar - Valor de K</description>
        <field>GNF_ValorK__c</field>
        <formula>Formula_contratada_tramo_Maestro__r.Termino_independiente__c</formula>
        <name>Actualizar - Valor de K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Volcar_Codigo_Concepto</fullName>
        <field>Name</field>
        <formula>TEXT(Codigo_Concepto_Ficticio__c)</formula>
        <name>Volcar Código Concepto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Volcar_Descripcion_Concepto</fullName>
        <field>Descripcion__c</field>
        <formula>TEXT( Descripcion_Concepto_Ficticio__c )</formula>
        <name>Volcar Descripción Concepto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Volcar_Tipo_Tarifa</fullName>
        <field>Tipo_Tarifa__c</field>
        <formula>IF ( 
	OR( ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP1064&apos;), ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP8003&apos;),ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP8004&apos;)), 
	&apos;Cost Plus&apos;, 
	IF ( 
	OR( ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP3020&apos;), ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP3033&apos;),ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP3034&apos;)), &apos;CostPlus Portugal&apos;, &apos;&apos; ) )</formula>
        <name>Volcar Tipo Tarifa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF Calculo Valores F y K - Concepto</fullName>
        <actions>
            <name>Actualizar_Valor_de_IHC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GNF_ActualizarValorF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GNF_actualizarValorK</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Valida las condiciones en las que se debe calcular los valores los valores de K y F en los conceptos.</description>
        <formula>AND( Contrato__r.Ficticio__c, NOT( ISBLANK( Formula_contratada_tramo_Maestro__c ) ), OR(      ISCHANGED( Formula_contratada_tramo_Maestro__c ),     ISCHANGED( Maestro_Peaje__c ),     ISCHANGED( Impuesto_Hidrocarburos__c ),     ISBLANK( Valor_de_F__c ),     ISBLANK( GNF_ValorK__c )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GNF_Volcar Código y Descripción</fullName>
        <actions>
            <name>Volcar_Codigo_Concepto</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Volcar_Descripcion_Concepto</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Volcar_Tipo_Tarifa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Para ficticios</description>
        <formula>AND( Ficticio__c,  NOT(ISNEW()) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GNF_Volcar_Peaje</fullName>
        <actions>
            <name>GNF_Actualizar_Peaje_Contrato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Ficticio__c ,       ISCHANGED( Maestro_Peaje__c ),       OR( ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP1064&apos;),      ISPICKVAL(Codigo_Concepto_Ficticio__c , &apos;CONCEP3020&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
