<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_SectorSuministroActualizarPais</fullName>
        <field>GNF_Pais__c</field>
        <formula>NIS__r.GNF_Pais__c</formula>
        <name>GNF Sector Suministro - Actualizar pais</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF Sector Suministro - Actualizar pais</fullName>
        <actions>
            <name>GNF_SectorSuministroActualizarPais</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
