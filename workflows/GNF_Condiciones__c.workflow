<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GNF_PDFAsignarIdExternoComponente</fullName>
        <field>GNF_IdExternoComponente__c</field>
        <formula>GNF_Componente__r.GNF_IdExterno__c</formula>
        <name>GNF PDF Asignar Id Externo de Componente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_PDFAsignarIdExternoSeccion</fullName>
        <field>GNF_IdExternoSeccion__c</field>
        <formula>GNF_Seccion__r.GNF_IdExterno__c</formula>
        <name>GNF PDF Asignar Id Externo de Sección</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GNF_PDFGenerarIdExternoCondiciones</fullName>
        <field>GNF_IdExterno__c</field>
        <formula>IF(
	NOT( ISBLANK(  GNF_Componente__c ) ),
	SUBSTITUTE( GNF_Componente__r.GNF_IdExterno__c, &quot;_&quot;, &quot;&quot; ) + SUBSTITUTE( Name, &quot;-&quot;, &quot;&quot; ),
	IF(
		NOT( ISBLANK(  GNF_Seccion__c ) ),
		SUBSTITUTE( GNF_Seccion__r.GNF_IdExterno__c, &quot;_&quot;, &quot;&quot; ) + SUBSTITUTE( Name, &quot;-&quot;, &quot;&quot; ),
		&quot;&quot;
	)
)</formula>
        <name>GNF PDF Generar Id Externo - Condiciones</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GNF_PDFAsignarIdExternoComponente</fullName>
        <actions>
            <name>GNF_PDFAsignarIdExternoComponente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 	ISCHANGED( GNF_Componente__c ), 	ISBLANK( GNF_IdExternoComponente__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GNF_PDFAsignarIdExternoSeccion</fullName>
        <actions>
            <name>GNF_PDFAsignarIdExternoSeccion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 	ISCHANGED( GNF_Seccion__c ), 	ISBLANK( GNF_IdExternoSeccion__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GNF_PDFGenerarIdExterno_Condiciones</fullName>
        <actions>
            <name>GNF_PDFGenerarIdExternoCondiciones</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>GNF_Condiciones__c.GNF_IdExterno__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
