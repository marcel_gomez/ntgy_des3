<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>InfoFactyCobroJerarquía</label>
    <protected>false</protected>
    <values>
        <field>Color__c</field>
        <value xsi:type="xsd:string">#f18423</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>GroupLabel__c</field>
        <value xsi:type="xsd:string">infoFacturacionYcobro</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">hierarchy</value>
    </values>
</CustomMetadata>
