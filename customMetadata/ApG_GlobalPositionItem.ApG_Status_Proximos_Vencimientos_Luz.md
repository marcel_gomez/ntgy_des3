<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG_Status_Proximos Vencimientos Luz</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ApexClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldToSum__c</field>
        <value xsi:type="xsd:string">Consumo_Contratado_GWh__c</value>
    </values>
    <values>
        <field>GlobalPositionGroup__c</field>
        <value xsi:type="xsd:string">InfoCarteraEstado</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Contrato__c</value>
    </values>
    <values>
        <field>Query_Filter__c</field>
        <value xsi:type="xsd:string">RecordTypeDevName__c = &apos;Contrato_de_Luz&apos; And Intervalo__c != null and Fecha_de_Fin__c &gt;= Today and Fecha_de_Fin__c   &lt;= NEXT_N_MONTHS: 6 and Contrato_Activo__c = true</value>
    </values>
    <values>
        <field>SummaryUnits__c</field>
        <value xsi:type="xsd:string">GWh</value>
    </values>
    <values>
        <field>TitleLabel__c</field>
        <value xsi:type="xsd:string">Próximos Vencimientos - Luz</value>
    </values>
    <values>
        <field>lvl0__c</field>
        <value xsi:type="xsd:string">Intervalo__c</value>
    </values>
    <values>
        <field>lvl1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl4__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
