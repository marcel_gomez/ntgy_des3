<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG_Status_Accesos Pendientes Gas</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ApexClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldToSum__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>GlobalPositionGroup__c</field>
        <value xsi:type="xsd:string">InfoCarteraEstado</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">11.0</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Gestion_Accesos__c</value>
    </values>
    <values>
        <field>Query_Filter__c</field>
        <value xsi:type="xsd:string">Estado_solicitud__c != null and Tipo_solicitud__c != null and Pendiente__c = true</value>
    </values>
    <values>
        <field>SummaryUnits__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>TitleLabel__c</field>
        <value xsi:type="xsd:string">Acceso Pendiente - Gas</value>
    </values>
    <values>
        <field>lvl0__c</field>
        <value xsi:type="xsd:string">Estado_solicitud__c</value>
    </values>
    <values>
        <field>lvl1__c</field>
        <value xsi:type="xsd:string">Tipo_solicitud__c</value>
    </values>
    <values>
        <field>lvl2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl4__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
