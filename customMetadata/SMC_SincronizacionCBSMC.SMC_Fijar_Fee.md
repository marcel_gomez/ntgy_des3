<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Fijar Fee</label>
    <protected>false</protected>
    <values>
        <field>SMC_CampoDestinoSMC__c</field>
        <value xsi:type="xsd:string">SMC_PDF_Fijar_Fee__c</value>
    </values>
    <values>
        <field>SMC_CampoOrigenCB__c</field>
        <value xsi:type="xsd:string">Fijar_Fee_gestion__c</value>
    </values>
</CustomMetadata>
