<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>InfoCarteraJerarquía</label>
    <protected>false</protected>
    <values>
        <field>Color__c</field>
        <value xsi:type="xsd:string">#6d96ac</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>GroupLabel__c</field>
        <value xsi:type="xsd:string">infoCartera</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">hierarchy</value>
    </values>
</CustomMetadata>
