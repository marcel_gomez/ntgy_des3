<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG_Status_Ofertas Curso Luz - Jer</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ApexClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldToSum__c</field>
        <value xsi:type="xsd:string">Consumo_GWh__c</value>
    </values>
    <values>
        <field>GlobalPositionGroup__c</field>
        <value xsi:type="xsd:string">infoComercialJerarquia</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Ofertas__c</value>
    </values>
    <values>
        <field>Query_Filter__c</field>
        <value xsi:type="xsd:string">RecordTypeDevName__c = &apos;Oferta_de_Luz&apos;  AND Estado__c != null AND Ofertas_en_curso__c = true AND Cartera_asignada__c = true AND Sector__c = &apos;Luz&apos;</value>
    </values>
    <values>
        <field>SummaryUnits__c</field>
        <value xsi:type="xsd:string">GWh</value>
    </values>
    <values>
        <field>TitleLabel__c</field>
        <value xsi:type="xsd:string">Ofertas En Curso - Luz</value>
    </values>
    <values>
        <field>lvl0__c</field>
        <value xsi:type="xsd:string">Estado__c</value>
    </values>
    <values>
        <field>lvl1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl4__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
