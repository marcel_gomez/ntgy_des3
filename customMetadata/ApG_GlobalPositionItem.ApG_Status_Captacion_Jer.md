<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG_Status_Captacion Jer</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ApexClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldToSum__c</field>
        <value xsi:type="xsd:string">Consumo_Contratado_GWh__c</value>
    </values>
    <values>
        <field>GlobalPositionGroup__c</field>
        <value xsi:type="xsd:string">InfoCarteraJerarquia</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Contrato__c</value>
    </values>
    <values>
        <field>Query_Filter__c</field>
        <value xsi:type="xsd:string">Tipo__c != null AND Producto__c != null AND Contrato_Activo__c = true and Fecha_de_Alta__c &lt;= TODAY and Fecha_de_Alta__c  = THIS_YEAR AND (Producto__c = &apos;Luz&apos; OR Producto__c  = &apos;Gas Canalizado&apos; OR Producto__c  =&apos;GNL&apos; OR Producto__c =&apos;Gas natural&apos;)</value>
    </values>
    <values>
        <field>SummaryUnits__c</field>
        <value xsi:type="xsd:string">GWh</value>
    </values>
    <values>
        <field>TitleLabel__c</field>
        <value xsi:type="xsd:string">Captación</value>
    </values>
    <values>
        <field>lvl0__c</field>
        <value xsi:type="xsd:string">Tipo__c</value>
    </values>
    <values>
        <field>lvl1__c</field>
        <value xsi:type="xsd:string">Producto__c</value>
    </values>
    <values>
        <field>lvl2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl4__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
