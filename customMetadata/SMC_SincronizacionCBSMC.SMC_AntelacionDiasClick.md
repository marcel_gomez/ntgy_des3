<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Antelación Días Click</label>
    <protected>false</protected>
    <values>
        <field>SMC_CampoDestinoSMC__c</field>
        <value xsi:type="xsd:string">Antelacion_de_click__c</value>
    </values>
    <values>
        <field>SMC_CampoOrigenCB__c</field>
        <value xsi:type="xsd:string">Antelacion_click_dias__c</value>
    </values>
</CustomMetadata>
