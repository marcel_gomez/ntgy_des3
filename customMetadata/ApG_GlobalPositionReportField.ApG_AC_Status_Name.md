<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG AC Status Name</label>
    <protected>false</protected>
    <values>
        <field>APInameOfTheLookup__c</field>
        <value xsi:type="xsd:string">Id</value>
    </values>
    <values>
        <field>ColumnLabel__c</field>
        <value xsi:type="xsd:string">Código Actividad Comercial</value>
    </values>
    <values>
        <field>ColumnNumber__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>FieldAPIName__c</field>
        <value xsi:type="xsd:string">Name</value>
    </values>
    <values>
        <field>GlobalPositionItem__c</field>
        <value xsi:type="xsd:string">ApG_Status_Actividades_Comerciales</value>
    </values>
</CustomMetadata>
