<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ApG_Status_Contrato Activo Gas</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ApexClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldToSum__c</field>
        <value xsi:type="xsd:string">Consumo_Contratado_GWh__c</value>
    </values>
    <values>
        <field>GlobalPositionGroup__c</field>
        <value xsi:type="xsd:string">InfoCarteraEstado</value>
    </values>
    <values>
        <field>GlobalPositionOrder__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Contrato__c</value>
    </values>
    <values>
        <field>Query_Filter__c</field>
        <value xsi:type="xsd:string">RecordTypeDevName__c = &apos;Contrato_de_Gas&apos; And Estado_Contrato__c != null AND Producto__c != null and Contrato_Activo__c = true</value>
    </values>
    <values>
        <field>SummaryUnits__c</field>
        <value xsi:type="xsd:string">GWh</value>
    </values>
    <values>
        <field>TitleLabel__c</field>
        <value xsi:type="xsd:string">Contratos Activos - Gas</value>
    </values>
    <values>
        <field>lvl0__c</field>
        <value xsi:type="xsd:string">Producto__c</value>
    </values>
    <values>
        <field>lvl1__c</field>
        <value xsi:type="xsd:string">Estado_Contrato__c</value>
    </values>
    <values>
        <field>lvl2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>lvl4__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
