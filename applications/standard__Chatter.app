<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>Posicion_Global</tab>
    <tab>Mis_Archivos</tab>
    <tab>GNF_Plantilla__c</tab>
    <tab>Concepto_Historico__c</tab>
    <tab>Cuenta_Contrato__c</tab>
</CustomApplication>
