<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Each record of this object is a child item in Posicion Global, grouped by their parent &quot;Global Position Group&quot;</description>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>ApG Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ApexClassName__c</fullName>
        <description>Contains the name of the apex class that should return de information displayed on the Posicion Global page</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>ApexClassName</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FieldToSum__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Optional: Type here the API name of the field where it is stored a numeric value that should be sumarized</inlineHelpText>
        <label>Field To Sum</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GlobalPositionGroup__c</fullName>
        <description>Items are grouped by a Global Position Group</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Global Position Group</label>
        <referenceTo>ApG_GlobalPositionGroup__mdt</referenceTo>
        <relationshipLabel>Global Position Items</relationshipLabel>
        <relationshipName>GlobalPositionItems</relationshipName>
        <required>true</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GlobalPositionOrder__c</fullName>
        <description>Stores the position under its parent group in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Order of the Item under its parent in Global Position</inlineHelpText>
        <label>Order in Global Position</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ObjectName__c</fullName>
        <description>Stores the API name of the object which should be displayed on this item of Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>API name of the object which should be displayed on this item of Posicion Global</inlineHelpText>
        <label>Object Name</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Query_Filter__c</fullName>
        <description>Filter that should be used to retrieve from database the records which should be displayed in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Type a filter for the query which should retrieve the records from database displayed on this item.
Use SOQL syntax, for instance: Type = &apos;Customer&apos; AND Industry__c = &apos;Banking&apos;</inlineHelpText>
        <label>Query filter</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>SummaryUnits__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>If the report in this item has a field to sumarize, type in this field the units of this summary field</inlineHelpText>
        <label>Summary Units</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TitleLabel__c</fullName>
        <description>Stores the name of the item which should appear in Global Position</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Name of the Item which should appear in Global Position</inlineHelpText>
        <label>Item Label</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Srs Activas</fullName>
                    <default>true</default>
                    <label>Srs Activas</label>
                </value>
                <value>
                    <fullName>Actividades Comerciales</fullName>
                    <default>false</default>
                    <label>Actividades Comerciales</label>
                </value>
                <value>
                    <fullName>Ofertas En Curso - Luz</fullName>
                    <default>false</default>
                    <label>Ofertas En Curso - Luz</label>
                </value>
                <value>
                    <fullName>Ofertas En Curso - Gas</fullName>
                    <default>false</default>
                    <label>Ofertas En Curso - Gas</label>
                </value>
                <value>
                    <fullName>Roles</fullName>
                    <default>false</default>
                    <label>Roles</label>
                </value>
                <value>
                    <fullName>Bajas</fullName>
                    <default>false</default>
                    <label>Bajas</label>
                </value>
                <value>
                    <fullName>Captación</fullName>
                    <default>false</default>
                    <label>Captación</label>
                </value>
                <value>
                    <fullName>Contratos Activos - Luz</fullName>
                    <default>false</default>
                    <label>Contratos Activos - Luz</label>
                </value>
                <value>
                    <fullName>Contratos Activos - Gas</fullName>
                    <default>false</default>
                    <label>Contratos Activos - Gas</label>
                </value>
                <value>
                    <fullName>Próximos Vencimientos - Luz</fullName>
                    <default>false</default>
                    <label>Próximos Vencimientos - Luz</label>
                </value>
                <value>
                    <fullName>Próximos Vencimientos - Gas</fullName>
                    <default>false</default>
                    <label>Próximos Vencimientos - Gas</label>
                </value>
                <value>
                    <fullName>Acceso Pendiente - Luz</fullName>
                    <default>false</default>
                    <label>Acceso Pendiente - Luz</label>
                </value>
                <value>
                    <fullName>Acceso Pendiente - Gas</fullName>
                    <default>false</default>
                    <label>Acceso Pendiente - Gas</label>
                </value>
                <value>
                    <fullName>Consumo Real - Luz</fullName>
                    <default>false</default>
                    <label>Consumo Real - Luz</label>
                </value>
                <value>
                    <fullName>Consumo Real - Gas</fullName>
                    <default>false</default>
                    <label>Consumo Real - Gas</label>
                </value>
                <value>
                    <fullName>Facturación Emitida - Luz GWh</fullName>
                    <default>false</default>
                    <label>Facturación Emitida - Luz GWh</label>
                </value>
                <value>
                    <fullName>Facturación Emitida - Gas GWh</fullName>
                    <default>false</default>
                    <label>Facturación Emitida - Gas GWh</label>
                </value>
                <value>
                    <fullName>Facturación Emitida - Luz M€</fullName>
                    <default>false</default>
                    <label>Facturación Emitida - Luz M€</label>
                </value>
                <value>
                    <fullName>Facturación Emitida - Gas M€</fullName>
                    <default>false</default>
                    <label>Facturación Emitida - Gas M€</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>lvl0__c</fullName>
        <description>Stores the field API name which value should appear as the level0 on the tree structure of the records in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>lvl0</label>
        <length>100</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>lvl1__c</fullName>
        <description>Stores the field API name which value should appear as the level1 on the tree structure of the records in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>lvl1</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>lvl2__c</fullName>
        <description>Stores the field API name which value should appear as the level2 on the tree structure of the records in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>lvl2</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>lvl3__c</fullName>
        <description>Stores the field API name which value should appear as the level3 on the tree structure of the records in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>lvl3</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>lvl4__c</fullName>
        <description>Stores the field API name which value should appear as the level4 on the tree structure of the records in Posicion Global</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>lvl4</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Global Position Item</label>
    <pluralLabel>Global Position Items</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
