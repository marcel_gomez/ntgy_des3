/**
* VASS
* @author           Manuel Medina manuel.medina@vass.es
* Project:          Gas Natural Fenosa
* Description:      Implementa funcinalidades adicionales para los consumos click.
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-09-13      Manuel Medina (MM)      Definicion inicial del trigger.
*********************************************************************************************************/
trigger GNF_ConsumoClick_tgr on Consumo_Click__c ( /*after insert,*/ after update ){
    GNF_ConsumoClickTriggerHandler_cls.createQuarterRecords( Trigger.newMap );
}