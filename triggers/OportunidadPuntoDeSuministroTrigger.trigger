/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades adicionales para los puntos de suministro de la oportunidad.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	2.0		2017-05-11		Manuel Medina (MM)		Lamado al metodo validateOpportunitySupplies
*															para evento before update.
*********************************************************************************************************/
trigger OportunidadPuntoDeSuministroTrigger on Oportunidad_Punto_de_Suministros__c ( before update ) {
    
	final OpPtoSuministroTriggerHandler handler = OpPtoSuministroTriggerHandler.getInstance();
	/* BEGIN - Manuel Medina - Se define control para ejecutar sentencia solo en after insert - 11052017 */
	/* BEGIN - Manuel Medina - Se comentarea llamado a metodo de creacion de cotizaciones - 12062017 */
	/*if( Trigger.isAfter && Trigger.isInsert ){
		handler.afterInsert(Trigger.new);
		
	}else*/ 
	/* END - Manuel Medina - Se comentarea llamado a metodo de creacion de cotizaciones - 12062017 */
	if( Trigger.isBefore && Trigger.isUpdate ){
		handler.validateOpportunitySupplies( Trigger.new, Trigger.oldMap );
	}
	/* END - Manuel Medina - Se define control para ejecutar sentencia solo en after insert - 11052017 */
}