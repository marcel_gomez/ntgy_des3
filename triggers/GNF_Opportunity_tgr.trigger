/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades adicionales para las oportunidades.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-08		Manuel Medina (MM)		Definicion inicial del trigger.
*********************************************************************************************************/
trigger GNF_Opportunity_tgr on Opportunity ( before insert, before update, after insert, after update ) {
	
	GNF_OpportunityTriggerHandler_cls.setMapRecordTypeIdByDevName();
	
	if( trigger.isBefore && trigger.isInsert ){
		GNF_OpportunityTriggerHandler_cls.validateUnclickSigned( Trigger.new, null );
		GNF_OpportunityTriggerHandler_cls.mapeoCodigoFormula( Trigger.new, null );
		GNF_OpportunityTriggerHandler_cls.setAccountOwnerToOpportunityOwner( Trigger.new, new Map<Id, Opportunity>() );
		GNF_OpportunityTriggerHandler_cls.fetchCountryFromAccount(Trigger.new);
		// GNF_OpportunityTriggerHandler_cls.updateEndDateCB(Trigger.new, Trigger.oldMap);
	
	}else if( trigger.isBefore && trigger.isUpdate ){
		GNF_OpportunityTriggerHandler_cls.validateUnclickSigned( Trigger.new, Trigger.oldMap );
		GNF_OpportunityTriggerHandler_cls.validateAccount( Trigger.new, Trigger.oldMap );
		GNF_OpportunityTriggerHandler_cls.valideOpportunityClosure( Trigger.new, Trigger.oldMap );
		GNF_OpportunityTriggerHandler_cls.mapeoCodigoFormula( Trigger.new, Trigger.old );
		GNF_OpportunityTriggerHandler_cls.setAccountOwnerToOpportunityOwner( Trigger.new, Trigger.oldMap );
		GNF_OpportunityTriggerHandler_cls.fetchCountryFromAccount(Trigger.new);
		GNF_OpportunityTriggerHandler_cls.validatePSWithCBCurrent(Trigger.new, Trigger.oldMap);
		GNF_OpportunityTriggerHandler_cls.generateCBClon(Trigger.new, Trigger.oldMap);
		GNF_OpportunityTriggerHandler_cls.updateEndDateCB(Trigger.new, Trigger.oldMap);
		GNF_OpportunityTriggerHandler_cls.setStampTimeDocuments(Trigger.new, Trigger.oldMap);


		
	}else if( trigger.isAfter && trigger.isUpdate ){
		GNF_OpportunityTriggerHandler_cls.assignApprovers( Trigger.newMap, Trigger.oldMap);
		// GNF_OpportunityTriggerHandler_cls.updateConsumosClick( Trigger.newMap );
		GNF_OpportunityTriggerHandler_cls.removeFlagFicticio( Trigger.new, Trigger.oldMap);
		
	}else if( trigger.isAfter && trigger.isInsert ){
		GNF_OpportunityTriggerHandler_cls.assignApprovers( Trigger.newMap, null );
	}
}