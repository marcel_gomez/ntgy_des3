/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Ejecuta funcionalidades adicionales del objeto de PDF asincronos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-09-29		Manuel Medina (MM)		Definicion inicial del trigger
*********************************************************************************************************/
trigger GNF_PDFAsincronos_tgr on GNF_PDFAsincronos__c ( before update, after update ){
	
	if( Trigger.isBefore && !GNF_PDFAsincronosTriggerHandler_cls.blnAlreadyExecuted_attachPDF ){
		GNF_PDFAsincronosTriggerHandler_cls.attachPDF( Trigger.newMap );
		
	}else if( Trigger.isAfter && !System.isFuture() && !System.isBatch() && !GNF_PDFAsincronosTriggerHandler_cls.blnExecutedSyncLogic ){
		GNF_PDFAsincronosTriggerHandler_cls.updateRecordsAsync( JSON.serialize( Trigger.newMap ) );
		
	}else if( Trigger.isAfter && !GNF_PDFAsincronosTriggerHandler_cls.blnAlreadyExecuted_updateRecords && GNF_PDFAsincronosTriggerHandler_cls.blnExecutedSyncLogic ){
		GNF_PDFAsincronosTriggerHandler_cls.updateRecordsSync( JSON.serialize( Trigger.newMap ) );
	}
}