/**
* VASS
* @author           Juan Cardona juan.cardona@vass.es
* Project:          Gas Natural Fenosa
* Description:      Desencadenador sobre el objeto Concepto
*                   
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2017-07-12      Juan Cardona (JSC)      Definicion inicial del trigger.
*********************************************************************************************************/

trigger GNF_concepto_tgr on Concepto__c (before insert, before update) 
{
    
    if( Trigger.isBefore)
    {
        if( Trigger.isInsert )
        {   
            GNF_ConceptoTriggerHandler_cls.on_beforeInsert( Trigger.new );
            
        }//ends isInsert
        
        if( Trigger.isUpdate )
        {   
            GNF_ConceptoTriggerHandler_cls.on_beforeUpdate( Trigger.oldmap, Trigger.newmap );
            
        }//ends isUpdate        

    }//ends isBefore   
}//ends GNF_concepto_tgr