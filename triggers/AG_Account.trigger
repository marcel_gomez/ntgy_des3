/**********************************************************************************
* @author       Aaron Dominguez - aarondominguezsanchez@gmail.com
* @date         27 July 2016
* @description  Trigger for Account object. DO NOT CREATE more triggers on Account.
*               Use TriggerHandlerAccount and LogicAccount classes instead.
* @Revision     [AD] - 27/07/2016 - Initial version
**********************************************************************************/

trigger AG_Account on Account  (before insert,  before update,  before delete, 
                                after insert,   after update,   after delete, after undelete) 
{

    //Get TriggerHandler with singleton instance
    //final AG_LogicAccount handler = AG_LogicAccount.getInstance();
    AG_LogicAccount logicAcc = new AG_LogicAccount();

    //ON BEFORE INSERT 
    if (Trigger.isBefore && Trigger.isInsert) {

           //handler.onBeforeInsert(Trigger.new);
           logicAcc.assignOwnerFromEmpleado(Trigger.new, null);
           //AG_LogicAccount.assignOwnerFromEmpleado(trigger.new, null);

    }
    //ON BEFORE UPDATE
    else if (Trigger.isBefore && Trigger.isUpdate) {
          //handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
          //AG_LogicAccount.assignOwnerFromEmpleado(trigger.new, trigger.old);
          logicAcc.assignOwnerFromEmpleado(Trigger.new, trigger.oldmap);
    } 

     /**
    * description: Se comenta metodos onAfterInsert, onAfterUpdate, onAfterDelete, onBeforeDelete, onAfterUnDelete) 
    * debido a que no tienen ninguna logica que se cumpla para su llamado
    * @param:  Req_RD_1_4 Metodos sin logica
    * @author: victor.velandia@vass.es
    * @date:   20/06/2018
    * Begin 
    */
    //ON BEFORE DELETE
    /*else if (Trigger.isBefore && Trigger.isDelete) {
      handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
    }*/
    //ON AFTER INSERT
    /*else if (Trigger.isAfter && Trigger.isInsert) {
      handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }*/
    //ON AFTER UPDATE
    /*else if (Trigger.isAfter && Trigger.isUpdate) {
      handler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
    } */  
    //ON AFTER DELETE
    /*else if (Trigger.isAfter && Trigger.isDelete) {
      handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }*/
    //ON AFTER UNDELETE
    /*else if (Trigger.isAfter && Trigger.isUndelete) {
      handler.onAfterUndelete(Trigger.new);
    }*/
    //End
  
}