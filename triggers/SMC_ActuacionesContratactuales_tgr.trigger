/**
* VASS
* @author 			Francisco Rojas francisco.rojas@mad.vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades adicionales para las actuaciones contractuales.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-12-11		Francisco Rojas (FR)		Definicion inicial del trigger.
*********************************************************************************************************/
trigger SMC_ActuacionesContratactuales_tgr on Actuacion_Contractual__c( after insert ){

	GNF_OpportunityTriggerHandler_cls.setMapRecordTypeIdByDevName();
	
	SMC_ActContratactualesHandler_cls.validateUpdateCoverageCondition( Trigger.newMap );
	SMC_ActContratactualesHandler_cls.linkCoverageConditionToNewContract( Trigger.newMap );
}