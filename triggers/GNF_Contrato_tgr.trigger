/**
* VASS Latam
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades adicionales para los contratos.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-05-10		Manuel Medina (MM)		Definicion inicial del trigger.
*********************************************************************************************************/
trigger GNF_Contrato_tgr on Contrato__c ( before update ) {
    
	GNF_ContratoTriggerHandler_cls.validateContract( Trigger.new, Trigger.oldMap );
}