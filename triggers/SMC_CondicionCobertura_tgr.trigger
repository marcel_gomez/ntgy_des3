/**
* VASS
* @author 			Manuel Medina manuel.medina@vass.es
* Project:			Gas Natural Fenosa
* Description:		Implementa funcinalidades adicionales para las condiciones de cobertura.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2017-11-07		Manuel Medina (MM)		Definicion inicial del trigger.
*********************************************************************************************************/
trigger SMC_CondicionCobertura_tgr on Condiciones_de_Cobertura__c ( after insert, after update ) {
	
	SMC_CondicionCoberturaTriggerHandler_cls.opportunitySync( Trigger.newMap );
	
	if( Trigger.isUpdate ){
		SMC_CondicionCoberturaTriggerHandler_cls.syncCfgApprovalConditions( Trigger.newMap, Trigger.oldMap );
	}
}