trigger AG_ProcesoImportacion on Proceso_Importacion__c (after insert, after update) {

	/*VASS 06-2018 - AppGestor
	Variables comentadas por no estar en uso en ningun sitio del proyecto
	final String FINALIZADO_EXITO = 'Finalizado con éxito';
	final String FINALZIADO_ERRORES= 'Finalizado con errores';
	final String PLANIFICADO = 'Planificado';
	final String EN_CURSO = 'En curso';*/

	for (Proceso_Importacion__c proc : trigger.new) {

		if 	(isFinished(proc.estado__c)) {

			if (proc.Recalcular_permisos_cliente__c!=label.ApG_Ninguno_DONT_TRANSLATE) {
				//Se comenta debug, porque esta dentro de un ciclo (Victor Velandia (VV))
				//System.Debug('Se lanza  proceso de recálculo de permisos de cuentas.'); 
				Boolean recalculateAll = (proc.Recalcular_permisos_cliente__c==label.ApG_Todos_DONT_TRANSLATE);//=='Todos'
				AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateObjetShares(recalculateAll, false);
			}

			if (proc.Recalcular_permisos_objeto__c!=label.ApG_Ninguno_DONT_TRANSLATE) {
				//Se comenta debug, porque esta dentro de un ciclo (Victor Velandia (VV))
				//System.Debug('Se lanza  proceso de recálculo de permisos de objetos.');
				Boolean recalculateAll = (proc.Recalcular_permisos_objeto__c==label.ApG_Todos_DONT_TRANSLATE);//=='Todos'
				AG_SecurityRecalculationProcessUtil.executeBatchToRecalculateAccountShares(recalculateAll, false);
			}
		}
	}

	/*
	* Valida que proceso esta en estado finalizado
	*/
	private Boolean isFinished(String estado) {

 		return ((estado == label.ApG_FinalizadoConExito_DONT_TRANSLATE) || 
				(estado == label.ApG_FinalizadoConErrores_DONT_TRANSLATE));
	}
}