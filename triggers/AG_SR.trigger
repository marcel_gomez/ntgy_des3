trigger AG_SR on SRs__c (before insert, before update, before delete, 
                         after insert, after update, after delete, 
                         after undelete) {
    //Get TriggerHandler
    //final AG_TriggerHandlerSR handler = AG_TriggerHandlerSR.getInstance();

  
    //ON BEFORE INSERT 
    if (Trigger.isBefore && Trigger.isInsert) {
        //handler.onBeforeInsert(Trigger.new);
        ApG_CommonLogic.assignOwnerFromEmpleado(trigger.new, null, 'SRs__c');
    }  
    //ON BEFORE UPDATE
    else if (Trigger.isBefore && Trigger.isUpdate) {
      //handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
        ApG_CommonLogic.assignOwnerFromEmpleado(trigger.new, trigger.oldMap, 'SRs__c');
    } 
    /* 
    //ON BEFORE DELETE
    else if (Trigger.isBefore && Trigger.isDelete) {
      handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    */
    //ON AFTER INSERT
    else if (Trigger.isAfter && Trigger.isInsert) {
      //handler.onAfterInsert(Trigger.new, Trigger.newMap);
        ApG_CommonLogic.setCustomSharing(trigger.new, null, 'SRs__c');
    }
    //ON AFTER UPDATE
    else if (Trigger.isAfter && Trigger.isUpdate) {
      //handler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
        ApG_CommonLogic.setCustomSharing(trigger.new, trigger.oldMap, 'SRs__c');

    }
    /*   
    //ON AFTER DELETE
    else if (Trigger.isAfter && Trigger.isDelete) {
      handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }
    //ON AFTER UNDELETE
    else if (Trigger.isAfter && Trigger.isUndelete) {
      handler.onAfterUndelete(Trigger.new);
    }*/   
}