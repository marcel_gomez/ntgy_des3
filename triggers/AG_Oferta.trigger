/**
* description: 
* Se comenta metodos onAfterDelete, onBeforeDelete, onAfterUndelete) debido a que no tienen ninguna logica que se cumpla para su llamado
* Se elimina clase handler y se hace el llamado directo desde la clase logica ApG_CommonLogic
* @param:  Req_RD_1_4 Metodos sin logica
* @author: victor.velandia@vass.es
* @date:   20/06/2018
* Begin 
*/
trigger AG_Oferta on Ofertas__c (before insert, before update, before delete, 
                                    after insert, after update, after delete, after undelete) {
  //Get TriggerHandler
  //final AG_TriggerHandlerOferta handler = AG_TriggerHandlerOferta.getInstance();
    
  //ON BEFORE INSERT 
  if (Trigger.isBefore && Trigger.isInsert) {
     // handler.onBeforeInsert(Trigger.new);
    ApG_CommonLogic.assignOwnerFromEmpleado(trigger.new, null, 'Ofertas__c');

  }  
  //ON BEFORE UPDATE
  else if (Trigger.isBefore && Trigger.isUpdate) {
    ApG_CommonLogic.assignOwnerFromEmpleado(trigger.new, trigger.oldMap, 'Ofertas__c');
    //handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
  } 
  //ON AFTER INSERT
  else if (Trigger.isAfter && Trigger.isInsert) {

    //handler.onAfterInsert(Trigger.new, Trigger.newMap);
    ApG_CommonLogic.setCustomSharing(trigger.new, null, 'Ofertas__c');
    
  }
  //ON AFTER UPDATE
  else if (Trigger.isAfter && Trigger.isUpdate) {

    //handler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
    ApG_CommonLogic.setCustomSharing(trigger.new, trigger.oldMap, 'Ofertas__c');
  }

  /* 
  //ON BEFORE DELETE
  else if (Trigger.isBefore && Trigger.isDelete) {
    handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
  }
  */
  
  /*   
  //ON AFTER DELETE
  else if (Trigger.isAfter && Trigger.isDelete) {
    handler.onAfterDelete(Trigger.old, Trigger.oldMap);
  }
  //ON AFTER UNDELETE
  else if (Trigger.isAfter && Trigger.isUndelete) {
    handler.onAfterUndelete(Trigger.new);
  }*/   
}